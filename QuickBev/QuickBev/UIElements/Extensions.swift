//
//  Extensions.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 2/4/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//
import UIKit

// cmd + shift + y open and close console
// cmd + shift + curly brace to go between tabs
extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = capitalizingFirstLetter()
    }

    func countCharacters() -> Int {
        var characterCount = count
        let spaceCount = filter { $0 == " " }.count
        characterCount += spaceCount
        return characterCount
    }

    var capitalizedFirst: String {
        guard !isEmpty else {
            return self
        }

        let capitalizedFirstLetter = self[0].uppercased()
        let secondIndex = index(after: startIndex)
        let remainingString = self[secondIndex ..< endIndex]

        let capitalizedString = "\(capitalizedFirstLetter)\(remainingString)"
        return capitalizedString
    }

    func formattedCurrencyString() -> String {
        return "$" + self
    }

    mutating func insert(string: String, ind: Int) {
        insert(contentsOf: string, at: index(startIndex, offsetBy: ind))
    }
}

extension StringProtocol {
    subscript(offset: Int) -> Character {
        self[index(startIndex, offsetBy: offset)]
    }

    subscript(range: Range<Int>) -> SubSequence {
        let startIndex = index(self.startIndex, offsetBy: range.lowerBound)
        return self[startIndex ..< index(startIndex, offsetBy: range.count)]
    }

    subscript(range: ClosedRange<Int>) -> SubSequence {
        let startIndex = index(self.startIndex, offsetBy: range.lowerBound)
        return self[startIndex ..< index(startIndex, offsetBy: range.count)]
    }

    subscript(range: PartialRangeFrom<Int>) -> SubSequence { self[index(startIndex, offsetBy: range.lowerBound)...] }
    subscript(range: PartialRangeThrough<Int>) -> SubSequence { self[...index(startIndex, offsetBy: range.upperBound)] }
    subscript(range: PartialRangeUpTo<Int>) -> SubSequence { self[..<index(startIndex, offsetBy: range.upperBound)] }
}

extension UILabel {
    func setLineHeight(lineHeight: CGFloat) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 1.0
        paragraphStyle.lineHeightMultiple = lineHeight
        paragraphStyle.alignment = textAlignment

        let attrString = NSMutableAttributedString()
        if attributedText != nil {
            attrString.append(attributedText!)
        } else {
            attrString.append(NSMutableAttributedString(string: text!))
            attrString.addAttribute(NSAttributedString.Key.font, value: font as Any, range: NSMakeRange(0, attrString.length))
        }
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attrString.length))
        attributedText = attrString
    }
}

extension UIColor {
    class var themeColor: UIColor {
        return UIColor(red: 134 / 255, green: 130 / 255, blue: 230 / 255, alpha: 1.0)
    }

    class var greyBorderColor: UIColor {
        return UIColor(red: 204.0 / 255.0, green: 204.0 / 255.0, blue: 204.0 / 255.0, alpha: 1.0)
    }

    func mix(with color: UIColor, amount: CGFloat) -> UIColor {
        var red1: CGFloat = 0
        var green1: CGFloat = 0
        var blue1: CGFloat = 0
        var alpha1: CGFloat = 0

        var red2: CGFloat = 0
        var green2: CGFloat = 0
        var blue2: CGFloat = 0
        var alpha2: CGFloat = 0

        getRed(&red1, green: &green1, blue: &blue1, alpha: &alpha1)
        color.getRed(&red2, green: &green2, blue: &blue2, alpha: &alpha2)

        return UIColor(
            red: red1 * (1.0 - amount) + red2 * amount,
            green: green1 * (1.0 - amount) + green2 * amount,
            blue: blue1 * (1.0 - amount) + blue2 * amount,
            alpha: alpha1
        )
    }
}

extension NSLayoutConstraint {
    func usingPriority(_ priority: UILayoutPriority) -> NSLayoutConstraint {
        self.priority = priority
        return self
    }
}

// this function allows you to plug in the current static font size you are using and replace it with a dynamic font size that scales with the phone's area based on the desired font size to area ratio tested on font size 20 with an iphone 11 pro max
public func getFontRatio(fontSize: CGFloat) -> CGFloat {
    let ratio = sqrt(UIViewController.screenSize.height * UIViewController.screenSize.width) / fontSize
    // desired ratio is based on Iphone 11 Pro Max with height of 896 and width of 414
    let desiredRatio = sqrt(414.0 * 896.0) / fontSize
    let differenceInRatios = ratio / desiredRatio
    let newFontSize = fontSize * differenceInRatios

    return newFontSize
}

public func getHeightRatio(verticalConstant: CGFloat) -> CGFloat {
    let multiplier = UIViewController.screenSize.height / 896
    let newVerticalConstant = multiplier * verticalConstant
    return newVerticalConstant
}

public func getWidthRatio(horizontalConstant: CGFloat) -> CGFloat {
    let multiplier = UIViewController.screenSize.width / 414
    let newHorizontalConstant = multiplier * horizontalConstant

    return newHorizontalConstant
}

extension UIFont {
    class var largeThemeLabelFont: UIFont {
        return UIFont(name: "GillSans", size: getFontRatio(fontSize: 35.3))!
    }

    class var themeButtonFont: UIFont {
        return UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 20.0))!
    }

    class var primaryTableCellContentViewFont: UIFont {
        return UIFont(name: "GillSans", size: getFontRatio(fontSize: 20.0))!
    }

    class var secondaryTableCellContentViewFont: UIFont {
        return UIFont(name: "GillSans", size: getFontRatio(fontSize: 18.0))!
    }

    class var mediumThemeButtonFont: UIFont {
        return UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 16.0))!
    }

    class var smallThemeButtonFont: UIFont {
        return UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 12.0))!
    }

    class var themeLabelFont: UIFont {
        return UIFont(name: "GillSans", size: getFontRatio(fontSize: 20.0))!
    }

    class var themeLabelSmallFont: UIFont {
        return UIFont(name: "GillSans", size: getFontRatio(fontSize: 14.0))!
    }
}

@propertyWrapper
public struct UsesAutoLayout<T: UIView> {
    public var wrappedValue: T {
        didSet {
            wrappedValue.translatesAutoresizingMaskIntoConstraints = false
        }
    }

    public init(wrappedValue: T) {
        self.wrappedValue = wrappedValue
        wrappedValue.translatesAutoresizingMaskIntoConstraints = false
    }
}

extension UIApplication {
    class func getTopViewController(base: UIViewController? = SceneDelegate.shared.rootViewController.current) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)

        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)

        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }

    static func checkForNotifications() {
        UNUserNotificationCenter.current().getDeliveredNotifications(completionHandler: { requests in
            NSLog("request.count")
            if !requests.isEmpty {
                NSLog("request.count > 0")
                SceneDelegate.shared.rootViewController.updateOrderStatus {
                    DispatchQueue.main.async {
                        CoreDataManager.sharedManager.saveContext()
                        handleNotification(userInfo: requests[0].request.content.userInfo)
                        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
                    }
                }
            }
        })
    }

    static func handleNotification(userInfo: [AnyHashable: Any]) {
        if let aps = userInfo["aps"] as? NSDictionary {
            // silent notification
            if aps["content-available"] as? Int == 1 {
            } else {
                if let alert = aps["alert"] as? NSDictionary, let body = alert["body"] as? NSString, let title = alert["title"] as? NSString, let category = aps["category"] as? NSString {
                    if category == "email" {
                        let alertController = UIAlertController(title: title as String, message: body as String, preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "Okay", style: .cancel) { _ in
                            CheckoutCart.shared.user?.emailVerified = true
                            CoreDataManager.sharedManager.saveContext()
                            SceneDelegate.shared.rootViewController.switchToHomePageViewController()

                        })
                    } else if category == "order_completed" || category == "order_refunded" {
                        let orderId = userInfo["order_id"] as? NSString
                        if let updatedOrder = CoreDataManager.sharedManager.fetchEntity(entityName: "Order", propertyToFilter: "id", propertyValue: orderId! as String) as? Order {
                            SceneDelegate.shared.rootViewController.checkForExistingViewController(updatedOrder: updatedOrder) {
                                _ in
                            }
                        }
                    }

                } else if let _ = aps["alert"] as? NSString {
                    // Do stuff
                }
            }
        }
        for vc in SceneDelegate.shared.rootViewController.current.viewControllers {
            if let controller = vc as? HomeViewController {
                controller.setConditionalUIProperties()
            }
        }
    }
}

extension UIViewController {
    class var screenSize: CGRect { return UIScreen.main.bounds }
    @objc func back() {
        SceneDelegate.shared.rootViewController.homeViewController?.setConditionalUIProperties()
        dismiss(animated: true, completion: nil)
    }

    @objc func popVC() {
        navigationController?.popViewController(animated: true)
    }

    /**
       returns true only if the viewcontroller is presented.
     */
    var isModal: Bool {
        if presentingViewController != nil {
            return true
        } else if presentedViewController != nil {
            return true
        } else if presentationController != nil {
            return true
        } else if let navController = navigationController, navController.presentingViewController?.presentedViewController == navController {
            return true
        } else if tabBarController?.presentingViewController is UITabBarController {
            return true
        }
        return false
    }

    typealias alertClosure = () -> Void
    // an optional closure is @escaping and survives in memory by default!
    func presentAlertController(title: String, message: String, completion: alertClosure? = nil) {
        SceneDelegate.shared.rootViewController.stopAnimatingActivityIndicator()

        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alertController.addAction(UIAlertAction(title: "Okay", style: .cancel) { _ in
            alertController.dismiss(animated: true) {
                if let completion = completion {
                    completion()
                }
            }
        })
        present(alertController, animated: true) {}
    }

    func presentNetworkError() {
        presentAlertController(title: "Network Error", message: "A network error has occured. Check your internet connection and if necessary, restart the app.")
    }

    func presentServerError() {
        presentAlertController(title: "Server Error", message: "Whoops, there's an issue on our end. We're figuring out what the problem is. Try again in a little bit!")
    }

    func presentStaleDataError(businessName: String, completion: @escaping () -> Void) {
        presentAlertController(title: "Stale Data", message: "Whoops, \(businessName) is not receiving QuickBev orders right now.") {
            completion()
        }
    }

    func updateQuickPass(businessId: UUID, completion: @escaping (Bool) -> Void) {
        APIClient().getQuickPassQueue(businessId: businessId) {
            quickPassDataFromAPI in guard quickPassDataFromAPI != nil else {
                completion(false)
                return
            }

            DispatchQueue.main.async {
                QuickPassData.shared = quickPassDataFromAPI!
                CoreDataManager.sharedManager.saveContext()
            }
            completion(true)
        }
    }

    func setupHeaderView(otherView: UIView, header: String, distanceFromHeader: CGFloat = 0.0) {
        let accountLabelContainerView = UIView(theme: Theme.UIView(props: []))
        let accountTextView = UITextView(theme: Theme.UITextView(props: [.textColor, .font(UIFont(name: "GillSans", size: 30.0)), .text(header)]))
        accountTextView.isUserInteractionEnabled = false
        view.addSubview(accountLabelContainerView)

        accountLabelContainerView.addSubview(accountTextView)
        accountLabelContainerView.addBottomBorder(with: UIColor.lightGray.withAlphaComponent(0.5), andWidth: 1.0)
        accountTextView.textAlignment = .justified

        accountTextView.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: getHeightRatio(verticalConstant: 15.0
        ), right: 0)
        accountTextView.isScrollEnabled = false
        accountTextView.textAlignment = .justified
        accountTextView.backgroundColor = .clear

        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            accountTextView.topAnchor.constraint(equalTo: accountLabelContainerView.topAnchor, constant: getHeightRatio(verticalConstant: 20.0)),
            accountTextView.bottomAnchor.constraint(equalTo: accountLabelContainerView.bottomAnchor),
            accountTextView.leadingAnchor.constraint(equalTo: accountLabelContainerView.leadingAnchor, constant: getWidthRatio(horizontalConstant: 15.0)),
            accountLabelContainerView.topAnchor.constraint(equalTo: safeArea.topAnchor),
            accountLabelContainerView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            accountLabelContainerView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
            accountLabelContainerView.bottomAnchor.constraint(equalTo: otherView.topAnchor, constant: getHeightRatio(verticalConstant: distanceFromHeader)),
        ])
    }

    func setNonModalNavbar() {
        let navBarStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(Float(getWidthRatio(horizontalConstant: 5.0)))]))

        navigationController?.configureNavigationBar(largeTitleColor: UIColor.white, backgroundColor: UIColor.white, tintColor: UIColor.white, preferredLargeTitle: false, titleTextAttributes: nil)
        let navBarImageView = AlignmentImageView(image: UIImage(named: "logo-no-background")!)

        navBarImageView.verticalAlignment = .top
        navBarImageView.contentMode = .scaleAspectFit

        let navBarTitle = VerticalAlignLabel(theme: Theme.UILabel(props: [.text("QuickBev"), .font(UIFont(name: "GillSans", size: 16.0))]))
        navBarTitle.verticalAlignment = .bottom
        navBarTitle.textColor = UIColor.themeColor

        navBarStackView.addArrangedSubview(navBarImageView)
        navBarStackView.addArrangedSubview(navBarTitle)
        navBarStackView.alignment = .top
        NSLayoutConstraint.activate([
            navBarImageView.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.06),
            navBarImageView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.035),
            navBarImageView.bottomAnchor.constraint(equalTo: navBarStackView.bottomAnchor, constant: getHeightRatio(verticalConstant: -5.0)),
            navBarTitle.bottomAnchor.constraint(equalTo: navBarStackView.bottomAnchor, constant: getHeightRatio(verticalConstant: -7.0)),
        ])

        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "back"), for: .normal) // Image can be downloaded from here below link
        backButton.setTitle(" Back", for: .normal)
        backButton.setTitleColor(.systemBlue, for: .normal) // You can change the TitleColor
        backButton.addTarget(self, action: #selector(popVC), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        navigationItem.titleView = navBarStackView
        navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
    }

    func setNavbar() {
        let navBarStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(Float(getWidthRatio(horizontalConstant: 5.0)))]))

        navigationController?.configureNavigationBar(largeTitleColor: UIColor.white, backgroundColor: UIColor.white, tintColor: UIColor.white, preferredLargeTitle: false, titleTextAttributes: nil)
        let navBarImageView = AlignmentImageView(image: UIImage(named: "logo-no-background")!)

        navBarImageView.verticalAlignment = .top
        navBarImageView.contentMode = .scaleAspectFit

        let navBarTitle = VerticalAlignLabel(theme: Theme.UILabel(props: [.text("QuickBev"), .font(UIFont(name: "GillSans", size: 16.0))]))
        navBarTitle.verticalAlignment = .bottom
        navBarTitle.textColor = UIColor.themeColor

        navBarStackView.addArrangedSubview(navBarImageView)
        navBarStackView.addArrangedSubview(navBarTitle)
        navBarStackView.alignment = .top
        NSLayoutConstraint.activate([
            navBarImageView.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.06),
            navBarImageView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.035),
            navBarImageView.bottomAnchor.constraint(equalTo: navBarStackView.bottomAnchor, constant: getHeightRatio(verticalConstant: -5.0)),
            navBarTitle.bottomAnchor.constraint(equalTo: navBarStackView.bottomAnchor, constant: getHeightRatio(verticalConstant: -7.0)),
        ])
        let backBtnImage = UIImage(systemName: "xmark")?.withTintColor(UIColor.black, renderingMode: .alwaysOriginal)
        let backBtn = UIBarButtonItem(image: backBtnImage,
                                      style: .plain,
                                      target: self,
                                      action: #selector(UIViewController.back))
        navigationItem.leftBarButtonItem = backBtn
        navigationItem.titleView = navBarStackView
        navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
    }
}

extension Double {
    func rounded(digits: Int) -> Double {
        let multiplier = pow(10.0, Double(digits))
        return Darwin.round(self * multiplier) / multiplier
    }

    func formattedCurrency() -> String {
        // remove trailing .0
        if floor(self) == self {
            return String(Int(self))
        } else if rounded(digits: 2) == rounded(digits: 1) {
            return String(rounded(digits: 2)) + "0"
        } else {
            return String(rounded(digits: 2))
        }
    }
}

extension UIImage {
    func imageWithInsets(insets: UIEdgeInsets) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: size.width + insets.left + insets.right,
                   height: size.height + insets.top + insets.bottom), false, scale
        )
        _ = UIGraphicsGetCurrentContext()
        let origin = CGPoint(x: insets.left, y: insets.top)
        draw(at: origin)
        let imageWithInsets = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return imageWithInsets
    }
}

extension UIImageView {
    class var LogoImageView: UIImageView {
        let imageViewToReturn = UIImageView(image: UIImage(named: "logo"))
        imageViewToReturn.translatesAutoresizingMaskIntoConstraints = false
        return imageViewToReturn
    }

    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}

extension Double {
    static var logoSizeMultiplier = 0.565217
}

extension UITextField {
    func setInputViewDatePicker(target: Any, selector: Selector) {
        // Create a UIDatePicker object and assign to inputView
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216)) // 1
        datePicker.datePickerMode = .date // 2
        datePicker.minimumDate = Date()
        // iOS 14 and above
        if #available(iOS 14, *) { // Added condition for iOS 14
            datePicker.preferredDatePickerStyle = .wheels
            datePicker.sizeToFit()
        }
        inputView = datePicker // 3

        // Create a toolBar and assign it to inputAccessoryView

        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) // 4
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) // 5
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: #selector(tapCancel)) // 6
        let barButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector) // 7
        toolBar.setItems([cancel, flexible, barButton], animated: false) // 8
        inputAccessoryView = toolBar // 9
    }

    @objc func tapCancel() {
        resignFirstResponder()
    }
}

extension UINavigationController {
    func configureNavigationBar(largeTitleColor: UIColor, backgroundColor: UIColor, tintColor: UIColor, preferredLargeTitle: Bool, titleTextAttributes: [NSAttributedString.Key: Any]?) {
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            if let titleTextAttributes = titleTextAttributes {
                var newTitleTextAttributes = titleTextAttributes
                newTitleTextAttributes[.foregroundColor] = largeTitleColor
                navBarAppearance.largeTitleTextAttributes = newTitleTextAttributes
                navBarAppearance.titleTextAttributes = newTitleTextAttributes
            } else {
                navBarAppearance.largeTitleTextAttributes = [.foregroundColor: largeTitleColor]
                navBarAppearance.titleTextAttributes = [.foregroundColor: largeTitleColor]
            }

            self.navigationBar.isTranslucent = false
            navBarAppearance.backgroundColor = backgroundColor

            self.navigationBar.standardAppearance = navBarAppearance
            self.navigationBar.compactAppearance = navBarAppearance
            self.navigationBar.scrollEdgeAppearance = navBarAppearance

            self.navigationBar.prefersLargeTitles = preferredLargeTitle
        } else {
            // Fallback on earlier versions
            navigationBar.barTintColor = backgroundColor
            navigationBar.tintColor = tintColor
            navigationBar.isTranslucent = false
        }
    }

    var isHiddenHairline: Bool {
        get {
            guard let hairline = findHairlineImageViewUnder(navigationBar) else { return true }
            return hairline.isHidden
        }
        set {
            if let hairline = findHairlineImageViewUnder(navigationBar) {
                hairline.isHidden = newValue
            }
        }
    }

    private func findHairlineImageViewUnder(_ view: UIView) -> UIImageView? {
        if view is UIImageView, view.bounds.size.height <= 1.0 {
            return view as? UIImageView
        }

        for subview in view.subviews {
            if let imageView = findHairlineImageViewUnder(subview) {
                return imageView
            }
        }

        return nil
    }
}

extension UIView {
    convenience init(theme: Theme) {
        self.init()
        translatesAutoresizingMaskIntoConstraints = false

        switch self {
        case let self as UIStackView:
            switch theme {
            case let .UIStackView(props):
                for prop in props {
                    switch prop {
                    case .vertical:
                        self.axis = .vertical
                    case .horizontal:
                        self.axis = .horizontal
                    case let .spacing(value):
                        self.spacing = CGFloat(value)
                    }
                }
            default:
                ()
            }
        case _ as RoundedImageView:
            switch theme {
            default:
                ()
            }
        case let self as RoundButton:

            switch theme {
            case let .RoundButton(props):
                for prop in props {
                    switch prop {
                    case .color:
                        let value = prop.rawValue as! UIColor
                        self.refreshColor(color: value)

                    case var .titleLabelFont(value):
                        if value == nil {
                            value = prop.rawValue as? UIFont
                        }
                        self.titleLabel?.font = value
                    case let .text(value):
                        self.refreshTitle(newTitle: value)
                    }
                }
            default:
                ()
            }
        case let self as UILabel:
            switch theme {
            case let .UILabel(props):
                for prop in props {
                    switch prop {
                    case let .font(value):
                        if value == nil {
                            self.font = prop.rawValue as? UIFont
                        } else {
                            self.font = value
                        }
                    case .center:
                        self.textAlignment = .center
                    case let .text(value):
                        self.text = value
                    case .textColor:
                        self.textColor = .black
                    }
                }
            default:
                ()
            }
        case let self as UITextView:
            switch theme {
            case let .UITextView(props):
                for prop in props {
                    switch prop {
                    case let .font(value):
                        if value == nil {
                            self.font = prop.rawValue as? UIFont
                        } else {
                            self.font = value
                        }
                    case let .backgroundColor(value):
                        self.backgroundColor = value
                    case let .text(value):
                        self.text = value
                    case .textColor:
                        self.textColor = .black
                    }
                }
            default:
                ()
            }
        case let self as UITextField:
            switch theme {
            case let .UITextField(props):
                for prop in props {
                    switch prop {
                    case let .font(value):
                        if value == nil {
                            self.font = prop.rawValue as? UIFont
                        } else {
                            self.font = value
                        }

                    case let .placeHolderText(value):
                        let attributedText = NSAttributedString(string: value, attributes: [NSAttributedString.Key.foregroundColor: UIColor.clear])
                        self.attributedPlaceholder = attributedText
                    case let .autocapitalizationType(value):
                        switch value {
                        case .none:
                            self.autocapitalizationType = .none
                        }
                    case let .borderStyle(value):
                        switch value {
                        case .roundedRect:
                            self.borderStyle = .none
                            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 20))
                            self.leftView = paddingView
                            self.leftViewMode = .always
                        }
                    case let .backgroundColor(value):
                        self.backgroundColor = value
                    }
                    self.textColor = .black
                }
            default:
                ()
            }
        default:
            ()
        }
    }
}

extension UIView {
    func removeBorder(border: BorderLineProps) {
        subviews.forEach {
            if let line = $0 as? BorderView {
                if line.arrangement == border {
                    $0.removeFromSuperview()
                }
            }
        }
    }

    func resetBorders() {
        subviews.forEach {
            if $0 is BorderView {
                $0.removeFromSuperview()
            }
        }
    }

    func addTopBorder(with color: UIColor?, andWidth borderWidth: CGFloat) {
        let border = BorderView()
        border.arrangement = .top
        border.backgroundColor = color
        border.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        border.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: borderWidth)
        addSubview(border)
    }

    func addBottomBorder(with color: UIColor?, andWidth borderWidth: CGFloat) {
        let border = BorderView()
        border.arrangement = .bottom
        border.backgroundColor = color
        border.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
        border.frame = CGRect(x: 0, y: frame.size.height - borderWidth, width: frame.size.width, height: borderWidth)
        addSubview(border)
    }

    func addLeftBorder(with color: UIColor?, andWidth borderWidth: CGFloat) {
        let border = BorderView()
        border.arrangement = .left
        border.backgroundColor = color
        border.frame = CGRect(x: 0, y: 0, width: borderWidth, height: frame.size.height)

        border.autoresizingMask = [.flexibleHeight, .flexibleRightMargin]
        addSubview(border)
    }

    func addRightBorder(with color: UIColor?, andWidth borderWidth: CGFloat) {
        let border = BorderView()
        border.arrangement = .right
        border.backgroundColor = color
        border.autoresizingMask = [.flexibleHeight, .flexibleLeftMargin]
        border.frame = CGRect(x: frame.size.width - borderWidth, y: 0, width: borderWidth, height: frame.size.height)
        addSubview(border)
    }
}

extension Date {
    static var tomorrow: Date { return Date().dayAfter }
    static var today: Date { return Date() }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: Date())!
    }

    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: Date())!
    }
}

// 11 pro height = 812 - optimal font size for sign up view is 30
// 11 width = 375, height = 736
// 8 width = 375, height = 667
