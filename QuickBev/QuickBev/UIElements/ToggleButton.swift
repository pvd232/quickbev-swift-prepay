//
//  ToggleButton.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 11/28/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit

enum ToggleButtonProps {
    case none
    case left
    case leftCenter
    case rightCenter
    case right
}

class ToggleButton: RoundButton {
    var isOn: Bool = false {
        didSet {
            updateDisplay()
        }
    }

    var toggleDoubleValue: Double?

    var selectedBorderColor: UIColor {
        if isOn == true {
            return UIColor.themeColor
        } else {
            return UIColor.lightGray.withAlphaComponent(0.8)
        }
    }

    var arrangement: ToggleButtonProps = .none

    func updateDisplay() {
        if isOn {
            refreshColor(color: UIColor.themeColor.mix(with: UIColor.white, amount: 0.75))
        } else {
            refreshColor(color: .white)
        }
    }

    override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        super.endTracking(touch, with: event)
        if isOn != true {
            isOn = !isOn
        }
    }
}
