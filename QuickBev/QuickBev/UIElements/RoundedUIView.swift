//
//  RoundedUIView.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 7/12/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
class RoundUIView: UIView {
    var cornerRadius: CGFloat = 15 {
        didSet {
            refreshCorners(value: cornerRadius)
        }
    }

    var borderColor = UIColor.lightGray {
        didSet {
            refreshBorderColor(color: borderColor)
        }
    }

    var borderWidth: CGFloat = 0.75 {
        didSet {
            refreshBorderWidth(value: borderWidth)
        }
    }

    var fillColor = UIColor.red {
        didSet {
            refreshBackgroundColor(color: fillColor)
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func sharedInit() {
        refreshCorners(value: cornerRadius)
        refreshBorderColor(color: borderColor)
        refreshBorderWidth(value: borderWidth)
        refreshBackgroundColor(color: fillColor)
    }

    func refreshBackgroundColor(color: UIColor) {
        layer.backgroundColor = color.cgColor
    }

    func refreshCorners(value: CGFloat) {
        layer.cornerRadius = value
    }

    func refreshBorderColor(color: UIColor) {
        layer.borderColor = color.cgColor
    }

    func refreshBorderWidth(value: CGFloat) {
        layer.borderWidth = value
    }

    func drawLine(with width: CGFloat) {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 10, y: 10))
        path.addLine(to: CGPoint(x: 100, y: 100))

        // Create a `CAShapeLayer` that uses that `UIBezierPath`:

        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = UIColor.blue.cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = width

        // Add that `CAShapeLayer` to your view's layer:

        layer.addSublayer(shapeLayer)
    }
}
