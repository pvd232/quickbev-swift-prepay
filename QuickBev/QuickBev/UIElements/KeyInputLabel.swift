//
//  KeyInputView.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 11/28/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//
import UIKit
class KeyInputView: UIView {
    var _inputView: UIView?

    override var canBecomeFirstResponder: Bool { return true }
    override var canResignFirstResponder: Bool { return true }

    override var inputView: UIView? {
        set { _inputView = newValue }
        get { return _inputView }
    }
}

// MARK: - UIKeyInput

// Modify if need more functionality
extension KeyInputView: UIKeyInput {
    var hasText: Bool { return false }
    func insertText(_: String) {}
    func deleteBackward() {}
}
