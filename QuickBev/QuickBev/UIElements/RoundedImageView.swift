//
//  RoundedImageView.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 8/26/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
class RoundedImageView: UIImageView {
    var cornerRadius: CGFloat = 15 {
        didSet {
            refreshCorners(value: cornerRadius)
        }
    }

    var borderColor = UIColor.clear {
        didSet {
            refreshBorderColor(color: borderColor)
        }
    }

    var borderWidth: CGFloat = 0.75 {
        didSet {
            refreshBorderWidth(value: borderWidth)
        }
    }

    func sharedInit() {
        refreshCorners(value: cornerRadius)
        refreshBorderColor(color: borderColor)
        refreshBorderWidth(value: borderWidth)
    }

    func refreshCorners(value: CGFloat) {
        layer.cornerRadius = value
    }

    func refreshBorderColor(color: UIColor) {
        layer.borderColor = color.cgColor
    }

    func refreshBorderWidth(value: CGFloat) {
        layer.borderWidth = value
    }

    func refreshColor(color: UIColor) {
        tintColor = color
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        sharedInit()
        clipsToBounds = true
    }
}
