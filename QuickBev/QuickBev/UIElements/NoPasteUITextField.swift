//
//  NoPasteUITextField.swift
//  QuickBev
//
//  Created by Peter Driscoll on 2/3/22.
//  Copyright © 2022 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
class NoPasteUITextField: RoundedUITextField {
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.paste(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
}
