//
//  CustomAnnotation.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 6/4/20.
//  Copyright © 2020 Peter Vail Driscoll II. All rights reserved.
//

import MapKit
import UIKit

class CustomAnnotation: NSObject, MKAnnotation {
    let coordinate: CLLocationCoordinate2D
    let businessName: String?
    let businessId: UUID?
    let radius: Double
    init(coordinate: CLLocationCoordinate2D, businessName: String, businessId: UUID, radius: Double) {
        self.coordinate = coordinate
        self.businessName = businessName
        self.businessId = businessId
        self.radius = radius
        super.init()
    }

    deinit {}

    func isClose(from userLocation: CLLocation) -> Bool {
        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        let distanceFromUser = location.distance(from: userLocation)
        return distanceFromUser < radius
    }
}
