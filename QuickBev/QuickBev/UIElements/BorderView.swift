//
//  BorderView.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 11/28/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
enum BorderLineProps {
    case none
    case top
    case right
    case bottom
    case left
}

class BorderView: UIView {
    var arrangement: BorderLineProps?
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
