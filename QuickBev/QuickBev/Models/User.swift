//
//  User.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 6/1/20.
//  Copyright © 2020 Peter Vail Driscoll II. All rights reserved.
//

import CoreData
import UIKit

public class User: NSManagedObject, Codable {
    enum CodingKeys: String, CodingKey {
        case email = "id"
        case password
        case firstName = "first_name"
        case lastName = "last_name"
        case stripeId = "stripe_id"
        case emailVerified = "email_verified"
        case appleId = "apple_id"
        case dateTime = "date_time"
    }

    public required convenience init(from decoder: Decoder) throws {
        self.init(context: CoreDataManager.sharedManager.managedContext)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        // this is a UUID that is used to create a unique key value to access user data from secure storage
        secureId = UUID()
        firstName = try container.decode(String.self, forKey: .firstName)
        lastName = try container.decode(String.self, forKey: .lastName)
        email = try container.decode(String.self, forKey: .email)
        password = try container.decode(String.self, forKey: .password)
        stripeId = try container.decode(String.self, forKey: .stripeId)
        emailVerified = try container.decode(Bool.self, forKey: .emailVerified)
        dateTime = try container.decode(Date.self, forKey: .dateTime)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(firstName, forKey: .firstName)
        try container.encode(lastName, forKey: .lastName)
        try container.encode(email, forKey: .email)
        try container.encode(password, forKey: .password)
        try container.encode(stripeId, forKey: .stripeId)
        try container.encode(appleId, forKey: .appleId)
        try container.encode(emailVerified, forKey: .emailVerified)
        try container.encode(dateTime.timeIntervalSince1970, forKey: .dateTime)
    }

    var email: String {
        get {
            if let email = try! SecureStore(secureStoreQueryable: GenericPasswordQueryable()).getValue(for: "\(secureId.uuidString)email")
            {
                return email
            } else {
                return ""
            }
        }
        set(newEmail) {
            try! SecureStore(secureStoreQueryable: GenericPasswordQueryable()).setValue(newEmail, for: "\(secureId.uuidString)email")
        }
    }

    var password: String {
        get {
            if let password = try! SecureStore(secureStoreQueryable: GenericPasswordQueryable()).getValue(for: "\(secureId.uuidString)password")
            {
                return password
            } else {
                return ""
            }
        }
        set(newPassword) {
            try! SecureStore(secureStoreQueryable: GenericPasswordQueryable()).setValue(newPassword, for: "\(secureId.uuidString)password")
        }
    }

    var stripeId: String {
        get {
            if let stripeId = try! SecureStore(secureStoreQueryable: GenericPasswordQueryable()).getValue(for: "\(secureId.uuidString)stripeId")
            {
                return stripeId
            } else {
                return ""
            }
        }
        set(newStripeId) {
            try! SecureStore(secureStoreQueryable: GenericPasswordQueryable()).setValue(newStripeId, for: "\(secureId.uuidString)stripeId")
        }
    }

    var appleId: String {
        get {
            if let appleId = try! SecureStore(secureStoreQueryable: GenericPasswordQueryable()).getValue(for: "\(secureId.uuidString)appleId")
            {
                return appleId
            } else {
                return ""
            }
        }
        set(newAppleId) {
            try! SecureStore(secureStoreQueryable: GenericPasswordQueryable()).setValue(newAppleId, for: "\(secureId.uuidString)appleId")
        }
    }

    func getVisitedBusinesses() -> [Business]? {
        // if the user has not placed any orders then they wont have any visited businesses
        guard let orders = CoreDataManager.sharedManager.queryEntities(entityName: "Order", propertyToFilter: "userId", propertyValue: email) as? [Order], orders.count >= 1 else {
            return nil
        }

        var uniqueBusinessIdList: [UUID] = []
        var visitedBusinesses: [Business] = []
        for order in orders {
            if uniqueBusinessIdList.first(where: {
                businessId in if businessId == order.businessId {
                    return true
                } else {
                    return false
                }
            }) == nil {
                uniqueBusinessIdList.append(order.businessId)
            }
        }
        for businessId in uniqueBusinessIdList {
            if let business = CheckoutCart.shared.activeBusinessArray.first(where: {
                $0.id == businessId
            }) {
                visitedBusinesses.append(business)
            }
        }
        return visitedBusinesses
    }
}

extension User {
    func update(userData: UserData) {
        email = userData.email
        password = userData.password
        firstName = userData.firstName
        lastName = userData.lastName
        stripeId = userData.stripeId
        appleId = userData.appleId
        emailVerified = userData.emailVerified
        dateTime = userData.dateTime
    }

    convenience init(userData: UserData) {
        self.init(context: CoreDataManager.sharedManager.managedContext)
        secureId = UUID()
        email = userData.email
        password = userData.password
        firstName = userData.firstName
        lastName = userData.lastName
        stripeId = userData.stripeId
        appleId = userData.appleId
        emailVerified = userData.emailVerified
        dateTime = userData.dateTime
    }

    // because email, password, and stripe id are computed properties that rely on first name and last name it is important we set them after first and last name
    convenience init(FirstName: String, LastName: String, Email: String, Password: String, EmailVerified: Bool) {
        self.init(context: CoreDataManager.sharedManager.managedContext)
        secureId = UUID()
        firstName = FirstName
        lastName = LastName
        email = Email
        password = Password
        emailVerified = EmailVerified
        dateTime = Date()
    }

    convenience init(FirstName: String, LastName: String, Email: String, Password: String, StripeId: String, EmailVerified: Bool) {
        self.init(context: CoreDataManager.sharedManager.managedContext)
        secureId = UUID()
        firstName = FirstName
        lastName = LastName
        email = Email
        password = Password
        stripeId = StripeId
        emailVerified = EmailVerified
        dateTime = Date()
    }

    convenience init(FirstName: String, LastName: String, Email: String) {
        self.init(context: CoreDataManager.sharedManager.managedContext)
        secureId = UUID()
        firstName = FirstName
        lastName = LastName
        email = Email
        password = ""
        stripeId = ""
        emailVerified = true
        dateTime = Date()
    }

    convenience init(FirstName: String, LastName: String, Email: String, AppleId: String) {
        self.init(context: CoreDataManager.sharedManager.managedContext)
        secureId = UUID()
        firstName = FirstName
        lastName = LastName
        email = Email
        appleId = AppleId
        password = ""
        stripeId = ""
        emailVerified = true
        dateTime = Date()
    }
}
