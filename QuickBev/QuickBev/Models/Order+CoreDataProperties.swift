//
//  Order+CoreDataProperties.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 1/29/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//
//

import CoreData
import Foundation

public extension Order {
    @nonobjc class func fetchRequest() -> NSFetchRequest<Order> {
        return NSFetchRequest<Order>(entityName: "Order")
    }

    @NSManaged var id: UUID
    @NSManaged var userId: String
    @NSManaged var businessId: UUID
    @NSManaged var merchantStripeId: String

    @NSManaged var salesTaxPercentage: Double
    @NSManaged var tipPercentage: Double
    @NSManaged var serviceFeePercentage: Double
    @NSManaged var stripeFeePercentage: Double
    @NSManaged var subtotal: Double
    @NSManaged var tipTotal: Double
    @NSManaged var serviceFeeTotal: Double
    @NSManaged var stripeApplicationFeeTotal: Double
    @NSManaged var salesTaxTotal: Double
    @NSManaged var total: Double
    @NSManaged var stripeFeeTotal: Double
    @NSManaged var netStripeApplicationFeeTotal: Double
    @NSManaged var netServiceFeeTotal: Double

    @NSManaged var dateTime: Date
    @NSManaged var paymentIntentId: String
    @NSManaged var cardInformation: String
    @NSManaged var completed: Bool
    @NSManaged var refunded: Bool
    @NSManaged var hasBeenDeactivated: Bool
    @NSManaged var activeOrderCount: Int16
    // relationships
    @NSManaged var orderDrink: NSSet
    @NSManaged var user: User
    @NSManaged var checkoutCart: CheckoutCart?
}

// MARK: Generated accessors for orderDrink

public extension Order {
    @objc(addOrderDrinkObject:)
    @NSManaged func addToOrderDrink(_ value: OrderDrink)

    @objc(removeOrderDrinkObject:)
    @NSManaged func removeFromOrderDrink(_ value: OrderDrink)

    @objc(addOrderDrink:)
    @NSManaged func addToOrderDrink(_ values: NSSet)

    @objc(removeOrderDrink:)
    @NSManaged func removeFromOrderDrink(_ values: NSSet)
}

extension Order: Identifiable {}
