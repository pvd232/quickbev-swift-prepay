//
//  QuickPassData+CoreDataProperties.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 12/21/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import CoreData
import Foundation

public extension QuickPassData {
    @nonobjc class func fetchRequest() -> NSFetchRequest<QuickPassData> {
        return NSFetchRequest<QuickPassData>(entityName: "QuickPassData")
    }

    @NSManaged var id: UUID
    @NSManaged var customerId: String
    @NSManaged var businessId: UUID
    @NSManaged var merchantStripeId: String
    @NSManaged var price: Double
    @NSManaged var salesTaxTotal: Double
    @NSManaged var salesTaxRate: Double
    @NSManaged var total: Double
    @NSManaged var subtotal: Double
    @NSManaged var dateTime: Date
    @NSManaged var activationTime: Date
    @NSManaged var expirationTime: Date
    @NSManaged var timeCheckedIn: Date?
    @NSManaged var paymentIntentId: String
    @NSManaged var shouldDisplayExpirationTime: Bool
    @NSManaged var soldOut: Bool
    @NSManaged var cardInformation: String
    @NSManaged var quickPassDataToBusiness: Business?
}

extension QuickPassData: Identifiable {}
