//
//  QuickPass+CoreDataClass.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 7/30/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//
//

import CoreData
import Foundation

public class QuickPass: NSManagedObject, Codable {
    enum CodingKeys: String, CodingKey {
        case id
        case customerId = "customer_id"
        case merchantStripeId = "merchant_stripe_id"
        case businessId = "business_id"
        case dateTime = "date_time"
        case total
        case subtotal
        case price
        case salesTaxTotal = "sales_tax_total"
        case salesTaxRate = "sales_tax_percentage"
        case paymentIntentId = "payment_intent_id"
        case activationTime = "activation_time"
        case expirationTime = "expiration_time"
        case timeCheckedIn = "time_checked_in"
        case shouldDisplayExpirationTime = "should_display_expiration_time"
        case soldOut = "sold_out"
        case cardInformation = "card_information"
    }

    public required convenience init(from decoder: Decoder) throws {
        self.init(context: CoreDataManager.sharedManager.managedContext)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(UUID.self, forKey: .id)
        merchantStripeId = try container.decode(String.self, forKey: .merchantStripeId)
        customerId = try container.decode(String.self, forKey: .customerId)
        businessId = try container.decode(UUID.self, forKey: .businessId)
        subtotal = try container.decode(Double.self, forKey: .subtotal)
        total = try container.decode(Double.self, forKey: .total)
        dateTime = try container.decode(Date.self, forKey: .dateTime)
        price = try container.decode(Double.self, forKey: .price)
        salesTaxTotal = try container.decode(Double.self, forKey: .salesTaxTotal)
        salesTaxRate = try container.decode(Double.self, forKey: .salesTaxRate)
        paymentIntentId = try container.decode(String.self, forKey: .paymentIntentId)
        activationTime = try container.decode(Date.self, forKey: .activationTime)
        expirationTime = try container.decode(Date.self, forKey: .expirationTime)
        timeCheckedIn = try? container.decode(Date.self, forKey: .timeCheckedIn)
        shouldDisplayExpirationTime = try container.decode(Bool.self, forKey: .shouldDisplayExpirationTime)
        soldOut = try container.decode(Bool.self, forKey: .soldOut)
        cardInformation = try container.decode(String.self, forKey: .cardInformation)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(merchantStripeId, forKey: .merchantStripeId)
        try container.encode(customerId, forKey: .customerId)
        try container.encode(businessId, forKey: .businessId)
        try container.encode(dateTime.timeIntervalSince1970, forKey: .dateTime)
        try container.encode(total, forKey: .total)
        try container.encode(subtotal, forKey: .total)
        try container.encode(price, forKey: .price)
        try container.encode(salesTaxTotal, forKey: .salesTaxTotal)
        try container.encode(salesTaxRate, forKey: .salesTaxRate)
        try container.encode(paymentIntentId, forKey: .paymentIntentId)
        try container.encode(activationTime.timeIntervalSince1970, forKey: .activationTime)
        try container.encode(expirationTime.timeIntervalSince1970, forKey: .expirationTime)
        if let timeCheckedIn = timeCheckedIn {
            try container.encode(timeCheckedIn.timeIntervalSince1970, forKey: .timeCheckedIn)
        }
        try container.encode(cardInformation, forKey: .cardInformation)

        // no need to encode sold out attribute, this is just a computed boolean prop that is set in the backend, not persisted
    }

    static var imageURL = URL(string: "https://storage.googleapis.com/my-new-quickbev-bucket/original.png")
    var isActive: Bool {
        // expiration time must be greater than the current time
        if expirationTime.timeIntervalSince1970 > Date().timeIntervalSince1970, timeCheckedIn == nil {
            return true
        } else {
            return false
        }
    }

    func formatTimeString(timeString: String?) -> Int {
        let timeSubString = timeString!.split(separator: ":")[0]
        let firstCharacter = timeSubString[0]
        var intCharacter: Int?

        if firstCharacter == "0" {
            let secondCharacter = timeSubString[1]
            intCharacter = Int(String(secondCharacter))
        } else {
            intCharacter = Int(timeSubString)
        }
        return intCharacter!
    }

    var formattedExpirationTime: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        let time = formatter.string(from: expirationTime)
        return time
    }

    var orderTime: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale

        let time = formatter.string(from: dateTime)
        let formattedFirstValue = formatTimeString(timeString: time)
        let timeSubString = time.split(separator: ":")
        let subTimeSubString = timeSubString.dropFirst(1)
        let newTimeSubString = subTimeSubString.joined()
        let timeFormatted = formattedFirstValue.description + ":" + newTimeSubString
        return timeFormatted
    }

    var orderDate: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, yyy"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        return formatter.string(from: dateTime)
    }
}

extension QuickPass {
    convenience init(quickPassData: QuickPassData) {
        self.init(context: CoreDataManager.sharedManager.managedContext)
        id = quickPassData.id
        customerId = quickPassData.customerId
        businessId = quickPassData.businessId
        merchantStripeId = quickPassData.merchantStripeId
        price = quickPassData.price
        salesTaxTotal = quickPassData.salesTaxTotal
        salesTaxRate = quickPassData.salesTaxRate
        total = quickPassData.total
        subtotal = quickPassData.subtotal
        dateTime = quickPassData.dateTime
        activationTime = quickPassData.activationTime
        expirationTime = quickPassData.expirationTime
        timeCheckedIn = quickPassData.timeCheckedIn
        paymentIntentId = quickPassData.paymentIntentId
        shouldDisplayExpirationTime = quickPassData.shouldDisplayExpirationTime
        soldOut = quickPassData.soldOut
        cardInformation = quickPassData.cardInformation
    }
}
