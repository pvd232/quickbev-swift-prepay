//
//  ImageURLProtocol.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 4/7/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//
//  Abstract:
//  The ImageURLProtocol of the sample.

import UIKit

class ImageURLProtocol: URLProtocol {
    var cancelledOrComplete: Bool = false
    var block: DispatchWorkItem!

    private static let queue = DispatchQueue(label: "com.apple.imageLoaderURLProtocol")

    override class func canInit(with _: URLRequest) -> Bool {
        return true
    }

    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }

    override class func requestIsCacheEquivalent(_: URLRequest, to _: URLRequest) -> Bool {
        return false
    }

    override final func startLoading() {
        guard let reqURL = request.url, let urlClient = client else {
            print("failed to get request.url")
            return
        }
        block = DispatchWorkItem(block: {
            if self.cancelledOrComplete == false {
                if let data = try? Data(contentsOf: reqURL) {
                    urlClient.urlProtocol(self, didLoad: data)
                    urlClient.urlProtocolDidFinishLoading(self)
                }
            }
            self.cancelledOrComplete = true
        })

        ImageURLProtocol.queue.asyncAfter(deadline: DispatchTime(uptimeNanoseconds: 500 * NSEC_PER_MSEC), execute: block)
    }

    override final func stopLoading() {
        ImageURLProtocol.queue.async {
            if self.cancelledOrComplete == false, let cancelBlock = self.block {
                cancelBlock.cancel()
                self.cancelledOrComplete = true
            }
        }
    }

    static func urlSession() -> URLSession {
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [ImageURLProtocol.classForCoder()]
        return URLSession(configuration: config)
    }
}
