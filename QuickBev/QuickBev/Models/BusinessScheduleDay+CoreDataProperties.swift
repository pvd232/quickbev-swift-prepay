//
//  BusinessScheduleDay+CoreDataProperties.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 8/8/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//
//

import CoreData
import Foundation

public extension BusinessScheduleDay {
    @nonobjc class func fetchRequest() -> NSFetchRequest<BusinessScheduleDay> {
        return NSFetchRequest<BusinessScheduleDay>(entityName: "BusinessScheduleDay")
    }

    @NSManaged var day: String
    @NSManaged var openingTime: String?
    @NSManaged var closingTime: String?
    @NSManaged var isClosed: Bool
    @NSManaged var businessId: UUID
    @NSManaged var business: Business
}

extension BusinessScheduleDay: Identifiable {}
