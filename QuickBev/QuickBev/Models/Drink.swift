//
//  Drink.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 12/25/20.
//  Copyright © 2020 Peter Vail Driscoll II. All rights reserved.
//

import CoreData
import os

public class Drink: NSManagedObject, Codable {
    enum CodingKeys: String, CodingKey {
        case drinkResponse = "drink"
        case id
        case name
        case category
        case detail = "description"
        case price
        case businessId = "business_id"
        case imageURL = "image_url"
        case isActive = "is_active"
    }

    public required convenience init(from decoder: Decoder) throws {
        self.init(context: CoreDataManager.sharedManager.managedContext)
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let nestedContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .drinkResponse)
        id = try nestedContainer.decode(UUID.self, forKey: .id)
        name = try nestedContainer.decode(String.self, forKey: .name)
        category = try nestedContainer.decode(String.self, forKey: .category)
        detail = try nestedContainer.decode(String.self, forKey: .detail)
        price = try nestedContainer.decode(Double.self, forKey: .price)
        businessId = try nestedContainer.decode(UUID.self, forKey: .businessId)
        imageURL = try nestedContainer.decode(String.self, forKey: .imageURL)
        isActive = try nestedContainer.decode(Bool.self, forKey: .isActive)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        var nestedContainer = container.nestedContainer(keyedBy: CodingKeys.self, forKey: .drinkResponse)
        try nestedContainer.encode(id, forKey: .id)
        try nestedContainer.encode(name, forKey: .name)
        try nestedContainer.encode(category, forKey: .category)
        try nestedContainer.encode(detail, forKey: .detail)
        try nestedContainer.encode(price, forKey: .price)
        try nestedContainer.encode(businessId, forKey: .businessId)
        try nestedContainer.encode(imageURL, forKey: .imageURL)
        try nestedContainer.encode(isActive, forKey: .isActive)
    }
}

extension Drink {
    var formattedName: String {
        var returnValue: String
        let nameWords = name.split(separator: " ")
        if nameWords.count > 1 {
            var newName = [String]()
            for word in nameWords {
                if Array(word).count > 2 {
                    if !word.first!.isUppercase {
                        let capitalizedWord = word.capitalized
                        newName.append(capitalizedWord)
                    } else {
                        let wordString = String(word)
                        newName.append(wordString)
                    }
                } else {
                    newName.append(String(word))
                }
            }
            returnValue = newName.joined(separator: " ")
            return returnValue
        } else {
            returnValue = name.capitalized
            return returnValue
        }
    }

    var drinkCategory: DrinkCategory {
        return DrinkCategory.allCases.first(where: { $0.rawValue == category })!
    }

    func update(with drinkData: DrinkData) {
        name = drinkData.name
        category = drinkData.category
        detail = drinkData.detail
        price = drinkData.price
        businessId = drinkData.businessId
        imageURL = drinkData.imageURL
        isActive = drinkData.isActive
    }

    convenience init(drinkData: DrinkData) {
        self.init(context: CoreDataManager.sharedManager.managedContext)
        id = drinkData.id
        name = drinkData.name
        category = drinkData.category
        detail = drinkData.detail
        price = drinkData.price
        businessId = drinkData.businessId
        imageURL = drinkData.imageURL
        isActive = drinkData.isActive
    }
}
