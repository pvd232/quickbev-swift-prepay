//
//  Drink+CoreDataProperties.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 1/29/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//
//

import CoreData
import Foundation

public extension Drink {
    @nonobjc class func fetchRequest() -> NSFetchRequest<Drink> {
        return NSFetchRequest<Drink>(entityName: "Drink")
    }

    @NSManaged var imageURL: String?
    @NSManaged var businessId: UUID
    @NSManaged var detail: String
    @NSManaged var id: UUID
    @NSManaged var name: String
    @NSManaged var category: String
    @NSManaged var price: Double
    @NSManaged var isActive: Bool
    @NSManaged var drinkToBusiness: Business?
    @NSManaged var drinkToCheckoutCart: CheckoutCart?
    @NSManaged var orderDrink: NSSet?
}

extension Drink: Identifiable {}

public extension Drink {
    @objc(addOrderDrinkObject:)
    @NSManaged func addToOrderDrink(_ value: OrderDrink)

    @objc(removeOrderDrinkObject:)
    @NSManaged func removeFromOrderDrink(_ value: OrderDrink)

    @objc(addOrderDrink:)
    @NSManaged func addToOrderDrink(_ values: NSSet)

    @objc(removeOrderDrink:)
    @NSManaged func removeFromOrderDrink(_ values: NSSet)
}
