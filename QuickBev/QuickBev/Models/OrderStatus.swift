//
//  OrderStatus.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 11/7/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import Foundation
class OrderStatus: Codable {
    let id: UUID!
    let completed: Bool
    let refunded: Bool

    enum CodingKeys: String, CodingKey {
        case id
        case completed
        case refunded
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(UUID.self, forKey: .id)
        completed = try container.decode(Bool.self, forKey: .completed)
        refunded = try container.decode(Bool.self, forKey: .refunded)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(completed, forKey: .completed)
        try container.encode(refunded, forKey: .refunded)
    }
}
