//
//  BusinessScheduleDay+CoreDataClass.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 8/8/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//
//

import CoreData
import Foundation

public class BusinessScheduleDay: NSManagedObject, Codable {
    enum CodingKeys: String, CodingKey {
        case businessId = "business_id"
        case day
        case openingTime = "opening_time"
        case closingTime = "closing_time"
        case isClosed = "is_closed"
    }

    public required convenience init(from decoder: Decoder) throws {
        self.init(context: CoreDataManager.sharedManager.managedContext)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        businessId = try container.decode(UUID.self, forKey: .businessId)
        day = try container.decode(String.self, forKey: .day)
        closingTime = try? container.decode(String.self, forKey: .closingTime)
        openingTime = try? container.decode(String.self, forKey: .openingTime)
        isClosed = try container.decode(Bool.self, forKey: .isClosed)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(businessId, forKey: .businessId)
        try container.encode(day, forKey: .day)
        try? container.encode(openingTime, forKey: .openingTime)
        try? container.encode(closingTime, forKey: .closingTime)
        try container.encode(isClosed, forKey: .isClosed)
    }
}
