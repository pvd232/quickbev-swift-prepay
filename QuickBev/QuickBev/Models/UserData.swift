//
//  UserData.swift
//  QuickBev
//
//  Created by Peter Driscoll on 1/21/22.
//  Copyright © 2022 Peter Vail Driscoll II. All rights reserved.
//

import Foundation
struct UserData: Codable {
    let firstName: String
    let lastName: String
    let email: String
    let password: String
    let stripeId: String
    let appleId: String
    let emailVerified: Bool
    let secureId: UUID
    let dateTime: Date

    enum CodingKeys: String, CodingKey {
        case email = "id"
        case password
        case firstName = "first_name"
        case lastName = "last_name"
        case stripeId = "stripe_id"
        case emailVerified = "email_verified"
        case appleId = "apple_id"
        case dateTime = "date_time"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        // this is a UUID that is used to create a unique key value to access user data from secure storage
        secureId = UUID()
        firstName = try container.decode(String.self, forKey: .firstName)
        lastName = try container.decode(String.self, forKey: .lastName)
        email = try container.decode(String.self, forKey: .email)
        password = try container.decode(String.self, forKey: .password)
        stripeId = try container.decode(String.self, forKey: .stripeId)
        appleId = (try? container.decode(String.self, forKey: .appleId)) ?? ""
        emailVerified = try container.decode(Bool.self, forKey: .emailVerified)
        dateTime = try container.decode(Date.self, forKey: .dateTime)
    }

    init(FirstName: String, LastName: String, Email: String, AppleId: String) {
        secureId = UUID()
        firstName = FirstName
        lastName = LastName
        email = Email
        appleId = AppleId
        password = ""
        stripeId = ""
        emailVerified = true
        dateTime = Date()
    }

    init(FirstName: String, LastName: String, Email: String) {
        secureId = UUID()
        firstName = FirstName
        lastName = LastName
        email = Email
        appleId = ""
        password = ""
        stripeId = ""
        emailVerified = true
        dateTime = Date()
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(firstName, forKey: .firstName)
        try container.encode(lastName, forKey: .lastName)
        try container.encode(email, forKey: .email)
        try container.encode(password, forKey: .password)
        try container.encode(stripeId, forKey: .stripeId)
        try container.encode(appleId, forKey: .appleId)
        try container.encode(emailVerified, forKey: .emailVerified)
        try container.encode(dateTime.timeIntervalSince1970, forKey: .dateTime)
    }
}
