//
//  OrderData.swift
//  QuickBev
//
//  Created by Peter Driscoll on 1/13/22.
//  Copyright © 2022 Peter Vail Driscoll II. All rights reserved.
//

import Foundation

struct OrderData: Codable {
    let id: UUID
    let merchantStripeId: String
    let businessId: UUID
    let userId: String
    let salesTaxPercentage: Double
    let tipPercentage: Double
    let serviceFeePercentage: Double
    let stripeFeePercentage: Double
    let subtotal: Double
    let tipTotal: Double
    let serviceFeeTotal: Double
    let stripeApplicationFeeTotal: Double
    let salesTaxTotal: Double
    let total: Double
    let stripeFeeTotal: Double
    let netStripeApplicationFeeTotal: Double
    let netServiceFeeTotal: Double

    let dateTime: Date
    var paymentIntentId: String = ""
    var cardInformation: String = ""
    let completed: Bool
    let refunded: Bool
    let activeOrderCount: Int16
    var orderDrink = [OrderDrinkData]()

    enum CodingKeys: String, CodingKey {
        case id
        case merchantStripeId = "merchant_stripe_id"
        case businessId = "business_id"
        case userId = "customer_id"
        case userStripeId = "customer_stripe_id"
        case user = "customer"
        case orderDrink = "order_drink"
        case dateTime = "date_time"
        case salesTaxPercentage = "sales_tax_percentage"
        case tipPercentage = "tip_percentage"
        case serviceFeePercentage = "service_fee_percentage"
        case stripeFeePercentage = "stripe_fee_percentage"
        case subtotal
        case tipTotal = "tip_total"
        case serviceFeeTotal = "service_fee_total"
        case stripeApplicationFeeTotal = "stripe_application_fee_total"
        case salesTaxTotal = "sales_tax_total"
        case total
        case stripeFeeTotal = "stripe_fee_total"
        case netStripeApplicationFeeTotal = "net_stripe_application_fee_total"
        case netServiceFeeTotal = "net_service_fee_total"
        case paymentIntentId = "payment_intent_id"
        case cardInformation = "card_information"
        case completed
        case refunded
        case activeOrderCount = "active_order_count"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(UUID.self, forKey: .id)
        merchantStripeId = try container.decode(String.self, forKey: .merchantStripeId)
        businessId = try container.decode(UUID.self, forKey: .businessId)
        userId = try container.decode(String.self, forKey: .userId)
        salesTaxPercentage = try container.decode(Double.self, forKey: .salesTaxPercentage)
        tipPercentage = try container.decode(Double.self, forKey: .tipPercentage)
        serviceFeePercentage = try container.decode(Double.self, forKey: .serviceFeePercentage)
        stripeFeePercentage = try container.decode(Double.self, forKey: .stripeFeePercentage)

        subtotal = try container.decode(Double.self, forKey: .subtotal)
        tipTotal = try container.decode(Double.self, forKey: .tipTotal)
        serviceFeeTotal = try container.decode(Double.self, forKey: .serviceFeeTotal)
        stripeApplicationFeeTotal = try container.decode(Double.self, forKey: .stripeApplicationFeeTotal)
        salesTaxTotal = try container.decode(Double.self, forKey: .salesTaxTotal)
        total = try container.decode(Double.self, forKey: .total)
        stripeFeeTotal = try container.decode(Double.self, forKey: .salesTaxTotal)
        netStripeApplicationFeeTotal = try container.decode(Double.self, forKey: .netStripeApplicationFeeTotal)
        netServiceFeeTotal = try container.decode(Double.self, forKey: .netServiceFeeTotal)

        dateTime = try container.decode(Date.self, forKey: .dateTime)
        paymentIntentId = try container.decode(String.self, forKey: .paymentIntentId)
        cardInformation = try container.decode(String.self, forKey: .cardInformation)
        completed = try container.decode(Bool.self, forKey: .completed)
        refunded = try container.decode(Bool.self, forKey: .refunded)
        activeOrderCount = try container.decode(Int16.self, forKey: .activeOrderCount)
        orderDrink = try container.decode([OrderDrinkData].self, forKey: .orderDrink)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(merchantStripeId, forKey: .merchantStripeId)
        try container.encode(userStripeId, forKey: .userStripeId)
        try container.encode(businessId, forKey: .businessId)
        try container.encode(userId, forKey: .userId)
        try container.encode(salesTaxPercentage, forKey: .salesTaxPercentage)
        try container.encode(tipPercentage, forKey: .tipPercentage)
        try container.encode(serviceFeePercentage, forKey: .serviceFeePercentage)
        try container.encode(stripeFeePercentage, forKey: .stripeFeePercentage)

        try container.encode(subtotal, forKey: .subtotal)
        try container.encode(tipTotal, forKey: .tipTotal)
        try container.encode(serviceFeeTotal, forKey: .serviceFeeTotal)
        try container.encode(stripeApplicationFeeTotal, forKey: .stripeApplicationFeeTotal)
        try container.encode(salesTaxTotal, forKey: .salesTaxTotal)
        try container.encode(total, forKey: .total)
        try container.encode(stripeFeeTotal, forKey: .stripeFeeTotal)
        try container.encode(netStripeApplicationFeeTotal, forKey: .netStripeApplicationFeeTotal)
        try container.encode(netServiceFeeTotal, forKey: .netServiceFeeTotal)

        try container.encode(dateTime.timeIntervalSince1970, forKey: .dateTime)
        try container.encode(paymentIntentId, forKey: .paymentIntentId)
        try container.encode(cardInformation, forKey: .cardInformation)
        try container.encode(completed, forKey: .completed)
        try container.encode(refunded, forKey: .refunded)
        try container.encode(activeOrderCount, forKey: .activeOrderCount)
        try container.encode(orderDrink, forKey: .orderDrink)
    }
}

extension OrderData {
    var user: User {
        return CheckoutCart.shared.user!
    }

    var userStripeId: String {
        return user.stripeId
    }
}
