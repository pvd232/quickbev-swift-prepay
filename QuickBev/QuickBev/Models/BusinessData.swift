//
//  BusinessData.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 12/20/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import Foundation
struct BusinessData: Codable {
    let id: UUID
    let address: String
    var coordinateString: String = ""
    let name: String
    let salesTaxRate: Double
    let serviceFeePercentage: Double
    let merchantStripeId: String
    var drinks = [DrinkData]()
    let atCapacity: Bool
    let isActive: Bool
    let deactivated: Bool
    let schedule: [BusinessScheduleDay]
    let imageURL: String?

    enum CodingKeys: String, CodingKey {
        case businessResponse = "business"
        case id
        case name
        case address
        case salesTaxRate = "sales_tax_rate"
        case serviceFeePercentage = "service_fee_percentage"
        case merchantStripeId = "merchant_stripe_id"
        case atCapacity = "at_capacity"
        case schedule
        case imageURL = "image_url"
        case isActive = "is_active"
        case deactivated
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let businessResponse = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .businessResponse)
        id = try businessResponse.decode(UUID.self, forKey: .id)
        name = (try? businessResponse.decode(String.self, forKey: .name)) ?? ""
        address = try businessResponse.decode(String.self, forKey: .address)
        salesTaxRate = try businessResponse.decode(Double.self, forKey: .salesTaxRate)
        serviceFeePercentage = try businessResponse.decode(Double.self, forKey: .serviceFeePercentage)
        merchantStripeId = try businessResponse.decode(String.self, forKey: .merchantStripeId)
        atCapacity = try businessResponse.decode(Bool.self, forKey: .atCapacity)
        isActive = try businessResponse.decode(Bool.self, forKey: .isActive)
        deactivated = try businessResponse.decode(Bool.self, forKey: .deactivated)
        schedule = try businessResponse.decode([BusinessScheduleDay].self, forKey: .schedule)
        imageURL = try? businessResponse.decode(String.self, forKey: .imageURL)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        var businessResponse = container.nestedContainer(keyedBy: CodingKeys.self, forKey: .businessResponse)
        try businessResponse.encode(name, forKey: .name)
        try businessResponse.encode(id, forKey: .id)
        try businessResponse.encode(address, forKey: .address)
        try businessResponse.encode(salesTaxRate, forKey: .salesTaxRate)
        try businessResponse.encode(serviceFeePercentage, forKey: .serviceFeePercentage)
        try businessResponse.encode(merchantStripeId, forKey: .merchantStripeId)
        try businessResponse.encode(atCapacity, forKey: .atCapacity)
        try businessResponse.encode(isActive, forKey: .isActive)
        try businessResponse.encode(deactivated, forKey: .deactivated)
        try businessResponse.encode(schedule, forKey: .schedule)
        try? businessResponse.encode(imageURL, forKey: .imageURL)
    }
}
