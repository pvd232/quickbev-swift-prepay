//
//  Business.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 1/28/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import CoreData
import CoreLocation

public class Business: NSManagedObject, Codable {
    enum CodingKeys: String, CodingKey {
        case businessResponse = "business"
        case id
        case name
        case address
        case salesTaxRate = "sales_tax_rate"
        case serviceFeePercentage = "service_fee_percentage"
        case merchantStripeId = "merchant_stripe_id"
        case atCapacity = "at_capacity"
        case schedule
        case imageURL = "image_url"
        case isActive = "is_active"
        case deactivated
        case detail = "description"
    }

    public required convenience init(from decoder: Decoder) throws {
        self.init(context: CoreDataManager.sharedManager.managedContext)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let businessResponse = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .businessResponse)
        id = try businessResponse.decode(UUID.self, forKey: .id)
        name = try businessResponse.decode(String.self, forKey: .name)
        address = try businessResponse.decode(String.self, forKey: .address)
        salesTaxRate = try businessResponse.decode(Double.self, forKey: .salesTaxRate)
        serviceFeePercentage = try businessResponse.decode(Double.self, forKey: .serviceFeePercentage)
        merchantStripeId = try businessResponse.decode(String.self, forKey: .merchantStripeId)
        atCapacity = try businessResponse.decode(Bool.self, forKey: .atCapacity)
        isActive = try businessResponse.decode(Bool.self, forKey: .isActive)
        deactivated = try businessResponse.decode(Bool.self, forKey: .deactivated)
        schedule = NSSet(array: try businessResponse.decode([BusinessScheduleDay].self, forKey: .schedule))
        imageURL = try? businessResponse.decode(String.self, forKey: .imageURL)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        var businessResponse = container.nestedContainer(keyedBy: CodingKeys.self, forKey: .businessResponse)
        try businessResponse.encode(name, forKey: .name)
        try businessResponse.encode(id, forKey: .id)
        try businessResponse.encode(address, forKey: .address)
        try businessResponse.encode(salesTaxRate, forKey: .salesTaxRate)
        try businessResponse.encode(serviceFeePercentage, forKey: .serviceFeePercentage)
        try businessResponse.encode(merchantStripeId, forKey: .merchantStripeId)
        try businessResponse.encode(atCapacity, forKey: .atCapacity)
        try businessResponse.encode(isActive, forKey: .isActive)
        try businessResponse.encode(deactivated, forKey: .deactivated)
        try businessResponse.encode(scheduleArray, forKey: .schedule)
        try? businessResponse.encode(imageURL, forKey: .imageURL)
    }

    var coordinate: CLLocationCoordinate2D {
        let latAndLonArray = coordinateString.split(separator: ",")
        let lattitude = latAndLonArray[0].split(separator: ":")[1].trimmingCharacters(in: .whitespacesAndNewlines)
        let longitude = latAndLonArray[1].split(separator: ":")[1].trimmingCharacters(in: .whitespacesAndNewlines).replacingOccurrences(of: ")", with: "")
        let locationCoordinate = CLLocationCoordinate2D(latitude: Double(lattitude)!, longitude: Double(longitude)!)
        return locationCoordinate
    }

    var location: CLLocation {
        let latAndLonArray = coordinateString.split(separator: ",")
        let lattitude = latAndLonArray[0].split(separator: ":")[1].trimmingCharacters(in: .whitespacesAndNewlines)
        let longitude = latAndLonArray[1].split(separator: ":")[1].trimmingCharacters(in: .whitespacesAndNewlines).replacingOccurrences(of: ")", with: "")
        let location = CLLocation(latitude: Double(lattitude)!, longitude: Double(longitude)!)
        return location
    }

    var distanceFromUser: CLLocationDistance {
        let distanceInKilometers = location.distance(from: SceneDelegate.shared.rootViewController.currentLocation) / 1000
        let distanceInMiles = distanceInKilometers * 0.621371
        return distanceInMiles
    }

    func getLocation(from address: String, completion: @escaping (_ status: Bool?) -> Void) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address) { placemarks, _ in
            if let placemarks = placemarks,
               let coordinate = placemarks.first?.location?.coordinate
            {
                self.coordinateString = String(describing: coordinate)
                CoreDataManager.sharedManager.saveContext()
            }
            completion(true)
        }
    }

    func isLocal(from userLocation: CLLocation) -> Bool {
        let distanceInKilometers = location.distance(from: userLocation) / 1000
        let distanceInMiles = distanceInKilometers * 0.621371
        return distanceInMiles <= 10
    }

    func isVisited() -> Bool {
        if let visitedBusinesses = CheckoutCart.shared.user?.getVisitedBusinesses() {
            for business in visitedBusinesses {
                if business.id == id {
                    return true
                }
            }
            return false
        } else {
            return false
        }
    }

    public let dayIndexTable = [
        "monday",
        "tuesday",
        "wednesday",
        "thursday",
        "friday",
        "saturday",
        "sunday",
    ]
}

extension Business {
    func update(with businessData: BusinessData) {
        name = businessData.name
        address = businessData.address
        salesTaxRate = businessData.salesTaxRate
        serviceFeePercentage = businessData.serviceFeePercentage
        merchantStripeId = businessData.merchantStripeId
        atCapacity = businessData.atCapacity
        isActive = businessData.isActive
        isActive = businessData.deactivated
        imageURL = businessData.imageURL
        schedule = NSSet(array: businessData.schedule)
    }

    var formattedName: String {
        var returnValue: String
        let nameWords = name.split(separator: " ")
        if nameWords.count > 1 {
            var newName = [String]()
            for word in nameWords {
                if Array(word).count > 2 {
                    if !word.first!.isUppercase {
                        let capitalizedWord = word.capitalized
                        newName.append(capitalizedWord)
                    } else {
                        let wordString = String(word)
                        newName.append(wordString)
                    }
                } else {
                    newName.append(String(word))
                }
            }
            returnValue = newName.joined(separator: " ")
            return returnValue
        } else {
            returnValue = name.capitalized
            return returnValue
        }
    }

    var drinkArray: [Drink] {
        let set = drinks as? Set<Drink> ?? []
        return set.sorted {
            $0.name < $1.name
        }
    }

    var scheduleArray: [BusinessScheduleDay] {
        let set = schedule as? Set<BusinessScheduleDay> ?? []
        return set.sorted {
            dayIndexTable.firstIndex(of: $0.day)! < dayIndexTable.firstIndex(of: $1.day)!
        }
    }

    var todayScheduleDay: BusinessScheduleDay {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let today = dateFormatter.string(from: date).lowercased()
        return scheduleArray.first(where: {
            $0.day == today
        })!
    }

    var yesterdayScheduleDay: BusinessScheduleDay {
        let date = Date().dayBefore
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let yesterday = dateFormatter.string(from: date).lowercased()
        return scheduleArray.first(where: {
            $0.day == yesterday
        })!
    }

    var openingTime: String? {
        guard let openingDateTime = openingDateTime else {
            return nil
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        let time = formatter.string(from: openingDateTime)
        let formattedFirstValue = formatTimeString(timeString: time)
        let timeSubString = time.split(separator: ":")
        let subTimeSubString = timeSubString.dropFirst(1)
        let newTimeSubString = subTimeSubString.joined()
        let timeFormatted = formattedFirstValue.description + ":" + newTimeSubString
        return timeFormatted
    }

    var closingTime: String? {
        guard let closingDateTime = closingDateTime else {
            return nil
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale

        let time = formatter.string(from: closingDateTime)
        let formattedFirstValue = formatTimeString(timeString: time)
        let timeSubString = time.split(separator: ":")
        let subTimeSubString = timeSubString.dropFirst(1)
        let newTimeSubString = subTimeSubString.joined()
        let timeFormatted = formattedFirstValue.description + ":" + newTimeSubString
        return timeFormatted
    }

    func formatTimeString(timeString: String?) -> Int {
        let timeSubString = timeString!.split(separator: ":")[0]
        let firstCharacter = timeSubString[0]
        var intCharacter: Int?

        if firstCharacter == "0" {
            let secondCharacter = timeSubString[1]
            intCharacter = Int(String(secondCharacter))
        } else {
            intCharacter = Int(timeSubString)
        }
        return intCharacter!
    }

    var openingDateTime: Date? {
        guard todayScheduleDay.isClosed != true else {
            // if the bar was open until the AM yesterday, it will be open today even though the bar is technically closed today
            guard yesterdayScheduleDay.isClosed != true else {
                return nil
            }
            let intCloseHour = formatTimeString(timeString: yesterdayScheduleDay.closingTime)
            let yesterdayClosingDateTime = Calendar.current.date(from: DateComponents(year: Calendar.current.component(.year, from: Date()), month: Calendar.current.component(.month, from: Date()), day: Calendar.current.component(.day, from: Date()), hour: intCloseHour, minute: Int(yesterdayScheduleDay.closingTime!.split(separator: ":")[1])))!
            if Date() <= yesterdayClosingDateTime {
                let intOpenHour = formatTimeString(timeString: yesterdayScheduleDay.openingTime)
                let yesterdayOpeningDateTime = Calendar.current.date(from: DateComponents(year: Calendar.current.component(.year, from: Date().dayBefore), month: Calendar.current.component(.month, from: Date().dayBefore), day: Calendar.current.component(.day, from: Date().dayBefore), hour: intOpenHour, minute: Int(yesterdayScheduleDay.openingTime!.split(separator: ":")[1])))!
                return yesterdayOpeningDateTime
            } else {
                return nil
            }
        }
        let currentHour = Calendar.current.component(.hour, from: Date())
        let intOpenHour = formatTimeString(timeString: todayScheduleDay.openingTime!)
        let intCloseHour = formatTimeString(timeString: todayScheduleDay.closingTime!)
        // the bar closes on the day after it opens (closes in AM of next day)
        if intCloseHour <= intOpenHour {
            // if it is in the AM and the bar has not closed yet then the bar opened yesterday
            if currentHour <= intCloseHour {
                return Calendar.current.date(from: DateComponents(year: Calendar.current.component(.year, from: Date().dayBefore), month: Calendar.current.component(.month, from: Date().dayBefore), day: Calendar.current.component(.day, from: Date().dayBefore), hour: intOpenHour, minute: Int(todayScheduleDay.openingTime!.split(separator: ":")[1])))!
            }
            // if its in the AM and after the bar closed then the opening time is today and closing time is tomorrow
            else {
                return Calendar.current.date(from: DateComponents(year: Calendar.current.component(.year, from: Date()), month: Calendar.current.component(.month, from: Date()), day: Calendar.current.component(.day, from: Date()), hour: intOpenHour, minute: Int(todayScheduleDay.openingTime!.split(separator: ":")[1])))!
            }
        } else {
            return Calendar.current.date(from: DateComponents(year: Calendar.current.component(.year, from: Date().dayBefore), month: Calendar.current.component(.month, from: Date().dayBefore), day: Calendar.current.component(.day, from: Date().dayBefore), hour: intOpenHour, minute: Int(todayScheduleDay.openingTime!.split(separator: ":")[1])))!
        }
    }

    var closingDateTime: Date? {
        guard todayScheduleDay.isClosed != true else {
            // if the bar was open until the AM yesterday, it will be open today even though the bar is technically closed today
            guard yesterdayScheduleDay.isClosed != true else {
                return nil
            }
            let intCloseHour = formatTimeString(timeString: yesterdayScheduleDay.closingTime)
            let yesterdayClosingDateTime = Calendar.current.date(from: DateComponents(year: Calendar.current.component(.year, from: Date()), month: Calendar.current.component(.month, from: Date()), day: Calendar.current.component(.day, from: Date()), hour: intCloseHour, minute: Int(yesterdayScheduleDay.closingTime!.split(separator: ":")[1])))!
            if Date() <= yesterdayClosingDateTime {
                return yesterdayClosingDateTime
            } else {
                return nil
            }
        }
        let currentHour = Calendar.current.component(.hour, from: Date())
        let intOpenHour = formatTimeString(timeString: todayScheduleDay.openingTime!)
        let intCloseHour = formatTimeString(timeString: todayScheduleDay.closingTime!)
        // the bar closes on the day after it opens (closes in AM of next day) if the closing hour is less than the opening hour
        if intCloseHour <= intOpenHour {
            // if it is in the AM and the bar has not closed yet then the bar opened yesterday and closes tomorrow
            if currentHour <= intCloseHour {
                return Calendar.current.date(from: DateComponents(year: Calendar.current.component(.year, from: Date()), month: Calendar.current.component(.month, from: Date()), day: Calendar.current.component(.day, from: Date()), hour: intCloseHour, minute: Int(todayScheduleDay.closingTime!.split(separator: ":")[1])))!
            }
            // if its in the AM and after the bar closed then the opening time is today and closing time is tomorrow
            else {
                return Calendar.current.date(from: DateComponents(year: Calendar.current.component(.year, from: Date().dayAfter), month: Calendar.current.component(.month, from: Date().dayAfter), day: Calendar.current.component(.day, from: Date().dayAfter), hour: intCloseHour, minute: Int(todayScheduleDay.closingTime!.split(separator: ":")[1])))!
            }
        }
        // if the bar opens and closes in the same day
        else {
            return Calendar.current.date(from: DateComponents(year: Calendar.current.component(.year, from: Date()), month: Calendar.current.component(.month, from: Date()), day: Calendar.current.component(.day, from: Date()), hour: intCloseHour, minute: Int(todayScheduleDay.closingTime!.split(separator: ":")[1])))!
        }
    }

    var isOpen: Bool {
        guard todayScheduleDay.isClosed != true else {
            // if the bar was open until the AM yesterday, it will be open today even though the bar is technically closed today
            guard yesterdayScheduleDay.isClosed != true else {
                return false
            }
            let intCloseHour = formatTimeString(timeString: yesterdayScheduleDay.closingTime)
            let yesterdayClosingDateTime = Calendar.current.date(from: DateComponents(year: Calendar.current.component(.year, from: Date()), month: Calendar.current.component(.month, from: Date()), day: Calendar.current.component(.day, from: Date()), hour: intCloseHour, minute: Int(yesterdayScheduleDay.closingTime!.split(separator: ":")[1])))!
            if Date() <= yesterdayClosingDateTime {
                return true
            } else {
                return false
            }
        }

        if Date() >= openingDateTime!, Date() <= closingDateTime! {
            return true
        } else {
            return false
        }
    }

    convenience init(businessData: BusinessData) {
        self.init(context: CoreDataManager.sharedManager.managedContext)
        id = businessData.id
        name = businessData.name
        address = businessData.address
        salesTaxRate = businessData.salesTaxRate
        serviceFeePercentage = businessData.serviceFeePercentage
        merchantStripeId = businessData.merchantStripeId
        atCapacity = businessData.atCapacity
        isActive = businessData.isActive
        deactivated = businessData.deactivated
        schedule = NSSet(array: businessData.schedule)
        for businessScheduleDay in scheduleArray {
            businessScheduleDay.business = self
        }
        drinks = NSSet()
        imageURL = businessData.imageURL
    }
}
