import UIKit

enum Section: String {
    case recentlyVisited = "Recently visited"
    case moreBars = "More bars"
    case nearbyBars = "Nearby bars"
    case main
    // in the future should add "popular bars" and have a query that aggregates the 5 most popular bars in a 10 mile radius
}

enum DrinkCategory: String, CaseIterable {
    case beer
    case cocktail
    case wine
    case shot
    case hardSeltzer = "hard seltzer"
}

class Item: Hashable {
    var image: UIImage!
    let identifier: UUID!
    static let sections = [Section.recentlyVisited, Section.nearbyBars, Section.recentlyVisited]
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }

    static func == (lhs: Item, rhs: Item) -> Bool {
        return lhs.identifier == rhs.identifier
    }

    init(image: UIImage) {
        self.image = image
        identifier = UUID()
    }
}

class DrinkItem: Item {
    let url: URL?
    let name: String
    let description: String
    let drinkId: UUID
    let quantity: Int16
    let price: Double
    let category: DrinkCategory
    init(orderDrink: OrderDrink? = nil, drink: Drink? = nil) {
        if let orderDrink = orderDrink {
            category = orderDrink.drink.drinkCategory
            quantity = orderDrink.quantity
            drinkId = orderDrink.drink.id
            name = orderDrink.drink.formattedName
            description = orderDrink.drink.detail
            price = orderDrink.drink.price
        } else {
            category = drink!.drinkCategory
            quantity = 1
            drinkId = drink!.id
            name = drink!.formattedName
            description = drink!.detail
            price = drink!.price
        }
        if let imageURL = drink?.imageURL ?? orderDrink?.drink.imageURL {
            url = URL(string: imageURL)
            super.init(image: UIImage(named: "logo")!)
        } else {
            url = nil
            super.init(image: UIImage())
        }
    }
}

class BusinessItem: Item {
    let url: URL?
    let name: String
    let atCapacity: Bool
    let isOpen: Bool
    let distance: Double
    let businessId: UUID
    let address: String
    init(business: Business, image _: UIImage? = nil) {
        name = business.formattedName
        atCapacity = business.atCapacity
        isOpen = business.isOpen
        businessId = business.id
        distance = business.distanceFromUser
        address = business.address
        if let imageURL = business.imageURL {
            url = URL(string: imageURL)
        } else {
            url = nil
        }
        super.init(image: ImageCache.publicCache.placeholderImage)
    }
}

class OrderItem: Item {
    let businessName: String!
    let orderDate: Date!
    let orderNumberofItems: Int!
    let orderTotal: Double!
    let orderStatus: String!
    let orderId: UUID!
    init(order: Order, business: Business) {
        businessName = business.formattedName
        orderDate = order.dateTime
        orderNumberofItems = order.numberOfItems
        orderTotal = order.total

        if order.completed == true {
            orderStatus = "completed"
        } else if order.active == true {
            orderStatus = "active"
        } else {
            orderStatus = "refunded"
        }
        orderId = order.id
        super.init(image: UIImage())
    }
}

class QuickPassItem: Item {
    let quickPassId: UUID!
    let businessId: UUID!
    let price: Double!
    let activationTime: Date!
    let active: Bool!
    let businessName: String
    let businessAddress: String
    let dateTime: Date!
    let expirationTime: Date!
    let shouldDisplayExpirationTime: Bool!
    init(quickPass: QuickPass, business: Business, image: UIImage? = nil) {
        quickPassId = quickPass.id
        price = quickPass.total
        activationTime = quickPass.activationTime
        businessId = quickPass.businessId
        active = quickPass.isActive
        businessName = business.formattedName
        businessAddress = business.address
        dateTime = quickPass.dateTime
        expirationTime = quickPass.expirationTime
        shouldDisplayExpirationTime = quickPass.shouldDisplayExpirationTime
        if let img = image {
            super.init(image: img)
        } else {
            super.init(image: UIImage())
        }
    }
}
