//
//  OrderDrink.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 12/21/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//
import CoreData

public class OrderDrink: NSManagedObject, Codable {
    enum CodingKeys: String, CodingKey {
        case id
        case drinkId = "drink_id"
        case orderId = "order_id"
        case quantity
        case price
    }

    public required convenience init(from decoder: Decoder) throws {
        self.init(context: CoreDataManager.sharedManager.managedContext)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(UUID.self, forKey: .id)
        drinkId = try container.decode(UUID.self, forKey: .drinkId)
        orderId = try container.decode(UUID.self, forKey: .orderId)
        quantity = try container.decode(Int16.self, forKey: .quantity)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(drinkId, forKey: .drinkId)
        try container.encode(orderId, forKey: .orderId)
        try container.encode(quantity, forKey: .quantity)

        // to prevent the backend from having to query the drink every time an order is placed
        try container.encode(drink.price, forKey: .price)
    }
}

public extension OrderDrink {
    var total: Double {
        return Double(quantity) * drink.price
    }

    convenience init(drink: Drink) {
        self.init(context: CoreDataManager.sharedManager.managedContext)
        id = UUID()
        drinkId = drink.id
        orderId = nil
        quantity = 0
    }

    internal convenience init(orderDrinkData: OrderDrinkData) {
        self.init(context: CoreDataManager.sharedManager.managedContext)
        id = orderDrinkData.id
        drinkId = orderDrinkData.drinkId
        orderId = orderDrinkData.orderId
        quantity = Int16(orderDrinkData.quantity)
    }
}
