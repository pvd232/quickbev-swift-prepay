//
//  OrderDrink+CoreDataProperties.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 12/21/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import CoreData
import Foundation

public extension OrderDrink {
    @nonobjc class func fetchRequest() -> NSFetchRequest<OrderDrink> {
        return NSFetchRequest<OrderDrink>(entityName: "OrderDrink")
    }

    @NSManaged var id: UUID
    @NSManaged var drinkId: UUID
    @NSManaged var orderId: UUID?
    @NSManaged var quantity: Int16
    @NSManaged var drink: Drink
    @NSManaged var order: Order?
    @NSManaged var checkoutCart: CheckoutCart?
}

extension OrderDrink: Identifiable {}
