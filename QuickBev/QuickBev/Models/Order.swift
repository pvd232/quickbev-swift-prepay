//
//  Order.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 4/16/20.
//  Copyright © 2020 Peter Vail Driscoll II. All rights reserved.
//
import CoreData
public class Order: NSManagedObject, Codable {
    enum CodingKeys: String, CodingKey {
        case id
        case merchantStripeId = "merchant_stripe_id"
        case businessId = "business_id"
        case userId = "customer_id"
        case userStripeId = "customer_stripe_id"
        case user = "customer"
        case orderDrink = "order_drink"
        case dateTime = "date_time"
        case salesTaxPercentage = "sales_tax_percentage"
        case tipPercentage = "tip_percentage"
        case serviceFeePercentage = "service_fee_percentage"
        case stripeFeePercentage = "stripe_fee_percentage"
        case subtotal
        case tipTotal = "tip_total"
        case serviceFeeTotal = "service_fee_total"
        case stripeApplicationFeeTotal = "stripe_application_fee_total"
        case salesTaxTotal = "sales_tax_total"
        case total
        case stripeFeeTotal = "stripe_fee_total"
        case netStripeApplicationFeeTotal = "net_stripe_application_fee_total"
        case netServiceFeeTotal = "net_service_fee_total"
        case paymentIntentId = "payment_intent_id"
        case cardInformation = "card_information"
        case completed
        case refunded
        case active
    }

    // currently this method is never called
    public required convenience init(from decoder: Decoder) throws {
        self.init(context: CoreDataManager.sharedManager.managedContext)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(UUID.self, forKey: .id)
        merchantStripeId = try container.decode(String.self, forKey: .merchantStripeId)
        businessId = try container.decode(UUID.self, forKey: .businessId)
        userId = try container.decode(String.self, forKey: .userId)
        salesTaxPercentage = try container.decode(Double.self, forKey: .salesTaxPercentage)
        tipPercentage = try container.decode(Double.self, forKey: .tipPercentage)
        serviceFeePercentage = try container.decode(Double.self, forKey: .serviceFeePercentage)
        stripeFeePercentage = try container.decode(Double.self, forKey: .stripeFeePercentage)

        subtotal = try container.decode(Double.self, forKey: .subtotal)
        tipTotal = try container.decode(Double.self, forKey: .tipTotal)
        serviceFeeTotal = try container.decode(Double.self, forKey: .serviceFeeTotal)
        stripeApplicationFeeTotal = try container.decode(Double.self, forKey: .stripeApplicationFeeTotal)
        salesTaxTotal = try container.decode(Double.self, forKey: .salesTaxTotal)
        total = try container.decode(Double.self, forKey: .total)
        stripeFeeTotal = try container.decode(Double.self, forKey: .salesTaxTotal)
        netStripeApplicationFeeTotal = try container.decode(Double.self, forKey: .netStripeApplicationFeeTotal)
        netServiceFeeTotal = try container.decode(Double.self, forKey: .netServiceFeeTotal)

        dateTime = try container.decode(Date.self, forKey: .dateTime)
        paymentIntentId = try container.decode(String.self, forKey: .paymentIntentId)
        cardInformation = try container.decode(String.self, forKey: .cardInformation)
        completed = try container.decode(Bool.self, forKey: .completed)
        refunded = try container.decode(Bool.self, forKey: .refunded)
        orderDrink = NSSet(array: try container.decode([OrderDrink].self, forKey: .orderDrink))
        hasBeenDeactivated = false

        // relationships
        user = CheckoutCart.shared.user!
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(merchantStripeId, forKey: .merchantStripeId)
        try container.encode(userStripeId, forKey: .userStripeId)
        try container.encode(businessId, forKey: .businessId)
        try container.encode(userId, forKey: .userId)

        try container.encode(salesTaxPercentage, forKey: .salesTaxPercentage)
        try container.encode(tipPercentage, forKey: .tipPercentage)
        try container.encode(serviceFeePercentage, forKey: .serviceFeePercentage)
        try container.encode(stripeFeePercentage, forKey: .stripeFeePercentage)

        try container.encode(subtotal, forKey: .subtotal)
        try container.encode(tipTotal, forKey: .tipTotal)
        try container.encode(serviceFeeTotal, forKey: .serviceFeeTotal)
        try container.encode(stripeApplicationFeeTotal, forKey: .stripeApplicationFeeTotal)
        try container.encode(salesTaxTotal, forKey: .salesTaxTotal)
        try container.encode(total, forKey: .total)
        try container.encode(stripeFeeTotal, forKey: .stripeFeeTotal)
        try container.encode(netStripeApplicationFeeTotal, forKey: .netStripeApplicationFeeTotal)
        try container.encode(netServiceFeeTotal, forKey: .netServiceFeeTotal)

        try container.encode(dateTime.timeIntervalSince1970, forKey: .dateTime)
        try container.encode(paymentIntentId, forKey: .paymentIntentId)
        try container.encode(cardInformation, forKey: .cardInformation)
        try container.encode(completed, forKey: .completed)
        try container.encode(refunded, forKey: .refunded)
        try container.encode(orderDrinkArray, forKey: .orderDrink)
        try container.encode(active, forKey: .active)
    }
}

extension Order {
    var orderDrinkArray: [OrderDrink] {
        let set = orderDrink as? Set<OrderDrink> ?? []
        return set.sorted {
            $0.quantity < $1.quantity
        }
    }

    var numberOfItems: Int {
        var numToReturn = 0
        let set = orderDrink as? Set<OrderDrink> ?? []
        let drinkArray = set.sorted {
            $0.quantity < $1.quantity
        }
        for item in drinkArray {
            numToReturn += Int(item.quantity)
        }
        return numToReturn
    }

    var stripeFeeAndServiceFeeTotal: Double {
        return stripeFeeTotal + serviceFeeTotal
    }

    var active: Bool {
        if completed == false, refunded == false {
            return true
        } else {
            return false
        }
    }

    func formatTimeString(timeString: String?) -> Int {
        let timeSubString = timeString!.split(separator: ":")[0]
        let firstCharacter = timeSubString[0]
        var intCharacter: Int?

        if firstCharacter == "0" {
            let secondCharacter = timeSubString[1]
            intCharacter = Int(String(secondCharacter))
        } else {
            intCharacter = Int(timeSubString)
        }
        return intCharacter!
    }

    var orderTime: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale

        let time = formatter.string(from: dateTime)
        let formattedFirstValue = formatTimeString(timeString: time)
        let timeSubString = time.split(separator: ":")
        let subTimeSubString = timeSubString.dropFirst(1)
        let newTimeSubString = subTimeSubString.joined()
        let timeFormatted = formattedFirstValue.description + ":" + newTimeSubString
        return timeFormatted
    }

    var orderDate: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, yyy"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        return formatter.string(from: dateTime)
    }

    var userStripeId: String {
        return user.stripeId
    }

    convenience init(checkoutCart: CheckoutCart) {
        self.init(context: CoreDataManager.sharedManager.managedContext)
        id = UUID()
        userId = checkoutCart.user!.email
        businessId = checkoutCart.userBusiness!.id
        merchantStripeId = checkoutCart.userBusiness!.merchantStripeId

        salesTaxPercentage = checkoutCart.userBusiness!.salesTaxRate
        tipPercentage = checkoutCart.tipPercentage
        serviceFeePercentage = 0.0
        stripeFeePercentage = 0.0

        subtotal = 0.0
        tipTotal = 0.0
        salesTaxTotal = 0.0
        stripeApplicationFeeTotal = 0.0
        serviceFeeTotal = 0.0
        total = 0.0
        stripeFeeTotal = 0.0
        netStripeApplicationFeeTotal = 0.0
        netServiceFeeTotal = 0.0

        dateTime = Date()
        paymentIntentId = ""
        cardInformation = ""
        completed = false
        refunded = false
        activeOrderCount = 0
        hasBeenDeactivated = false
        // relationships
        for orderDrinkObject in checkoutCart.cart {
            orderDrinkObject.orderId = id
        }
        orderDrink = NSSet(array: checkoutCart.cart)
        user = checkoutCart.user!
    }

    func update(orderData: OrderData) {
        id = orderData.id
        userId = orderData.userId
        businessId = orderData.businessId
        merchantStripeId = orderData.merchantStripeId
        salesTaxPercentage = orderData.salesTaxPercentage
        tipPercentage = orderData.tipPercentage
        serviceFeePercentage = orderData.serviceFeePercentage
        stripeFeePercentage = orderData.stripeFeePercentage
        subtotal = orderData.subtotal
        tipTotal = orderData.tipTotal
        salesTaxTotal = orderData.salesTaxTotal
        stripeApplicationFeeTotal = orderData.stripeApplicationFeeTotal
        serviceFeeTotal = orderData.serviceFeeTotal
        total = orderData.total
        stripeFeeTotal = orderData.stripeFeeTotal
        netStripeApplicationFeeTotal = orderData.netStripeApplicationFeeTotal
        netServiceFeeTotal = orderData.netServiceFeeTotal
        dateTime = orderData.dateTime
        paymentIntentId = orderData.paymentIntentId
        cardInformation = orderData.cardInformation
        completed = orderData.completed
        refunded = orderData.refunded
        activeOrderCount = orderData.activeOrderCount

        // relationships
        // no need to update orderdrink because there is no scenario where it will be modified by the backend
        // also no need to update hasBeenDeactivated and active becuase it is a localized property
    }
}
