//
//  Business+CoreDataProperties.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 1/29/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//
//

import CoreData
import Foundation

public extension Business {
    @nonobjc class func fetchRequest() -> NSFetchRequest<Business> {
        return NSFetchRequest<Business>(entityName: "Business")
    }

    @NSManaged var id: UUID
    @NSManaged var address: String
    @NSManaged var coordinateString: String
    @NSManaged var name: String
    @NSManaged var salesTaxRate: Double
    @NSManaged var serviceFeePercentage: Double
    @NSManaged var merchantStripeId: String
    @NSManaged var atCapacity: Bool
    @NSManaged var isActive: Bool
    @NSManaged var deactivated: Bool
    @NSManaged var schedule: NSSet
    @NSManaged var imageURL: String?

    // relationships
    @NSManaged var businessToCheckoutCart: CheckoutCart?
    @NSManaged var businessToUser: User?
    @NSManaged var businessToQuickPassData: QuickPassData?
    @NSManaged var drinks: NSSet?
}

// MARK: Generated accessors for drinks

public extension Business {
    @objc(addDrinksObject:)
    @NSManaged func addToDrinks(_ value: Drink)

    @objc(removeDrinksObject:)
    @NSManaged func removeFromDrinks(_ value: Drink)

    @objc(addDrinks:)
    @NSManaged func addToDrinks(_ values: NSSet)

    @objc(removeDrinks:)
    @NSManaged func removeFromDrinks(_ values: NSSet)
}

public extension Business {
    @objc(addScheduleObject:)
    @NSManaged func addToSchedule(_ value: Drink)

    @objc(removeScheduleObject:)
    @NSManaged func removeFromSchedule(_ value: Drink)

    @objc(addSchedule:)
    @NSManaged func addToSchedule(_ values: NSSet)

    @objc(removeSchedule:)
    @NSManaged func removeFromSchedule(_ values: NSSet)
}

extension Business: Identifiable {}
