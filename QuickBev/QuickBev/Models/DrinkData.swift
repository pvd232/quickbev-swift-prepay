//
//  DrinkData.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 12/20/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import Foundation
struct DrinkData: Codable {
    let id: UUID
    let name: String
    let category: String
    let detail: String
    let price: Double
    let businessId: UUID
    let quantity: Int16
    let imageURL: String
    let isActive: Bool

    enum CodingKeys: String, CodingKey {
        case drinkResponse = "drink"
        case id
        case name
        case category
        case detail = "description"
        case price
        case quantity
        case businessId = "business_id"
        case imageURL = "image_url"
        case isActive = "is_active"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let nestedContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .drinkResponse)
        id = try nestedContainer.decode(UUID.self, forKey: .id)
        name = try nestedContainer.decode(String.self, forKey: .name)
        category = try nestedContainer.decode(String.self, forKey: .category)
        detail = try nestedContainer.decode(String.self, forKey: .detail)
        price = try nestedContainer.decode(Double.self, forKey: .price)
        businessId = try nestedContainer.decode(UUID.self, forKey: .businessId)
        quantity = try nestedContainer.decode(Int16.self, forKey: .quantity)
        imageURL = try nestedContainer.decode(String.self, forKey: .imageURL)
        isActive = try nestedContainer.decode(Bool.self, forKey: .isActive)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        var nestedContainer = container.nestedContainer(keyedBy: CodingKeys.self, forKey: .drinkResponse)
        try nestedContainer.encode(id, forKey: .id)
        try nestedContainer.encode(name, forKey: .name)
        try nestedContainer.encode(category, forKey: .category)
        try nestedContainer.encode(detail, forKey: .detail)
        try nestedContainer.encode(price, forKey: .price)
        try nestedContainer.encode(businessId, forKey: .businessId)
        try nestedContainer.encode(quantity, forKey: .quantity)
        try nestedContainer.encode(imageURL, forKey: .imageURL)
        try nestedContainer.encode(isActive, forKey: .isActive)
    }
}
