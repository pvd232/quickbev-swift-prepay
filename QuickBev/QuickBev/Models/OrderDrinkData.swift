//
//  OrderDrinkData.swift
//  QuickBev
//
//  Created by Peter Driscoll on 1/13/22.
//  Copyright © 2022 Peter Vail Driscoll II. All rights reserved.
//

import Foundation

struct OrderDrinkData: Codable {
    let id: UUID
    let drinkId: UUID
    let orderId: UUID
    let quantity: Int
    enum CodingKeys: String, CodingKey {
        case id
        case drinkId = "drink_id"
        case orderId = "order_id"
        case quantity
        case price
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(UUID.self, forKey: .id)
        drinkId = try container.decode(UUID.self, forKey: .drinkId)
        orderId = try container.decode(UUID.self, forKey: .orderId)
        quantity = try container.decode(Int.self, forKey: .quantity)
    }

    init(orderDrink: OrderDrink, orderId: UUID) {
        id = orderDrink.id
        drinkId = orderDrink.drinkId
        self.orderId = orderId
        quantity = Int(orderDrink.quantity)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(drinkId, forKey: .drinkId)
        try container.encode(orderId, forKey: .orderId)
        try container.encode(quantity, forKey: .quantity)

        // to prevent the backend from having to query the drink every time an order is placed
        try container.encode(drink.price, forKey: .price)
    }
}

extension OrderDrinkData {
    var total: Double {
        return Double(quantity) * drink.price
    }

    var drink: Drink {
        let drink = CoreDataManager.sharedManager.fetchEntity(entityName: "Drink", propertyToFilter: "id", propertyValue: drinkId.uuidString) as! Drink
        return drink
    }
}
