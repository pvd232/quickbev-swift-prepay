import CoreData
import CoreLocation
import Foundation
import Stripe

public class CheckoutCart: NSManagedObject {
    private convenience init() {
        self.init(context: CoreDataManager.sharedManager.managedContext)
    }

    // stored property is computed with a closure one time only upon object initialization
    static var shared: CheckoutCart = {
        if let fetchedResults = CoreDataManager.sharedManager.fetchEntities(entityName: "CheckoutCart") {
            if !fetchedResults.isEmpty {
                return fetchedResults[0] as! CheckoutCart
            }
        }
        return CheckoutCart()
    }()

    // this has to be lazy otherwise it will initialize before the stripe api client initializes
    lazy var customerContext: STPCustomerContext = {
        STPCustomerContext(keyProvider: StripeAPIClient.shared)
    }()

    // computed variable will be initialized as a new instance each time it is called upon
    var businessETag: ETag {
        get {
            if let fetchedResults = CoreDataManager.sharedManager.fetchEntities(entityName: "ETag") as? [ETag], !fetchedResults.isEmpty {
                for fetchedResult in fetchedResults {
                    if fetchedResult.category == "business" {
                        return fetchedResult
                    }
                }
            }
            let newBusinessEtag = ETag(Id: -1, Category: "business")
            CoreDataManager.sharedManager.saveContext()
            return newBusinessEtag
        }
        set {}
    }

    var laCavernaLocation: CLLocation = {
        let lat = 40.72000100308587
        let lon = -73.98711772825237
        let location = CLLocation(latitude: lat, longitude: lon)
        return location
    }()

    var drinkETag: ETag {
        get {
            if let fetchedResults = CoreDataManager.sharedManager.fetchEntities(entityName: "ETag") as? [ETag], !fetchedResults.isEmpty {
                for fetchedResult in fetchedResults {
                    if fetchedResult.category == "drink" {
                        return fetchedResult
                    }
                }
            }
            let newDrinkEtag = ETag(Id: -1, Category: "drink")
            CoreDataManager.sharedManager.saveContext()
            return newDrinkEtag
        }
        set {}
    }

    var userBusiness: Business? {
        if let user = user {
            if let userBusiness = user.userToBusiness {
                return userBusiness
            }
        }
        return nil
    }

    var quickPassBusiness: Business? {
        if let quickPassBusiness = QuickPassData.shared.quickPassDataToBusiness {
            return quickPassBusiness
        }
        return nil
    }

    var didUpdateDeviceToken: String {
        get {
            if let update = try? SecureStore(secureStoreQueryable: GenericPasswordQueryable()).getValue(for: "didUpdateDeviceToken") {
                return update
            } else {
                return "no"
            }
        }
        set(newStatus) {
            try! SecureStore(secureStoreQueryable: GenericPasswordQueryable()).setValue(newStatus, for: "didUpdateDeviceToken")
        }
    }

    var shouldDisplayQuickPass: Bool {
        get {
            if let update = try? SecureStore(secureStoreQueryable: GenericPasswordQueryable()).getValue(for: "shouldDisplayQuickPass") {
                if update == "yes" {
                    return true
                }
            }
            return false
        }
        set(newStatus) {
            if newStatus == true {
                try! SecureStore(secureStoreQueryable: GenericPasswordQueryable()).setValue("yes", for: "shouldDisplayQuickPass")
            } else {
                try! SecureStore(secureStoreQueryable: GenericPasswordQueryable()).setValue("no", for: "shouldDisplayQuickPass")
            }
        }
    }
}

public extension CheckoutCart {
    var cart: [OrderDrink] {
        let set = orderDrink as? Set<OrderDrink> ?? []

        return set.sorted {
            $0.quantity < $1.quantity
        }
    }

    var orderDrinkCount: Int {
        let set = orderDrink as? Set<OrderDrink> ?? []
        var orderDrinkQuantity = 0
        let orderDrinkList = set.sorted {
            $0.quantity < $1.quantity
        }
        for orderDrink in orderDrinkList {
            orderDrinkQuantity += Int(orderDrink.quantity)
        }
        return orderDrinkQuantity
    }

    var activeBusinessArray: [Business] {
        let set = business as? Set<Business> ?? []
        let filteredSet = set.filter { business in
            business.deactivated == false && business.isActive == true
        }
        return filteredSet.sorted {
            $0.name < $1.name
        }
    }

    var businessList: [Business] {
        let set = business as? Set<Business> ?? []

        return set.sorted {
            $0.name < $1.name
        }
    }

    var userBusinessDrinks: [Drink] {
        let set = userBusiness!.drinks as? Set<Drink> ?? []

        return set.sorted {
            $0.name < $1.name
        }
    }

    var activeUserBusinessDrinks: [Drink] {
        let set = userBusiness!.drinks as? Set<Drink> ?? []
        let filteredSet = set.filter { drink in
            drink.isActive == true
        }
        return filteredSet.sorted {
            $0.name < $1.name
        }
    }

    var activeQuickPasses: [QuickPass] {
        var quickPassList: [QuickPass] = []
        if let quickPasses = CoreDataManager.sharedManager.fetchEntities(entityName: "QuickPass") as? [QuickPass] {
            for quickPass in quickPasses {
                if quickPass.isActive == true {
                    quickPassList.append(quickPass)
                }
            }
        }
        return quickPassList
    }

    var allQuickPasses: [QuickPass] {
        var quickPassList: [QuickPass] = []
        if let quickPasses = CoreDataManager.sharedManager.fetchEntities(entityName: "QuickPass") as? [QuickPass] {
            quickPassList = quickPasses
        }
        return quickPassList
    }

    var activeOrders: [Order] {
        var orderList: [Order] = []
        if let orders = CoreDataManager.sharedManager.fetchEntities(entityName: "Order") as? [Order] {
            for order in orders {
                if order.active == true {
                    orderList.append(order)
                }
            }
        }
        return orderList
    }

    var canPay: Bool {
        return !cart.isEmpty
    }

    var stripeFeeAndServiceFeeTotal: Double {
        return stripeFeeTotal + serviceFeeTotal
    }

    func calculateCost() {
        if canPay {
            total = 0
            subtotal = (cart.reduce(0) { currentCheckoutCartTotal, orderDrink -> Double in
                currentCheckoutCartTotal + orderDrink.total
            }).rounded(digits: 2)
            tipTotal = (tipPercentage * subtotal).rounded(digits: 2)
            let preServiceFeeTotal = tipTotal + subtotal
            serviceFeeTotal = userBusiness!.serviceFeePercentage * preServiceFeeTotal
            stripeFeeTotal = (stripeFeePercentage * total) + 0.3
            let preSalesTaxTotal = subtotal + serviceFeeTotal
            salesTaxTotal = (preSalesTaxTotal * userBusiness!.salesTaxRate).rounded(digits: 2)
            total = (subtotal + salesTaxTotal + tipTotal + serviceFeeTotal + stripeFeeTotal).rounded(digits: 2)
        }
    }

    func calculateQuickPassSalesTax() -> Double {
        let salesTax = QuickPassData.shared.salesTaxRate * QuickPassData.shared.price
        return salesTax
    }

    func calculateQuickPassTotal() -> Double {
        let salesTax = calculateQuickPassSalesTax()
        var total = 0.0
        total += QuickPassData.shared.price
        total += salesTax
        return total
    }

    var sessionToken: String {
        if let theToken = try? SecureStore(secureStoreQueryable: GenericPasswordQueryable()).getValue(for: "sessionToken") {
            return theToken
        } else {
            return ""
        }
    }

    var deviceToken: String {
        if let theToken = try? SecureStore(secureStoreQueryable: GenericPasswordQueryable()).getValue(for: "deviceToken") {
            return theToken
        } else {
            return ""
        }
    }

    func emptyCart() {
        total = 0.0
        orderDrink = NSSet(array: [OrderDrink]())
    }

    func resetCart() {
        emptyCart()
        user?.userToBusiness = nil
        user = nil
        currentOrder = nil
        CoreDataManager.sharedManager.saveContext()
    }

    func resetCartAndKeepUser() {
        emptyCart()
        user?.userToBusiness = nil
        currentOrder = nil
    }
}
