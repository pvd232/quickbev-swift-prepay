//
//  RootViewController.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 1/9/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//
import CoreData
import CoreLocation
import UIKit

class RootViewController: UIViewController {
    var current: UINavigationController!
    var navigationPathways: [UINavigationController] = []
    var businessData = [BusinessData]()
    var businesses = [Business]()
    var drinkData = [DrinkData]()
    var drinks = [Drink]()
    var didUpdateBusinesses: Bool = false
    var didUpdateDrinks: Bool = false
    @UsesAutoLayout var activityIndicator = UIActivityIndicatorView(style: .large)

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init() {
        _ = CheckoutCart.shared

        super.init(nibName: nil, bundle: nil)
        view.addSubview(activityIndicator)
        view.backgroundColor = .white
        activityIndicator.backgroundColor = UIColor(white: 0, alpha: 0.4)
        animateActivityIndicator()

        navigationPathways.append(TemplateNavigationController(rootViewController: SignUpOptionsViewController()))

        current = navigationPathways[0]

        current.view.isHidden = true
        setupCurrentViewController()

        // need to check if the user email and user password are empty strings, if they are then the user has attempted to create an account but have not completed the process

        if let user = CheckoutCart.shared.user, user.email != "", user.password != "" {
            getDrinksAndBusinesses {
                _ in
                self.switchToHomePageViewController()
            }
        } else {
            current.view.isHidden = false
        }
    }

    var homeViewController: HomeViewController? {
        for vc in current.viewControllers {
            if let controller = vc as? HomeViewController {
                return controller
            }
        }
        return nil
    }

    var orderConfirmationViewController: OrderConfirmationViewController? {
        for vc in current.viewControllers {
            if let controller = vc as? OrderConfirmationViewController {
                return controller
            }
        }
        return nil
    }

    var toolbarView: UIView {
        homeViewController!.bottomButtonsViewContainer
    }

    var currentLocation: CLLocation {
        let locationManager = CLLocationManager()
        var theCurrentLocation: CLLocation
        if let notNilLocation = locationManager.location {
            theCurrentLocation = notNilLocation
        } else {
            theCurrentLocation = CheckoutCart.shared.laCavernaLocation
        }
        return theCurrentLocation
    }

    func checkForExistingViewController(updatedOrder: Order, completion: @escaping ((Bool) -> Void)) {
        if current.viewControllers.last!.isModal == true {
            if let orderConfirmationNav = homeViewController?.presentedViewController as? UINavigationController, let orderConfirmationVC = orderConfirmationNav.viewControllers.last! as? OrderConfirmationViewController, orderConfirmationVC.selectedOrder.id == updatedOrder.id {
                orderConfirmationVC.updateOrderStatus(updatedOrder: updatedOrder)
                completion(true)
            } else {
                completion(false)
            }
        } else {
            completion(false)
        }
    }

    func checkForPresentedViewControllers(completion: @escaping ((Bool) -> Void)) {
        if current.viewControllers.last!.isModal == true {
            current.viewControllers.last!.dismiss(animated: true) {
                completion(false)
            }
        } else {
            completion(false)
        }
    }

    func presentOrderConfirmationViewController(updatedOrder: Order) {
        checkForExistingViewController(updatedOrder: updatedOrder) { status in
            if status == true, let orderConfirmationNav = self.homeViewController?.presentedViewController as? UINavigationController {
                if let confirmationViewController = orderConfirmationNav.viewControllers.last! as? OrderConfirmationViewController {
                    confirmationViewController.updateOrderStatus(updatedOrder: updatedOrder)
                }
            } else {
                self.checkForPresentedViewControllers {
                    _ in self.homeViewController?.presentOrdersViewController()
                    if let newOrderConfirmationNavController = self.current.viewControllers.last?.presentedViewController as? UINavigationController {
                        newOrderConfirmationNavController.pushViewController(OrderConfirmationViewController(SelectedOrder: updatedOrder, IsModal: false), animated: true)
                    }
                }
            }
        }
    }

    func setupCurrentViewController() {
        addChild(current)
        current.view.frame = view.bounds
        view.addSubview(current.view)
        current.didMove(toParent: self)
        view.bringSubviewToFront(activityIndicator)
    }

    func switchCurrentViewController(to new: UINavigationController) {
        current.willMove(toParent: self)
        addChild(new)
        current.view.frame = new.view.bounds
        view.addSubview(new.view)
        current.removeFromParent()
        current = new
        current.didMove(toParent: self)

        // viewWillAppear method was not being called when signing out and signing back in
        homeViewController?.viewWillAppear(true)
        view.bringSubviewToFront(activityIndicator)
    }

    func updateActivityIndicatorFrame() {
        activityIndicator.frame = view.bounds
        view.bringSubviewToFront(activityIndicator)
    }

    func animateActivityIndicator() {
        updateActivityIndicatorFrame()
        activityIndicator.startAnimating()
    }

    func stopAnimatingActivityIndicator() {
        activityIndicator.stopAnimating()
    }

    func switchToSplashPageViewController() {
        // reset splash page rather than create a new one each time
        navigationPathways.remove(at: 0)
        navigationPathways.insert(TemplateNavigationController(rootViewController: SignUpOptionsViewController()), at: 0)
        current = navigationPathways[0]
        setupCurrentViewController()
    }

    func switchToHomePageViewController() {
        if navigationPathways.count == 1 {
            navigationPathways.append(TemplateNavigationController(rootViewController: HomeViewController()))
            APIClient().updateDeviceToken { updateStatus in
                if updateStatus == true {
                    // if the update is performed successfully here then reset the didUpdate flag to false b/c this is the last time the update will be attempted and the value should be no when a new app lifecycle begins
                    CheckoutCart.shared.didUpdateDeviceToken = "no"
                }
            }
        }
        switchCurrentViewController(to: navigationPathways[1])
    }

    func switchToOrderPathway() {
        if !current.children.contains(where: { $0.isKind(of: CheckoutCartViewController.self) }), !current.children.contains(where: { $0.isKind(of: OrderConfirmationViewController.self) }) {
            current.pushViewController(CheckoutCartViewController(), animated: true)
        }
    }

    func getDrinksAndBusinesses(completion: @escaping (_ status: Bool?) -> Void) {
        animateActivityIndicator()
        let group = DispatchGroup()
        group.enter()
        if let fetchedBusinesses = CoreDataManager.sharedManager.fetchEntities(entityName: "Business") as? [Business], !fetchedBusinesses.isEmpty {
            APIClient().getBusinesses {
                businessesFromAPICall in
                guard businessesFromAPICall != nil else {
                    group.leave()
                    return
                }
                CheckoutCart.shared.resetCartAndKeepUser()
                CoreDataManager.sharedManager.saveContext()
                self.businessData = businessesFromAPICall!
                self.didUpdateBusinesses = true
                group.leave()
            }
        } else {
            APIClient().getBusinesses {
                businessesFromAPICall in
                guard businessesFromAPICall != nil else {
                    group.leave()
                    return
                }
                self.didUpdateBusinesses = true
                self.businessData = businessesFromAPICall!
                group.leave()
            }
        }
        group.enter()
        if let fetchedDrinks = CoreDataManager.sharedManager.fetchEntities(entityName: "Drink") as? [Drink], !fetchedDrinks.isEmpty {
            // even if there are drinks stored in core data, the drink list might have been updated, so we make the call to the backend with the If-None-Match header and if no drinks are returned then we set the page's drink list to be the still current drink list in core data. we don't set the fetch bool to be true because the relationships between the businesses and drinks in core data is still intact
            APIClient().getDrinks { drinksFromAPICall in
                guard drinksFromAPICall != nil else {
                    self.drinks = fetchedDrinks
                    group.leave()
                    return
                }
                CheckoutCart.shared.emptyCart()
                self.drinkData = drinksFromAPICall!
                self.didUpdateDrinks = true
                group.leave()
            }
        } else {
            APIClient().getDrinks { drinksFromAPICall in
                guard drinksFromAPICall != nil else {
                    group.leave()
                    return
                }
                self.didUpdateDrinks = true
                self.drinkData = drinksFromAPICall!
                group.leave()
            }
        }
        group.enter()
        APIClient().getShouldDisplayQuickPass {
            _ in
            group.leave()
        }

        group.notify(queue: .main, execute: {
            // if the drinks or businesses were fetched from an API call we must establish their relationships in core data and set them in the checkout cart
            for businessIndex in 0 ..< self.businessData.count {
                for drinkIndex in 0 ..< self.drinkData.count {
                    if self.drinkData[drinkIndex].businessId == self.businessData[businessIndex].id {
                        self.businessData[businessIndex].drinks.append(self.drinkData[drinkIndex])
                    }
                }
            }

            // if this is the first time the user is logging in
            if CheckoutCart.shared.businessList.isEmpty {
                for index in 0 ..< self.businessData.count {
                    var businessDrinks = [Drink]()
                    let newBusiness = Business(businessData: self.businessData[index])
                    CheckoutCart.shared.addToBusiness(newBusiness)
                    newBusiness.businessToCheckoutCart = CheckoutCart.shared
                    for drinkIndex in 0 ..< self.businessData[index].drinks.count {
                        let newDrink = Drink(drinkData: self.businessData[index].drinks[drinkIndex])
                        newDrink.drinkToBusiness = newBusiness
                        businessDrinks.append(newDrink)
                    }
                    newBusiness.drinks = NSSet(array: businessDrinks)
                }
            } else {
                if self.didUpdateDrinks == true, self.didUpdateBusinesses == true {
                    // a return user
                    let businesses = CoreDataManager.sharedManager.fetchEntities(entityName: "Business") as! [Business]
                    // update businesses
                    for index in 0 ..< self.businessData.count {
                        // if the business already exists then update it
                        if let matchingBusiness = businesses.first(where: {
                            $0.id == self.businessData[index].id
                        }) {
                            matchingBusiness.update(with: self.businessData[index])
                        }
                        // otherwise create a new business
                        else {
                            let newBusiness = Business(businessData: self.businessData[index])
                            CheckoutCart.shared.addToBusiness(newBusiness)
                            newBusiness.businessToCheckoutCart = CheckoutCart.shared
                            for drinkIndex in 0 ..< self.businessData[index].drinks.count {
                                let newDrink = Drink(drinkData: self.businessData[index].drinks[drinkIndex])
                                newDrink.drinkToBusiness = newBusiness
                                newBusiness.addToDrinks(newDrink)
                            }
                        }

                        // update drinks
                        let updatedBusinesses = CoreDataManager.sharedManager.fetchEntities(entityName: "Business") as! [Business]
                        for business in updatedBusinesses {
                            for drinkIndex in 0 ..< self.drinkData.count {
                                if self.drinkData[drinkIndex].businessId == business.id {
                                    if let matchingDrink = business.drinkArray.first(where: {
                                        $0.id == self.drinkData[drinkIndex].id
                                    }) {
                                        matchingDrink.update(with: self.drinkData[drinkIndex])
                                    }
                                    // if there is no corresponding drink then a new drink has been created by the business
                                    else {
                                        let newDrink = Drink(drinkData: self.drinkData[drinkIndex])
                                        newDrink.drinkToBusiness = business
                                        business.addToDrinks(newDrink)
                                    }
                                }
                            }
                        }
                    }
                } else if self.didUpdateDrinks == true, self.didUpdateBusinesses == false {
                    let businesses = CoreDataManager.sharedManager.fetchEntities(entityName: "Business") as! [Business]
                    for business in businesses {
                        for drinkIndex in 0 ..< self.drinkData.count {
                            if self.drinkData[drinkIndex].businessId == business.id {
                                if let matchingDrink = business.drinkArray.first(where: {
                                    $0.id == self.drinkData[drinkIndex].id
                                }) {
                                    matchingDrink.update(with: self.drinkData[drinkIndex])
                                }
                                // if there is no corresponding drink then a new drink has been created by the business
                                else {
                                    let newDrink = Drink(drinkData: self.drinkData[drinkIndex])
                                    newDrink.drinkToBusiness = business
                                    business.addToDrinks(newDrink)
                                }
                            }
                        }
                    }
                } else if self.didUpdateDrinks == false, self.didUpdateBusinesses == true {
                    let businesses = CoreDataManager.sharedManager.fetchEntities(entityName: "Business") as! [Business]
                    for index in 0 ..< self.businessData.count {
                        if let matchingBusiness = businesses.first(where: {
                            $0.id == self.businessData[index].id
                        }) {
                            matchingBusiness.update(with: self.businessData[index])
                        } else {
                            let newBusiness = Business(businessData: self.businessData[index])
                            newBusiness.businessToCheckoutCart = CheckoutCart.shared
                            CheckoutCart.shared.addToBusiness(newBusiness)
                        }
                    }
                }
            }
            CoreDataManager.sharedManager.saveContext()
            completion(true)
        })
    }

    func updateOrderStatus(completion: @escaping (() -> Void)) {
        APIClient().getOrderStatus { orderStatusList in
            if let orderStatusList = orderStatusList {
                for orderStatus in orderStatusList {
                    if let dbOrder = CoreDataManager.sharedManager.fetchEntity(entityName: "Order", propertyToFilter: "id", propertyValue: orderStatus.id.uuidString) as? Order {
                        dbOrder.completed = orderStatus.completed
                        dbOrder.refunded = orderStatus.refunded
                    }
                }
                DispatchQueue.main.async {
                    CoreDataManager.sharedManager.saveContext()
                    completion()
                }
            } else {
                completion()
            }
        }
    }

    private func animateFadeTransition(to new: UINavigationController, completion: (() -> Void)? = nil) {
        current.willMove(toParent: nil)
        addChild(new)

        transition(from: current, to: new, duration: 0.3, options: [.transitionCrossDissolve, .curveEaseOut], animations: {}) { _ in
            self.current.removeFromParent()
            new.didMove(toParent: self)
            self.current = new
            completion?() // 1
        }
    }
}
