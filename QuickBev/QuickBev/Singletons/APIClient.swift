//
//  APIClient.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 2/4/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//
import Foundation

struct APIClient {
    typealias APIClientCompletion = (APIResult<Data?>) -> Void

    private let session = URLSession.shared
//    private let baseURL = URL(string: "https://quickbev.us")!
    private let baseURL = URL(string: "http://192.168.1.71:5000")!
    func perform(_ request: APIRequest, _ completion: @escaping APIClientCompletion) {
        var urlComponents = URLComponents()
        urlComponents.scheme = baseURL.scheme
        if baseURL.port != nil {
            urlComponents.port = baseURL.port
        }
        urlComponents.host = baseURL.host
        urlComponents.path = baseURL.path
        urlComponents.queryItems = request.queryItems

        guard let url = urlComponents.url?.appendingPathComponent(request.path) else {
            completion(.failure(.invalidURL)); return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = request.method.rawValue
        urlRequest.httpBody = request.body
        if urlRequest.httpBody != nil {
            urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        }
        request.headers?.forEach { urlRequest.addValue($0.value, forHTTPHeaderField: $0.field) }

        let task = session.dataTask(with: urlRequest) { data, response, error in
            // this is where the APIResponse is declared using optional binding to downcast the type. the new variable httpResponse is declared and binded to the value of the response on the condition that it is of the type HTTPURLResponse
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(.failure(.requestFailed)); return
            }
            guard let _ = data,
                  error == nil
            else {
                debugPrint(error?.localizedDescription ?? "Response Error")
                return
            }
            completion(.success(APIResponse<Data?>(statusCode: httpResponse.statusCode, body: data, headers: httpResponse.allHeaderFields)))
        }
        task.resume()
    }

    func getBusinesses(completion: @escaping ([BusinessData]?) -> Void) {
        let jsonData = try! JSONEncoder().encode(CheckoutCart.shared.businessETag)
        let headerString = String(data: jsonData, encoding: .utf8)!
        let ifNoneMatchHeader: [HTTPHeader] = [HTTPHeader(field: "If-None-Match", value: headerString)]
        let request = try! APIRequest(method: .get, path: "/business/" + CheckoutCart.shared.sessionToken, headers: ifNoneMatchHeader)
        perform(request) { result in
            switch result {
            case let .success(response):
                if response.statusCode == 200, let response = try? response.decode(to: [String: [BusinessData]].self) {
                    if let etagId = response.headers["e-tag-id"] as? String, let etagCategory = response.headers["e-tag-category"] as? String {
                        let intEtagId = Int(etagId)!
                        let int64EtagId = Int64(intEtagId)
                        DispatchQueue.main.async {
                            CheckoutCart.shared.businessETag.setValue(int64EtagId, forKey: "id")
                            CheckoutCart.shared.businessETag.setValue(etagCategory, forKey: "category")
                        }
                    }
                    let businesses = response.body["businesses"]
                    completion(businesses)
                } else {
                    completion(nil)
                }
            case let .failure(error):
                completion(nil)
                print(error)
            }
        }
    }

    func getDrinks(completion: @escaping ([DrinkData]?) -> Void) {
        let jsonData = try! JSONEncoder().encode(CheckoutCart.shared.drinkETag)
        let headerString = String(data: jsonData, encoding: .utf8)!
        let ifNoneMatchHeader: [HTTPHeader] = [HTTPHeader(field: "If-None-Match", value: headerString)]
        let request = try! APIRequest(method: .get, path: "/drink/" + CheckoutCart.shared.sessionToken, headers: ifNoneMatchHeader)
        perform(request) { result in
            switch result {
            case let .success(response):
                if response.statusCode == 200, let response = try? response.decode(to: [String: [DrinkData]].self) {
                    if let etagId = response.headers["e-tag-id"] as? String, let etagCategory = response.headers["e-tag-category"] as? String {
                        let intEtagId = Int(etagId)!
                        let int64EtagId = Int64(intEtagId)
                        DispatchQueue.main.async {
                            CheckoutCart.shared.drinkETag.setValue(int64EtagId, forKey: "id")
                            CheckoutCart.shared.drinkETag.setValue(etagCategory, forKey: "category")
                        }
                    }
                    let drinks = response.body["drinks"]
                    completion(drinks)
                } else {
                    completion(nil)
                }
            case let .failure(error):
                completion(nil)
                print(error)
            }
        }
    }

    func getQuickPassQueue(businessId: UUID, completion: @escaping (QuickPassData?) -> Void) {
        let customerId = CheckoutCart.shared.user?.email ?? ""
        let requestHeaders: [HTTPHeader] = [HTTPHeader(field: "customer-id", value: customerId), HTTPHeader(field: "business-id", value: businessId.uuidString)]
        let request = try! APIRequest(method: .get, path: "/quick_pass/" + CheckoutCart.shared.sessionToken, headers: requestHeaders)
        perform(request) { result in
            switch result {
            case let .success(response):
                if response.statusCode == 200, let response = try? response.decode(to: [String: QuickPassData].self) {
                    if let quickPass = response.body["quick_pass"] {
                        completion(quickPass)
                    }
                } else {
                    completion(nil)
                }
            case let .failure(error):
                completion(nil)
                print(error)
            }
        }
    }

    func getOrderStatus(completion: @escaping ([OrderStatus]?) -> Void) {
        let customerId = CheckoutCart.shared.user?.email ?? "no-customer-id"
        let businessId = CheckoutCart.shared.userBusiness?.id.uuidString ?? "no-business-id"
        let requestHeaders: [HTTPHeader] = [HTTPHeader(field: "customer-id", value: customerId), HTTPHeader(field: "business-id", value: businessId)]
        let request = try! APIRequest(method: .get, path: "/customer/order/" + CheckoutCart.shared.sessionToken, headers: requestHeaders)

        perform(request) { result in
            switch result {
            case let .success(response):
                if response.statusCode == 200, let response = try? response.decode(to: [String: [OrderStatus]].self) {
                    if let orderStatus = response.body["orders"] {
                        completion(orderStatus)
                    }
                } else {
                    completion(nil)
                }
            case let .failure(error):
                completion(nil)
                print(error)
            }
        }
    }

    func checkUserAppleId(appleId: String, completion: @escaping (Bool?) -> Void) {
        let requestHeaders: [HTTPHeader] = [HTTPHeader(field: "apple-unique-identifier", value: appleId)]
        let request = try! APIRequest(method: .get, path: "/customer/validate/apple", headers: requestHeaders)
        // this is the apple ID registered with the user
        perform(request) { result in
            switch result {
            case let .success(response):
                if response.statusCode == 201, let response = try? response.decode(to: [String: UserData].self) {
                    if let userFromBackend = response.body["customer"] {
                        if let users = CoreDataManager.sharedManager.fetchEntities(entityName: "User") as? [User], users.count >= 1 {
                            for user in users {
                                if user.email == userFromBackend.email {
                                    DispatchQueue.main.async {
                                        user.appleId = appleId
                                        user.password = userFromBackend.password
                                        CheckoutCart.shared.user = user
                                        user.userToCheckoutCart = CheckoutCart.shared
                                        CheckoutCart.shared.stripeId = user.stripeId
                                        let sessionToken = response.headers["jwt-token"] as! String

                                        // set the token in keychain and use it whenever calling the backend
                                        try! SecureStore(secureStoreQueryable: GenericPasswordQueryable()).setValue(sessionToken, for: "sessionToken")
                                        CoreDataManager.sharedManager.saveContext()
                                    }
                                    completion(true)
                                    return
                                }
                            }

                            // if the user doesnt already exist then the function wont return and this block of code will get executed, thus not creating duplicative users in Core Data

                            DispatchQueue.main.async {
                                let newUser = User(userData: userFromBackend)
                                CheckoutCart.shared.user = newUser
                                newUser.userToCheckoutCart = CheckoutCart.shared
                                CheckoutCart.shared.stripeId = newUser.stripeId
                                CoreDataManager.sharedManager.saveContext()
                            }
                            completion(true)
                            return
                        }
                    }
                }
                // server error
                else {
                    completion(false)
                }
            // network error
            case let .failure(error):
                completion(nil)
                print(error)
            }
        }
    }

    func setAppleId(userId: String, appleId: String, completion: @escaping (Bool) -> Void) {
        let requestHeaders: [HTTPHeader] = [HTTPHeader(field: "apple-id", value: appleId), HTTPHeader(field: "user-id", value: userId)]

        let request = try! APIRequest(method: .post, path: "/customer/apple", headers: requestHeaders)
        perform(request) { result in
            switch result {
            case .success:
                completion(true)
                return

            // network error
            case let .failure(error):
                print("error \(error)")
                completion(false)
                return
            }
        }
    }

    func updateDeviceToken(completion: @escaping ((Bool) -> Void)) {
        // update device token is only called twice, once during application startup and once during initial rendering of the home page, thus it cannot be called twice because if it initially succeeds during app startup then the boolean will equal yes, upon which it will not run the second time
        // if the update works the second time, then the boolean wont be reset but i have accounted for this by adding a completion
        if CheckoutCart.shared.didUpdateDeviceToken != "yes" {
            if let user = CheckoutCart.shared.user {
                let deviceTokenHeader = HTTPHeader(field: "device-token", value: CheckoutCart.shared.deviceToken)
                let customerIdHeader = HTTPHeader(field: "customer-id", value: user.email)
                let updateDeviceTokenRequest = try! APIRequest(method: .get, path: "/customer/device_token/\(CheckoutCart.shared.sessionToken)", headers: [deviceTokenHeader, customerIdHeader])
                perform(updateDeviceTokenRequest) {
                    result in switch result {
                    case .success:
                        completion(true)
                    case let .failure(error):
                        print("failure to update Device token \(error.localizedDescription)")
                        completion(false)
                    }
                }
            }
        } else {
            CheckoutCart.shared.didUpdateDeviceToken = "no"
        }
    }

    func getShouldDisplayQuickPass(completion: @escaping ((Bool) -> Void)) {
        // update device token is only called twice, once during application startup and once during initial rendering of the home page, thus it cannot be called twice because if it initially succeeds during app startup then the boolean will equal yes, upon which it will not run the second time
        // if the update works the second time, then the boolean wont be reset but i have accounted for this by adding a completion
        let shouldDisplayQuickPassRequest = APIRequest(method: .get, path: "/quick_pass/display/\(CheckoutCart.shared.sessionToken)")
        perform(shouldDisplayQuickPassRequest) { result in
            switch result {
            case let .success(response):
                let data = ((try? JSONSerialization.jsonObject(with: response.body!, options: []) as? [String: Any]) as [String: Any]??)
                if let json = data as? NSDictionary {
                    if let shouldDisplayQuickPass = json["status"] as? Bool {
                        CheckoutCart.shared.shouldDisplayQuickPass = shouldDisplayQuickPass
                        completion(shouldDisplayQuickPass)
                        return
                    }
                }
            case let .failure(error):
                print("failure to update Device token \(error.localizedDescription)")
                completion(false)
                return
            }
        }
    }
}

enum HTTPMethod: String {
    case get = "GET"
    case put = "PUT"
    case post = "POST"
    case delete = "DELETE"
    case head = "HEAD"
    case options = "OPTIONS"
    case trace = "TRACE"
    case connect = "CONNECT"
}

struct HTTPHeader {
    let field: String
    let value: String
}

class APIRequest {
    let method: HTTPMethod
    let path: String
    var queryItems: [URLQueryItem]?
    var headers: [HTTPHeader]?
    var body: Data?

    init(method: HTTPMethod, path: String) {
        self.method = method
        self.path = path
    }

    init<Body: Encodable>(method: HTTPMethod, path: String, body: Body) throws {
        self.method = method
        self.path = path
        self.body = try JSONEncoder().encode(body)
    }

    init<Body: Encodable>(method: HTTPMethod, path: String, body: Body, headers: [HTTPHeader]) throws {
        self.method = method
        self.path = path
        self.headers = headers
        self.body = try JSONEncoder().encode(body)
    }

    init(method: HTTPMethod, path: String, headers: [HTTPHeader]) throws {
        self.method = method
        self.path = path
        self.headers = headers
    }
}

struct APIResponse<Body> {
    let statusCode: Int
    let body: Body
    let headers: [AnyHashable: Any]
}

extension APIResponse where Body == Data? {
    func decode<BodyType: Decodable>(to _: BodyType.Type) throws -> APIResponse<BodyType> {
        guard let data = body else {
            throw APIError.decodingFailure
        }
        let jsonDecoder = JSONDecoder()
        jsonDecoder.dateDecodingStrategy = .secondsSince1970
        let decodedJSON = try jsonDecoder.decode(BodyType.self, from: data)
        return APIResponse<BodyType>(statusCode: statusCode,
                                     body: decodedJSON, headers: headers)
    }
}

enum APIError: Error {
    case invalidURL
    case requestFailed
    case decodingFailure
    case resourceNotFound
}

enum APIResult<Body> {
    case success(APIResponse<Body>)
    case failure(APIError)
}
