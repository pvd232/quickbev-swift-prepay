//
//  StripeAPIClient.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 12/21/20.
//  Copyright © 2020 Peter Vail Driscoll II. All rights reserved.
//

import Foundation
import PassKit
import Stripe

enum StripeAPIError: Error {
    case unknown
    case staleData
    var localizedDescription: String {
        switch self {
        case .unknown:
            return "Unknown error"
        case .staleData:
            return "Stale data"
        }
    }
}

final class StripeAPIClient: NSObject, STPCustomerEphemeralKeyProvider {
    static let shared = StripeAPIClient()

    override private init() {
        // private
    }

//    private let baseURLString = "https://quickbev.us"

    private let baseURLString = "http://192.168.1.71:5000"

    private lazy var baseURL: URL = {
        guard let url = URL(string: baseURLString) else {
            fatalError("Invalid URL")
        }
        return url
    }()

    func createPaymentIntent(order: Order?, quickPassData: QuickPassData?, completion: @escaping ((Result<String, Error>) -> Void)) {
        if quickPassData == nil, order != nil {
            let url = baseURL.appendingPathComponent("/customer/stripe/payment_intent/" + CheckoutCart.shared.sessionToken)
            var params: [String: Order] = [:]
            let encoder = JSONEncoder()

            params["order"] = order
            let encodedParams = try! encoder.encode(params)

            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = encodedParams
            let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
                guard let response = response as? HTTPURLResponse,
                      response.statusCode == 200,
                      let data = data,
                      let json = ((try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]) as [String: Any]??),
                      let secret = json?["secret"] as? String, let paymentIntentId = json?["payment_intent_id"] as? String

                else {
                    if let response = response as? HTTPURLResponse {
                        if response.statusCode == 409 {
                            completion(.failure(StripeAPIError.staleData))
                            return
                        }
                    }
                    completion(.failure(error ?? StripeAPIError.unknown))
                    return
                }
                CheckoutCart.shared.currentOrder!.paymentIntentId = paymentIntentId
                CoreDataManager.sharedManager.saveContext()
                completion(.success(secret))
            })
            task.resume()
        } else if quickPassData != nil, order == nil {
            let url = baseURL.appendingPathComponent("quick_pass/payment_intent/" + CheckoutCart.shared.sessionToken)
            var params: [String: QuickPassData] = [:]
            let encoder = JSONEncoder()

            params["quick_pass"] = quickPassData
            let encodedParams = try! encoder.encode(params)

            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = encodedParams
            let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
                guard let response = response as? HTTPURLResponse,
                      response.statusCode == 200,
                      let data = data,
                      let json = ((try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]) as [String: Any]??),
                      let secret = json?["secret"] as? String, let paymentIntentId = json?["payment_intent_id"] as? String
                else {
                    completion(.failure(error ?? StripeAPIError.unknown))
                    return
                }
                QuickPassData.shared.paymentIntentId = paymentIntentId
                CoreDataManager.sharedManager.saveContext()
                completion(.success(secret))
            })
            task.resume()
        }
    }

    func createCustomerKey(withAPIVersion apiVersion: String, completion: @escaping STPJSONResponseCompletionBlock) {
        let url = baseURL.appendingPathComponent("customer/stripe/ephemeral_key/" + CheckoutCart.shared.sessionToken)
        let customerStripeId: String? = CheckoutCart.shared.user?.stripeId
        let params: [String: Any] = [
            "api_version": apiVersion,
            "stripe_id": String(describing: customerStripeId!),
        ]
        let jsonData = try? JSONSerialization.data(withJSONObject: params)

        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            guard let response = response as? HTTPURLResponse,
                  response.statusCode == 200,
                  let data = data,
                  let json = ((try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]) as [String: Any]??)
            else {
                if error == nil {
                    completion(nil, StripeAPIError.unknown)
                } else {
                    completion(nil, error)
                }
                return
            }
            completion(json, nil)
        })
        task.resume()
    }
}
