//
//  CheckoutCart+CoreDataProperties.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 2/2/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//
//

import CoreData
import Foundation

public extension CheckoutCart {
    @nonobjc class func fetchRequest() -> NSFetchRequest<CheckoutCart> {
        return NSFetchRequest<CheckoutCart>(entityName: "CheckoutCart")
    }

    @NSManaged var total: Double
    @NSManaged var currentOrderId: UUID?
    @NSManaged var salesTaxTotal: Double
    @NSManaged var stripeId: String?
    @NSManaged var subtotal: Double
    @NSManaged var tipTotal: Double
    @NSManaged var stripeFeeTotal: Double
    @NSManaged var stripeFeePercentage: Double
    @NSManaged var serviceFeeTotal: Double
    @NSManaged var tipPercentage: Double
    // relationships
    @NSManaged var business: NSSet?
    @NSManaged var orderDrink: NSSet
    @NSManaged var user: User?
    @NSManaged var currentOrder: Order?
}

// MARK: Generated accessors for business

public extension CheckoutCart {
    @objc(addBusinessObject:)
    @NSManaged func addToBusiness(_ value: Business)

    @objc(removeBusinessObject:)
    @NSManaged func removeFromBusiness(_ value: Business)

    @objc(addBusiness:)
    @NSManaged func addToBusiness(_ values: NSSet)

    @objc(removeBusiness:)
    @NSManaged func removeFromBusiness(_ values: NSSet)
}

// MARK: Generated accessors for OrderDrink

public extension CheckoutCart {
    @objc(addOrderDrinkObject:)
    @NSManaged func addToOrderDrink(_ value: OrderDrink)

    @objc(removeOrderDrinkObject:)
    @NSManaged func removeFromOrderDrink(_ value: OrderDrink)

    @objc(addOrderDrink:)
    @NSManaged func addToOrderDrink(_ values: NSSet)

    @objc(removeOrderDrink:)
    @NSManaged func removeFromOrderDrink(_ values: NSSet)
}

extension CheckoutCart: Identifiable {}
