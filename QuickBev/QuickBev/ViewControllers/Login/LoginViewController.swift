//
//  LoginViewController.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 6/1/20.
//  Copyright © 2020 Peter Vail Driscoll II. All rights reserved.
//

import CoreData
import Stripe
import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    private let signInButtonsStackView = UIStackView(theme: Theme.UIView(props: []))
    private let letsGetStartedStackView = UIStackView(theme: Theme.UIView(props: []))
    @UsesAutoLayout private var newActivityIndicator = UIActivityIndicatorView(style: .large)

    private let letsGetStartedLabel: UILabel
        = {
            let label = UILabel(theme: Theme.UILabel(props: [.textColor]))
            label.font = UIFont.largeThemeLabelFont
            return label
        }()

    private let logoImageView = UIImageView(theme: Theme.UIView(props: []))
    private let emailAddressLabel = UILabel(theme: Theme.UILabel(props: [.textColor]))
    private let emailTextField = RoundedUITextField(theme: Theme.UITextField(props: [.font(nil), .placeHolderText("email"), .autocapitalizationType(autocapitalizationType: .none), .borderStyle(borderStyle: .roundedRect), .backgroundColor(UIColor.clear)]))
    private let passwordLabel = UILabel(theme: Theme.UILabel(props: [.textColor]))
    private let passwordTextField = RoundedUITextField(theme: Theme.UITextField(props: [.font(nil), .placeHolderText("password"), .autocapitalizationType(autocapitalizationType: .none), .borderStyle(borderStyle: .roundedRect), .backgroundColor(UIColor.clear)]))
    private let forgotPasswordButton = UIButton(theme: Theme.UIView(props: []))
    private let submitButton = RoundButton(theme: Theme.UIView(props: []))

    private let logoImage = UIImage(named: "charterRomanPurpleLogo-30")

    private var formValue: [String: String] = [:]
    init() {
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(logoImageView)
        view.addSubview(letsGetStartedStackView)
        view.addSubview(signInButtonsStackView)
        view.addSubview(newActivityIndicator)

        letsGetStartedLabel.text = "Sign in"
        letsGetStartedLabel.textAlignment = .center
        letsGetStartedLabel.textColor = .black

        emailAddressLabel.text = "Email Address"
        emailAddressLabel.font = UIFont.themeLabelFont
        emailAddressLabel.textColor = .black

        passwordLabel.text = "Password"
        passwordLabel.textColor = .black
        passwordLabel.font = UIFont.themeLabelFont
        passwordTextField.isSecureTextEntry = true

        forgotPasswordButton.setTitle("Forgot Password?", for: .normal)
        forgotPasswordButton.titleLabel?.font = UIFont(name: "GillSans", size: getFontRatio(fontSize: 18.0))
        forgotPasswordButton.setTitleColor(.black, for: .normal)
        forgotPasswordButton.contentHorizontalAlignment = .center

        logoImageView.image = logoImage

        letsGetStartedStackView.axis = .vertical
        letsGetStartedStackView.addArrangedSubview(letsGetStartedLabel)
        letsGetStartedStackView.spacing = CGFloat(getHeightRatio(verticalConstant: 3.0))

        signInButtonsStackView.axis = .vertical
        signInButtonsStackView.addArrangedSubview(emailAddressLabel)
        signInButtonsStackView.addArrangedSubview(emailTextField)
        signInButtonsStackView.addArrangedSubview(passwordLabel)
        signInButtonsStackView.addArrangedSubview(passwordTextField)
        signInButtonsStackView.addArrangedSubview(forgotPasswordButton)
        signInButtonsStackView.addArrangedSubview(submitButton)
        signInButtonsStackView.spacing = CGFloat(getHeightRatio(verticalConstant: 2.0))

        submitButton.refreshTitle(newTitle: "Submit")
        submitButton.madeStandard()

        emailTextField.delegate = self
        passwordTextField.delegate = self

        newActivityIndicator.backgroundColor = UIColor(white: 0, alpha: 0.4)
        newActivityIndicator.frame = view.bounds

        let safeArea = view.safeAreaLayoutGuide

        NSLayoutConstraint.activate([
            logoImageView.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: CGFloat(0.5)),
            logoImageView.heightAnchor.constraint(equalTo: logoImageView.widthAnchor),
            logoImageView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            logoImageView.bottomAnchor.constraint(lessThanOrEqualTo: letsGetStartedStackView.topAnchor, constant: getHeightRatio(verticalConstant: -10.0)),
            logoImageView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: getHeightRatio(verticalConstant: 40.0)),

            letsGetStartedStackView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            letsGetStartedStackView.bottomAnchor.constraint(lessThanOrEqualTo: signInButtonsStackView.topAnchor, constant: -(0.095 * UIViewController.screenSize.height)),
            letsGetStartedStackView.heightAnchor.constraint(greaterThanOrEqualTo: safeArea.heightAnchor, multiplier: 0.12),

            signInButtonsStackView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            signInButtonsStackView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -10.0)),
            signInButtonsStackView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            signInButtonsStackView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: getHeightRatio(verticalConstant: -10.0)),
            signInButtonsStackView.heightAnchor.constraint(greaterThanOrEqualTo: safeArea.heightAnchor, multiplier: 0.28),
            signInButtonsStackView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),

            newActivityIndicator.topAnchor.constraint(equalTo: safeArea.topAnchor),
            newActivityIndicator.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            newActivityIndicator.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            newActivityIndicator.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),

            forgotPasswordButton.heightAnchor.constraint(equalTo: safeArea.heightAnchor, multiplier: 0.07),
            emailTextField.widthAnchor.constraint(equalTo: emailTextField.heightAnchor, multiplier: 197 / 25),
            passwordTextField.widthAnchor.constraint(equalTo: passwordTextField.heightAnchor, multiplier: 197 / 25),
            submitButton.widthAnchor.constraint(equalTo: submitButton.heightAnchor, multiplier: 197 / 25),
        ])
        submitButton.addTarget(self, action: #selector(submitButtonPressed), for: .touchUpInside)
        forgotPasswordButton.addTarget(self, action: #selector(forgotPasswordButtonPressed), for: .touchUpInside)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.placeholder == "email" {
            formValue["email"] = textField.text!
        } else if textField.placeholder == "password" {
            formValue["password"] = textField.text!
        }
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.placeholder == "email" {
            formValue["email"] = textField.text!
        } else if textField.placeholder == "password" {
            formValue["password"] = textField.text!
        }
    }

    @objc private func forgotPasswordButtonPressed(_: RoundButton) {
        navigationController!.pushViewController(PasswordResetViewController(), animated: true)
    }

    @objc private func submitButtonPressed(_: RoundButton) {
        newActivityIndicator.startAnimating()

        let userCredentials: [HTTPHeader] = [HTTPHeader(field: "email", value: emailTextField.text!), HTTPHeader(field: "password", value: passwordTextField.text!)]
        let request = try! APIRequest(method: .get, path: "/login", headers: userCredentials)

        APIClient().perform(request) { result in
            switch result {
            case let .success(response):
                if response.statusCode == 201 {
                    let data = ((try? JSONSerialization.jsonObject(with: response.body!, options: []) as? [String: Any]) as [String: Any]??)
                    if let json = data!!["customer"] as? NSDictionary {
                        if let users = CoreDataManager.sharedManager.fetchEntities(entityName: "User") as? [User], users.count >= 1 {
                            for user in users {
                                if user.email == json["id"] as! String {
                                    DispatchQueue.main.async {
                                        self.newActivityIndicator.stopAnimating()

                                        self.emailTextField.text = ""
                                        self.passwordTextField.text = ""

                                        user.firstName = json["first_name"] as! String
                                        user.lastName = json["last_name"] as! String
                                        user.password = json["password"] as! String
                                        user.stripeId = json["stripe_id"] as! String
                                        user.appleId = json["apple_id"] as? String ?? ""
                                        user.emailVerified = json["email_verified"] as! Bool
                                        user.dateTime = Date(timeIntervalSince1970: json["date_time"] as! Double)

                                        CheckoutCart.shared.user = user
                                        user.userToCheckoutCart = CheckoutCart.shared
                                        CheckoutCart.shared.stripeId = user.stripeId

                                        let sessionToken = response.headers["jwt-token"] as! String
                                        try! SecureStore(secureStoreQueryable: GenericPasswordQueryable()).setValue(sessionToken, for: "sessionToken")
                                        CoreDataManager.sharedManager.saveContext()
                                        SceneDelegate.shared.rootViewController.switchToHomePageViewController()
                                    }
                                    return
                                }
                            }
                        }
                        if let response = try? response.decode(to: [String: User].self) {
                            if let verifiedUser = response.body["customer"] {
                                DispatchQueue.main.async {
                                    CheckoutCart.shared.user = verifiedUser
                                    verifiedUser.userToCheckoutCart = CheckoutCart.shared
                                    CheckoutCart.shared.stripeId = verifiedUser.stripeId
                                    CoreDataManager.sharedManager.saveContext()
                                    SceneDelegate.shared.rootViewController.switchToHomePageViewController()
                                }
                            }
                        }
                    }
                } else if response.statusCode == 404 {
                    DispatchQueue.main.async {
                        self.newActivityIndicator.stopAnimating()
                        self.presentAlertController(title: "Incorrect Username", message: "Your username or password was incorrect. Please try again.")
                    }
                }
                // server error
                else {
                    DispatchQueue.main.async {
                        self.presentServerError()
                    }
                }

            // if the result is failure then the API was not able to be executed, indicating a local network error
            case let .failure(error): // if the API fails the enum API result will be of the type failure
                DispatchQueue.main.async {
                    self.newActivityIndicator.stopAnimating()
                    self.presentNetworkError()
                    print("error case .failure", error)
                }
            }
        }
    }
}
