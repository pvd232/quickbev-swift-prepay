//
//  emailResetViewController.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 2/28/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
class PasswordResetViewController: UIViewController, UITextFieldDelegate {
    private let emailTextField = RoundedUITextField(theme: Theme.UITextField(props: [.font(nil), .placeHolderText("Your email"), .autocapitalizationType(autocapitalizationType: .none), .borderStyle(borderStyle: .roundedRect), .backgroundColor(UIColor.clear)]))
    private let emailLabel = UILabel(theme: Theme.UILabel(props: [.text("Email"), .font(nil), .textColor]))
    private let registrationStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 10.0)))]))
    private let submitButton = RoundButton(theme: Theme.RoundButton(props: [.color, .text("Submit"), .titleLabelFont(nil)]))
    private let logoImageView = UIImageView(theme: Theme.UIView(props: []))
    private let logoImage = UIImage(named: "charterRomanPurpleLogo-30")
    private let centerStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 10.0)))]))
    private let centerLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Reset your password"), .center, .font(UIFont.largeThemeLabelFont)]))

    @UsesAutoLayout var newActivityIndicator = UIActivityIndicatorView(style: .large)

    private var formValues: [String: String] = [:]
    private lazy var navController: UINavigationController = {
        SceneDelegate.shared.rootViewController.current
    }()

    init() {
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewWillAppear(_: Bool) {
        navigationController?.isHiddenHairline = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(logoImageView)
        view.addSubview(registrationStackView)
        view.addSubview(centerStackView)
        view.addSubview(submitButton)
        view.addSubview(newActivityIndicator)
        submitButton.madeStandard()

        logoImageView.image = logoImage

        registrationStackView.addArrangedSubview(emailLabel)
        registrationStackView.addArrangedSubview(emailTextField)

        centerStackView.addArrangedSubview(centerLabel)

        emailTextField.delegate = self

        newActivityIndicator.backgroundColor = UIColor(white: 0, alpha: 0.4)
        newActivityIndicator.frame = view.bounds

        let safeArea = view.safeAreaLayoutGuide

        NSLayoutConstraint.activate([
            logoImageView.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: CGFloat(0.5)),
            logoImageView.heightAnchor.constraint(equalTo: logoImageView.widthAnchor),
            logoImageView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            logoImageView.bottomAnchor.constraint(lessThanOrEqualTo: centerStackView.topAnchor, constant: getHeightRatio(verticalConstant: -10.0)),
            logoImageView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: getHeightRatio(verticalConstant: 40.0)),

            registrationStackView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -10.0)),
            registrationStackView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            registrationStackView.bottomAnchor.constraint(equalTo: submitButton.topAnchor, constant: getHeightRatio(verticalConstant: -20.0)),

            submitButton.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: getHeightRatio(verticalConstant: -10.0)),
            submitButton.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            submitButton.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -10.0)),
            submitButton.widthAnchor.constraint(equalTo: submitButton.heightAnchor, multiplier: 197 / 25),
            submitButton.topAnchor.constraint(lessThanOrEqualTo: registrationStackView.bottomAnchor, constant: UIViewController.screenSize.height * 0.31),

            centerStackView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            centerStackView.bottomAnchor.constraint(lessThanOrEqualTo: registrationStackView.topAnchor, constant: -(0.21 * UIViewController.screenSize.height)),
            centerStackView.heightAnchor.constraint(greaterThanOrEqualTo: safeArea.heightAnchor, multiplier: 0.12),

            emailTextField.widthAnchor.constraint(equalTo: emailTextField.heightAnchor, multiplier: 197 / 25),

            newActivityIndicator.topAnchor.constraint(equalTo: view.topAnchor),
            newActivityIndicator.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            newActivityIndicator.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            newActivityIndicator.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),

        ])

        submitButton.addTarget(self, action: #selector(submitRegistration), for: .touchUpInside)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.placeholder == "Your email" {
            formValues["email"] = textField.text
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.placeholder == "Your email" {
            formValues["email"] = textField.text
        }
        return true
    }

    func checkFormValues() -> Bool {
        if formValues["email"] == "" || formValues["email"] == nil {
            return false
        } else if formValues["email"]?.contains("@") != true || formValues["email"]?.contains(".") != true
        {
            return false
        } else {
            return true
        }
    }

    func presentAlert() {
        var title = ""
        var message = ""
        if formValues["email"] == "" || formValues["email"] == nil {
            title = "Missing information"
            message = "Some information is blank. Please correct this and resubmit."
        } else if formValues["email"]?.contains("@") != true || formValues["email"]?.contains(".") != true {
            title = "Invalid email"
            message = "Your email is in an invalid format. Please correct it and resubmit."
        }
        DispatchQueue.main.async {
            self.newActivityIndicator.stopAnimating()
            self.presentAlertController(title: title, message: message)
        }
    }

    @objc private func submitRegistration(_: RoundButton) {
        newActivityIndicator.startAnimating()

        if checkFormValues() == false {
            presentAlert()
            return
        }

        let entityTypeHeader: [HTTPHeader] = [HTTPHeader(field: "entity", value: "customer")]
        let request = try! APIRequest(method: .get, path: "/password/reset/\(formValues["email"]!)", headers: entityTypeHeader)
        APIClient().perform(request) { result in
            switch result {
            case .success:
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                    self.newActivityIndicator.stopAnimating()
                    self.alert(title: "Password reset email sent", message: "If that email was valid, we sent you an email to reset your password!")
                }

            case let .failure(error):
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                    self.newActivityIndicator.stopAnimating()
                    self.alert(title: "Network error", message: "Oops, something happened. Check your internet and try again.")
                }
                print("error reset pwd", error)
            }
        }
    }

    private func alert(title: String, message: String) {
        let alertCtrl = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertCtrl.addAction(UIAlertAction(title: "Okay", style: .cancel) { _ in
            self.newActivityIndicator.stopAnimating()
            self.dismiss(animated: true)
        })
        present(alertCtrl, animated: true, completion: nil)
    }
}
