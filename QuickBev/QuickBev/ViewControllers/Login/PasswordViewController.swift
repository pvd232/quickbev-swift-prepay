//
//  PasswordViewController.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 11/21/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
class PasswordViewController: UIViewController, UITextFieldDelegate {
    private let passwordTextField = RoundedUITextField(theme: Theme.UITextField(props: [.font(nil), .placeHolderText("Your password"), .autocapitalizationType(autocapitalizationType: .none), .borderStyle(borderStyle: .roundedRect), .backgroundColor(UIColor.clear)]))
    private let passwordLabel = UILabel(theme: Theme.UILabel(props: [.text("Password"), .font(nil), .textColor]))
    private let registrationStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 10.0)))]))
    private let submitButton = RoundButton(theme: Theme.RoundButton(props: [.color, .text("Submit"), .titleLabelFont(nil)]))
    private let logoImageView = UIImageView(theme: Theme.UIView(props: []))
    private let logoImage = UIImage(named: "charterRomanPurpleLogo-30")
    private let centerStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 10.0)))]))
    private let centerLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Choose a password"), .center, .font(UIFont.largeThemeLabelFont)]))

    @UsesAutoLayout private var newActivityIndicator = UIActivityIndicatorView(style: .large)

    private var formValues: [String: String] = [:]
    private lazy var navController: UINavigationController = {
        SceneDelegate.shared.rootViewController.current
    }()

    init() {
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(registrationStackView)
        view.addSubview(submitButton)
        view.addSubview(newActivityIndicator)
        view.addSubview(centerStackView)
        view.addSubview(logoImageView)

        registrationStackView.addArrangedSubview(passwordLabel)
        registrationStackView.addArrangedSubview(passwordTextField)

        centerStackView.addArrangedSubview(centerLabel)

        passwordTextField.delegate = self
        passwordTextField.isSecureTextEntry = true

        newActivityIndicator.backgroundColor = UIColor(white: 0, alpha: 0.4)
        newActivityIndicator.frame = view.bounds

        logoImageView.image = logoImage

        let safeArea = view.safeAreaLayoutGuide

        NSLayoutConstraint.activate([
            logoImageView.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: CGFloat(0.5)),
            logoImageView.heightAnchor.constraint(equalTo: logoImageView.widthAnchor),
            logoImageView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            logoImageView.bottomAnchor.constraint(lessThanOrEqualTo: centerStackView.topAnchor, constant: getHeightRatio(verticalConstant: -10.0)),
            logoImageView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: getHeightRatio(verticalConstant: 40.0)),

            submitButton.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: getHeightRatio(verticalConstant: -10.0)),
            submitButton.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            submitButton.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -10.0)),
            submitButton.widthAnchor.constraint(equalTo: submitButton.heightAnchor, multiplier: 197 / 25),
            submitButton.topAnchor.constraint(lessThanOrEqualTo: registrationStackView.bottomAnchor, constant: UIViewController.screenSize.height * 0.31),

            passwordTextField.widthAnchor.constraint(equalTo: passwordTextField.heightAnchor, multiplier: 197 / 25),

            newActivityIndicator.topAnchor.constraint(equalTo: safeArea.topAnchor),
            newActivityIndicator.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            newActivityIndicator.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            newActivityIndicator.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),

            registrationStackView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -10.0)),
            registrationStackView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            registrationStackView.bottomAnchor.constraint(equalTo: submitButton.topAnchor, constant: getHeightRatio(verticalConstant: -20.0)),

            centerStackView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            centerStackView.bottomAnchor.constraint(lessThanOrEqualTo: registrationStackView.topAnchor, constant: -(0.095 * UIViewController.screenSize.height)),
            centerStackView.heightAnchor.constraint(greaterThanOrEqualTo: safeArea.heightAnchor, multiplier: 0.12),
        ])
        submitButton.addTarget(self, action: #selector(submitRegistration), for: .touchUpInside)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.placeholder == "Your password" {
            formValues["password"] = textField.text
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.placeholder == "Your password" {
            formValues["password"] = textField.text
        }
        return true
    }

    @objc private func submitRegistration(_: RoundButton) {
        DispatchQueue.main.async {
            self.view.bringSubviewToFront(self.newActivityIndicator)
            self.newActivityIndicator.startAnimating()
        }

        CheckoutCart.shared.user?.password = passwordTextField.text!
        CoreDataManager.sharedManager.saveContext()
        let userCredentials: [HTTPHeader] = [HTTPHeader(field: "email", value: CheckoutCart.shared.user!.email), HTTPHeader(field: "password", value: passwordTextField.text!)]
        let request = try! APIRequest(method: .get, path: "/login", headers: userCredentials)

        APIClient().perform(request) { result in
            switch result {
            case let .success(response):
                if response.statusCode == 200, let response = try? response.decode(to: User.self) {
                    let user = response.body

                    let sessionToken = response.headers["jwt-token"] as! String

                    // set the token in keychain and use it whenever calling the backend
                    try! SecureStore(secureStoreQueryable: GenericPasswordQueryable()).setValue(sessionToken, for: "sessionToken")

                    let userCredentials: [HTTPHeader] = [HTTPHeader(field: "device-token", value: CheckoutCart.shared.deviceToken)]
                    let sendDeviceTokenRequest = try! APIRequest(method: .post, path: "/apn_token/\(user.email)/\(sessionToken)", headers: userCredentials)
                    APIClient().perform(sendDeviceTokenRequest) { result in
                        switch result {
                        case let .success(response):
                            if response.statusCode == 200 {
                                DispatchQueue.main.async {
                                    CheckoutCart.shared.user = user
                                    user.userToCheckoutCart = CheckoutCart.shared

                                    CheckoutCart.shared.stripeId = user.stripeId

                                    CoreDataManager.sharedManager.saveContext()
                                    self.newActivityIndicator.stopAnimating()

                                    SceneDelegate.shared.rootViewController.switchToHomePageViewController()
                                }
                            }
                        case let .failure(error):
                            print("error", error.localizedDescription)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        self.newActivityIndicator.stopAnimating()
                        let alertController = UIAlertController(title: "Log in failed", message: "Your username or password was incorrect. Please try again.", preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "Try again", style: .cancel) { _ in
                            alertController.dismiss(animated: true)
                        })
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            // if the result is failure then the API was not able to be executed, indicating a local network error
            case let .failure(error): // if the API fails the enum API result will be of the type failure
                DispatchQueue.main.async {
                    self.newActivityIndicator.stopAnimating()

                    let alertController = UIAlertController(title: "Network Error", message: "A network error has occured. Check your internet connection and if necessary, restart the app.", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Okay", style: .cancel) { _ in
                        self.dismiss(animated: true)
                    })
                    self.present(alertController, animated: true, completion: nil)
                    print("error case .failure", error)
                }
            }
        }
    }

    private func alertError(message: String, title: String) {
        return alert(
            title: title,
            message: message,
            alertType: "error"
        )
    }

    private func alert(title: String, message: String, alertType: String) {
        let alertCtrl = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if alertType == "accountCreation" {
            alertCtrl.addAction(UIAlertAction(title: "Okay", style: .cancel) { _ in
                SceneDelegate.shared.rootViewController.switchToHomePageViewController()
            })
        } else if alertType == "error" {
            alertCtrl.addAction(UIAlertAction(title: "Okay", style: .cancel) { _ in
                self.dismiss(animated: true) {
                    DispatchQueue.main.async {
                        self.newActivityIndicator.stopAnimating()
                    }
                }
            })
        }
        present(alertCtrl, animated: true, completion: nil)
    }
}
