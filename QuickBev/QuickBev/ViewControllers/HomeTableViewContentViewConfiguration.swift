//
//  HomeContentViewConfiguration.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 9/1/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit

struct HomeTableViewContentConfiguration: UIContentConfiguration {
    func updated(for _: UIConfigurationState) -> HomeTableViewContentConfiguration {
        return self
    }

    let name: String
    let businessId: String
    let distancePrecise: Double
    var businessImage: UIImage

    init(businessItem: BusinessItem) {
        name = businessItem.name
        businessId = businessItem.businessId.uuidString
        businessImage = businessItem.image
        distancePrecise = businessItem.distance
    }

    func makeContentView() -> UIView & UIContentView {
        let c = HomeTableViewContentView(configuration: self)
        return c
    }
}

extension HomeTableViewContentConfiguration {
    var distance: String {
        return distancePrecise.rounded(digits: 1).description
    }
}
