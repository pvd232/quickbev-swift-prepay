//
//  BusinessSearchContentViewConfiguration.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 9/20/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
struct BusinessSearchContentViewConfiguration: UIContentConfiguration {
    func updated(for _: UIConfigurationState) -> BusinessSearchContentViewConfiguration {
        return self
    }

    let name: String
    let businessId: String
    let address: String
    let distancePrecise: Double
    let isOpen: Bool

    init(businessItem: BusinessItem) {
        name = businessItem.name
        businessId = businessItem.businessId.uuidString
        address = businessItem.address
        distancePrecise = businessItem.distance
        isOpen = businessItem.isOpen
    }

    func makeContentView() -> UIView & UIContentView {
        let c = BusinessSearchContentView(configuration: self)
        return c
    }
}

extension BusinessSearchContentViewConfiguration {
    var distance: String {
        return distancePrecise.rounded(digits: 1).description
    }
}
