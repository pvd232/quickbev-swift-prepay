//
//  BottomSheetViewController.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 9/18/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
protocol ButtonPressedProtocol {
    func getQuickPassButtonPressed(businessId: UUID)
    func checkInButtonPressed()
}

class BottomSheetViewController: UIViewController {
    var businessDetails: Business!
    private var hasSetPointOrigin = false
    private var pointOrigin = CGPoint()

    // this value is optional because the ViewController managing this ViewController will assign itself as the delegate of the protocol defined in this ViewController
    var buttonPressedDelegate: ButtonPressedProtocol!
    private let name = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 18.0)))]))
    private let addy = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.secondaryTableCellContentViewFont)]))
    private let hours = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.secondaryTableCellContentViewFont)]))
    private let atCapacity = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.secondaryTableCellContentViewFont)]))
    private let distance = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.secondaryTableCellContentViewFont)]))
    private let interpunctLabel = UILabel(theme: Theme.UILabel(props: [.center, .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 20.0))), .text("·"), .textColor]))

    private let topDarkLine: UIView = {
        let view = UIView(theme: Theme.UIView(props: []))
        view.backgroundColor = .darkGray
        view.layer.cornerRadius = 3
        return view
    }()

    init(business: Business) {
        businessDetails = business
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .custom
        transitioningDelegate = self
        setupView()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    func setupView() {
        view = RoundUIView()

        if let view = view as? RoundUIView {
            view.refreshBorderWidth(value: 0.0)
        }

        view.backgroundColor = UIColor.themeColor.mix(with: UIColor.white, amount: 0.75)
        view.addSubview(topDarkLine)
        let safeArea = view.safeAreaLayoutGuide

        let businessDetailsStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 18.0)))]))
        view.addSubview(businessDetailsStackView)

        let addyAndOpenStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(Float(getWidthRatio(horizontalConstant: 10.0)))]))
        addyAndOpenStackView.addArrangedSubview(addy)
        addyAndOpenStackView.addArrangedSubview(interpunctLabel)
        addyAndOpenStackView.addArrangedSubview(distance)

        businessDetailsStackView.addArrangedSubview(name)
        businessDetailsStackView.addArrangedSubview(addyAndOpenStackView)
        businessDetailsStackView.addArrangedSubview(hours)

        let checkInButton: RoundButton = {
            if let userBusinessId = CheckoutCart.shared.userBusiness?.id, userBusinessId == businessDetails.id {
                let buttonToReturn = RoundButton(theme: Theme.RoundButton(props: [.text("Checked in"), .titleLabelFont(UIFont.secondaryTableCellContentViewFont)]))
                buttonToReturn.refreshBorderWidth(value: 0.0)
                buttonToReturn.refreshColor(color: UIColor.systemGreen.mix(with: UIColor.white, amount: 0.3))
                buttonToReturn.refreshCorners(value: 18.0)
                buttonToReturn.setTitleColor(UIColor.black, for: .normal)
                buttonToReturn.isUserInteractionEnabled = false
                return buttonToReturn
            } else {
                let otherButtonToReturn = RoundButton(theme: Theme.RoundButton(props: [.text("Check in"), .titleLabelFont(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 14.0)))]))
                otherButtonToReturn.refreshColor(color: UIColor.themeColor)
                otherButtonToReturn.refreshCorners(value: 8.0)
                otherButtonToReturn.refreshBorderWidth(value: 2.0)
                otherButtonToReturn.setTitleColor(UIColor.white, for: .normal)
                otherButtonToReturn.refreshBorderColor(color: UIColor.themeColor)
                otherButtonToReturn.addTarget(self, action: #selector(setBusinessAndCheckIn), for: .touchUpInside)
                return otherButtonToReturn
            }
        }()

        businessDetailsStackView.alignment = .leading

        view.bringSubviewToFront(topDarkLine)
        view.bringSubviewToFront(businessDetailsStackView)

        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerAction))
        view.addGestureRecognizer(panGesture)

        NSLayoutConstraint.activate([
            topDarkLine.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: getHeightRatio(verticalConstant: 12.0)),
            topDarkLine.heightAnchor.constraint(equalToConstant: getHeightRatio(verticalConstant: 8.0)),
            topDarkLine.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.15),
            topDarkLine.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            businessDetailsStackView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 20.0)),
        ])

        // if the business is closed we add the closed label and dont add the option to purchase anything
        if !businessDetails.isOpen {
            let closedLabel = RoundButton(theme: Theme.RoundButton(props: [.text("Closed"), .titleLabelFont(UIFont.secondaryTableCellContentViewFont)]))
            closedLabel.refreshBorderWidth(value: 0.0)
            closedLabel.refreshColor(color: UIColor.red.mix(with: UIColor.white, amount: 0.78))
            closedLabel.refreshCorners(value: 18.0)
            closedLabel.setTitleColor(UIColor.red, for: .normal)
            view.addSubview(closedLabel)

            NSLayoutConstraint.activate([
                closedLabel.widthAnchor.constraint(equalToConstant: getWidthRatio(horizontalConstant: 115)),
                closedLabel.heightAnchor.constraint(equalToConstant: getHeightRatio(verticalConstant: 30.0)),
                closedLabel.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
                closedLabel.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: getHeightRatio(verticalConstant: 35.0)),
                businessDetailsStackView.topAnchor.constraint(equalTo: closedLabel.bottomAnchor, constant: getHeightRatio(verticalConstant: 18.0)),
            ])
        } else if businessDetails.atCapacity { // if the business is at capacity then we add the full label and add option to both check in and get a quick pass
            view.addSubview(checkInButton)
            let atCapacityLabel = RoundButton(theme: Theme.RoundButton(props: [.text("At capacity"), .titleLabelFont(UIFont.secondaryTableCellContentViewFont)]))
            atCapacityLabel.refreshBorderWidth(value: 0.0)
            atCapacityLabel.refreshColor(color: UIColor.red.mix(with: UIColor.white, amount: 0.78))
            atCapacityLabel.refreshCorners(value: 18.0)
            atCapacityLabel.setTitleColor(UIColor.red, for: .normal)
            view.addSubview(atCapacityLabel)

            let buyQuickPassButton = RoundButton(theme: Theme.RoundButton(props: [.text("Get QuickPass"), .titleLabelFont(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 14.0)))]))
            buyQuickPassButton.refreshColor(color: UIColor.themeColor)
            buyQuickPassButton.refreshCorners(value: 8.0)
            buyQuickPassButton.refreshBorderWidth(value: 2.0)
            buyQuickPassButton.setTitleColor(UIColor.white, for: .normal)
            buyQuickPassButton.refreshBorderColor(color: UIColor.themeColor)
            buyQuickPassButton.addTarget(self, action: #selector(getQuickPassTouchup), for: .touchUpInside)
            view.addSubview(buyQuickPassButton)
            buyQuickPassButton.isHidden = !CheckoutCart.shared.shouldDisplayQuickPass

            NSLayoutConstraint.activate([
                atCapacityLabel.widthAnchor.constraint(equalToConstant: getWidthRatio(horizontalConstant: 135)),
                atCapacityLabel.heightAnchor.constraint(equalToConstant: getHeightRatio(verticalConstant: 30.0)),
                atCapacityLabel.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
                atCapacityLabel.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: getHeightRatio(verticalConstant: 35.0)),

                checkInButton.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.33),
                checkInButton.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.038),
                checkInButton.bottomAnchor.constraint(equalTo: businessDetailsStackView.bottomAnchor),
                checkInButton.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -20.0)),

                businessDetailsStackView.topAnchor.constraint(equalTo: atCapacityLabel.bottomAnchor, constant: getHeightRatio(verticalConstant: 18.0)),
                buyQuickPassButton.centerYAnchor.constraint(equalTo: atCapacityLabel.centerYAnchor),
                buyQuickPassButton.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -20.0)),
                buyQuickPassButton.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.33),
                buyQuickPassButton.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.038),
            ])
        } else { // if the business is open and not at capacity then we just add the button to check in
            view.addSubview(checkInButton)

            NSLayoutConstraint.activate([
                businessDetailsStackView.topAnchor.constraint(equalTo: topDarkLine.bottomAnchor, constant: getHeightRatio(verticalConstant: 10.0)),

                checkInButton.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.33),
                checkInButton.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.038),
                checkInButton.bottomAnchor.constraint(equalTo: businessDetailsStackView.bottomAnchor),
                checkInButton.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -20.0)),
            ])
        }
        setViewProperties()
    }

    @objc private func getQuickPassTouchup() {
        buttonPressedDelegate.getQuickPassButtonPressed(businessId: businessDetails.id)
    }

    @objc private func setBusinessAndCheckIn() {
        if CheckoutCart.shared.canPay == true {
            CheckoutCart.shared.emptyCart()
        }
        CheckoutCart.shared.user?.userToBusiness = businessDetails
        businessDetails.businessToUser = CheckoutCart.shared.user
        CoreDataManager.sharedManager.saveContext()
        buttonPressedDelegate?.checkInButtonPressed()
    }

    private func setViewProperties() {
        name.text = businessDetails.formattedName
        addy.text = businessDetails.address.split(separator: ",")[0].description
        distance.text = "\(Int(businessDetails.distanceFromUser)) miles away"
        if businessDetails.isOpen {
            hours.text = "Closes at \(businessDetails.closingTime!)"
        } else {
            hours.text = ""
        }
    }

    // 1.
    override func viewDidLayoutSubviews() {
        if !hasSetPointOrigin {
            hasSetPointOrigin = true
            pointOrigin = view.frame.origin
        }
    }

    // 2.
    @objc private func panGestureRecognizerAction(sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: view)

        // Not allowing the user to drag the view upward
        guard translation.y >= 0 else { return }

        // setting x as 0 because we don't want users to move the frame side ways!! Only want straight up or down
        view.frame.origin = CGPoint(x: 0, y: pointOrigin.y + translation.y)

        if sender.state == .ended {
            // get the amount that the user dragged the modal view downwards, for whatever reason all the values are negative
            let dif = -(pointOrigin.y - view.frame.minY)

            // if the difference is greater than or equal to half of the size of the modal view then dismiss it
            if dif >= (view.frame.height / 4) {
                dismiss(animated: true, completion: nil)
            }
            // otherwise put the modal view back where it stated
            else {
                UIView.animate(withDuration: 0.3) {
                    self.view.frame.origin = self.pointOrigin
                }
            }
        }
    }
}

extension BottomSheetViewController: UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning {
    func animateTransition(using _: UIViewControllerContextTransitioning) {}

    func animationController(forPresented _: UIViewController, presenting _: UIViewController, source _: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }

    func animationController(forDismissed _: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }

    func transitionDuration(using _: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 1
    }
}
