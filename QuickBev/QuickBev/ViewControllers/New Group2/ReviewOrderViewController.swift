//
//  ReviewOrderViewController.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 1/1/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import PassKit
import Stripe
import UIKit
class ReviewOrderViewController: UIViewController, UIPopoverPresentationControllerDelegate, UITextFieldDelegate {
    private let orderSummaryStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 20.0)))]))

    private let subtotalStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal]))
    private let subtotalHeaderLabel = UILabel(theme: Theme.UILabel(props: [.text("Subtotal"), .font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 22.0)))]))
    private let subtotalLabel = UILabel(theme: Theme.UILabel(props: [.font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 22.0)))]))

    private let tipStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(0.0)]))
    private let tipLabel = UILabel(theme: Theme.UILabel(props: [.text("Tip  "), .font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 22.0)))]))
    private let tipTotalLabel = UILabel(theme: Theme.UILabel(props: [.font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 22.0)))]))

    private let salesTaxStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal]))
    private let salesTaxHeaderLabel = UILabel(theme: Theme.UILabel(props: [.text("Sales Tax"), .font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 22.0)))]))
    private let salesTaxLabel = UILabel(theme: Theme.UILabel(props: [.font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 22.0)))]))

    private let CCAndServiceFeeStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(5.0)]))
    private let CCAndServiceFeeHeaderLabel = UILabel(theme: Theme.UILabel(props: [.text("Credit Card/Service Fee"), .font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 22.0)))]))
    private let CCAndServiceFeeLabel = UILabel(theme: Theme.UILabel(props: [.font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 22.0)))]))

    private let totalStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal]))
    private let totalHeaderLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Total"), .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 24.0)))]))
    private let totalLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 24.0)))]))

    private let defaultPaymentMethodView = UIView(theme: Theme.UIView(props: []))
    private let cardLogoImageView = UIImageView(theme: Theme.UIView(props: []))
    private let cardCompanyName = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.themeLabelFont)]))

    private let servicePlusCreditFeeLabel = UILabel(theme: Theme.UILabel(props: [.textColor]))

    private let addPaymentMethodButton = RoundButton(theme: Theme.UIView(props: []))
    private let editPaymentMethodButton = RoundButton(theme: Theme.UIView(props: []))
    private let passwordTextField = UITextField(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    private let editTipButtonCustom = ToggleButton(theme: Theme.RoundButton(props: []))
    private let editTipButton10 = ToggleButton(theme: Theme.RoundButton(props: []))
    private let editTipButton15 = ToggleButton(theme: Theme.RoundButton(props: []))
    private let editTipButton20 = ToggleButton(theme: Theme.RoundButton(props: []))

    private let editTipStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(0)]))

    private let applePayButton = PKPaymentButton(paymentButtonType: .plain, paymentButtonStyle: .black)
    @UsesAutoLayout var newActivityIndicator = UIActivityIndicatorView(style: .large)

    lazy var paymentContext: STPPaymentContext = {
        STPPaymentContext(customerContext: CheckoutCart.shared.customerContext)
    }()

    private var paymentIntentId = ""
    private var businessError = false
    private var serverError = false
    init() {
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white
        CheckoutCart.shared.tipPercentage = 0.1
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // load subviews
        view.addSubview(orderSummaryStackView)
        view.addSubview(defaultPaymentMethodView)

        view.addSubview(newActivityIndicator)

        editTipButtonCustom.addSubview(passwordTextField)

        passwordTextField.resetBorders()

        setupHeaderView(otherView: orderSummaryStackView, header: "Order summary", distanceFromHeader: getHeightRatio(verticalConstant: -20.0))
        // setup stripe
        paymentContext.delegate = self
        paymentContext.hostViewController = self

        // page content and formatting

        subtotalHeaderLabel.textColor = .systemGray.mix(with: .black, amount: 0.3)
        subtotalLabel.textColor = .systemGray.mix(with: .black, amount: 0.3)
        subtotalLabel.setContentHuggingPriority(UILayoutPriority(500), for: .horizontal)

        salesTaxHeaderLabel.textColor = .systemGray.mix(with: .black, amount: 0.3)
        salesTaxLabel.textColor = .systemGray.mix(with: .black, amount: 0.3)
        salesTaxLabel.setContentHuggingPriority(UILayoutPriority(500), for: .horizontal)

        tipLabel.textColor = .systemGray.mix(with: .black, amount: 0.3)
        tipTotalLabel.textColor = .systemGray.mix(with: .black, amount: 0.3)
        tipTotalLabel.setContentHuggingPriority(UILayoutPriority(100), for: .horizontal)
        tipTotalLabel.textAlignment = .right

        CCAndServiceFeeHeaderLabel.textColor = .systemGray.mix(with: .black, amount: 0.3)
        CCAndServiceFeeLabel.textColor = .systemGray.mix(with: .black, amount: 0.3)
        CCAndServiceFeeLabel.setContentHuggingPriority(UILayoutPriority(500), for: .horizontal)

        totalLabel.setContentHuggingPriority(UILayoutPriority(500), for: .horizontal)

        orderSummaryStackView.addArrangedSubview(subtotalStackView)
        orderSummaryStackView.addArrangedSubview(salesTaxStackView)
        orderSummaryStackView.addArrangedSubview(tipStackView)
        orderSummaryStackView.addArrangedSubview(CCAndServiceFeeStackView)
        orderSummaryStackView.addArrangedSubview(totalStackView)

        subtotalStackView.addArrangedSubview(subtotalHeaderLabel)
        subtotalStackView.addArrangedSubview(subtotalLabel)

        salesTaxStackView.addArrangedSubview(salesTaxHeaderLabel)
        salesTaxStackView.addArrangedSubview(salesTaxLabel)

        tipStackView.addArrangedSubview(tipLabel)
        tipStackView.addArrangedSubview(editTipStackView)
        tipStackView.addArrangedSubview(tipTotalLabel)

        CCAndServiceFeeStackView.addArrangedSubview(CCAndServiceFeeHeaderLabel)
        CCAndServiceFeeStackView.addArrangedSubview(CCAndServiceFeeLabel)

        totalStackView.addArrangedSubview(totalHeaderLabel)
        totalStackView.addArrangedSubview(totalLabel)

        defaultPaymentMethodView.addSubview(cardLogoImageView)
        defaultPaymentMethodView.addSubview(cardCompanyName)
        defaultPaymentMethodView.addSubview(editPaymentMethodButton)

        editPaymentMethodButton.refreshColor(color: UIColor.themeColor.mix(with: .white, amount: 0.75))
        editPaymentMethodButton.refreshCorners(value: 8.0)
        editPaymentMethodButton.setTitleColor(UIColor.black, for: .normal)
        editPaymentMethodButton.refreshBorderColor(color: UIColor.themeColor)
        editPaymentMethodButton.refreshTitle(newTitle: "edit")
        editPaymentMethodButton.titleLabel?.font = UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 18.0))

        editTipButtonCustom.refreshTitle(newTitle: " Custom ")
        editTipButton10.refreshTitle(newTitle: "  10%  ")
        editTipButton15.refreshTitle(newTitle: "  15%  ")
        editTipButton20.refreshTitle(newTitle: "  20%  ")

        configureToggleButton(toggleButton: editTipButtonCustom)
        configureToggleButton(toggleButton: editTipButton10)
        configureToggleButton(toggleButton: editTipButton15)
        configureToggleButton(toggleButton: editTipButton20)

        editTipButtonCustom.arrangement = .left
        editTipButton10.arrangement = .leftCenter
        editTipButton15.arrangement = .rightCenter
        editTipButton20.arrangement = .right

        editTipButtonCustom.addLeftBorder(with: UIColor.lightGray.withAlphaComponent(0.8), andWidth: 1.5)
        editTipButtonCustom.addTopBorder(with: UIColor.lightGray.withAlphaComponent(0.8), andWidth: 1.5)
        editTipButtonCustom.addBottomBorder(with: UIColor.lightGray.withAlphaComponent(0.8), andWidth: 1.5)

        editTipButton10.addRightBorder(with: UIColor.themeColor, andWidth: 1.5)
        editTipButton10.addLeftBorder(with: UIColor.themeColor, andWidth: 1.5)
        editTipButton10.addBottomBorder(with: UIColor.themeColor, andWidth: 1.5)
        editTipButton10.addTopBorder(with: UIColor.themeColor, andWidth: 1.5)

        editTipButton10.isOn = true
        CheckoutCart.shared.tipPercentage = 0.10

        editTipButton10.toggleDoubleValue = 0.1

        editTipButton15.addRightBorder(with: UIColor.lightGray.withAlphaComponent(0.8), andWidth: 1.5)
        editTipButton15.addTopBorder(with: UIColor.lightGray.withAlphaComponent(0.8), andWidth: 1.5)
        editTipButton15.addBottomBorder(with: UIColor.lightGray.withAlphaComponent(0.8), andWidth: 1.5)
        editTipButton15.toggleDoubleValue = 0.15

        editTipButton20.addRightBorder(with: UIColor.lightGray.withAlphaComponent(0.8), andWidth: 1.5)
        editTipButton20.addTopBorder(with: UIColor.lightGray.withAlphaComponent(0.8), andWidth: 1.5)
        editTipButton20.addBottomBorder(with: UIColor.lightGray.withAlphaComponent(0.8), andWidth: 1.5)
        editTipButton20.toggleDoubleValue = 0.2

        editTipStackView.addArrangedSubview(editTipButtonCustom)
        editTipStackView.addArrangedSubview(editTipButton10)
        editTipStackView.addArrangedSubview(editTipButton15)
        editTipStackView.addArrangedSubview(editTipButton20)

        addPaymentMethodButton.madeStandard()
        addPaymentMethodButton.refreshTitle(newTitle: "Add payment method")

        applePayButton.titleLabel?.font = UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 20.0))

        newActivityIndicator.backgroundColor = UIColor(white: 0, alpha: 0.4)
        newActivityIndicator.frame = view.bounds
        view.bringSubviewToFront(newActivityIndicator)
        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            orderSummaryStackView.heightAnchor.constraint(equalTo: safeArea.heightAnchor, multiplier: 0.3),
            orderSummaryStackView.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.90),
            orderSummaryStackView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            defaultPaymentMethodView.heightAnchor.constraint(equalTo: safeArea.heightAnchor, multiplier: 0.0841584),
            defaultPaymentMethodView.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.90),
            defaultPaymentMethodView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            defaultPaymentMethodView.topAnchor.constraint(equalTo: orderSummaryStackView.bottomAnchor, constant: getHeightRatio(verticalConstant: 35.0)),

            editTipStackView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.04),

            cardLogoImageView.centerYAnchor.constraint(equalTo: cardLogoImageView.superview!.centerYAnchor),
            cardLogoImageView.widthAnchor.constraint(equalTo: cardLogoImageView.superview!.widthAnchor, multiplier: 0.15),
            cardLogoImageView.heightAnchor.constraint(equalTo: cardLogoImageView.superview!.heightAnchor, multiplier: 0.529412),
            cardLogoImageView.leadingAnchor.constraint(equalTo: cardLogoImageView.superview!.leadingAnchor),

            cardCompanyName.widthAnchor.constraint(equalTo: cardCompanyName.superview!.widthAnchor, multiplier: 0.365517),
            cardCompanyName.leadingAnchor.constraint(equalTo: cardLogoImageView.trailingAnchor, constant: getWidthRatio(horizontalConstant: 20.0)),
            cardCompanyName.centerYAnchor.constraint(equalTo: cardCompanyName.superview!.centerYAnchor),

            editPaymentMethodButton.heightAnchor.constraint(equalTo: editPaymentMethodButton.superview!.heightAnchor, multiplier: 0.497059),
            editPaymentMethodButton.trailingAnchor.constraint(equalTo: editPaymentMethodButton.superview!.trailingAnchor),
            editPaymentMethodButton.widthAnchor.constraint(greaterThanOrEqualTo: editPaymentMethodButton.superview!.widthAnchor, multiplier: 0.25),
            editPaymentMethodButton.centerYAnchor.constraint(equalTo: editPaymentMethodButton.superview!.centerYAnchor),

            newActivityIndicator.topAnchor.constraint(equalTo: safeArea.topAnchor),
            newActivityIndicator.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            newActivityIndicator.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            newActivityIndicator.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
        ])
        passwordTextField.delegate = self
        addPaymentMethodButton.addTarget(self, action: #selector(choosePaymentButtonTapped), for: .touchUpInside)
        applePayButton.addTarget(self, action: #selector(handleApplePayButtonTapped), for: .touchUpInside)
        editPaymentMethodButton.addTarget(self, action: #selector(changeDefaultPaymentMethodButtonClicked), for: .touchUpInside)
        conditionallyDisplayPaymentButtons()
        paymentContextDidChange(paymentContext)
    }

    private func conditionallyDisplayPaymentButtons() {
        // Only offer Apple Pay if the customer can pay with it
        let safeArea = view.safeAreaLayoutGuide
        if !StripeAPI.deviceSupportsApplePay() {
            view.addSubview(addPaymentMethodButton)
            NSLayoutConstraint.activate([
                addPaymentMethodButton.widthAnchor.constraint(equalTo: addPaymentMethodButton.heightAnchor, multiplier: 373 / 60),
                addPaymentMethodButton.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
                addPaymentMethodButton.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -10.0)),
                addPaymentMethodButton.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: getHeightRatio(verticalConstant: -20.0)),
            ])
        } else {
            let bottomButtonStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical]))
            view.addSubview(bottomButtonStackView)

            bottomButtonStackView.addArrangedSubview(applePayButton)
            bottomButtonStackView.addArrangedSubview(addPaymentMethodButton)
            bottomButtonStackView.distribution = .equalSpacing
            bottomButtonStackView.spacing = getHeightRatio(verticalConstant: 10.0)
            NSLayoutConstraint.activate([
                addPaymentMethodButton.widthAnchor.constraint(equalTo: addPaymentMethodButton.heightAnchor, multiplier: 373 / 60),
                applePayButton.widthAnchor.constraint(equalTo: applePayButton.heightAnchor, multiplier: 373 / 60),
                bottomButtonStackView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: getHeightRatio(verticalConstant: -20.0)),
                bottomButtonStackView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
                bottomButtonStackView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -10.0)),
                bottomButtonStackView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            ])
        }
    }

    @objc private func handleApplePayButtonTapped() {
        CheckoutCart.shared.calculateCost()

        let merchantIdentifier = "merchant.com.theQuickCompany.QuickBev"
        let paymentRequest = StripeAPI.paymentRequest(withMerchantIdentifier: merchantIdentifier, country: "US", currency: "USD")

        // Configure the line items on the payment request
        paymentRequest.paymentSummaryItems = [
            // The final line should represent your company;
            // it'll be prepended with the word "Pay" (i.e. "Pay iHats, Inc $50")
            PKPaymentSummaryItem(label: CheckoutCart.shared.userBusiness!.name, amount: NSDecimalNumber(value: CheckoutCart.shared.total.rounded(digits: 2))),
        ]
        // Initialize an STPApplePayContext instance
        if let applePayContext = STPApplePayContext(paymentRequest: paymentRequest, delegate: self) {
            // Present Apple Pay payment sheet
            applePayContext.presentApplePay()
        } else {
            // There is a problem with your Apple Pay configuration
            print("error with apple pay configuration")
        }
    }

    private func updateOrderTotal() {
        CheckoutCart.shared.calculateCost()
        salesTaxLabel.text = CheckoutCart.shared.salesTaxTotal.formattedCurrency().formattedCurrencyString()
        tipTotalLabel.text = CheckoutCart.shared.tipTotal.formattedCurrency().formattedCurrencyString()
        CCAndServiceFeeLabel.text = CheckoutCart.shared.stripeFeeAndServiceFeeTotal.formattedCurrency().formattedCurrencyString()
        subtotalLabel.text = CheckoutCart.shared.subtotal.formattedCurrency().formattedCurrencyString()
        totalLabel.text = CheckoutCart.shared.total.formattedCurrency().formattedCurrencyString()
        paymentContext.paymentAmount = Int((CheckoutCart.shared.total * 100).rounded(digits: 2)) // This is in cents, i.e. $50 USD
    }

    override func viewWillAppear(_: Bool) {
        updateOrderTotal()
        navigationController?.isHiddenHairline = true
    }

    func adaptivePresentationStyle(for _: UIPresentationController, traitCollection _: UITraitCollection) -> UIModalPresentationStyle
    {
        return .none
    }

    // Configure the toggle button for this app.
    private func configureToggleButton(toggleButton: ToggleButton) {
        toggleButton.refreshCorners(value: 0.0)
        toggleButton.refreshBorderWidth(value: 0.0)
        toggleButton.setTitleColor(.black, for: .normal)
        toggleButton.titleLabel?.font = UIFont.primaryTableCellContentViewFont
        toggleButton.addTarget(self, action: #selector(editTipButtonClicked), for: .touchUpInside)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // if the backspace button is pressed the string will equal "", and I want to allow backspacing
        let textFieldText: NSString = (textField.text ?? "") as NSString
        var txtAfterUpdate: String = textFieldText.replacingCharacters(in: range, with: string)

        if string != "", textFieldText.replacingCharacters(in: range, with: string).count > 0 {
            guard Int(string) != nil else {
                return false
            }
        }
        // if the user deletes all the characters in the customer tip button then make it 0% tip
        else if string == "", textFieldText.replacingCharacters(in: range, with: string).count == 0 {
            editTipButtonCustom.toggleDoubleValue = 0.0
            editTipButtonCustom.setTitle(" 0% ", for: .normal)
            return true
        }
        // if the user previously deleted all text in the custom

        else if string == "", editTipButtonCustom.titleLabel?.text == " 0% " {
            return false
        }
        guard Double(txtAfterUpdate) != nil else {
            return true
        }
        editTipButtonCustom.toggleDoubleValue = (Double(txtAfterUpdate)! / 100)

        txtAfterUpdate.insert(" ", at: txtAfterUpdate.startIndex)
        txtAfterUpdate.insert("%", at: txtAfterUpdate.endIndex)
        txtAfterUpdate.insert(" ", at: txtAfterUpdate.endIndex)
        editTipButtonCustom.setTitle(txtAfterUpdate, for: .normal)

        return true
    }

    func textFieldShouldClear(_: UITextField) -> Bool {
        editTipButtonCustom.setTitle("", for: .normal)
        editTipButtonCustom.toggleDoubleValue = 0.0
        return true
    }

    func textFieldDidEndEditing(_: UITextField) {
        if let toggleDouble = editTipButtonCustom.toggleDoubleValue {
            CheckoutCart.shared.tipPercentage = toggleDouble
        }
        updateOrderTotal()
    }

    func textFieldShouldReturn(_: UITextField) -> Bool {
        if let toggleDouble = editTipButtonCustom.toggleDoubleValue {
            CheckoutCart.shared.tipPercentage = toggleDouble
        }
        updateOrderTotal()
        return true
    }

    @objc private func editTipButtonClicked(_ toggleButton: ToggleButton) {
        for view in editTipStackView.subviews {
            if let button = view as? ToggleButton {
                if button.arrangement != toggleButton.arrangement, button.isOn == true {
                    button.isOn = false
                    button.resetBorders()
                    button.addLeftBorder(with: button.selectedBorderColor, andWidth: 1.5)
                    button.addRightBorder(with: button.selectedBorderColor, andWidth: 1.5)
                    button.addTopBorder(with: button.selectedBorderColor, andWidth: 1.5)
                    button.addBottomBorder(with: button.selectedBorderColor, andWidth: 1.5)
                }
                if button.arrangement != toggleButton.arrangement, button.arrangement == .left {
                    button.setTitle(" Custom ", for: .normal)
                    editTipButtonCustom.toggleDoubleValue = nil
                    passwordTextField.text = ""
                }
            }
        }

        var borderColor: UIColor {
            if toggleButton.isOn == true {
                return UIColor.themeColor
            } else {
                return UIColor.lightGray.withAlphaComponent(0.8)
            }
        }

        toggleButton.resetBorders()
        if toggleButton.arrangement == .left {
            editTipButton10.removeBorder(border: .left)
        } else if toggleButton.arrangement == .leftCenter {
            editTipButtonCustom.removeBorder(border: .right)

            editTipButton15.removeBorder(border: .left)
        } else if toggleButton.arrangement == .rightCenter {
            editTipButton10.removeBorder(border: .right)
            editTipButton20.removeBorder(border: .left)
        } else if toggleButton.arrangement == .right {
            editTipButton15.removeBorder(border: .right)
        }

        toggleButton.addLeftBorder(with: toggleButton.selectedBorderColor, andWidth: 1.5)
        toggleButton.addRightBorder(with: toggleButton.selectedBorderColor, andWidth: 1.5)
        toggleButton.addTopBorder(with: toggleButton.selectedBorderColor, andWidth: 1.5)
        toggleButton.addBottomBorder(with: toggleButton.selectedBorderColor, andWidth: 1.5)
        if toggleButton.arrangement == .left {
            passwordTextField.becomeFirstResponder()
        }
        if let toggleDouble = toggleButton.toggleDoubleValue {
            CheckoutCart.shared.tipPercentage = toggleDouble
        }
        updateOrderTotal()
    }

    @objc private func choosePaymentButtonTapped(_: RoundButton) {
        // TODO: finish setting up payment method and implement submit order functionality. chain customer to their payment method in backend
        if paymentContext.selectedPaymentOption != nil {
            newActivityIndicator.startAnimating()
            paymentContext.requestPayment()
        } else {
            paymentContext.presentPaymentOptionsViewController()
        }
    }

    @objc private func changeDefaultPaymentMethodButtonClicked(_: RoundButton) {
        paymentContext.presentPaymentOptionsViewController()
    }
}

extension ReviewOrderViewController: STPApplePayContextDelegate {
    // this is the first method that runs when a user orders through apple pay
    func applePayContext(_: STPApplePayContext, didCreatePaymentMethod _: STPPaymentMethod, paymentInformation _: PKPayment, completion: @escaping STPIntentClientSecretCompletionBlock) {
        let orderToBeSubmitted = Order(checkoutCart: CheckoutCart.shared)
        orderToBeSubmitted.cardInformation = "Apple Pay"

        CheckoutCart.shared.currentOrder = orderToBeSubmitted
        orderToBeSubmitted.checkoutCart = CheckoutCart.shared

        StripeAPIClient.shared.createPaymentIntent(order: orderToBeSubmitted, quickPassData: nil) { result in
            switch result {
            case let .success(clientSecret):
                // Call the completion block with the client secret or an error
                completion(clientSecret, nil)
            case let .failure(error):
                if let error = error as? StripeAPIError {
                    switch error {
                    case .unknown:
                        completion(nil, error)
                    case .staleData:
                        self.businessError = true
                        completion(nil, error)
                    }
                }
                print("error in attaching payment method id \(error.localizedDescription)")
                completion(nil, error)
            }
        }
    }

    // this is the second method that runs when a user orders through apple pay
    func applePayContext(_: STPApplePayContext, didCompleteWith status: STPPaymentStatus, error _: Error?) {
        var message = ""
        var title = ""

        let group = DispatchGroup()
        group.enter()

        switch status {
        case .success:
            // Your backend asynchronously fulfills the customer's order, e.g. via webhook
            let request = try! APIRequest(method: .post, path: "/order/" + CheckoutCart.shared.sessionToken, body: CheckoutCart.shared.currentOrder)
            APIClient().perform(request) { result in
                switch result {
                case let .success(response):
                    if response.statusCode == 500 {
                        self.serverError = true
                        group.leave()
                    } else {
                        if response.statusCode == 200 {
                            if let response = try? response.decode(to: [String: OrderData].self) {
                                if let confirmedOrder = response.body["order"] {
                                    CheckoutCart.shared.currentOrder!.update(orderData: confirmedOrder)
                                    if !CheckoutCart.shared.activeOrders.isEmpty {
                                        for order in CheckoutCart.shared.activeOrders {
                                            order.hasBeenDeactivated = true
                                        }
                                    }
                                    CheckoutCart.shared.emptyCart()
                                    CoreDataManager.sharedManager.saveContext()
                                }
                            }
                        } else {
                            title = "Server error"
                            message = "Whoops, there's an issue on our end. We're figuring out what the problem is. Try again in a little bit!"
                        }
                        group.leave()
                    }
                case let .failure(error):
                    title = "Network Error"
                    message = "A network error has occured. Check your internet connection and if necessary, restart the app."

                    group.leave()
                    print("network error", error)
                }
            }

        case .error:
            // Payment failed, show the error
            group.leave()
        case .userCancellation:
            group.leave()
            // return so the alert controller is not presented
            return ()
        @unknown default:
            group.leave()
            return ()
        }
        group.notify(queue: .main, execute: {
            if title != "", message != "" {
                CoreDataManager.sharedManager.deleteEntity(entity: CheckoutCart.shared.currentOrder!)
                CheckoutCart.shared.currentOrder = nil
                self.presentAlertController(title: title, message: message)
            } else if self.businessError == true {
                CoreDataManager.sharedManager.deleteEntity(entity: CheckoutCart.shared.currentOrder!)
                CheckoutCart.shared.currentOrder = nil

                let businessName = CheckoutCart.shared.userBusiness?.formattedName
                CheckoutCart.shared.userBusiness?.isActive = false
                CheckoutCart.shared.userBusiness?.businessToUser = nil
                CheckoutCart.shared.user?.userToBusiness = nil
                CheckoutCart.shared.emptyCart()
                CoreDataManager.sharedManager.saveContext()

                self.presentStaleDataError(businessName: businessName!) {
                    self.dismiss(animated: true)
                }
            } else {
                SceneDelegate.shared.rootViewController.presentOrderConfirmationViewController(updatedOrder: CheckoutCart.shared.currentOrder!)
            }
            SceneDelegate.shared.rootViewController.homeViewController?.setConditionalUIProperties()
        })
    }
}

extension ReviewOrderViewController: STPPaymentContextDelegate {
    func paymentContext(_: STPPaymentContext, didFailToLoadWithError error: Error) {
        print(error)
    }

    func paymentContextDidChange(_: STPPaymentContext) {
        if paymentContext.selectedPaymentOption != nil {
            defaultPaymentMethodView.isHidden = false
            cardCompanyName.text = paymentContext.selectedPaymentOption?.label
            cardLogoImageView.image = paymentContext.selectedPaymentOption!.templateImage
            addPaymentMethodButton.refreshTitle(newTitle: "Pay with card")
        } else {
            defaultPaymentMethodView.isHidden = true
        }
    }

    func paymentContext(_ paymentContext: STPPaymentContext, didCreatePaymentResult paymentResult: STPPaymentResult, completion: @escaping STPPaymentStatusBlock) {
        let orderToBeSubmitted = Order(checkoutCart: CheckoutCart.shared)
        orderToBeSubmitted.cardInformation = cardCompanyName.text!
        CheckoutCart.shared.currentOrder = orderToBeSubmitted
        orderToBeSubmitted.checkoutCart = CheckoutCart.shared

        // stripe paymentIntentId is attached to order in this function
        StripeAPIClient.shared.createPaymentIntent(order: orderToBeSubmitted, quickPassData: nil) { result in
            switch result {
            case let .success(clientSecret):
                // Assemble the PaymentIntent parameters
                let paymentIntentParams = STPPaymentIntentParams(clientSecret: clientSecret)
                paymentIntentParams.paymentMethodId = paymentResult.paymentMethod?.stripeId

                // Confirm the PaymentIntent
                STPPaymentHandler.shared().confirmPayment(paymentIntentParams, with: paymentContext) { status, _, error in
                    switch status {
                    case .succeeded:
                        // Your backend asynchronously fulfills the customer's order, e.g. via webhook
                        let request = try! APIRequest(method: .post, path: "/order/" + CheckoutCart.shared.sessionToken, body: CheckoutCart.shared.currentOrder)
                        APIClient().perform(request) { result in
                            switch result {
                            case let .success(response):
                                if response.statusCode == 500 {
                                    self.serverError = true
                                    completion(.error, nil)
                                } else {
                                    if response.statusCode == 200 {
                                        if let response = try? response.decode(to: [String: OrderData].self) {
                                            if let confirmedOrder = response.body["order"] {
                                                CheckoutCart.shared.currentOrder!.update(orderData: confirmedOrder)
                                                if !CheckoutCart.shared.activeOrders.isEmpty {
                                                    for order in CheckoutCart.shared.activeOrders {
                                                        order.hasBeenDeactivated = true
                                                    }
                                                }
                                            }
                                        }
                                        completion(.success, nil)
                                    } else {
                                        self.serverError = true
                                        completion(.error, nil)
                                    }
                                }

                            case let .failure(error):
                                completion(.error, nil)
                                print("error", error)
                            }
                        }

                    case .failed:
                        completion(.error, error) // Report error
                    case .canceled:
                        completion(.userCancellation, nil) // Customer cancelled
                    @unknown default:
                        completion(.error, nil)
                    }
                }

            case let .failure(error):
                print("error in attaching payment method id \(error.localizedDescription)")
                completion(.error, error)
            }
        }
    }

    func paymentContext(_: STPPaymentContext, didFinishWith status: STPPaymentStatus, error: Error?) {
        let title: String
        switch status {
        case .error:
            title = "Error"
            print("error with order payment", error?.localizedDescription ?? "")
        case .success:
            CheckoutCart.shared.emptyCart()
            CoreDataManager.sharedManager.saveContext()
            title = "Success"
        case .userCancellation:
            return ()
        @unknown default:
            return ()
        }
        newActivityIndicator.stopAnimating()
        if title != "Success" {
            CoreDataManager.sharedManager.deleteEntity(entity: CheckoutCart.shared.currentOrder!)
            CheckoutCart.shared.currentOrder = nil
            if businessError == true {
                let businessName = CheckoutCart.shared.userBusiness?.formattedName
                CheckoutCart.shared.userBusiness?.isActive = false
                CheckoutCart.shared.userBusiness?.businessToUser = nil
                CheckoutCart.shared.user?.userToBusiness = nil
                CheckoutCart.shared.emptyCart()
                CoreDataManager.sharedManager.saveContext()
                presentStaleDataError(businessName: businessName!) {
                    self.dismiss(animated: true)
                }
            } else if serverError == true {
                presentServerError()
            } else {
                presentNetworkError()
            }
        } else {
            navigationController?.setViewControllers([OrderConfirmationViewController(SelectedOrder: CheckoutCart.shared.currentOrder!, IsModal: true)], animated: true)
        }
        SceneDelegate.shared.rootViewController.homeViewController?.setConditionalUIProperties()
    }
}
