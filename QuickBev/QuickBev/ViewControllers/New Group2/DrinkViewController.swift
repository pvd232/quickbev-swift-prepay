//
//  DrinkViewController.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 3/29/20.
//  Copyright © 2020 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
class DrinkViewController: UIViewController {
    private let orderDrink: OrderDrink?
    private let drink: Drink
    private let isCheckout: Bool
    private let spacerView = UIView(theme: Theme.UIView(props: []))
    private let drinkImageStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 30.0)))]))
    private let drinkDescriptionTextView = UITextView(theme: Theme.UIView(props: []))
    private let drinkImageView = RoundedImageView(theme: Theme.UIView(props: []))
    private let drinkImage: UIImage
    private let drinkQuantityStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(Float(getHeightRatio(verticalConstant: 30.0)))]))
    private let drinkQuantityLabel = UILabel(theme: Theme.UILabel(props: [.textColor]))

    private let minusButton = RoundButton(theme: Theme.UIView(props: []))
    private let plusButton = RoundButton(theme: Theme.UIView(props: []))

    private let addToOrderButton = UIButton(theme: Theme.UIView(props: []))
    private let removeFromOrderButton = UIButton(theme: Theme.UIView(props: []))
    @UsesAutoLayout private var newActivityIndicator = UIActivityIndicatorView(style: .large)

    private var drinkQuantity = 1
    init(drink: Drink? = nil, orderDrink: OrderDrink? = nil, isCheckout: Bool, drinkUIImage: UIImage) {
        self.orderDrink = orderDrink
        self.isCheckout = isCheckout
        if isCheckout == true, let orderDrink = orderDrink {
            self.drink = orderDrink.drink
            drinkQuantity = Int(orderDrink.quantity)
        } else {
            self.drink = drink!
        }
        drinkImage = drinkUIImage
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
        setNonModalNavbar()
        setupHeaderView(otherView: spacerView, header: drink.formattedName)
    }

    override func viewWillAppear(_: Bool) {
        navigationController?.isHiddenHairline = true
    }

    override func viewWillDisappear(_: Bool) {
        CheckoutCart.shared.calculateCost()
        CoreDataManager.sharedManager.saveContext()
    }

    private func setupView() {
        view.backgroundColor = .white

        view.addSubview(drinkImageStackView)
        view.addSubview(drinkQuantityStackView)
        view.addSubview(addToOrderButton)
        view.addSubview(newActivityIndicator)
        view.addSubview(spacerView)
        newActivityIndicator.backgroundColor = UIColor(white: 0, alpha: 0.4)

        drinkImageView.image = drinkImage

        drinkImageStackView.alignment = .center
        drinkImageStackView.addArrangedSubview(drinkImageView)
        drinkImageStackView.addArrangedSubview(drinkDescriptionTextView)

        drinkQuantityStackView.axis = .horizontal
        drinkQuantityStackView.spacing = getWidthRatio(horizontalConstant: 30.0)
        drinkQuantityStackView.addArrangedSubview(minusButton)
        drinkQuantityStackView.addArrangedSubview(drinkQuantityLabel)

        drinkQuantityStackView.addArrangedSubview(plusButton)

        drinkDescriptionTextView.backgroundColor = .clear
        drinkDescriptionTextView.textColor = .black
        drinkDescriptionTextView.isEditable = false

        // if isCheckout, add the remove drink button to the view
        let safeArea = view.safeAreaLayoutGuide

        if isCheckout == true {
            view.addSubview(removeFromOrderButton)
            removeFromOrderButton.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.4).isActive = true
            removeFromOrderButton.setTitle("Remove drink", for: .normal)
            removeFromOrderButton.titleLabel?.font = UIFont(name: "GillSans", size: getFontRatio(fontSize: 24.0))
            removeFromOrderButton.setTitleColor(UIColor.red, for: .normal)
            removeFromOrderButton.topAnchor.constraint(equalTo: drinkQuantityStackView.bottomAnchor, constant: UIViewController.screenSize.height * 0.01).isActive = true
            removeFromOrderButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            removeFromOrderButton.heightAnchor.constraint(equalTo: safeArea.heightAnchor, multiplier: 0.09).isActive = true
        }

        let drinkDescriptionText = "\(drink.detail)\n\(drink.price.formattedCurrency().formattedCurrencyString())"

        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        style.alignment = .center
        let attributes = [NSAttributedString.Key.paragraphStyle: style, NSAttributedString.Key.font: UIFont(name: "GillSans", size: getFontRatio(fontSize: 20.0))!]
        drinkDescriptionTextView.attributedText = NSAttributedString(string: drinkDescriptionText, attributes: attributes)

        drinkQuantityLabel.text = String(drinkQuantity)
        drinkQuantityLabel.font = UIFont(name: "GillSans", size: getFontRatio(fontSize: 40.0))
        drinkQuantityLabel.textAlignment = .center

        minusButton.setTitle("-", for: .normal)
        minusButton.titleLabel?.font = UIFont.systemFont(ofSize: getFontRatio(fontSize: 55.0))
        minusButton.contentVerticalAlignment = .bottom
        minusButton.refreshColor(color: UIColor.themeColor)
        minusButton.refreshCorners(value: getWidthRatio(horizontalConstant: 30.0))

        plusButton.cornerRadius = plusButton.safeAreaLayoutGuide.layoutFrame.width / 2
        plusButton.setTitle("+", for: .normal)
        plusButton.titleLabel?.font = UIFont.systemFont(ofSize: getFontRatio(fontSize: 45.0))
        plusButton.refreshCorners(value: getWidthRatio(horizontalConstant: 30.0))
        plusButton.contentVerticalAlignment = .top
        plusButton.refreshColor(color: UIColor.themeColor)
        if isCheckout == true {
            addToOrderButton.setTitle("Update order", for: .normal)
        } else {
            addToOrderButton.setTitle("Add to cart", for: .normal)
        }
        addToOrderButton.backgroundColor = UIColor.themeColor
        addToOrderButton.titleLabel?.font = UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 35.0))
        addToOrderButton.contentHorizontalAlignment = .center

        minusButton.addTarget(self, action: #selector(decreaseDrinkQuantity), for: .touchUpInside)
        plusButton.addTarget(self, action: #selector(increaseDrinkQuantity), for: .touchUpInside)
        addToOrderButton.addTarget(self, action: #selector(addToOrderButtonPressed), for: .touchUpInside)
        removeFromOrderButton.addTarget(self, action: #selector(removeButtonPressed), for: .touchUpInside)
    }

    private func setupConstraints() {
        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            addToOrderButton.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),

            drinkImageView.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.35),
            drinkImageView.heightAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.45),

            drinkImageStackView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            drinkImageStackView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            drinkImageStackView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -10.0)),

            drinkQuantityStackView.topAnchor.constraint(equalTo: drinkImageStackView.bottomAnchor, constant: getHeightRatio(verticalConstant: 15)),
            drinkQuantityStackView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),

            drinkDescriptionTextView.leadingAnchor.constraint(equalTo: drinkImageStackView.leadingAnchor),
            drinkDescriptionTextView.trailingAnchor.constraint(equalTo: drinkImageStackView.trailingAnchor),

            spacerView.bottomAnchor.constraint(equalTo: drinkImageStackView.topAnchor, constant: getHeightRatio(verticalConstant: -80.0)),

            addToOrderButton.topAnchor.constraint(greaterThanOrEqualTo: drinkImageStackView.bottomAnchor, constant: getHeightRatio(verticalConstant: 70.0)),
            addToOrderButton.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            addToOrderButton.widthAnchor.constraint(equalTo: addToOrderButton.superview!.widthAnchor),
            addToOrderButton.heightAnchor.constraint(equalTo: safeArea.heightAnchor, multiplier: 0.143032),
            addToOrderButton.bottomAnchor.constraint(equalTo: addToOrderButton.superview!.bottomAnchor),

            minusButton.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.144928),
            minusButton.heightAnchor.constraint(equalTo: minusButton.widthAnchor, multiplier: 1.0),

            plusButton.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.144928),
            plusButton.heightAnchor.constraint(equalTo: plusButton.widthAnchor, multiplier: 1.0),

            newActivityIndicator.topAnchor.constraint(equalTo: safeArea.topAnchor),
            newActivityIndicator.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            newActivityIndicator.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            newActivityIndicator.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),

        ])

        if drinkDescriptionTextView.text.count >= 30 {
            drinkImageStackView.heightAnchor.constraint(equalTo: safeArea.heightAnchor, multiplier: 0.5).isActive = true
        } else {
            drinkImageStackView.heightAnchor.constraint(equalTo: safeArea.heightAnchor, multiplier: 0.4).isActive = true
        }
    }

    @objc private func increaseDrinkQuantity() {
        drinkQuantity += 1
        drinkQuantityLabel.text = drinkQuantity.description
    }

    @objc private func decreaseDrinkQuantity() {
        guard drinkQuantity != 1 else {
            return
        }
        drinkQuantity -= 1
        drinkQuantityLabel.text = drinkQuantity.description
    }

    @objc private func removeButtonPressed() {
        let delay = 300 // miliseconds
        newActivityIndicator.startAnimating()
        CheckoutCart.shared.removeFromOrderDrink(orderDrink!)
        drink.removeFromOrderDrink(orderDrink!)
        CoreDataManager.sharedManager.deleteEntity(entity: orderDrink!)
        CoreDataManager.sharedManager.saveContext()
        SceneDelegate.shared.rootViewController.homeViewController?.setConditionalUIProperties()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(delay)) {
            SceneDelegate.shared.rootViewController.activityIndicator.stopAnimating()
            self.navigationController!.popToRootViewController(animated: true)
        }
    }

    @objc private func addToOrderButtonPressed(_: RoundButton) {
        newActivityIndicator.startAnimating()
        let delay = 300 // miliseconds
        if isCheckout == true {
            orderDrink!.quantity = Int16(drinkQuantity)
            CoreDataManager.sharedManager.saveContext()
            SceneDelegate.shared.rootViewController.homeViewController?.setConditionalUIProperties()
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(delay)) {
                SceneDelegate.shared.rootViewController.activityIndicator.stopAnimating()
                self.dismiss(animated: true, completion: nil)
            }
        } else {
            let newOrderDrink = OrderDrink(drink: drink)
            newOrderDrink.quantity = Int16(drinkQuantity)

            newOrderDrink.drink = drink
            newOrderDrink.checkoutCart = CheckoutCart.shared

            drink.addToOrderDrink(newOrderDrink)
            CheckoutCart.shared.addToOrderDrink(newOrderDrink)

            CoreDataManager.sharedManager.saveContext()
            SceneDelegate.shared.rootViewController.homeViewController?.setConditionalUIProperties()
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(delay)) {
                SceneDelegate.shared.rootViewController.activityIndicator.stopAnimating()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
