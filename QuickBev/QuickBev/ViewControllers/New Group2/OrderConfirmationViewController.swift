//
//  OrderConfirmationViewController.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 1/7/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit

class OrderConfirmationViewController: UIViewController {
    private var dataSource: UITableViewDiffableDataSource<Section, DrinkItem>!
    private let orderItemsTableView = UITableView(theme: Theme.UIView(props: []))

    private var drinkItems = [DrinkItem]()

    private let imagesStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical]))

    private let locationImageView = UIImageView(theme: Theme.UIView(props: []))
    private let locationImage = UIImage(systemName: "mappin")

    private let walkUpImageView = UIImageView(theme: Theme.UIView(props: []))
    private let walkUpImage = UIImage(systemName: "figure.walk")

    private let orderItemsImageView = UIImageView(theme: Theme.UIView(props: []))
    private let orderItemsImage = UIImage(systemName: "list.bullet")

    private let pickupDetailsStackView = UIStackView(theme: Theme.UIView(props: []))
    private let pickupAddressLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Location"), .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 22.0)))]))

    private let locationStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical]))
    private let businessNameLabel = UILabel(theme: Theme.UILabel(props: [.textColor]))
    private let addressLabel = UILabel(theme: Theme.UILabel(props: [.textColor]))

    private let pickupInstructionsStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical]))
    private let pickupInstructionsLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("How to Get Your Drink"), .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 22.0)))]))
    private let pickupInstructionsText = UILabel(theme: Theme.UILabel(props: [.font(.themeLabelFont), .textColor]))

    private let orderItemsContainerView = UIView(theme: Theme.UIView(props: []))
    private let orderItemsStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical]))
    private let orderItemsLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Order Items"), .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 22.0)))]))

    private let orderStatusContainerView = UIView(theme: Theme.UIView(props: []))
    private let orderConfirmedHeaderImageView = UIImageView(theme: Theme.UIView(props: []))
    private let orderConfirmedHeaderImage = UIImage(systemName: "hand.raised.fill")
    private let orderStatusHeader = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Order Status"), .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 22.0)))]))

    private let orderStatusImageView = UIImageView(theme: Theme.UIView(props: []))
    private var orderStatusImage = UIImage()
    private let orderStatusLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 17.0)))]))

    private let containerView = UIView(theme: Theme.UIView(props: []))
    private var userBusiness: Business!
    var selectedOrder: Order!
    private let localIsModal: Bool!

    init(SelectedOrder: Order, IsModal: Bool) {
        selectedOrder = SelectedOrder
        localIsModal = IsModal
        let timeToWait = Int(Double(SelectedOrder.activeOrderCount) * 1.5)
        pickupInstructionsText.text = "Your are number \(SelectedOrder.activeOrderCount.description) in line. Head to the bar in \(timeToWait) min and join the line for QuickBev orders."
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if let business = CheckoutCart.shared.businessList.first(where: {
            $0.id == selectedOrder.businessId
        }) {
            userBusiness = business
        }
        addViews()
        setViewPropertyAttributes()
        setViewContraints()
        setupTableView()
        loadTable()
        setupHeaderView(otherView: pickupDetailsStackView, header: "Order Details", distanceFromHeader: getHeightRatio(verticalConstant: -20.0))
        if localIsModal == true {
            setNavbar()
        } else {
            setNonModalNavbar()
        }
    }

    @objc private func backButton() {
        navigationController?.popViewController(animated: true)
    }

    private func addViews() {
        navigationItem.setHidesBackButton(true, animated: false)
        view.addSubview(walkUpImageView)
        view.addSubview(locationImageView)
        view.addSubview(orderItemsImageView)
        view.addSubview(pickupDetailsStackView)
        view.addSubview(orderItemsTableView)
        view.addSubview(orderStatusContainerView)
        view.addSubview(orderConfirmedHeaderImageView)

        orderItemsTableView.allowsSelection = false
        pickupDetailsStackView.addArrangedSubview(locationStackView)
        pickupDetailsStackView.addArrangedSubview(pickupInstructionsStackView)
        pickupDetailsStackView.addArrangedSubview(orderItemsStackView)

        orderStatusContainerView.addSubview(orderStatusHeader)
        orderStatusContainerView.addSubview(orderStatusImageView)
        orderStatusContainerView.addSubview(orderStatusLabel)

        locationStackView.addArrangedSubview(pickupAddressLabel)
        locationStackView.addArrangedSubview(businessNameLabel)
        locationStackView.addArrangedSubview(addressLabel)

        pickupInstructionsStackView.addArrangedSubview(pickupInstructionsLabel)
        pickupInstructionsStackView.addArrangedSubview(pickupInstructionsText)

        orderItemsStackView.addArrangedSubview(orderItemsLabel)
    }

    private func setViewPropertyAttributes() {
        pickupDetailsStackView.axis = .vertical
        pickupDetailsStackView.spacing = CGFloat(getHeightRatio(verticalConstant: 30.0))

        locationStackView.spacing = CGFloat(getHeightRatio(verticalConstant: 5.0))
        pickupInstructionsStackView.spacing = CGFloat(getHeightRatio(verticalConstant: 5.0))

        businessNameLabel.text = userBusiness.formattedName
        businessNameLabel.font = UIFont.themeLabelFont

        addressLabel.lineBreakMode = .byWordWrapping
        addressLabel.numberOfLines = 0
        addressLabel.text = userBusiness!.address
        addressLabel.font = UIFont.themeLabelFont

        pickupInstructionsText.numberOfLines = 0

        orderStatusImageView.image = orderStatusImage
        orderStatusImageView.tintColor = .themeColor

        locationImageView.image = locationImage?.withRenderingMode(.alwaysTemplate)
        locationImageView.tintColor = .themeColor

        walkUpImageView.image = walkUpImage?.withRenderingMode(.alwaysTemplate)
        walkUpImageView.tintColor = .themeColor

        orderItemsImageView.image = orderItemsImage?.withRenderingMode(.alwaysTemplate)
        orderItemsImageView.tintColor = .themeColor

        orderConfirmedHeaderImageView.image = orderConfirmedHeaderImage?.withRenderingMode(.alwaysTemplate)
        orderConfirmedHeaderImageView.tintColor = .themeColor

        if selectedOrder.refunded == true {
            orderStatusImage = UIImage(systemName: "xmark")!
            orderStatusImageView.image = orderStatusImage.withRenderingMode(.alwaysTemplate)
            orderStatusImageView.tintColor = .systemRed

            orderStatusLabel.textColor = .systemRed
            orderStatusLabel.text = "Order Refunded"
        } else if selectedOrder.active == true {
            orderStatusImage = UIImage(systemName: "checkmark.circle")!
            orderStatusImageView.image = orderStatusImage.withRenderingMode(.alwaysTemplate)
            orderStatusImageView.tintColor = .themeColor

            orderStatusLabel.textColor = .themeColor
            orderStatusLabel.text = "Order Confirmed"
        } else if selectedOrder.completed == true {
            orderStatusImage = UIImage(systemName: "checkmark.circle")!
            orderStatusImageView.image = orderStatusImage.withRenderingMode(.alwaysTemplate)
            orderStatusImageView.tintColor = UIColor.systemGreen.mix(with: UIColor.white, amount: 0.3)

            orderStatusLabel.textColor = UIColor.systemGreen.mix(with: UIColor.white, amount: 0.3)
            orderStatusLabel.text = "Order Completed"
        }
    }

    private func setViewContraints() {
        let safeArea = view.safeAreaLayoutGuide

        NSLayoutConstraint.activate([
            locationImageView.centerYAnchor.constraint(equalTo: locationStackView.centerYAnchor),
            locationImageView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.05),
            locationImageView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            locationImageView.trailingAnchor.constraint(equalTo: pickupDetailsStackView.leadingAnchor, constant: getWidthRatio(horizontalConstant: -15.0)),

            walkUpImageView.centerYAnchor.constraint(equalTo: pickupInstructionsStackView.centerYAnchor, constant: getHeightRatio(verticalConstant: 5.0)),
            walkUpImageView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.05),
            walkUpImageView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            walkUpImageView.trailingAnchor.constraint(equalTo: pickupDetailsStackView.leadingAnchor, constant: getWidthRatio(horizontalConstant: -15.0)),

            orderItemsImageView.centerYAnchor.constraint(equalTo: orderItemsStackView.centerYAnchor),
            orderItemsImageView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.05),
            orderItemsImageView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            orderItemsImageView.trailingAnchor.constraint(equalTo: pickupDetailsStackView.leadingAnchor, constant: getWidthRatio(horizontalConstant: -15.0)),

            pickupDetailsStackView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -20.0)),
            // this constraint sets the icons to be the proper width because they are constrained to the stackView's leading constraint
            pickupDetailsStackView.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.8),

            orderStatusContainerView.topAnchor.constraint(equalTo: orderItemsTableView.bottomAnchor, constant: getHeightRatio(verticalConstant: 15.0)),
            orderStatusContainerView.leadingAnchor.constraint(equalTo: pickupDetailsStackView.leadingAnchor),
            orderStatusContainerView.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.35),

            orderStatusHeader.topAnchor.constraint(equalTo: orderStatusContainerView.topAnchor),

            orderConfirmedHeaderImageView.centerYAnchor.constraint(equalTo: orderStatusImageView.centerYAnchor),
            orderConfirmedHeaderImageView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.05),
            orderConfirmedHeaderImageView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            orderConfirmedHeaderImageView.trailingAnchor.constraint(equalTo: pickupDetailsStackView.leadingAnchor, constant: getWidthRatio(horizontalConstant: -15.0)),

            orderStatusImageView.centerXAnchor.constraint(equalTo: orderStatusHeader.centerXAnchor),
            orderStatusImageView.topAnchor.constraint(equalTo: orderStatusHeader.bottomAnchor, constant: getHeightRatio(verticalConstant: 5.0)),
            orderStatusImageView.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.13),
            orderStatusImageView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.067),

            orderStatusLabel.topAnchor.constraint(equalTo: orderStatusImageView.bottomAnchor, constant: getHeightRatio(verticalConstant: 5.0)),
            orderStatusLabel.bottomAnchor.constraint(equalTo: orderStatusContainerView.bottomAnchor),
            orderStatusLabel.centerXAnchor.constraint(equalTo: orderStatusHeader.centerXAnchor),
        ])
    }

    private func loadTable() {
        dataSource = UITableViewDiffableDataSource<Section, DrinkItem>(tableView: orderItemsTableView) {
            (orderItemsTableView: UITableView, indexPath: IndexPath, drinkItem: DrinkItem) -> UITableViewCell? in
                let cell = orderItemsTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
                cell.contentConfiguration = nil

                cell.clipsToBounds = true
                let content = OrderConfirmationContentConfiguration(drinkItem: drinkItem)

                cell.contentConfiguration = content

                if let url = drinkItem.url as NSURL? {
                    ImageCache.publicCache.load(url: url as NSURL, item: drinkItem) { fetchedItem, image in
                        if let img = image, img != fetchedItem.image {
                            var updatedSnapshot = self.dataSource.snapshot()
                            if let datasourceIndex = updatedSnapshot.indexOfItem(fetchedItem as! DrinkItem) {
                                let drinkItem = self.drinkItems[datasourceIndex]
                                drinkItem.image = img
                                updatedSnapshot.reloadItems([drinkItem])
                                self.dataSource.apply(updatedSnapshot, animatingDifferences: false)
                            }
                        }
                    }
                }
                return cell
        }
        dataSource.defaultRowAnimation = .fade

        // Get our image URLs for processing.
        if drinkItems.isEmpty, !selectedOrder.orderDrinkArray.isEmpty {
            for index in 0 ..< selectedOrder.orderDrinkArray.count {
                drinkItems.append(DrinkItem(orderDrink: selectedOrder.orderDrinkArray[index]))
            }
            var initialSnapshot = NSDiffableDataSourceSnapshot<Section, DrinkItem>()
            initialSnapshot.appendSections([.main])
            initialSnapshot.appendItems(drinkItems)
            dataSource.apply(initialSnapshot, animatingDifferences: false)
        }
    }

    func updateOrderStatus(updatedOrder: Order) {
        if updatedOrder.completed != selectedOrder.completed || updatedOrder.refunded != selectedOrder.refunded || updatedOrder.active != selectedOrder.active {
            if selectedOrder.refunded == true {
                let orderRefundedImage = UIImage(systemName: "xmark")
                orderStatusLabel.textColor = .systemRed
                orderStatusImageView.image = orderRefundedImage?.withRenderingMode(.alwaysTemplate)
                orderStatusImageView.tintColor = .systemRed
                pickupInstructionsText.text = "Unfortunately, the drink you ordered is out of stock. You have been refunded to your original payment method."
            } else if selectedOrder.completed == true {
                orderStatusLabel.textColor = UIColor.systemGreen.mix(with: UIColor.white, amount: 0.3)
                orderStatusImageView.tintColor = UIColor.systemGreen.mix(with: UIColor.white, amount: 0.3)
            }
        }
        selectedOrder = updatedOrder
    }

    private func setupTableView() {
        view.backgroundColor = .white

        view.addSubview(orderItemsTableView)
        view.addSubview(orderItemsContainerView)

        orderItemsContainerView.addSubview(orderItemsTableView)

        orderItemsTableView.backgroundColor = .white
        orderItemsTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        orderItemsTableView.separatorColor = .clear

        let safeArea = view.safeAreaLayoutGuide
        if selectedOrder.orderDrinkArray.count > 1 {
            NSLayoutConstraint.activate([
                orderItemsTableView.heightAnchor.constraint(equalTo: safeArea.heightAnchor, multiplier: 0.30),
            ])
        } else {
            NSLayoutConstraint.activate([
                orderItemsTableView.heightAnchor.constraint(equalTo: safeArea.heightAnchor, multiplier: 0.15),
            ])
        }
        NSLayoutConstraint.activate([
            orderItemsTableView.topAnchor.constraint(equalTo: pickupDetailsStackView.bottomAnchor, constant: getHeightRatio(verticalConstant: 30.0)),
            orderItemsTableView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 0.0)),
            orderItemsTableView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: 0.0)),
            orderItemsContainerView.topAnchor.constraint(equalTo: orderItemsTableView.topAnchor),
            orderItemsContainerView.leadingAnchor.constraint(equalTo: orderItemsTableView.leadingAnchor),
            orderItemsContainerView.trailingAnchor.constraint(equalTo: orderItemsTableView.trailingAnchor),
            orderItemsContainerView.bottomAnchor.constraint(equalTo: orderItemsTableView.bottomAnchor),
        ])

        orderItemsTableView.delegate = self
        orderItemsContainerView.addTopBorder(with: UIColor.lightGray.withAlphaComponent(0.5), andWidth: 1.0)
        orderItemsContainerView.addBottomBorder(with: UIColor.lightGray.withAlphaComponent(0.5), andWidth: 1.0)
    }

    override func viewWillAppear(_: Bool) {
        if let indexPath = orderItemsTableView.indexPathForSelectedRow {
            orderItemsTableView.deselectRow(at: indexPath, animated: false)
        }
        navigationController?.isHiddenHairline = true
        updateOrderStatus(updatedOrder: selectedOrder)
    }
}

extension OrderConfirmationViewController: UITableViewDelegate {
    func numberOfSections(in _: UITableView) -> Int {
        return 1
    }

    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        // if there is only one order the two row table looks awkward
        return selectedOrder.orderDrinkArray.count
    }

    func tableView(_ orderItemsTableView: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        if selectedOrder.orderDrinkArray.count > 1 {
            return (orderItemsTableView.frame.height / 2)
        } else {
            return (orderItemsTableView.frame.height)
        }
    }

    func tableView(_: UITableView, didSelectRowAt _: IndexPath) {}
}
