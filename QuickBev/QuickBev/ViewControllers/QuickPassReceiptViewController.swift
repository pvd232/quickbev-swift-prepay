//
//  QuickPassReceiptViewController.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 12/14/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//
import UIKit

class QuickPassReceiptViewController: UIViewController {
    private let locationImageView = UIImageView(theme: Theme.UIView(props: []))
    private let locationImage = UIImage(systemName: "mappin")

    private let dateTimeImageView = UIImageView(theme: Theme.UIView(props: []))
    private let dateTimeImage = UIImage(systemName: "calendar")

    private let expirationTimeImageView = UIImageView(theme: Theme.UIView(props: []))
    private let expirationTimeImage = UIImage(systemName: "calendar")

    private let orderFinancialsImageView = UIImageView(theme: Theme.UIView(props: []))
    private let orderFinancialsImage = UIImage(systemName: "dollarsign.circle")

    private let paymentMethodImageView = UIImageView(theme: Theme.UIView(props: []))
    private let paymentMethodImage = UIImage(systemName: "creditcard")

    private let quickPassReceiptDetailsStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 30.0)))]))

    private let locationLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Location"), .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 22.0)))]))
    private let locationStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical]))

    private let businessNameLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.themeLabelFont)]))
    private let addressLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.themeLabelFont)]))

    private let dateTimeStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical]))
    private let dateTimeLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Date & Time"), .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 22.0)))]))
    private let dateTimeValueLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.themeLabelFont)]))

    private let expirationTimeStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical]))
    private let expirationTimeLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Expiration Time"), .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 22.0)))]))
    private let expirationTimeValueLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.themeLabelFont)]))

    private let orderFinancialsStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical]))
    private let orderFinancialsLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Order Details"), .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 22.0)))]))

    private let paymentMethodStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical]))
    private let paymentMethodLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Payment Method"), .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 22.0)))]))
    private let paymentMethodValue = UILabel(theme: Theme.UILabel(props: [.font(UIFont.themeLabelFont), .textColor]))

    private let totalCalculationStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(Float(getWidthRatio(horizontalConstant: 20.0)))]))

    private let calcLabelsStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 5.0)))]))
    private let calcValuesStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 5.0)))]))

    private let taxLabel = UILabel(theme: Theme.UILabel(props: [.text("Sales Tax"), .font(UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 22.0)))]))
    private let taxValueLabel = UILabel(theme: Theme.UILabel(props: [.font(UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 22.0)))]))

    private let subtotalLabel = UILabel(theme: Theme.UILabel(props: [.text("Subtotal"), .font(UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 22.0)))]))
    private let subtotalValueLabel = UILabel(theme: Theme.UILabel(props: [.font(UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 22.0)))]))

    private let totalLabel = UILabel(theme: Theme.UILabel(props: [.text("Total"), .font(UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 22.0))), .textColor]))
    private let totalValueLabel = UILabel(theme: Theme.UILabel(props: [.font(UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 22.0))), .textColor]))

    private let containerView = UIView(theme: Theme.UIView(props: []))
    private var userBusiness: Business!
    private var selectedQuickPass: QuickPass!
    private let localIsModal: Bool!

    init(SelectedQuickPass: QuickPass, IsModal: Bool) {
        selectedQuickPass = SelectedQuickPass
        localIsModal = IsModal
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if let business = CheckoutCart.shared.businessList.first(where: { $0.id == selectedQuickPass.businessId }) {
            userBusiness = business
        }
        addViews()
        setViewPropertyAttributes()
        setViewContraints()
        setupHeaderView(otherView: quickPassReceiptDetailsStackView, header: "Receipt", distanceFromHeader: getHeightRatio(verticalConstant: -20.0))
        if localIsModal == true {
            setNavbar()
        } else {
            setNonModalNavbar()
        }
    }

    private func addViews() {
        navigationItem.setHidesBackButton(true, animated: false)
        view.addSubview(locationImageView)
        view.addSubview(dateTimeImageView)
        view.addSubview(expirationTimeImageView)
        view.addSubview(paymentMethodImageView)
        view.addSubview(orderFinancialsImageView)
        view.addSubview(quickPassReceiptDetailsStackView)
        view.addSubview(totalCalculationStackView)

        quickPassReceiptDetailsStackView.addArrangedSubview(locationStackView)
        quickPassReceiptDetailsStackView.addArrangedSubview(dateTimeStackView)
        quickPassReceiptDetailsStackView.addArrangedSubview(expirationTimeStackView)
        quickPassReceiptDetailsStackView.addArrangedSubview(paymentMethodStackView)
        quickPassReceiptDetailsStackView.addArrangedSubview(orderFinancialsStackView)

        locationStackView.addArrangedSubview(locationLabel)
        locationStackView.addArrangedSubview(businessNameLabel)
        locationStackView.addArrangedSubview(addressLabel)

        dateTimeStackView.addArrangedSubview(dateTimeLabel)
        dateTimeStackView.addArrangedSubview(dateTimeValueLabel)

        expirationTimeStackView.addArrangedSubview(expirationTimeLabel)
        expirationTimeStackView.addArrangedSubview(expirationTimeValueLabel)

        orderFinancialsStackView.addArrangedSubview(orderFinancialsLabel)

        paymentMethodStackView.addArrangedSubview(paymentMethodLabel)
        paymentMethodStackView.addArrangedSubview(paymentMethodValue)

        totalCalculationStackView.addArrangedSubview(calcLabelsStackView)
        totalCalculationStackView.addArrangedSubview(calcValuesStackView)

        calcLabelsStackView.addArrangedSubview(subtotalLabel)
        calcLabelsStackView.addArrangedSubview(taxLabel)
        calcLabelsStackView.addArrangedSubview(totalLabel)

        calcValuesStackView.addArrangedSubview(subtotalValueLabel)
        calcValuesStackView.addArrangedSubview(taxValueLabel)
        calcValuesStackView.addArrangedSubview(totalValueLabel)
    }

    private func setViewPropertyAttributes() {
        locationStackView.spacing = CGFloat(getHeightRatio(verticalConstant: 5.0))
        dateTimeStackView.spacing = CGFloat(getHeightRatio(verticalConstant: 5.0))
        expirationTimeStackView.spacing = CGFloat(getHeightRatio(verticalConstant: 5.0))
        paymentMethodStackView.spacing = CGFloat(getHeightRatio(verticalConstant: 5.0))

        businessNameLabel.text = userBusiness.formattedName

        addressLabel.lineBreakMode = .byWordWrapping
        addressLabel.numberOfLines = 0
        addressLabel.text = userBusiness!.address

        taxValueLabel.textColor = .systemGray.mix(with: .black, amount: 0.3)
        taxLabel.textColor = .systemGray.mix(with: .black, amount: 0.3)

        subtotalValueLabel.textColor = .systemGray.mix(with: .black, amount: 0.3)
        subtotalLabel.textColor = .systemGray.mix(with: .black, amount: 0.3)

        dateTimeValueLabel.numberOfLines = 0
        dateTimeValueLabel.text = selectedQuickPass.orderDate + " at " + selectedQuickPass.orderTime

        expirationTimeValueLabel.numberOfLines = 0
        expirationTimeValueLabel.text = selectedQuickPass.orderDate + " at " + selectedQuickPass.formattedExpirationTime

        paymentMethodValue.text = selectedQuickPass.cardInformation

        locationImageView.image = locationImage?.withRenderingMode(.alwaysTemplate)
        locationImageView.tintColor = .themeColor

        dateTimeImageView.image = dateTimeImage?.withRenderingMode(.alwaysTemplate)
        dateTimeImageView.tintColor = .themeColor

        expirationTimeImageView.image = dateTimeImage?.withRenderingMode(.alwaysTemplate)
        expirationTimeImageView.tintColor = .themeColor

        paymentMethodImageView.image = paymentMethodImage?.withRenderingMode(.alwaysTemplate)
        paymentMethodImageView.tintColor = .themeColor

        orderFinancialsImageView.image = orderFinancialsImage?.withRenderingMode(.alwaysTemplate)
        orderFinancialsImageView.tintColor = .themeColor

        subtotalValueLabel.text = selectedQuickPass.subtotal.formattedCurrency().formattedCurrencyString()
        taxValueLabel.text = selectedQuickPass.salesTaxTotal.formattedCurrency().formattedCurrencyString()
        totalValueLabel.text = selectedQuickPass.total.formattedCurrency().formattedCurrencyString()
    }

    private func setViewContraints() {
        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            locationImageView.centerYAnchor.constraint(equalTo: locationStackView.centerYAnchor),
            locationImageView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            locationImageView.trailingAnchor.constraint(equalTo: quickPassReceiptDetailsStackView.leadingAnchor, constant: getWidthRatio(horizontalConstant: -25.0)),
            locationImageView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.05),

            dateTimeImageView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            dateTimeImageView.trailingAnchor.constraint(equalTo: quickPassReceiptDetailsStackView.leadingAnchor, constant: getWidthRatio(horizontalConstant: -15.0)),
            dateTimeImageView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.05),
            dateTimeImageView.topAnchor.constraint(equalTo: dateTimeLabel.topAnchor),

            expirationTimeImageView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            expirationTimeImageView.trailingAnchor.constraint(equalTo: quickPassReceiptDetailsStackView.leadingAnchor, constant: getWidthRatio(horizontalConstant: -15.0)),
            expirationTimeImageView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.05),
            expirationTimeImageView.topAnchor.constraint(equalTo: expirationTimeLabel.topAnchor),

            paymentMethodImageView.topAnchor.constraint(equalTo: paymentMethodLabel.topAnchor),
            paymentMethodImageView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            paymentMethodImageView.trailingAnchor.constraint(equalTo: quickPassReceiptDetailsStackView.leadingAnchor, constant: getWidthRatio(horizontalConstant: -15.0)),
            paymentMethodImageView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.05),

            orderFinancialsImageView.topAnchor.constraint(equalTo: orderFinancialsLabel.topAnchor),
            orderFinancialsImageView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            orderFinancialsImageView.trailingAnchor.constraint(equalTo: quickPassReceiptDetailsStackView.leadingAnchor, constant: getWidthRatio(horizontalConstant: -15.0)),
            orderFinancialsImageView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.05),

            quickPassReceiptDetailsStackView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -20.0)),

            totalCalculationStackView.topAnchor.constraint(equalTo: quickPassReceiptDetailsStackView.bottomAnchor, constant: getHeightRatio(verticalConstant: 10.0)),
            totalCalculationStackView.leadingAnchor.constraint(equalTo: quickPassReceiptDetailsStackView.leadingAnchor),

        ])
    }

    override func viewWillAppear(_: Bool) {
        navigationController?.isHiddenHairline = true
    }
}
