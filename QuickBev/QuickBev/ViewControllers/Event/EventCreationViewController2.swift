//
//  TabCreationViewController2.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 12/21/20.
//  Copyright © 2020 Peter Vail Driscoll II. All rights reserved.
//

import UIKit

protocol TabCreationSecondPagePickerProtocol {
    func selectedValues(_ selectedValues: String)
}

class TabCreationViewController2: UIViewController {
    let submitButton = RoundButton(theme: Theme.RoundButton(props: [.color, .text("Next"), .titleLabelFont(UIFont.themeButtonFont)]))
    let eventPrivacySegmentedControl = UISegmentedControl(theme: Theme.UIView(props: []))

    init() {
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        eventPrivacySegmentedControl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(eventPrivacyEventHandler)))

        let attributes = [NSAttributedString.Key.font: UIFont.primaryTableCellContentViewFont]
        navigationController!.navigationBar.standardAppearance.titleTextAttributes = attributes
    }

    @objc func eventPrivacyEventHandler() {
        print("eventPrivacySegmentedControl.selectedSegmentIndex", eventPrivacySegmentedControl.selectedSegmentIndex)
    }
}
