//
//  TabCreationViewController.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 12/17/20.
//  Copyright © 2020 Peter Vail Driscoll II. All rights reserved.
//

import UIKit

class TabCreationViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    private let eventNameLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("I'm collecting money for"), .font(nil), .textColor]))
    private let businessNameLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("I'm holding the event at"), .font(nil), .textColor]))
    private let dateAndTimeLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Everyone should show up "), .font(nil), .textColor]))
    private let fundraisingGoalLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("I need to raise"), .font(nil), .textColor]))
    private let minimumContributionLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Each person pays"), .font(nil), .textColor]))
    private let descriptionLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Description"), .font(nil), .textColor]))
    private let atLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("at"), .font(nil), .textColor]))

    private let eventNameTextField = RoundedUITextField(theme: Theme.UITextField(props: [.font(nil), .autocapitalizationType(autocapitalizationType: .none), .borderStyle(borderStyle: .roundedRect), .backgroundColor(UIColor.clear), .placeHolderText("eventName")]))
    private let businessNameTextField = RoundedUITextField(theme: Theme.UITextField(props: [.font(nil), .autocapitalizationType(autocapitalizationType: .none), .borderStyle(borderStyle: .roundedRect), .backgroundColor(UIColor.clear), .placeHolderText("businessName")]))
    private let dateTextField = RoundedUITextField(theme: Theme.UITextField(props: [.font(nil), .autocapitalizationType(autocapitalizationType: .none), .borderStyle(borderStyle: .roundedRect), .backgroundColor(UIColor.clear), .placeHolderText("date")]))
    private let timeTextField = RoundedUITextField(theme: Theme.UITextField(props: [.font(nil), .autocapitalizationType(autocapitalizationType: .none), .borderStyle(borderStyle: .roundedRect), .backgroundColor(UIColor.clear), .placeHolderText("time")]))
    private let fundraisingGoalTextField = RoundedUITextField(theme: Theme.UITextField(props: [.font(nil), .autocapitalizationType(autocapitalizationType: .none), .borderStyle(borderStyle: .roundedRect), .backgroundColor(UIColor.clear), .placeHolderText("fundraisingGoal")]))
    private let minimumContributionTextField = RoundedUITextField(theme: Theme.UITextField(props: [.font(nil), .autocapitalizationType(autocapitalizationType: .none), .borderStyle(borderStyle: .roundedRect), .backgroundColor(UIColor.clear), .placeHolderText("minimumContribution")]))
    private let descriptionTextField = RoundedUITextField(theme: Theme.UITextField(props: [.font(nil), .autocapitalizationType(autocapitalizationType: .none), .borderStyle(borderStyle: .roundedRect), .backgroundColor(UIColor.clear), .placeHolderText("description")]))
    private let createYourTabStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 10.0)))]))
    private let dateTimeStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(Float(getWidthRatio(horizontalConstant: 5.0)))]))
    private let nextPageButton = RoundButton(theme: Theme.RoundButton(props: [.color, .text("Next"), .titleLabelFont(UIFont.themeButtonFont)]))

    var tab: Tab?
    init() {
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupViewContraints()
    }

    func setupView() {
        view.addSubview(createYourTabStackView)
        view.addSubview(nextPageButton)

        createYourTabStackView.addArrangedSubview(eventNameLabel)
        createYourTabStackView.addArrangedSubview(eventNameTextField)

        createYourTabStackView.addArrangedSubview(businessNameLabel)
        createYourTabStackView.addArrangedSubview(businessNameTextField)

        createYourTabStackView.addArrangedSubview(dateAndTimeLabel)
        createYourTabStackView.addArrangedSubview(dateTimeStackView)

        createYourTabStackView.addArrangedSubview(fundraisingGoalLabel)
        createYourTabStackView.addArrangedSubview(fundraisingGoalTextField)

        // move these properties to a new ViewController

//        createYourTabStackView.addArrangedSubview(minimumContributionLabel)
//        createYourTabStackView.addArrangedSubview(minimumContributionTextField)
//
//        createYourTabStackView.addArrangedSubview(descriptionLabel)
//        createYourTabStackView.addArrangedSubview(descriptionTextField)

        dateTimeStackView.addArrangedSubview(dateTextField)
        dateTimeStackView.addArrangedSubview(atLabel)
        dateTimeStackView.addArrangedSubview(timeTextField)

        businessNameTextField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(launchBusinessArrayViewController)))

        fundraisingGoalTextField.delegate = self
        minimumContributionTextField.delegate = self
        descriptionTextField.delegate = self

        dateTextField.setInputViewDatePicker(target: self, selector: #selector(tapDone))

        dateTimeStackView.distribution = .equalSpacing

        nextPageButton.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)
        nextPageButton.madeStandard()

        setupHeaderView(otherView: createYourTabStackView, header: "Create your tab (1/3)", distanceFromHeader: getHeightRatio(verticalConstant: -20.0))
    }

    func setupViewContraints() {
        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            createYourTabStackView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            createYourTabStackView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -10.0)),

            nextPageButton.heightAnchor.constraint(equalTo: nextPageButton.widthAnchor, multiplier: 60 / 373),
            nextPageButton.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.9),
            nextPageButton.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: getHeightRatio(verticalConstant: -20.0)),
            nextPageButton.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),

            eventNameTextField.widthAnchor.constraint(equalTo: eventNameTextField.heightAnchor, multiplier: 197 / 25),
            businessNameTextField.widthAnchor.constraint(equalTo: businessNameTextField.heightAnchor, multiplier: 197 / 25),

            timeTextField.heightAnchor.constraint(equalTo: eventNameTextField.heightAnchor),
            timeTextField.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.4),

            dateTextField.heightAnchor.constraint(equalTo: eventNameTextField.heightAnchor),
            dateTextField.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.4),

            fundraisingGoalTextField.widthAnchor.constraint(equalTo: fundraisingGoalTextField.heightAnchor, multiplier: 197 / 25),

            // move these properties to a new ViewController

//            dateTimeStackView.widthAnchor.constraint(equalTo: dateTimeStackView.heightAnchor, multiplier: 197 / 25),
//            minimumContributionTextField.widthAnchor.constraint(equalTo: minimumContributionTextField.heightAnchor, multiplier: 197 / 25),
//            descriptionTextField.widthAnchor.constraint(equalTo: descriptionTextField.heightAnchor, multiplier: 197 / 25),
        ])
    }

    @objc func nextButtonTapped() {
        let nextViewController = TabCreationViewController2()
        navigationController?.pushViewController(nextViewController, animated: true)
    }

    @objc func submitTab(_: RoundButton) {
        if eventNameTextField.text! == "" || businessNameTextField.text! == "" || dateTextField.text! == "" || fundraisingGoalTextField.text! == "" || fundraisingGoalTextField.text! == "" || minimumContributionTextField.text! == "" || descriptionTextField.text! == "" {
            alertError()
        } else {
            tab!.name = eventNameTextField.text!
            tab!.userId = CheckoutCart.shared.user!.email
            tab!.fundraisingGoal = Int64(fundraisingGoalTextField.text!)!
            tab!.minimumContribution = Int64(minimumContributionTextField.text!)!
            tab!.detail = descriptionTextField.text!

            APIClient().perform(try! APIRequest(method: .post, path: "/tabs", body: tab)) {
                result in
                switch result {
                case .success:
                    self.navigationController!.popViewController(animated: true)
                case let .failure(error):
                    print("error", error)
                    return
                }
            }
        }
    }

    override func viewWillAppear(_: Bool) {
        navigationController?.navigationBar.isHidden = false
        createYourTabStackView.isHidden = false
    }

    @objc func launchBusinessArrayViewController() {
        let viewController = BusinessSearchViewController()
        viewController.eventBusinessPickerDelegate = self
        let transition = CATransition()
        transition.duration = 0.65
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.default)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        navigationController!.view.layer.add(transition, forKey: kCATransition)
        navigationController?.pushViewController(viewController, animated: true)
    }

    @objc func tapDone() {
        if let datePicker = dateTextField.inputView as? UIDatePicker { // 2-1
            let dateformatter = DateFormatter() // 2-2
            dateformatter.dateStyle = .medium // 2-3
            tab!.dateTime = datePicker.date
            dateTextField.text = dateformatter.string(from: datePicker.date) // 2-4
        }
        dateTextField.resignFirstResponder() // 2-5
    }

    override func viewWillDisappear(_: Bool) {
        createYourTabStackView.isHidden = true
    }

    func textField(_: UITextField, shouldChangeCharactersIn _: NSRange, replacementString string: String) -> Bool {
        return string == "" || Int(string) != nil
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.placeholderText {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
}

extension TabCreationViewController {
    private func alertError() {
        return alert(
            title: "Missing Tab Information",
            message: "You need to fill out all the information to create your tab"
        )
    }

    private func alert(title: String, message: String) {
        let alertCtrl = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertCtrl.addAction(UIAlertAction(title: "Okay", style: .cancel) { _ in
            self.dismiss(animated: true, completion: nil)
        })
        present(alertCtrl, animated: true, completion: nil)
    }
}

extension TabCreationViewController: BusinessPickerProtocol {
    func selectedBusinessHandler(_ selectedBusiness: Business) {
        tab!.businessId = selectedBusiness.id
        tab!.address = selectedBusiness.address
        businessNameTextField.text! = selectedBusiness.name
    }
}
