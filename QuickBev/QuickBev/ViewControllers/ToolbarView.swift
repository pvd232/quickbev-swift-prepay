//
//  ToolbarView.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 2/2/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
protocol LaunchBusinessSearchControllerProtocol {
    func presentSearchViewController()
}

protocol LaunchBusinessMapViewControllerProtocol {
    func presentBusinessMapViewController()
}

protocol LaunchActiveQuickPassViewControllerProtocol {
    func presentActiveQuickPassViewController()
}

protocol LaunchMenuViewControllerProtocol {
    func presentMenuViewController()
}

protocol LaunchOrdersControllerProtocol {
    func presentOrdersViewController()
}

class ToolbarView: UIView {
    private let bottomButtonsStackView = UIStackView(theme: Theme.UIView(props: []))

    private let quickPassImageView = UIImageView(theme: Theme.UIView(props: []))
    private let quickPassLabel = VerticalAlignLabel(theme: Theme.UILabel(props: [.center, .font(UIFont.themeLabelSmallFont), .text("QuickPass")]))
    private let quickPassStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 3.0)))]))

    private let menuStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 0.5)))]))
    private let menuImageView = UIImageView(theme: Theme.UIView(props: []))
    private let menuLabel = VerticalAlignLabel(theme: Theme.UILabel(props: [.center, .font(UIFont.themeLabelSmallFont), .text("Menu")]))

    private let searchImageView = UIImageView(theme: Theme.UIView(props: []))
    private let searchLabel = VerticalAlignLabel(theme: Theme.UILabel(props: [.center, .font(UIFont.themeLabelSmallFont), .text("Search")]))
    private let searchStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 1.5)))]))

    private let orderImageView = UIImageView(theme: Theme.UIView(props: []))
    private let orderLabel = VerticalAlignLabel(theme: Theme.UILabel(props: [.center, .font(UIFont.themeLabelSmallFont), .text("Orders")]))
    private let orderStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 3.5)))]))

    var businessSearchControllerDelegate: LaunchBusinessSearchControllerProtocol!
    var businessMapViewControllerDelegate: LaunchBusinessMapViewControllerProtocol!
    var activeQuickPassViewControllerDelegate: LaunchActiveQuickPassViewControllerProtocol!
    var menuViewControllerDelegate: LaunchMenuViewControllerProtocol!
    var ordersViewControllerDelegate: LaunchOrdersControllerProtocol!
    private var shouldSetupConstraints = true

    private let orderStatus = RoundUIView(theme: Theme.UIView(props: []))
    private let quickPassOrderStatus = RoundUIView(theme: Theme.UIView(props: []))

    private let orderStatusLabel = UILabel(theme: Theme.UILabel(props: [.font(UIFont.systemFont(ofSize: getFontRatio(fontSize: 12.0)))]))
    private let quickPassStatusLabel = UILabel(theme: Theme.UILabel(props: [.font(UIFont.systemFont(ofSize: getFontRatio(fontSize: 12.0)))]))

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private lazy var navigationController = {
        SceneDelegate.shared.rootViewController.current
    }()

    private lazy var rootViewController: RootViewController = {
        SceneDelegate.shared.rootViewController
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor(red: 233 / 255, green: 233 / 255, blue: 233 / 255, alpha: 1.0)
        layoutButtons()
        layoutBottomButtonsStackView()

        NSLayoutConstraint.activate([
            bottomButtonsStackView.centerXAnchor.constraint(equalTo: centerXAnchor),
            bottomButtonsStackView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.8),
            bottomButtonsStackView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.95),
            bottomButtonsStackView.topAnchor.constraint(equalTo: topAnchor, constant: getHeightRatio(verticalConstant: 5.0)),

            quickPassImageView.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.1),
            quickPassImageView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.08),
            quickPassImageView.topAnchor.constraint(equalTo: quickPassStackView.topAnchor),

            quickPassStackView.topAnchor.constraint(equalTo: bottomButtonsStackView.topAnchor),
            quickPassStackView.bottomAnchor.constraint(equalTo: bottomButtonsStackView.bottomAnchor),

            menuImageView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.085),
            menuImageView.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.075),
            menuImageView.topAnchor.constraint(equalTo: menuStackView.topAnchor),

            menuStackView.topAnchor.constraint(equalTo: bottomButtonsStackView.topAnchor),
            menuStackView.bottomAnchor.constraint(equalTo: bottomButtonsStackView.bottomAnchor),

            searchImageView.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.1),
            searchImageView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.08),
            searchImageView.topAnchor.constraint(equalTo: searchStackView.topAnchor),

            searchStackView.topAnchor.constraint(equalTo: bottomButtonsStackView.topAnchor),
            searchStackView.bottomAnchor.constraint(equalTo: bottomButtonsStackView.bottomAnchor),

            orderImageView.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.1),
            orderImageView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.08),
            orderImageView.topAnchor.constraint(equalTo: orderStackView.topAnchor),

            orderStackView.topAnchor.constraint(equalTo: bottomButtonsStackView.topAnchor),
            orderStackView.bottomAnchor.constraint(equalTo: bottomButtonsStackView.bottomAnchor),

            orderStatus.widthAnchor.constraint(equalToConstant: getHeightRatio(verticalConstant: 20.0)),
            orderStatus.heightAnchor.constraint(equalToConstant: getHeightRatio(verticalConstant: 20.0)),
            orderStatus.topAnchor.constraint(equalTo: orderImageView.bottomAnchor, constant: getHeightRatio(verticalConstant: -33.0)),
            orderStatus.rightAnchor.constraint(equalTo: orderImageView.leftAnchor, constant: getWidthRatio(horizontalConstant: -3.0)),

            quickPassOrderStatus.widthAnchor.constraint(equalToConstant: getHeightRatio(verticalConstant: 20.0)),
            quickPassOrderStatus.heightAnchor.constraint(equalToConstant: getHeightRatio(verticalConstant: 20.0)),
            quickPassOrderStatus.topAnchor.constraint(equalTo: quickPassImageView.bottomAnchor, constant: getHeightRatio(verticalConstant: -33.0)),
            quickPassOrderStatus.rightAnchor.constraint(equalTo: quickPassImageView.leftAnchor, constant: getWidthRatio(horizontalConstant: -3.0)),

            orderStatusLabel.centerXAnchor.constraint(equalTo: orderStatus.centerXAnchor),
            orderStatusLabel.centerYAnchor.constraint(equalTo: orderStatus.centerYAnchor),

            quickPassStatusLabel.centerXAnchor.constraint(equalTo: quickPassOrderStatus.centerXAnchor),
            quickPassStatusLabel.centerYAnchor.constraint(equalTo: quickPassOrderStatus.centerYAnchor),
        ])
        quickPassStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(launchActiveQuickPassViewController)))
        menuStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(launchMenuViewController)))
        searchStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(launchSearchViewController)))
        orderStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(launchOrdersViewController)))

        bottomButtonsStackView.isHidden = false
        quickPassStackView.isHidden = !CheckoutCart.shared.shouldDisplayQuickPass
    }

    func updateOrderStatus() {
        if CheckoutCart.shared.currentOrder == nil || CheckoutCart.shared.activeOrders.isEmpty == true {
            orderStatus.isHidden = true
        } else {
            orderStatus.isHidden = false
            orderStatusLabel.text = CheckoutCart.shared.activeOrders.count.description
        }
    }

    func updateQuickPassStatus() {
        if CheckoutCart.shared.activeQuickPasses.isEmpty == true {
            quickPassOrderStatus.isHidden = true
        } else {
            quickPassOrderStatus.isHidden = false
            quickPassStatusLabel.text = CheckoutCart.shared.activeQuickPasses.count.description
        }
    }

    private func layoutButtons() {
        // set home properties
        quickPassLabel.textColor = UIColor.themeColor
        quickPassLabel.verticalAlignment = .top
        quickPassImageView.contentMode = .scaleToFill

        let homeImage = UIImage(systemName: "ticket")
        quickPassImageView.image = homeImage
        quickPassImageView.tintColor = UIColor.themeColor
        quickPassStackView.addArrangedSubview(quickPassImageView)
        quickPassStackView.addArrangedSubview(quickPassLabel)
        quickPassStackView.alignment = .center

        // set menu properties
        menuLabel.textColor = UIColor.themeColor
        menuLabel.verticalAlignment = .top
        menuImageView.contentMode = .scaleToFill
        let menuImage = UIImage(named: "logo-no-background")
        menuImageView.image = menuImage
        menuImageView.tintColor = UIColor.themeColor
        menuStackView.addArrangedSubview(menuImageView)
        menuStackView.addArrangedSubview(menuLabel)
        menuStackView.alignment = .center

        // set search properties
        searchLabel.textColor = UIColor.themeColor
        searchLabel.verticalAlignment = .top
        searchImageView.contentMode = .scaleToFill
        let searchImage = UIImage(systemName: "magnifyingglass")
        searchImageView.image = searchImage
        searchImageView.tintColor = UIColor.themeColor
        searchStackView.addArrangedSubview(searchImageView)
        searchStackView.addArrangedSubview(searchLabel)
        searchStackView.alignment = .center

        // set search properties
        orderLabel.textColor = UIColor.themeColor
        orderLabel.verticalAlignment = .top
        orderImageView.contentMode = .scaleToFill
        let orderImage = UIImage(systemName: "bag")
        orderImageView.image = orderImage
        orderImageView.tintColor = UIColor.themeColor
        orderStackView.addArrangedSubview(orderImageView)
        orderStackView.addArrangedSubview(orderLabel)
        orderStackView.alignment = .center

        // set order status properties
        orderStatus.refreshBackgroundColor(color: .themeColor)
        orderStatus.refreshBorderColor(color: .clear)
        orderStatus.refreshCorners(value: getHeightRatio(verticalConstant: 20.0) / 2)

        // set order status label properties
        orderStatusLabel.textColor = .white
        orderStatusLabel.text = CheckoutCart.shared.activeOrders.count.description

        if CheckoutCart.shared.activeOrders.isEmpty == true {
            orderStatus.isHidden = true
        }

        // set order status properties
        quickPassOrderStatus.refreshBackgroundColor(color: .themeColor)
        quickPassOrderStatus.refreshBorderColor(color: .clear)
        quickPassOrderStatus.refreshCorners(value: getHeightRatio(verticalConstant: 20.0) / 2)

        // set order status label properties
        quickPassStatusLabel.textColor = .white
        quickPassStatusLabel.text = CheckoutCart.shared.activeQuickPasses.count.description
        if CheckoutCart.shared.cart.isEmpty == true {
            quickPassOrderStatus.isHidden = true
        }
    }

    private func layoutBottomButtonsStackView() {
        addSubview(bottomButtonsStackView)
        addSubview(orderStatus)
        addSubview(quickPassOrderStatus)

        orderStatus.addSubview(orderStatusLabel)
        quickPassOrderStatus.addSubview(quickPassStatusLabel)

        bottomButtonsStackView.axis = .horizontal
        bottomButtonsStackView.distribution = .fillEqually
        bottomButtonsStackView.alignment = .top
        bottomButtonsStackView.spacing = CGFloat(getWidthRatio(horizontalConstant: 30.0))
        bottomButtonsStackView.addArrangedSubview(quickPassStackView)
        bottomButtonsStackView.addArrangedSubview(menuStackView)
        bottomButtonsStackView.addArrangedSubview(searchStackView)
        bottomButtonsStackView.addArrangedSubview(orderStackView)
    }

    override func updateConstraints() {
        if shouldSetupConstraints {
            // AutoLayout constraints
            shouldSetupConstraints = false
        }
        super.updateConstraints()
    }

    @objc private func launchSearchViewController() {
        businessSearchControllerDelegate.presentSearchViewController()
    }

    @objc func launchMenuViewController() {
        if CheckoutCart.shared.userBusiness != nil {
            menuViewControllerDelegate.presentMenuViewController()
        } else if CheckoutCart.shared.userBusiness == nil {
            businessMapViewControllerDelegate.presentBusinessMapViewController()
        }
    }

    @objc private func launchOrdersViewController() {
        ordersViewControllerDelegate.presentOrdersViewController()
    }

    @objc private func launchActiveQuickPassViewController() {
        activeQuickPassViewControllerDelegate.presentActiveQuickPassViewController()
    }
}
