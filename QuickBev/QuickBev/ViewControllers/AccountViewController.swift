//
//  AccountViewController.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 7/12/20.
//  Copyright © 2020 Peter Vail Driscoll II. All rights reserved.
//

import Stripe
import UIKit
class AccountViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    private let tableView = UITableView(theme: Theme.UIView(props: []))
    private let accountOptions = ["Manage payment methods", "Sign out"]
    private let accountLabelContainerView = UIView(theme: Theme.UIView(props: []))
    private let accountTextView = UITextView(theme: Theme.UITextView(props: [.textColor, .font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 30.0))), .text("Account")]))

    lazy var paymentContext: STPPaymentContext = {
        STPPaymentContext(customerContext: CheckoutCart.shared.customerContext)
    }()

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isHiddenHairline = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        accountTextView.isUserInteractionEnabled = false
        view.backgroundColor = .white
        view.addSubview(accountLabelContainerView)
        view.addSubview(tableView)

        accountLabelContainerView.addSubview(accountTextView)
        accountLabelContainerView.addBottomBorder(with: UIColor.lightGray.withAlphaComponent(0.5), andWidth: 1.0)
        accountTextView.textAlignment = .justified
        tableView.backgroundColor = .white

        paymentContext.delegate = self
        paymentContext.hostViewController = self

        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            accountTextView.topAnchor.constraint(equalTo: accountLabelContainerView.topAnchor, constant: getHeightRatio(verticalConstant: 20.0)),
            accountTextView.bottomAnchor.constraint(equalTo: accountLabelContainerView.bottomAnchor),
            accountTextView.leadingAnchor.constraint(equalTo: accountLabelContainerView.leadingAnchor, constant: getWidthRatio(horizontalConstant: 15.0)),
            accountLabelContainerView.topAnchor.constraint(equalTo: safeArea.topAnchor),
            accountLabelContainerView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            accountLabelContainerView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: accountLabelContainerView.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor),
        ])

        accountTextView.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: getHeightRatio(verticalConstant: 15.0
        ), right: 0)
        accountTextView.isScrollEnabled = false
        accountTextView.textAlignment = .justified
        accountTextView.backgroundColor = .clear

        // set delegate and datasource
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView(theme: Theme.UIView(props: []))
        // register a default cell
        tableView.register(AccountTableViewCell.self, forCellReuseIdentifier: "cell")
        // to get rid of default grey lines that border cells in UITableView
        tableView.separatorColor = UIColor.clear
        setNavbar()
    }

    // Note: because this is NOT a subclassed UITableViewController, DataSource and Delegate functions are NOT overridden

    // this is for custom UITableView Cell Seperators
    func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let placeCell = cell as? AccountTableViewCell {
            placeCell.render(position: indexPath.row, total: accountOptions.count)
        }
    }

    func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        tableView.frame.height / 8
    }

    func numberOfSections(in _: UITableView) -> Int {
        return 1
    }

    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return accountOptions.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AccountTableViewCell
        cell.myLabel.text = "\(accountOptions[indexPath.row])"
        return cell
    }

    private func alert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

        CoreDataManager.sharedManager.saveContext()
        let logoutAction = UIAlertAction(title: "Log Out", style: .default) { _ in
            SceneDelegate.shared.rootViewController.animateActivityIndicator()
            CheckoutCart.shared.resetCart()
            self.dismiss(animated: true) {
                SceneDelegate.shared.rootViewController.switchToSplashPageViewController()
            }
        }

        logoutAction.setValue(UIColor.red, forKey: "titleTextColor")
        alertController.addAction(logoutAction)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in
            alertController.dismiss(animated: true) {}
        })
        present(alertController, animated: true) {}
    }

    func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedAccountOption = accountOptions[indexPath.row]
        if selectedAccountOption == "Sign out" {
            alert(title: "Log out of \(String(describing: CheckoutCart.shared.user!.email))?", message: "")
        } else if selectedAccountOption == "Manage payment methods" {
            paymentContext.presentPaymentOptionsViewController()
        }
    }
}

extension AccountViewController: STPPaymentContextDelegate {
    func paymentContext(_: STPPaymentContext, didFailToLoadWithError error: Error) {
        print(error)
    }

    func paymentContextDidChange(_: STPPaymentContext) {}

    func paymentContext(_: STPPaymentContext, didCreatePaymentResult _: STPPaymentResult, completion _: @escaping STPPaymentStatusBlock) {}

    func paymentContext(_: STPPaymentContext, didFinishWith _: STPPaymentStatus, error _: Error?) {}
}
