//
//  HomeTableViewContentView.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 9/1/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
class HomeTableViewContentView: UIView, UIContentView {
    var configuration: UIContentConfiguration {
        didSet {
            configure()
        }
    }

    private let businessImageView = RoundedImageView(theme: Theme.UIView(props: []))
    private let name = VerticalAlignLabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.primaryTableCellContentViewFont)]))
    private let distance = VerticalAlignLabel(theme: Theme.UILabel(props: [.font(UIFont.secondaryTableCellContentViewFont)]))
    private let textStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical]))
    private let labelsStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(Float(getHeightRatio(verticalConstant: 15)))]))

    init(configuration: UIContentConfiguration) {
        self.configuration = configuration
        super.init(frame: .zero)
        // ... configure the subviews ...
        configure()
        // ... and add them as subviews to self ...
        setupView()
    }

    private func setupView() {
        addSubview(businessImageView)
        addSubview(labelsStackView)

        backgroundColor = .white

        distance.textColor = .systemGray
        distance.verticalAlignment = .bottom
        name.verticalAlignment = .top

        if name.text!.count >= 20 {
            name.font = UIFont.secondaryTableCellContentViewFont
        } else {
            name.font = UIFont.primaryTableCellContentViewFont
        }

        textStackView.distribution = .fill
        textStackView.alignment = .fill
        textStackView.addArrangedSubview(name)
        textStackView.addArrangedSubview(distance)

        labelsStackView.distribution = .fill
        labelsStackView.alignment = .fill
        labelsStackView.addArrangedSubview(businessImageView)
        labelsStackView.addArrangedSubview(textStackView)

        let safeArea = safeAreaLayoutGuide

        NSLayoutConstraint.activate([
            businessImageView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: getHeightRatio(verticalConstant: 16.0)),
            businessImageView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: getHeightRatio(verticalConstant: -16.0)),
            businessImageView.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.15),

            // had to do these constraints to remove the purple layout error of ambiguous height and vertical position for name and distance labels

            name.topAnchor.constraint(equalTo: textStackView.topAnchor),
            name.bottomAnchor.constraint(equalTo: distance.topAnchor),
            name.heightAnchor.constraint(equalTo: distance.heightAnchor),

            distance.topAnchor.constraint(equalTo: name.bottomAnchor),
            distance.bottomAnchor.constraint(equalTo: textStackView.bottomAnchor),

            textStackView.heightAnchor.constraint(equalTo: labelsStackView.heightAnchor),
            textStackView.topAnchor.constraint(equalTo: labelsStackView.topAnchor),
            textStackView.bottomAnchor.constraint(equalTo: labelsStackView.bottomAnchor),

            labelsStackView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 20.0)),
            labelsStackView.topAnchor.constraint(equalTo: businessImageView.topAnchor),
            labelsStackView.bottomAnchor.constraint(equalTo: businessImageView.bottomAnchor),
        ])
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configure() {
        guard let config = configuration as? HomeTableViewContentConfiguration else { return }
        businessImageView.image = config.businessImage
        name.text = config.name
        if config.distancePrecise <= 1.0 {
            let distanceFeet = Int(5280 * config.distancePrecise)
            distance.text = distanceFeet.description + " feet away"
        } else {
            distance.text = config.distance + " miles away"
        }
    }
}
