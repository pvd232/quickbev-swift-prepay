//
//  MenuCollectionContentViewConfiguration.swift
//  QuickBev
//
//  Created by Peter Driscoll on 3/17/22.
//  Copyright © 2022 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
struct MenuCollectionContentConfiguration: UIContentConfiguration {
    func updated(for _: UIConfigurationState) -> MenuCollectionContentConfiguration {
        return self
    }

    let drinkCategory: DrinkCategory

    init(drinkCategory: DrinkCategory) {
        self.drinkCategory = drinkCategory
    }

    func makeContentView() -> UIView & UIContentView {
        let c = MenuCollectionContentView(configuration: self)
        return c
    }
}
