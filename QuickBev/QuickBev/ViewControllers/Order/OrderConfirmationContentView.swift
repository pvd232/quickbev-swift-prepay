//
//  OrderConfirmationDrinkContentView.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 12/6/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import Foundation
import UIKit
class OrderConfirmationContentView: UIView, UIContentView {
    var configuration: UIContentConfiguration {
        didSet {
            configure(configuration: configuration as! OrderConfirmationContentConfiguration)
        }
    }

    private let drinkImageView = RoundedImageView(theme: Theme.UIView(props: []))
    private let name = VerticalAlignLabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.primaryTableCellContentViewFont)]))
    private let drinkDescription = VerticalAlignLabel(theme: Theme.UILabel(props: [.font(UIFont.secondaryTableCellContentViewFont)]))
    private let textStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical]))
    private let labelsStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(Float(getHeightRatio(verticalConstant: 20)))]))
    private let quantity = RoundButton(theme: Theme.UIView(props: []))

    init(configuration: OrderConfirmationContentConfiguration) {
        self.configuration = configuration

        super.init(frame: .zero)
        // ... configure the subviews ...
        configure(configuration: configuration)
    }

    private func setupView() {
        addSubview(drinkImageView)
        addSubview(labelsStackView)
        addSubview(quantity)
        backgroundColor = .white

        drinkDescription.textColor = .systemGray
        drinkDescription.verticalAlignment = .middle
        drinkDescription.numberOfLines = 0
        drinkDescription.lineBreakMode = .byWordWrapping
        drinkDescription.setContentCompressionResistancePriority(UILayoutPriority.required, for: .vertical)

        name.verticalAlignment = .top

        if name.text!.count >= 20 {
            name.font = UIFont.secondaryTableCellContentViewFont
        }
        if drinkDescription.text!.count >= 65 {
            drinkDescription.font = UIFont(name: "GillSans", size: getFontRatio(fontSize: 14.0))
        }

        quantity.refreshCorners(value: 6.0)
        quantity.refreshColor(color: UIColor.lightGray.mix(with: UIColor.white, amount: 0.7))
        quantity.refreshBorderWidth(value: 0.0)
        quantity.setTitleColor(UIColor.black, for: .normal)
        quantity.titleLabel?.font = UIFont.secondaryTableCellContentViewFont

        textStackView.distribution = .fill
        textStackView.alignment = .fill
        textStackView.addArrangedSubview(name)
        textStackView.addArrangedSubview(drinkDescription)

        labelsStackView.distribution = .fill
        labelsStackView.alignment = .fill
        labelsStackView.addArrangedSubview(drinkImageView)
        labelsStackView.addArrangedSubview(textStackView)
        let safeArea = safeAreaLayoutGuide

        NSLayoutConstraint.activate([
            drinkImageView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: getHeightRatio(verticalConstant: 20.0)),
            drinkImageView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: getHeightRatio(verticalConstant: -20.0)),
            drinkImageView.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.13),

            quantity.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.065),
            quantity.heightAnchor.constraint(equalTo: quantity.widthAnchor),
            quantity.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 20.0)),
            quantity.centerYAnchor.constraint(equalTo: safeArea.centerYAnchor),

            name.topAnchor.constraint(equalTo: textStackView.topAnchor),

            // somehow by not constraining the UILabels in the stackview (drinkDescription.bottom to name.top, heights, ect. it freed up the layout engine to adapt to longer descriptions! phewwww

            drinkDescription.trailingAnchor.constraint(equalTo: textStackView.trailingAnchor),
            drinkDescription.leadingAnchor.constraint(equalTo: textStackView.leadingAnchor),

            textStackView.topAnchor.constraint(equalTo: labelsStackView.topAnchor),
            textStackView.trailingAnchor.constraint(equalTo: labelsStackView.trailingAnchor),

            labelsStackView.topAnchor.constraint(equalTo: drinkImageView.topAnchor),
            labelsStackView.leadingAnchor.constraint(equalTo: quantity.trailingAnchor, constant: getWidthRatio(horizontalConstant: 20.0)),
            labelsStackView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -20.0)),
            labelsStackView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: getHeightRatio(verticalConstant: -20.0)),
        ])
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configure(configuration: OrderConfirmationContentConfiguration) {
        drinkImageView.image = configuration.drinkImage
        name.text = configuration.name
        drinkDescription.text = configuration.drinkDescription
        quantity.refreshTitle(newTitle: configuration.quantity.description)
        setupView()
    }
}
