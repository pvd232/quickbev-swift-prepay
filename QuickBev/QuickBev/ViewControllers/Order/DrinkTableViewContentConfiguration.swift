//
//  DrinkTableViewCellContentConfiguration.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 4/7/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//
import UIKit
struct DrinkTableViewContentConfiguration: UIContentConfiguration {
    func updated(for _: UIConfigurationState) -> DrinkTableViewContentConfiguration {
        return self
    }

    var name = ""
    var description = ""
    var drinkImage = UIImage()
    func makeContentView() -> UIView & UIContentView {
        let c = DrinkTableViewContentView(configuration: self)
        return c
    }
}
