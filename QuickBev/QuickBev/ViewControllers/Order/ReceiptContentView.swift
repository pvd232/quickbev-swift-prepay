//
//  ReceiptContentView.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 12/8/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
class ReceiptContentView: UIView, UIContentView {
    var configuration: UIContentConfiguration {
        didSet {
            configure(configuration: configuration as! ReceiptContentConfiguration)
        }
    }

    private let drinkImageView = RoundedImageView(theme: Theme.UIView(props: []))
    private let name = VerticalAlignLabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.primaryTableCellContentViewFont)]))
    private let drinkDescription = VerticalAlignLabel(theme: Theme.UILabel(props: [.font(UIFont.secondaryTableCellContentViewFont)]))
    private let textStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical]))
    private let labelsStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(Float(getHeightRatio(verticalConstant: 20)))]))
    private let quantity = RoundButton(theme: Theme.UIView(props: []))
    private let priceLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.primaryTableCellContentViewFont)]))

    init(configuration: ReceiptContentConfiguration) {
        self.configuration = configuration
        super.init(frame: .zero)
        // ... configure the subviews ...
        configure(configuration: configuration)
    }

    private func setupView() {
        addSubview(drinkImageView)
        addSubview(labelsStackView)
        addSubview(quantity)
        addSubview(priceLabel)

        backgroundColor = .white

        drinkDescription.textColor = .systemGray
        drinkDescription.verticalAlignment = .bottom
        drinkDescription.numberOfLines = 0
        drinkDescription.lineBreakMode = .byWordWrapping
        drinkDescription.preferredMaxLayoutWidth = bounds.size.width

        name.verticalAlignment = .top

        if name.text!.count >= 20 {
            name.font = UIFont.secondaryTableCellContentViewFont
        }
        if drinkDescription.text!.count >= 65 {
            drinkDescription.font = UIFont(name: "GillSans", size: getFontRatio(fontSize: 14.0))
        }

        quantity.refreshCorners(value: 6.0)
        quantity.refreshColor(color: UIColor.lightGray.mix(with: UIColor.white, amount: 0.7))
        quantity.refreshBorderWidth(value: 0.0)
        quantity.setTitleColor(UIColor.black, for: .normal)
        quantity.titleLabel?.font = UIFont.secondaryTableCellContentViewFont

        textStackView.distribution = .fill
        textStackView.alignment = .fill
        textStackView.addArrangedSubview(name)
        textStackView.addArrangedSubview(drinkDescription)

        labelsStackView.distribution = .fill
        labelsStackView.alignment = .fill
        labelsStackView.addArrangedSubview(drinkImageView)
        labelsStackView.addArrangedSubview(textStackView)

        let safeArea = safeAreaLayoutGuide

        NSLayoutConstraint.activate([
            drinkImageView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: getHeightRatio(verticalConstant: 15.0)),
            drinkImageView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: getHeightRatio(verticalConstant: -15.0)),
            drinkImageView.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.13),

            quantity.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.065),
            quantity.heightAnchor.constraint(equalTo: quantity.widthAnchor),
            quantity.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 20.0)),
            quantity.centerYAnchor.constraint(equalTo: safeArea.centerYAnchor),
            // had to do these constraints to remove the purple layout error of ambiguous height and vertical position for name and drinkDescription labels

            name.topAnchor.constraint(equalTo: textStackView.topAnchor),

            drinkDescription.topAnchor.constraint(equalTo: name.bottomAnchor),
            drinkDescription.bottomAnchor.constraint(equalTo: textStackView.bottomAnchor),
            drinkDescription.trailingAnchor.constraint(equalTo: textStackView.trailingAnchor),
            drinkDescription.leadingAnchor.constraint(equalTo: textStackView.leadingAnchor),

            textStackView.topAnchor.constraint(equalTo: labelsStackView.topAnchor),
            textStackView.trailingAnchor.constraint(equalTo: labelsStackView.trailingAnchor),

            labelsStackView.topAnchor.constraint(equalTo: drinkImageView.topAnchor),
            labelsStackView.leadingAnchor.constraint(equalTo: quantity.trailingAnchor, constant: getWidthRatio(horizontalConstant: 20.0)),
            labelsStackView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -20.0)),
            labelsStackView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: getHeightRatio(verticalConstant: -15.0)),

            priceLabel.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -47.0)),
            priceLabel.topAnchor.constraint(equalTo: name.topAnchor),
        ])
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configure(configuration: ReceiptContentConfiguration) {
        drinkImageView.image = configuration.drinkImage
        name.text = configuration.name.capitalizedFirst
        drinkDescription.text = configuration.drinkDescription
        quantity.refreshTitle(newTitle: configuration.quantity.description)

        let itemCost = configuration.price * Double(configuration.quantity)
        // test if the double is a whole number for formatting purposes to remove the trailing .0
        if floor(itemCost) == itemCost {
            priceLabel.text = "$\(Int(configuration.price * Double(configuration.quantity)))"
        } else {
            priceLabel.text = "$\(configuration.price * Double(configuration.quantity))"
        }
        setupView()
    }
}
