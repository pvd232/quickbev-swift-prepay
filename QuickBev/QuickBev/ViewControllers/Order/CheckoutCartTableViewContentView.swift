import UIKit
class CheckoutCartTableViewContentView: UIView, UIContentView {
    var configuration: UIContentConfiguration {
        didSet {
            configure()
        }
    }

    private let drinkImageView = RoundedImageView(theme: Theme.UIView(props: []))
    private let name = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont(name: "GillSans", size: 20.0))]))
    private let price = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont(name: "GillSans", size: 20.0))]))
    private let quantity = RoundButton(theme: Theme.UIView(props: []))
    private let textStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(Float(getWidthRatio(horizontalConstant: 10)))]))
    private let labelsStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 20)))]))

    init(configuration: UIContentConfiguration) {
        self.configuration = configuration
        super.init(frame: .zero)
        // ... configure the subviews ...
        configure()
        // ... and add them as subviews to self ...
        setupView()
    }

    private func setupView() {
        addSubview(drinkImageView)
        addSubview(labelsStackView)

        backgroundColor = .white
        if name.text!.count >= 20 {
            name.font = UIFont(name: "GillSans", size: getFontRatio(fontSize: 16.0))
        } else {
            name.font = UIFont(name: "GillSans", size: getFontRatio(fontSize: 20.0))
        }

        quantity.refreshCorners(value: 6.0)
        quantity.refreshColor(color: UIColor.lightGray.mix(with: UIColor.white, amount: 0.7))
        quantity.refreshBorderWidth(value: 0.0)
        quantity.setTitleColor(UIColor.black, for: .normal)
        quantity.titleLabel?.font = UIFont(name: "GillSans", size: getFontRatio(fontSize: 16.0))

        textStackView.distribution = .fill
        textStackView.alignment = .fill
        textStackView.addArrangedSubview(quantity)
        textStackView.addArrangedSubview(name)

        labelsStackView.distribution = .fill
        labelsStackView.alignment = .fill

        labelsStackView.addArrangedSubview(textStackView)
        labelsStackView.addArrangedSubview(price)

        let safeArea = safeAreaLayoutGuide

        NSLayoutConstraint.activate([
            quantity.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.055),
            quantity.heightAnchor.constraint(equalTo: quantity.widthAnchor),

            drinkImageView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -20.0)),
            drinkImageView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: getHeightRatio(verticalConstant: 20.0)),
            drinkImageView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: getHeightRatio(verticalConstant: -20.0)),
            drinkImageView.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.23),

            labelsStackView.trailingAnchor.constraint(equalTo: drinkImageView.leadingAnchor, constant: getWidthRatio(horizontalConstant: -30.0)),
            labelsStackView.topAnchor.constraint(equalTo: drinkImageView.topAnchor),
            labelsStackView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 20.0)),
        ])
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configure() {
        // determine based on item boolean if it is quickpass content view or a drink content view
        // maybe just determine this in configuration and then create either a quickpass content view or a checkout cart content view
        guard let config = configuration as? CheckoutCartTableViewContentConfiguration else { return }
        drinkImageView.image = config.drinkImage
        name.text = config.name.capitalized
        quantity.refreshTitle(newTitle: config.quantity.description)
        price.text = config.price.formattedCurrency().formattedCurrencyString()
    }
}
