//
//  PreviousOrdersContentView.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 7/6/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
class PreviousOrdersContentView: UIView, UIContentView {
    var configuration: UIContentConfiguration {
        didSet {
            configure(configuration: configuration as! PreviousOrdersContentViewConfiguration)
        }
    }

    private let orderStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 10.0)))]))
    private let orderItemsStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(Float(getWidthRatio(horizontalConstant: 10.0)))]))
    private let orderDateStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(Float(getWidthRatio(horizontalConstant: 10.0)))]))

    private let barName = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.primaryTableCellContentViewFont)]))
    private let orderNumberOfItems = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.secondaryTableCellContentViewFont)]))
    private let orderTotal = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.secondaryTableCellContentViewFont)]))
    private let orderDate = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.secondaryTableCellContentViewFont)]))
    private var orderStatus = ""
    private let drinkImageView = RoundedImageView(theme: Theme.UIView(props: []))
    private let orderButton = RoundButton(theme: Theme.RoundButton(props: []))
    private let orderLabel = RoundButton(theme: Theme.RoundButton(props: [.titleLabelFont(UIFont.secondaryTableCellContentViewFont)]))
    private let completedOrderStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(Float(getWidthRatio(horizontalConstant: 10.0)))]))

    init(configuration: PreviousOrdersContentViewConfiguration) {
        self.configuration = configuration
        super.init(frame: .zero)
        configure(configuration: configuration)
    }

    private func setupView() {
        addSubview(orderStackView)
        addSubview(completedOrderStackView)

        orderNumberOfItems.textColor = .systemGray
        orderTotal.textColor = .systemGray

        backgroundColor = .white
        orderStackView.distribution = .equalSpacing
        orderStackView.alignment = .leading
        orderLabel.refreshBorderWidth(value: 0.0)
        orderLabel.refreshCorners(value: 18.0)
        NSLayoutConstraint.activate([
            orderLabel.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.038),
            orderLabel.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.45),
            completedOrderStackView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            completedOrderStackView.bottomAnchor.constraint(equalTo: orderStackView.topAnchor, constant: getHeightRatio(verticalConstant: -15.0)),
            orderStackView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor, constant: getHeightRatio(verticalConstant: 20.0)),
            orderStackView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: getWidthRatio(horizontalConstant: 18.0)),
        ])
        if orderStatus == "active" {
            orderLabel.refreshTitle(newTitle: "Order in Progress")
            orderLabel.refreshColor(color: UIColor.themeColor.mix(with: UIColor.white, amount: 0.78))
            orderLabel.setTitleColor(UIColor.purple, for: .normal)
        } else if orderStatus == "completed" {
            orderLabel.refreshTitle(newTitle: "Order Completed")
            orderLabel.refreshColor(color: UIColor.systemGreen.mix(with: UIColor.white, amount: 0.3))
            orderLabel.setTitleColor(.black, for: .normal)
        } else if orderStatus == "refunded" {
            orderLabel.refreshTitle(newTitle: "Order Refunded")
            orderLabel.refreshColor(color: UIColor.red.mix(with: UIColor.white, amount: 0.78))
            orderLabel.setTitleColor(.red, for: .normal)
        }

        completedOrderStackView.addArrangedSubview(orderLabel)

        orderStackView.addArrangedSubview(orderDateStackView)
        orderStackView.addArrangedSubview(orderItemsStackView)

        orderItemsStackView.addArrangedSubview(orderNumberOfItems)
        orderItemsStackView.addArrangedSubview(orderTotal)
        orderDateStackView.addArrangedSubview(barName)
        orderDateStackView.addArrangedSubview(orderDate)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configure(configuration: PreviousOrdersContentViewConfiguration) {
        barName.text = configuration.businessName
        orderNumberOfItems.text = configuration.orderNumberOfItems
        orderDate.text = configuration.orderDate
        orderTotal.text = configuration.orderTotal
        orderStatus = configuration.orderStatus
        drinkImageView.image = configuration.businessImage
        setupView()
    }
}
