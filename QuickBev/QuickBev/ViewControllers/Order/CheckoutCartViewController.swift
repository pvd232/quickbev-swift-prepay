import UIKit

class CheckoutCartViewController: UIViewController, UITableViewDelegate {
    private let reviewOrderButton = RoundButton(theme: Theme.UIView(props: []))
    private let tableView = UITableView(theme: Theme.UIView(props: []))
    private var dataSource: MyDataSource!
    private var cartCount: Int16! = 0
    private var imageObjects = [DrinkItem]()

    init() {
        super.init(nibName: nil, bundle: nil)
        loadTable()
        for orderDrink in CheckoutCart.shared.cart {
            cartCount += orderDrink.quantity
        }
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupHeaderView(otherView: tableView, header: "Your cart")
        setNavbar()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isHiddenHairline = true

        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
        var newCartCount: Int16 = 0
        for drink in CheckoutCart.shared.cart {
            newCartCount += drink.quantity
        }
        if newCartCount != cartCount {
            // must reset image objects to reset table
            imageObjects = []
            loadTable()
        }
    }

    class MyDataSource: UITableViewDiffableDataSource<Section, DrinkItem> {
        // MARK: editing support

        override func tableView(_: UITableView, canEditRowAt _: IndexPath) -> Bool {
            return true
        }

        override func tableView(_: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
            if editingStyle == .delete {
                if let identifierToDelete = itemIdentifier(for: indexPath) {
                    var snapshot = self.snapshot()
                    snapshot.deleteItems([identifierToDelete])
                    apply(snapshot)
                    CheckoutCart.shared.removeFromOrderDrink(CheckoutCart.shared.cart[indexPath.row])
                    CoreDataManager.sharedManager.saveContext()
                    SceneDelegate.shared.rootViewController.homeViewController?.setConditionalUIProperties()
                }
            }
        }
    }

    private func loadTable() {
        dataSource = MyDataSource(tableView: tableView) {
            (tableView: UITableView, indexPath: IndexPath, drinkItem: DrinkItem) -> UITableViewCell? in
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
                cell.backgroundColor = .clear

                // change this to item, and let the content configuration figure out if the item is quickpass or drink
                let content = CheckoutCartTableViewContentConfiguration(drinkItem: drinkItem)

                // change this to item.url
                if let url = drinkItem.url as NSURL? {
                    ImageCache.publicCache.load(url: url as NSURL, item: drinkItem) { fetchedItem, image in
                        if let img = image, img != fetchedItem.image {
                            var updatedSnapshot = self.dataSource.snapshot()
                            // delete this and just update the fetched item
                            if let datasourceIndex = updatedSnapshot.indexOfItem(fetchedItem as! DrinkItem) {
                                let drinkItem = self.imageObjects[datasourceIndex]
                                drinkItem.image = img

                                updatedSnapshot.reloadItems([drinkItem])
                                self.dataSource.apply(updatedSnapshot, animatingDifferences: false)
                            }
                        }
                    }
                }
                cell.contentConfiguration = content
                return cell
        }
        dataSource.defaultRowAnimation = .fade

        // Get our image URLs for processing.
        if imageObjects.isEmpty {
            for orderDrink in CheckoutCart.shared.cart {
                imageObjects.append(DrinkItem(orderDrink: orderDrink))
            }
            var initialSnapshot = NSDiffableDataSourceSnapshot<Section, DrinkItem>()
            initialSnapshot.appendSections([.main])
            // append quickPasses to a quickpass list here

            // append a quick pass section here
            initialSnapshot.appendItems(imageObjects)
            dataSource.apply(initialSnapshot, animatingDifferences: false)
        }
    }

    private func setupView() {
        view.backgroundColor = .white

        view.addSubview(tableView)
        tableView.backgroundColor = .white
        tableView.separatorColor = .clear

        view.addSubview(reviewOrderButton)
        reviewOrderButton.madeStandard()
        reviewOrderButton.refreshTitle(newTitle: "Review order")

        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            reviewOrderButton.heightAnchor.constraint(equalTo: reviewOrderButton.widthAnchor, multiplier: 60 / 373),
            reviewOrderButton.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.9),
            reviewOrderButton.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: getHeightRatio(verticalConstant: -20.0)),
            reviewOrderButton.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),

            tableView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
            tableView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            tableView.bottomAnchor.constraint(equalTo: reviewOrderButton.topAnchor, constant: -20.0),
        ])

        tableView.delegate = self
        tableView.dataSource = dataSource
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")

        reviewOrderButton.addTarget(self, action: #selector(proceedToReviewOrder), for: .touchUpInside)
        CheckoutCart.shared.calculateCost()
    }

    @objc private func proceedToReviewOrder() {
        guard CheckoutCart.shared.userBusiness != nil else {
            alertReviewOrder()
            return
        }
        guard CheckoutCart.shared.canPay == true else {
            alertReviewOrderNoDrinks()
            return
        }
        navigationController?.pushViewController(ReviewOrderViewController(), animated: true)
    }

    func tableView(_ tableView: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        return (tableView.frame.height / 4)
    }

    func numberOfSections(in _: UITableView) -> Int {
        return 1
    }

    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        if CheckoutCart.shared.canPay == true {
            return CheckoutCart.shared.cart.count
        } else {
            return 6
        }
    }

    func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedOrderDrink = CheckoutCart.shared.cart[indexPath.row]
        navigationController!.pushViewController(DrinkViewController(drink: selectedOrderDrink.drink, orderDrink: selectedOrderDrink, isCheckout: true, drinkUIImage: imageObjects[indexPath.row].image), animated: true)
    }
}

extension CheckoutCartViewController {
    private func alertAddItems() {
        let alertCtrl = UIAlertController(title: "Empty checkout cart", message: "You must select a bar before trying to add drinks", preferredStyle: .alert)
        alertCtrl.addAction(UIAlertAction(title: "Okay", style: .cancel)
        )
        present(alertCtrl, animated: true, completion: nil)
    }

    private func alertReviewOrder() {
        let alertCtrl = UIAlertController(title: "Empty checkout cart", message: "Please select a bar before trying to review your order", preferredStyle: .alert)
        alertCtrl.addAction(UIAlertAction(title: "Okay", style: .cancel)
        )
        present(alertCtrl, animated: true, completion: nil)
    }

    private func alertReviewOrderNoDrinks() {
        let alertCtrl = UIAlertController(title: "Empty checkout cart", message: "Please add a drink to your cart before reviewing your order", preferredStyle: .alert)
        alertCtrl.addAction(UIAlertAction(title: "Okay", style: .cancel)
        )
        present(alertCtrl, animated: true, completion: nil)
    }
}
