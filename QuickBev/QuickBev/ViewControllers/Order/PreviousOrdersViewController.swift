//
//  PreviousOrdersViewController.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 7/6/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//
import UIKit

class PreviousOrdersViewController: UIViewController, UITableViewDelegate {
    private var tableView = UITableView(theme: Theme.UIView(props: []))
    private var previousOrders = [Order]()
    private var sortedPreviousOrders = [Order]()
    private var orderItems = [OrderItem]()
    private let cellReuseIdentifier = "cell"
    private lazy var dataSource = makeDataSource()

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupHeaderView(otherView: tableView, header: "Orders")
        setNavbar()
        loadTable()
    }

    override func viewWillAppear(_: Bool) {
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
        navigationController?.isHiddenHairline = true
    }

    private func loadTable() {
        previousOrders = [Order]()
        orderItems = [OrderItem]()
        if let orders = CoreDataManager.sharedManager.fetchEntities(entityName: "Order") as? [Order] {
            for order in orders {
                previousOrders.append(order)
            }
        }
        previousOrders.sort {
            $0.dateTime.timeIntervalSince1970 > $1.dateTime.timeIntervalSince1970
        }
        var activeOrders = [Order]()
        for previousOrder in previousOrders {
            if previousOrder.active == true {
                activeOrders.append(previousOrder)
            }
        }
        activeOrders.forEach { order in
            if let matchingOrder = previousOrders.first(where: {
                $0.id == order.id
            }
            ) {
                if let index = previousOrders.firstIndex(of: matchingOrder) {
                    previousOrders.remove(at: index)
                }
            }
        }
        sortedPreviousOrders = activeOrders + previousOrders

        for order in sortedPreviousOrders {
            if let orderBar = CoreDataManager.sharedManager.fetchEntity(entityName: "Business", propertyToFilter: "id", propertyValue: order.businessId.uuidString) as? Business {
                let orderItem = OrderItem(order: order, business: orderBar)
                orderItems.append(orderItem)
            }
        }
        update(with: orderItems)
    }

    private func update(with newOrders: [OrderItem], animate _: Bool = true) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, OrderItem>()
        snapshot.appendSections([.main])
        snapshot.appendItems(newOrders, toSection: .main)
        dataSource.apply(snapshot, animatingDifferences: false)
    }

    private func pushOrderConfirmationViewController(theVC: UIViewController) {
        navigationController?.pushViewController(theVC, animated: true)
    }

    private func makeDataSource() -> UITableViewDiffableDataSource<Section, OrderItem> {
        return UITableViewDiffableDataSource(
            tableView: tableView,
            cellProvider: { tableView, indexPath, orderItem in
                let cell = tableView.dequeueReusableCell(
                    withIdentifier: self.cellReuseIdentifier,
                    for: indexPath
                )
                cell.backgroundColor = .clear
                let contentConfig = PreviousOrdersContentViewConfiguration(orderItem: orderItem)
                cell.contentConfiguration = contentConfig
                return cell
            }
        )
    }

    private func setupView() {
        view.backgroundColor = .white
        view.addSubview(tableView)
        tableView.backgroundColor = .white
        tableView.separatorColor = .clear
        tableView.delegate = self
        tableView.register(UITableViewCell.self,
                           forCellReuseIdentifier: cellReuseIdentifier)
        tableView.dataSource = makeDataSource()

        navigationItem.title = "Previous orders"

        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor),
        ])
    }

    func tableView(_ tableView: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        return (tableView.frame.height / 5)
    }

    func numberOfSections(in _: UITableView) -> Int {
        return 1
    }

    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return orderItems.count
    }

    func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedOrder = sortedPreviousOrders.first(where: {
            $0.id == dataSource.itemIdentifier(for: indexPath)!.orderId
        })
        if selectedOrder!.active == true {
            navigationController?.pushViewController(OrderConfirmationViewController(SelectedOrder: selectedOrder!, IsModal: false), animated: true)
        } else {
            navigationController?.pushViewController(ReceiptViewController(SelectedOrder: selectedOrder!, IsModal: false), animated: true)
        }
    }
}
