//
//  MenuCollectionContentView.swift
//  QuickBev
//
//  Created by Peter Driscoll on 3/17/22.
//  Copyright © 2022 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
class MenuCollectionContentView: UIView, UIContentView {
    var configuration: UIContentConfiguration {
        didSet {
            configure(configuration: configuration)
        }
    }

    private let name = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont(name: "GillSans", size: 20.0))]))

    init(configuration: UIContentConfiguration) {
        self.configuration = configuration
        super.init(frame: .zero)
        // ... configure the subviews ...
        configure(configuration: self.configuration)
        // ... and add them as subviews to self ...
    }

    private func setupView() {
        addSubview(name)

        let safeArea = safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            name.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            name.centerYAnchor.constraint(equalTo: safeArea.centerYAnchor),
        ])
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configure(configuration: UIContentConfiguration) {
        guard let config = configuration as? MenuCollectionContentConfiguration else { return }
        name.text = config.drinkCategory.rawValue.capitalized
        setupView()
    }
}
