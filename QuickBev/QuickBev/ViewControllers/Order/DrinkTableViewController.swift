//
//  DrinkTableViewController.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 3/29/20.
//  Copyright © 2020 Peter Vail Driscoll II. All rights reserved.
//

import UIKit

class DrinkTableViewController: UIViewController, UITableViewDelegate {
    private var dataSource: UITableViewDiffableDataSource<Section, DrinkItem>!
    private let tableView = UITableView(theme: Theme.UIView(props: []))

    private var imageObjects = [DrinkItem]()

    // MARK: View

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupHeaderView(otherView: tableView, header: "Menu")

        loadTable()
        setNavbar()
    }

    override func viewWillAppear(_: Bool) {
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: false)
        }
        navigationController?.isHiddenHairline = true
    }

    private func loadTable() {
        dataSource = UITableViewDiffableDataSource<Section, DrinkItem>(tableView: tableView) {
            (tableView: UITableView, indexPath: IndexPath, drinkItem: DrinkItem) -> UITableViewCell? in
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
                cell.clipsToBounds = true
                var content = DrinkTableViewContentConfiguration()
                content.drinkImage = drinkItem.image
                content.description = drinkItem.description
                content.name = drinkItem.name
                if let url = drinkItem.url as NSURL? {
                    ImageCache.publicCache.load(url: url as NSURL, item: drinkItem) { fetchedItem, image in
                        if let img = image, fetchedItem.image != image {
                            var updatedSnapshot = self.dataSource.snapshot()
                            if let datasourceIndex = updatedSnapshot.indexOfItem(fetchedItem as! DrinkItem) {
                                let drinkItem = self.imageObjects[datasourceIndex]
                                drinkItem.image = img
                                updatedSnapshot.reloadItems([drinkItem])
                                self.dataSource.apply(updatedSnapshot, animatingDifferences: false)
                            }
                        }
                    }
                }

                cell.contentConfiguration = content
                return cell
        }
        dataSource.defaultRowAnimation = .fade

        // Get our image URLs for processing.
        if imageObjects.isEmpty, !CheckoutCart.shared.activeUserBusinessDrinks.isEmpty {
            for index in 0 ..< CheckoutCart.shared.activeUserBusinessDrinks.count {
                let drink = CheckoutCart.shared.activeUserBusinessDrinks[index]
                imageObjects.append(DrinkItem(drink: drink))
            }
            var initialSnapshot = NSDiffableDataSourceSnapshot<Section, DrinkItem>()
            initialSnapshot.appendSections([.main])
            initialSnapshot.appendItems(imageObjects)
            dataSource.apply(initialSnapshot, animatingDifferences: false)
        }
    }

    private func setupView() {
        tableView.backgroundColor = .white
        view.backgroundColor = .white
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.separatorColor = .clear
        view.addSubview(tableView)
        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 0),
            tableView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: 0),
            tableView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: 0),
        ])

        tableView.delegate = self
    }

    func numberOfSections(in _: UITableView) -> Int {
        return 1
    }

    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return CheckoutCart.shared.activeUserBusinessDrinks.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        return (tableView.frame.height / 5)
    }

    func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedDrink = CheckoutCart.shared.activeUserBusinessDrinks[indexPath.row]
        let itemForIndex = imageObjects[indexPath.row]
        let drinkImage = itemForIndex.image
        navigationController!.pushViewController(DrinkViewController(drink: selectedDrink, isCheckout: false, drinkUIImage: drinkImage!), animated: true)
    }
}
