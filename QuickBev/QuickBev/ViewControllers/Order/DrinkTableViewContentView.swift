//
//  DrinkTableViewContentView.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 4/7/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//
import UIKit
class DrinkTableViewContentView: UIView, UIContentView {
    var configuration: UIContentConfiguration {
        didSet {
            configure(configuration: configuration)
        }
    }

    private let drinkImageView = RoundedImageView(theme: Theme.UIView(props: []))
    private let name = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont(name: "GillSans", size: 20.0))]))
    private let drinkDescription = UILabel(theme: Theme.UILabel(props: [.font(UIFont(name: "GillSans", size: 16.0))]))
    private let stackView = UIStackView(theme: Theme.UIView(props: []))

    init(configuration: UIContentConfiguration) {
        self.configuration = configuration
        super.init(frame: .zero)
        // ... configure the subviews ...
        configure(configuration: self.configuration)
        // ... and add them as subviews to self ...
    }

    private func setupView() {
        addSubview(drinkImageView)
        backgroundColor = .white
        clipsToBounds = true
        drinkDescription.numberOfLines = 0
        drinkDescription.lineBreakMode = .byWordWrapping
        drinkDescription.textColor = UIColor.systemGray

        let safeArea = safeAreaLayoutGuide
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.addArrangedSubview(name)
        stackView.addArrangedSubview(drinkDescription)
        stackView.spacing = 10
        addSubview(stackView)
        NSLayoutConstraint.activate([
            drinkImageView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -20.0)),
            drinkImageView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: getHeightRatio(verticalConstant: 30.0)),
            drinkImageView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: getHeightRatio(verticalConstant: -30.0)),
            drinkImageView.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.23),
            stackView.trailingAnchor.constraint(equalTo: drinkImageView.leadingAnchor, constant: getWidthRatio(horizontalConstant: -30.0)),
            stackView.topAnchor.constraint(equalTo: drinkImageView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 20.0)),
        ])
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configure(configuration: UIContentConfiguration) {
        guard let config = configuration as? DrinkTableViewContentConfiguration else { return }
        name.text = config.name.capitalized
        drinkDescription.text = config.description.capitalizedFirst
        drinkImageView.image = config.drinkImage
        setupView()
    }
}
