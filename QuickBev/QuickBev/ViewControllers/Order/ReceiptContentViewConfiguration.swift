//
//  ReceiptContentViewConfiguration.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 12/8/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit

struct ReceiptContentConfiguration: UIContentConfiguration {
    func updated(for _: UIConfigurationState) -> ReceiptContentConfiguration {
        return self
    }

    var name = ""
    var drinkDescription = ""
    var drinkImage = UIImage()
    var quantity = 0
    var price = 0.0
    init(drinkItem: DrinkItem) {
        drinkImage = drinkItem.image
        drinkDescription = drinkItem.description
        name = drinkItem.name
        quantity = Int(drinkItem.quantity)
        price = drinkItem.price
    }

    func makeContentView() -> UIView & UIContentView {
        let c = ReceiptContentView(configuration: self)
        return c
    }
}
