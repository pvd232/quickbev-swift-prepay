//
//  MenuViewController.swift
//  QuickBev
//
//  Created by Peter Driscoll on 3/17/22.
//  Copyright © 2022 Peter Vail Driscoll II. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    private var tableViewDataSource: UITableViewDiffableDataSource<DrinkCategory, DrinkItem>!
    private lazy var collectionViewDataSource = makeCollectionView()
    private lazy var tableView: UITableView = {
        let view = UITableView(frame: UIScreen.main.bounds, style: UITableView.Style.grouped)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
    private var drinkItemsWithCategories = [[DrinkCategory: [DrinkItem]]]()
    private var drinkItems = [DrinkItem]()
    private var drinkCategories = [DrinkCategory]()
    private var firstTouch = true
    private var idx: IndexPath?
    private var resetPriorCell = false

    // MARK: View

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupCollectionView()
        setupTableView()
        setupHeaderView(otherView: collectionView, header: "Menu")
        loadData()
        setNavbar()
    }

    override func viewWillAppear(_: Bool) {
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: false)
        }
        navigationController?.isHiddenHairline = true
    }

    private func makeCollectionView() -> UICollectionViewDiffableDataSource<Section, DrinkCategory> {
        return UICollectionViewDiffableDataSource<Section, DrinkCategory>(collectionView: collectionView) {
            (collectionView: UICollectionView, indexPath: IndexPath, drinkCategory: DrinkCategory) -> UICollectionViewCell? in
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)

                cell.clipsToBounds = true
                let contentConfiguration = MenuCollectionContentConfiguration(drinkCategory: drinkCategory)
                cell.contentConfiguration = contentConfiguration

                if indexPath.row == 0 {
                    cell.addBottomBorder(with: .themeColor, andWidth: 2.0)
                }
                return cell
        }
    }

    private func loadData() {
        tableViewDataSource = UITableViewDiffableDataSource<DrinkCategory, DrinkItem>(tableView: tableView) {
            (tableView: UITableView, indexPath: IndexPath, drinkItem: DrinkItem) -> UITableViewCell? in
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
                cell.clipsToBounds = true
                var content = DrinkTableViewContentConfiguration()
                content.drinkImage = drinkItem.image
                content.description = drinkItem.description
                content.name = drinkItem.name
                if let url = drinkItem.url as NSURL? {
                    ImageCache.publicCache.load(url: url as NSURL, item: drinkItem) { fetchedItem, image in
                        if let img = image, fetchedItem.image != image {
                            var updatedSnapshot = self.tableViewDataSource.snapshot()
                            fetchedItem.image = img
                            updatedSnapshot.reloadItems([fetchedItem as! DrinkItem])
                            self.tableViewDataSource.apply(updatedSnapshot, animatingDifferences: false)
                        }
                    }
                }

                cell.contentConfiguration = content
                return cell
        }
        tableViewDataSource.defaultRowAnimation = .fade

        // Get our image URLs for processing.
        if drinkItems.isEmpty, !CheckoutCart.shared.activeUserBusinessDrinks.isEmpty {
            var initialDrinkItemSnapshot = NSDiffableDataSourceSnapshot<DrinkCategory, DrinkItem>()
            for drinkCategory in DrinkCategory.allCases {
                var drinkItemList = [DrinkItem]()

                for index in 0 ..< CheckoutCart.shared.activeUserBusinessDrinks.count {
                    let drink = CheckoutCart.shared.activeUserBusinessDrinks[index]
                    if drink.drinkCategory == drinkCategory {
                        let drinkItem = DrinkItem(drink: drink)
                        drinkItems.append(drinkItem)
                        drinkItemList.append(drinkItem)
                    }
                }
                if drinkItemList.isEmpty == false {
                    drinkCategories.append(drinkCategory)
                    initialDrinkItemSnapshot.appendSections([drinkCategory])
                    let newDrinkItemCategoryDict = [drinkCategory: drinkItemList]
                    drinkItemsWithCategories.append(newDrinkItemCategoryDict)
                    initialDrinkItemSnapshot.appendItems(drinkItemList, toSection: drinkCategory)
                }
            }
            tableViewDataSource.apply(initialDrinkItemSnapshot, animatingDifferences: false)

            var drinkCategorySnapshot = NSDiffableDataSourceSnapshot<Section, DrinkCategory>()
            drinkCategorySnapshot.appendSections([.main])
            drinkCategorySnapshot.appendItems(drinkCategories, toSection: .main)
            collectionViewDataSource.apply(drinkCategorySnapshot, animatingDifferences: false)
        }
    }

    private func setupTableView() {
        tableView.backgroundColor = .white
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.separatorColor = .clear

        view.addSubview(tableView)
        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 0),
            tableView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: 0),
            tableView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: 0),
            tableView.topAnchor.constraint(equalTo: collectionView.bottomAnchor, constant: 10),
        ])
        tableView.delegate = self
    }

    private func setupCollectionView() {
        view.addSubview(collectionView)

        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .white
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        collectionView.showsHorizontalScrollIndicator = false

        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            collectionView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.065),
            collectionView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 6.0)),
            collectionView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: 0),
        ])

        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
        }
        collectionView.delegate = self
    }
}

extension MenuViewController: UITableViewDelegate {
    func numberOfSections(in _: UITableView) -> Int {
        return drinkCategories.count
    }

    func tableView(_: UITableView, numberOfRowsInSection index: Int) -> Int {
        // return the number of sections corresponding to the number of items in that section
        let theSection = drinkCategories[index]
        return drinkItemsWithCategories.first(where: { $0.index(forKey: theSection) != nil
        })!.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        return (tableView.frame.height / 3.5
        )
    }

    func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let drinkItem = tableViewDataSource.itemIdentifier(for: indexPath) {
            guard let selectedDrink = CheckoutCart.shared.activeUserBusinessDrinks.first(where: { $0.id == drinkItem.drinkId }) else {
                return
            }
            navigationController!.pushViewController(DrinkViewController(drink: selectedDrink, isCheckout: false, drinkUIImage: drinkItem.image), animated: true)
        }
    }

    func tableView(_: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let section = drinkCategories[section]
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0))
        view.backgroundColor = .white

        let label = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 26.0)))]))

        label.textAlignment = .center
        label.text = section.rawValue.capitalized

        view.addSubview(label)

        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: getWidthRatio(horizontalConstant: 20.0)),
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: getHeightRatio(verticalConstant: 2.0)),
            label.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
        return view
    }
}

extension MenuViewController: UICollectionViewDelegate {
    func collectionView(_: UICollectionView, numberOfItemsInSection _: Int) -> Int {
        return drinkCategories.count
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let idx = idx {
            collectionView.deselectItem(at: idx, animated: false)
            if let cell = collectionView.cellForItem(at: idx) {
                cell.resetBorders()
            }
        }
        idx = indexPath
        if let cell = collectionView.cellForItem(at: indexPath) {
            cell.addBottomBorder(with: UIColor.themeColor, andWidth: 2.0)
            if firstTouch == true {
                if drinkCategories[indexPath.row] != .beer {
                    if let firstCell = collectionView.cellForItem(at: IndexPath(row: 0, section: 0)) {
                        firstCell.resetBorders()
                    }
                }
            }
        }
        DispatchQueue.main.async {
            collectionView.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition.left, animated: true)
            if self.firstTouch == true {
                self.firstTouch = false
                if let lastElement = indexPath.last, lastElement == self.drinkCategories.count {
                    self.tableView.scrollToRow(at: IndexPath(row: NSNotFound, section: indexPath.row), at: .top, animated: false)
                    return
                }
            }
            print("hi")
            self.tableView.scrollToRow(at: IndexPath(row: NSNotFound, section: indexPath.row), at: .top, animated: true)
        }
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) {
            cell.resetBorders()
        }
    }

    func numberOfSections(in _: UICollectionView) -> Int {
        return 1
    }
}

extension MenuViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout _: UICollectionViewLayout, sizeForItemAt _: IndexPath) -> CGSize
    {
        let cellSize = CGSize(width: collectionView.bounds.width / 3, height: collectionView.bounds.height)
        return cellSize
    }
}
