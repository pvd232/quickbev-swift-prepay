//
//  CheckoutCartTableViewContentConfiguration.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 4/9/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit

struct CheckoutCartTableViewContentConfiguration: UIContentConfiguration {
    func updated(for _: UIConfigurationState) -> CheckoutCartTableViewContentConfiguration {
        return self
    }

    let name: String
    let quantity: Int16
    let drinkImage: UIImage
    let price: Double
    // regular item
    init(drinkItem: DrinkItem) {
        name = drinkItem.name
        quantity = drinkItem.quantity
        drinkImage = drinkItem.image
        price = drinkItem.price
    }

    func makeContentView() -> UIView & UIContentView {
        // dynamically make this a chekcout cart content view or a quick pass content view
        let c = CheckoutCartTableViewContentView(configuration: self)
        return c
    }
}
