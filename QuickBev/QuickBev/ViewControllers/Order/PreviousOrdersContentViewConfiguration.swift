//
//  PreviousOrdersContentViewConfiguration.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 7/6/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//
import UIKit

struct PreviousOrdersContentViewConfiguration: UIContentConfiguration, Hashable {
    func updated(for state: UIConfigurationState) -> PreviousOrdersContentViewConfiguration {
        guard state is UICellConfigurationState else { return self }
        let updatedConfig = self
        return updatedConfig
    }

    var businessName = ""
    var orderNumberOfItems = ""
    var orderDate = ""
    var orderTotal = ""
    var orderStatus = ""
    var businessImage = UIImage()
    init(orderItem: OrderItem) {
        businessName = orderItem.businessName!
        if orderItem.orderNumberofItems > 1, orderItem.orderNumberofItems != 0 {
            orderNumberOfItems = "\(orderItem.orderNumberofItems!) items, "
        } else {
            orderNumberOfItems = "\(orderItem.orderNumberofItems!) item, "
        }

        let dateformatter = DateFormatter() // 2-2
        dateformatter.dateStyle = .short
        orderDate = dateformatter.string(from: orderItem.orderDate)
        orderTotal = orderItem.orderTotal.formattedCurrency().formattedCurrencyString()
        orderStatus = orderItem.orderStatus
        businessImage = orderItem.image
    }

    func makeContentView() -> UIView & UIContentView {
        return PreviousOrdersContentView(configuration: self)
    }
}
