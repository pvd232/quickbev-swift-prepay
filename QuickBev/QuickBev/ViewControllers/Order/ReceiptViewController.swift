//
//  PreviousOrdersDetailViewController.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 7/6/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit

class ReceiptViewController: UIViewController {
    private var dataSource: UITableViewDiffableDataSource<Section, DrinkItem>!
    private let orderItemsTableView = UITableView(theme: Theme.UIView(props: []))

    private var drinkItems = [DrinkItem]()

    private let locationImageView = UIImageView(theme: Theme.UIView(props: []))
    private let locationImage = UIImage(systemName: "mappin")

    private let dateTimeImageView = UIImageView(theme: Theme.UIView(props: []))
    private let dateTimeImage = UIImage(systemName: "calendar")

    private let paymentMethodImageView = UIImageView(theme: Theme.UIView(props: []))
    private let paymentMethodImage = UIImage(systemName: "creditcard")

    private let orderItemsImageView = UIImageView(theme: Theme.UIView(props: []))
    private let orderItemsImage = UIImage(systemName: "list.bullet")

    private let pickupDetailsStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 25.0)))]))

    private let locationLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Location"), .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 22.0)))]))
    private let locationStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical]))
    private let businessNameLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.themeLabelFont)]))
    private let addressLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.themeLabelFont)]))

    private let pickupInstructionsStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical]))
    private let dateTimeLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Date & Time"), .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 22.0)))]))
    private let dateTimeText = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.themeLabelFont)]))

    let orderItemsContainerView = UIView(theme: Theme.UIView(props: []))
    let orderItemsStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical]))
    let orderItemsLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Order Items"), .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 22.0)))]))

    let paymentMethodStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical]))
    let paymentMethodLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Payment Method"), .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 22.0)))]))
    let paymentMethodText = UILabel(theme: Theme.UILabel(props: [.font(UIFont.themeLabelFont), .textColor]))

    let totalCalculationStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(Float(getWidthRatio(horizontalConstant: 20.0)))]))

    let calcLabelsStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 10.0)))]))
    let calcValuesStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 10.0)))]))

    let creditCardFeeLabel = UILabel(theme: Theme.UILabel(props: [.text("Credit Card/Service Fee"), .font(UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 20.0)))]))
    let creditCardFeeValueLabel = UILabel(theme: Theme.UILabel(props: [.font(UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 20.0)))]))

    let serviceFeeLabel = UILabel(theme: Theme.UILabel(props: [.text("Service fee"), .font(UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 20.0)))]))
    let serviceFeeValueLabel = UILabel(theme: Theme.UILabel(props: [.font(UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 20.0)))]))

    let tipLabel = UILabel(theme: Theme.UILabel(props: [.text("Tip"), .font(UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 20.0)))]))
    let tipValueLabel = UILabel(theme: Theme.UILabel(props: [.font(UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 20.0)))]))

    let taxLabel = UILabel(theme: Theme.UILabel(props: [.text("Sales Tax"), .font(UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 20.0)))]))
    let taxValueLabel = UILabel(theme: Theme.UILabel(props: [.font(UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 20.0)))]))

    let subtotalLabel = UILabel(theme: Theme.UILabel(props: [.text("Subtotal"), .font(UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 20.0)))]))
    let subtotalValueLabel = UILabel(theme: Theme.UILabel(props: [.font(UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 20.0)))]))

    let totalLabel = UILabel(theme: Theme.UILabel(props: [.text("Total"), .font(UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 20.0))), .textColor]))
    let totalValueLabel = UILabel(theme: Theme.UILabel(props: [.font(UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 20.0))), .textColor]))

    let containerView = UIView(theme: Theme.UIView(props: []))
    var userBusiness: Business!
    var selectedOrder: Order!
    let localIsModal: Bool!
    var hasOrderItemsImageView = false

    init(SelectedOrder: Order, IsModal: Bool) {
        selectedOrder = SelectedOrder
        localIsModal = IsModal

        if selectedOrder.orderDrinkArray.count == 1 {
            hasOrderItemsImageView = true
        }

        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if let business = CoreDataManager.sharedManager.fetchEntity(entityName: "Business", propertyToFilter: "id", propertyValue: selectedOrder.businessId.uuidString) as? Business {
            userBusiness = business
        }
        addViews()
        setViewPropertyAttributes()
        setViewContraints()
        setupTableView()
        loadTable()
        setupHeaderView(otherView: pickupDetailsStackView, header: "Receipt", distanceFromHeader: getHeightRatio(verticalConstant: -20.0))
        if localIsModal == true {
            setNavbar()
        } else {
            setNonModalNavbar()
        }
    }

    @objc private func backButton() {
        navigationController?.popViewController(animated: true)
    }

    private func addViews() {
        navigationItem.setHidesBackButton(true, animated: false)
        view.addSubview(locationImageView)
        view.addSubview(dateTimeImageView)
        view.addSubview(paymentMethodImageView)

        // not enough space for this is multiple drinks in order
        if hasOrderItemsImageView == true {
            view.addSubview(orderItemsImageView)
        }

        view.addSubview(pickupDetailsStackView)
        view.addSubview(orderItemsTableView)
        view.addSubview(totalCalculationStackView)

        pickupDetailsStackView.addArrangedSubview(locationStackView)
        pickupDetailsStackView.addArrangedSubview(pickupInstructionsStackView)
        pickupDetailsStackView.addArrangedSubview(paymentMethodStackView)

        if hasOrderItemsImageView == true {
            pickupDetailsStackView.addArrangedSubview(orderItemsStackView)
        }

        locationStackView.addArrangedSubview(locationLabel)
        locationStackView.addArrangedSubview(businessNameLabel)
        locationStackView.addArrangedSubview(addressLabel)

        pickupInstructionsStackView.addArrangedSubview(dateTimeLabel)
        pickupInstructionsStackView.addArrangedSubview(dateTimeText)

        paymentMethodStackView.addArrangedSubview(paymentMethodLabel)
        paymentMethodStackView.addArrangedSubview(paymentMethodText)

        if hasOrderItemsImageView == true {
            orderItemsStackView.addArrangedSubview(orderItemsLabel)
        }

        totalCalculationStackView.addArrangedSubview(calcLabelsStackView)
        totalCalculationStackView.addArrangedSubview(calcValuesStackView)

        calcLabelsStackView.addArrangedSubview(subtotalLabel)
        calcLabelsStackView.addArrangedSubview(tipLabel)
        calcLabelsStackView.addArrangedSubview(taxLabel)
        calcLabelsStackView.addArrangedSubview(creditCardFeeLabel)
        calcLabelsStackView.addArrangedSubview(totalLabel)

        calcValuesStackView.addArrangedSubview(subtotalValueLabel)
        calcValuesStackView.addArrangedSubview(tipValueLabel)
        calcValuesStackView.addArrangedSubview(taxValueLabel)
        calcValuesStackView.addArrangedSubview(creditCardFeeValueLabel)
        calcValuesStackView.addArrangedSubview(totalValueLabel)
    }

    private func setViewPropertyAttributes() {
        locationStackView.spacing = CGFloat(getHeightRatio(verticalConstant: 5.0))
        pickupInstructionsStackView.spacing = CGFloat(getHeightRatio(verticalConstant: 5.0))
        paymentMethodStackView.spacing = CGFloat(getHeightRatio(verticalConstant: 5.0))

        businessNameLabel.text = userBusiness.formattedName

        addressLabel.lineBreakMode = .byWordWrapping
        addressLabel.numberOfLines = 0
        addressLabel.text = userBusiness!.address

        taxValueLabel.textColor = .systemGray
        tipValueLabel.textColor = .systemGray
        creditCardFeeValueLabel.textColor = .systemGray
        serviceFeeValueLabel.textColor = .systemGray
        subtotalValueLabel.textColor = .systemGray

        dateTimeText.numberOfLines = 0

        dateTimeText.text = selectedOrder.orderDate + " at " + selectedOrder.orderTime

        paymentMethodText.text = selectedOrder.cardInformation

        locationImageView.image = locationImage?.withRenderingMode(.alwaysTemplate)
        locationImageView.tintColor = .themeColor

        dateTimeImageView.image = dateTimeImage?.withRenderingMode(.alwaysTemplate)
        dateTimeImageView.tintColor = .themeColor

        paymentMethodImageView.image = paymentMethodImage?.withRenderingMode(.alwaysTemplate)
        paymentMethodImageView.tintColor = .themeColor

        orderItemsImageView.image = orderItemsImage?.withRenderingMode(.alwaysTemplate)
        orderItemsImageView.tintColor = .themeColor

        subtotalValueLabel.text = selectedOrder.subtotal.formattedCurrency().formattedCurrencyString()
        tipValueLabel.text = selectedOrder.tipTotal.formattedCurrency().formattedCurrencyString()
        taxValueLabel.text = selectedOrder.salesTaxTotal.formattedCurrency().formattedCurrencyString()
        creditCardFeeValueLabel.text = selectedOrder.stripeFeeAndServiceFeeTotal.formattedCurrency().formattedCurrencyString()
        serviceFeeValueLabel.text = selectedOrder.serviceFeeTotal.formattedCurrency().formattedCurrencyString()
        totalValueLabel.text = selectedOrder.total.formattedCurrency().formattedCurrencyString()
    }

    private func setViewContraints() {
        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            locationImageView.centerYAnchor.constraint(equalTo: locationStackView.centerYAnchor),
            locationImageView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.05),
            locationImageView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            locationImageView.trailingAnchor.constraint(equalTo: pickupDetailsStackView.leadingAnchor, constant: getWidthRatio(horizontalConstant: -15.0)),

            dateTimeImageView.centerYAnchor.constraint(equalTo: pickupInstructionsStackView.centerYAnchor),
            dateTimeImageView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.05),
            dateTimeImageView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            dateTimeImageView.trailingAnchor.constraint(equalTo: pickupDetailsStackView.leadingAnchor, constant: getWidthRatio(horizontalConstant: -15.0)),

            paymentMethodImageView.centerYAnchor.constraint(equalTo: paymentMethodStackView.centerYAnchor),
            paymentMethodImageView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.05),
            paymentMethodImageView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            paymentMethodImageView.trailingAnchor.constraint(equalTo: pickupDetailsStackView.leadingAnchor, constant: getWidthRatio(horizontalConstant: -15.0)),

            pickupDetailsStackView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -20.0)),

            totalCalculationStackView.topAnchor.constraint(equalTo: orderItemsTableView.bottomAnchor, constant: getHeightRatio(verticalConstant: 20.0)),
            totalCalculationStackView.trailingAnchor.constraint(equalTo: pickupDetailsStackView.trailingAnchor),
        ])

        if hasOrderItemsImageView == true {
            NSLayoutConstraint.activate([
                orderItemsImageView.centerYAnchor.constraint(equalTo: orderItemsStackView.centerYAnchor),
                orderItemsImageView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.05),
                orderItemsImageView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
                orderItemsImageView.trailingAnchor.constraint(equalTo: pickupDetailsStackView.leadingAnchor, constant: getWidthRatio(horizontalConstant: -15.0)),
            ])
        }
    }

    private func loadTable() {
        dataSource = UITableViewDiffableDataSource<Section, DrinkItem>(tableView: orderItemsTableView) {
            (orderItemsTableView: UITableView, indexPath: IndexPath, drinkItem: DrinkItem) -> UITableViewCell? in
                let cell = orderItemsTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
                cell.contentConfiguration = nil
                cell.clipsToBounds = true
                let content = ReceiptContentConfiguration(drinkItem: drinkItem)
                if let url = drinkItem.url as NSURL? {
                    ImageCache.publicCache.load(url: url as NSURL, item: drinkItem) { fetchedItem, image in
                        if let img = image, img != fetchedItem.image {
                            var updatedSnapshot = self.dataSource.snapshot()
                            if let datasourceIndex = updatedSnapshot.indexOfItem(fetchedItem as! DrinkItem) {
                                let drinkItem = self.drinkItems[datasourceIndex]
                                drinkItem.image = img
                                updatedSnapshot.reloadItems([drinkItem])
                                self.dataSource.apply(updatedSnapshot, animatingDifferences: false)
                            }
                        }
                    }
                }
                cell.contentConfiguration = content
                return cell
        }
        dataSource.defaultRowAnimation = .fade

        // Get our image URLs for processing.
        if drinkItems.isEmpty, !selectedOrder.orderDrinkArray.isEmpty {
            for index in 0 ..< selectedOrder.orderDrinkArray.count {
                drinkItems.append(DrinkItem(orderDrink: selectedOrder.orderDrinkArray[index]))
            }
            var initialSnapshot = NSDiffableDataSourceSnapshot<Section, DrinkItem>()
            initialSnapshot.appendSections([.main])
            initialSnapshot.appendItems(drinkItems)
            dataSource.apply(initialSnapshot, animatingDifferences: false)
        }
    }

    private func setupTableView() {
        view.backgroundColor = .white

        view.addSubview(orderItemsTableView)
        view.addSubview(orderItemsContainerView)

        orderItemsContainerView.addSubview(orderItemsTableView)

        orderItemsTableView.backgroundColor = .white
        orderItemsTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        orderItemsTableView.separatorColor = .clear

        let safeArea = view.safeAreaLayoutGuide
        if selectedOrder.orderDrinkArray.count > 1 {
            NSLayoutConstraint.activate([
                orderItemsTableView.heightAnchor.constraint(equalTo: safeArea.heightAnchor, multiplier: 0.27),
            ])
        } else {
            NSLayoutConstraint.activate([
                orderItemsTableView.heightAnchor.constraint(equalTo: safeArea.heightAnchor, multiplier: 0.13),
            ])
        }
        NSLayoutConstraint.activate([
            orderItemsTableView.topAnchor.constraint(equalTo: pickupDetailsStackView.bottomAnchor, constant: getHeightRatio(verticalConstant: 30.0)),
            orderItemsTableView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 0.0)),
            orderItemsTableView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: 0.0)),
            orderItemsContainerView.topAnchor.constraint(equalTo: orderItemsTableView.topAnchor),
            orderItemsContainerView.leadingAnchor.constraint(equalTo: orderItemsTableView.leadingAnchor),
            orderItemsContainerView.trailingAnchor.constraint(equalTo: orderItemsTableView.trailingAnchor),
            orderItemsContainerView.bottomAnchor.constraint(equalTo: orderItemsTableView.bottomAnchor),
        ])
        orderItemsTableView.delegate = self
        orderItemsContainerView.addTopBorder(with: UIColor.lightGray.withAlphaComponent(0.5), andWidth: 1.0)
        orderItemsContainerView.addBottomBorder(with: UIColor.lightGray.withAlphaComponent(0.5), andWidth: 1.0)
    }

    override func viewWillAppear(_: Bool) {
        if let indexPath = orderItemsTableView.indexPathForSelectedRow {
            orderItemsTableView.deselectRow(at: indexPath, animated: false)
        }
        navigationController?.isHiddenHairline = true
    }
}

extension ReceiptViewController: UITableViewDelegate {
    func numberOfSections(in _: UITableView) -> Int {
        return 1
    }

    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return selectedOrder.orderDrinkArray.count
    }

    func tableView(_ orderItemsTableView: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        if selectedOrder.orderDrinkArray.count > 1 {
            return (orderItemsTableView.frame.height / 2)
        } else {
            return (orderItemsTableView.frame.height)
        }
    }

    func tableView(_: UITableView, didSelectRowAt _: IndexPath) {}
}
