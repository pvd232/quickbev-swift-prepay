//
//  OrderConfirmationDrinkContentViewConfiguration.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 12/6/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
struct OrderConfirmationContentConfiguration: UIContentConfiguration {
    func updated(for _: UIConfigurationState) -> OrderConfirmationContentConfiguration {
        return self
    }

    var name = ""
    var drinkDescription = ""
    var drinkImage = UIImage()
    var quantity = 0

    init(drinkItem: DrinkItem) {
        drinkImage = drinkItem.image
        drinkDescription = drinkItem.description
        name = drinkItem.name
        quantity = Int(drinkItem.quantity)
    }

    func makeContentView() -> UIView & UIContentView {
        let c = OrderConfirmationContentView(configuration: self)
        return c
    }
}
