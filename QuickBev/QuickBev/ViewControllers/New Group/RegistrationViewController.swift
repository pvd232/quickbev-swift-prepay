//
//  RegistrationViewController.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 6/4/20.
//  Copyright © 2020 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
class RegistrationViewController: UIViewController, UITextFieldDelegate {
    private let firstNameTextField = RoundedUITextField(theme: Theme.UITextField(props: [.font(nil), .placeHolderText("Your first name"), .autocapitalizationType(autocapitalizationType: .none), .borderStyle(borderStyle: .roundedRect), .backgroundColor(UIColor.clear)]))
    private let lastNameTextField = RoundedUITextField(theme: Theme.UITextField(props: [.font(nil), .placeHolderText("Your last name"), .autocapitalizationType(autocapitalizationType: .none), .borderStyle(borderStyle: .roundedRect), .backgroundColor(UIColor.clear)]))
    private let emailTextField = RoundedUITextField(theme: Theme.UITextField(props: [.font(nil), .placeHolderText("Your email address"), .autocapitalizationType(autocapitalizationType: .none), .borderStyle(borderStyle: .roundedRect), .backgroundColor(UIColor.clear)]))
    private let confirmEmailTextField = NoPasteUITextField(theme: Theme.UITextField(props: [.font(nil), .placeHolderText("Confirm your email address"), .autocapitalizationType(autocapitalizationType: .none), .borderStyle(borderStyle: .roundedRect), .backgroundColor(UIColor.clear)]))
    private let passwordTextField = RoundedUITextField(theme: Theme.UITextField(props: [.font(nil), .placeHolderText("Your password"), .autocapitalizationType(autocapitalizationType: .none), .borderStyle(borderStyle: .roundedRect), .backgroundColor(UIColor.clear)]))
    private let confirmPasswordTextField = NoPasteUITextField(theme: Theme.UITextField(props: [.font(nil), .placeHolderText("Confirm your password"), .autocapitalizationType(autocapitalizationType: .none), .borderStyle(borderStyle: .roundedRect), .backgroundColor(UIColor.clear)]))
    private let firstNameLabel = UILabel(theme: Theme.UILabel(props: [.text("First Name"), .font(nil), .textColor]))
    private let lastNameLabel = UILabel(theme: Theme.UILabel(props: [.text("Last Name"), .font(nil), .textColor]))
    private let emailLabel = UILabel(theme: Theme.UILabel(props: [.text("Email"), .font(nil), .textColor]))
    private let passwordLabel = UILabel(theme: Theme.UILabel(props: [.text("Password"), .font(nil), .textColor]))
    private let confirmEmailLabel = UILabel(theme: Theme.UILabel(props: [.text("Confirm Email"), .font(nil), .textColor]))
    private let confirmPasswordLabel = UILabel(theme: Theme.UILabel(props: [.text("Confirm Password"), .font(nil), .textColor]))
    private let registrationStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 10.0)))]))
    private let submitButton = RoundButton(theme: Theme.RoundButton(props: [.color, .text("Submit"), .titleLabelFont(nil)]))
    @UsesAutoLayout private var newActivityIndicator = UIActivityIndicatorView(style: .large)

    private var formValues: [String: String] = [:]
    private lazy var navController: UINavigationController = {
        SceneDelegate.shared.rootViewController.current
    }()

    init() {
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(registrationStackView)
        view.addSubview(submitButton)
        view.addSubview(newActivityIndicator)
        registrationStackView.addArrangedSubview(firstNameLabel)
        registrationStackView.addArrangedSubview(firstNameTextField)
        registrationStackView.addArrangedSubview(lastNameLabel)
        registrationStackView.addArrangedSubview(lastNameTextField)
        registrationStackView.addArrangedSubview(emailLabel)
        registrationStackView.addArrangedSubview(emailTextField)
        registrationStackView.addArrangedSubview(confirmEmailLabel)
        registrationStackView.addArrangedSubview(confirmEmailTextField)
        registrationStackView.addArrangedSubview(passwordLabel)
        registrationStackView.addArrangedSubview(passwordTextField)
        registrationStackView.addArrangedSubview(confirmPasswordLabel)
        registrationStackView.addArrangedSubview(confirmPasswordTextField)

        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        emailTextField.delegate = self
        confirmEmailTextField.delegate = self
        passwordTextField.delegate = self
        confirmPasswordTextField.delegate = self

        passwordTextField.isSecureTextEntry = true
        confirmPasswordTextField.isSecureTextEntry = true

        newActivityIndicator.backgroundColor = UIColor(white: 0, alpha: 0.4)
        newActivityIndicator.frame = view.bounds

        let safeArea = view.safeAreaLayoutGuide

        NSLayoutConstraint.activate([
            submitButton.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: getHeightRatio(verticalConstant: -10.0)),
            submitButton.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            submitButton.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -10.0)),
            submitButton.widthAnchor.constraint(equalTo: submitButton.heightAnchor, multiplier: 197 / 25),
            submitButton.topAnchor.constraint(lessThanOrEqualTo: registrationStackView.bottomAnchor, constant: UIViewController.screenSize.height * 0.31),

            firstNameTextField.widthAnchor.constraint(equalTo: firstNameTextField.heightAnchor, multiplier: 197 / 25),
            lastNameTextField.widthAnchor.constraint(equalTo: lastNameTextField.heightAnchor, multiplier: 197 / 25),
            emailTextField.widthAnchor.constraint(equalTo: emailTextField.heightAnchor, multiplier: 197 / 25),
            confirmEmailTextField.widthAnchor.constraint(equalTo: confirmEmailTextField.heightAnchor, multiplier: 197 / 25),
            passwordTextField.widthAnchor.constraint(equalTo: passwordTextField.heightAnchor, multiplier: 197 / 25),
            confirmPasswordTextField.widthAnchor.constraint(equalTo: confirmPasswordTextField.heightAnchor, multiplier: 197 / 25),

            newActivityIndicator.topAnchor.constraint(equalTo: safeArea.topAnchor),
            newActivityIndicator.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            newActivityIndicator.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            newActivityIndicator.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),

            registrationStackView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -10.0)),
            registrationStackView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            registrationStackView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: getHeightRatio(verticalConstant: 10.0)),
        ])
        submitButton.addTarget(self, action: #selector(submitRegistration), for: .touchUpInside)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.placeholder == "Your first name" {
            formValues["firstName"] = textField.text?.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        }
        if textField.placeholder == "Your last name" {
            formValues["lastName"] = textField.text?.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        }
        if textField.placeholder == "Your email address" {
            formValues["email"] = textField.text?.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        }
        if textField.placeholder == "Confirm your email address" {
            formValues["confirmEmail"] = textField.text?.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        }
        if textField.placeholder == "Your password" {
            formValues["password"] = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        if textField.placeholder == "Confirm your password" {
            formValues["confirmPassword"] = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.placeholder == "Your first name" {
            formValues["firstName"] = textField.text?.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        }
        if textField.placeholder == "Your last name" {
            formValues["lastName"] = textField.text?.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        }
        if textField.placeholder == "Your email address" {
            formValues["email"] = textField.text?.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        }
        if textField.placeholder == "Confirm your email address" {
            formValues["confirmEmail"] = textField.text?.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        }
        if textField.placeholder == "Your password" {
            formValues["password"] = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        if textField.placeholder == "Confirm your password" {
            formValues["confirmPassword"] = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        return true
    }

    func checkFormValues() -> Bool {
        if formValues["firstName"] == "" || formValues["lastName"] == "" || formValues["email"] == "" || formValues["confirmEmail"] == "" || formValues["password"] == "" || formValues["confirmPassword"] == "" || formValues["firstName"] == nil || formValues["lastName"] == nil || formValues["email"] == nil || formValues["confirmEmail"] == nil || formValues["password"] == nil || formValues["confirmPassword"] == nil {
            return false
        } else if formValues["email"] != formValues["confirmEmail"] || formValues["password"] != formValues["confirmPassword"] {
            return false
        } else if formValues["email"]?.contains("@") != true || formValues["email"]?.contains(".") != true
        {
            return false
        } else {
            return true
        }
    }

    func presentAlert() {
        var title = ""
        var message = ""
        if formValues["firstName"] == "" || formValues["lastName"] == "" || formValues["email"] == "" || formValues["confirmEmail"] == "" || formValues["password"] == "" || formValues["confirmPassword"] == "" || formValues["firstName"] == nil || formValues["lastName"] == nil || formValues["email"] == nil || formValues["confirmEmail"] == nil || formValues["password"] == nil || formValues["confirmPassword"] == nil {
            title = "Missing information"
            message = "Some information is blank. Please correct this and resubmit."
        } else if formValues["email"] != formValues["confirmEmail"] || formValues["password"] != formValues["confirmPassword"] {
            if formValues["email"] != formValues["confirmEmail"], formValues["password"] == formValues["confirmPassword"] {
                title = "Email mismatch"
                message = "Email and confirmation email are different. Please correct them and resubmit."
            } else if formValues["password"] != formValues["confirmPassword"], formValues["email"] == formValues["confirmEmail"] {
                title = "Password mismatch"
                message = "Password and confirmation password are different. Please correct them and resubmit."
            } else if formValues["email"] != formValues["confirmEmail"], formValues["password"] != formValues["confirmPassword"] {
                title = "Email and password mismatch"
                message = "Email, confirmation email, password and confirmation password are different. Please correct them and resubmit."
            }
        } else if formValues["email"]?.contains("@") != true || formValues["email"]?.contains(".") != true {
            title = "Invalid email"
            message = "Your email is in an invalid format. Please correct it and resubmit."
        }

        DispatchQueue.main.async {
            self.newActivityIndicator.stopAnimating()
            self.presentAlertController(title: title, message: message)
        }
    }

    @objc private func submitRegistration(_: RoundButton) {
        DispatchQueue.main.async {
            self.newActivityIndicator.startAnimating()
        }

        if checkFormValues() == false {
            presentAlert()
            return
        }

        let requestedNewUser = User(FirstName: formValues["firstName"]!, LastName: formValues["lastName"]!, Email: formValues["email"]!, Password: formValues["password"]!, EmailVerified: false)
        let userCredentials: [HTTPHeader] = [HTTPHeader(field: "device-token", value: CheckoutCart.shared.deviceToken)]
        let request = try! APIRequest(method: .post, path: "/customer/\(CheckoutCart.shared.sessionToken)", body: requestedNewUser, headers: userCredentials)
        APIClient().perform(request) { result in
            switch result {
            case let .success(response):
                if response.statusCode == 200, let response = try? response.decode(to: User.self) {
                    let fetchedUser = response.body as User
                    CheckoutCart.shared.user = fetchedUser
                    fetchedUser.userToCheckoutCart = CheckoutCart.shared

                    // if the new user's stripe id is nill then this is the regular user creation process and a stripe id will be sent from the backend
                    CheckoutCart.shared.stripeId = fetchedUser.stripeId
                    requestedNewUser.stripeId = fetchedUser.stripeId

                    let sessionToken = response.headers["jwt-token"] as! String
                    try! SecureStore(secureStoreQueryable: GenericPasswordQueryable()).setValue(sessionToken, for: "sessionToken")
                    // set the token in the shopping cart and use it whenever calling the backend
                    DispatchQueue.main.async {
                        CoreDataManager.sharedManager.saveContext()
                        self.newActivityIndicator.stopAnimating()
                        SceneDelegate.shared.rootViewController.switchToHomePageViewController()
                    }
                } else if response.statusCode == 201, let response = try? response.decode(to: [String: UserData].self) {
                    if let userData = response.body["customer"] {
                        if let users = CoreDataManager.sharedManager.fetchEntities(entityName: "User") as? [User], users.count >= 1 {
                            // there will always be a matching user here unless the user deleted the app and redownloaded it
                            if let matchingUser = users.first(where: {
                                $0.email == userData.email
                            }) {
                                matchingUser.update(userData: userData)
                                CheckoutCart.shared.user = matchingUser
                                matchingUser.userToCheckoutCart = CheckoutCart.shared
                                CoreDataManager.sharedManager.saveContext()
                            } else {
                                let localCopyOfExistingUser = User(userData: userData)
                                CheckoutCart.shared.user = localCopyOfExistingUser
                                localCopyOfExistingUser.userToCheckoutCart = CheckoutCart.shared
                                CoreDataManager.sharedManager.saveContext()
                            }
                        }
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                        CoreDataManager.sharedManager.saveContext()
                        self.newActivityIndicator.stopAnimating()
                        SceneDelegate.shared.rootViewController.switchToHomePageViewController()
                    }

                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                        self.newActivityIndicator.stopAnimating()
                        self.presentAlertController(title: "Username error", message: "Your username is taken. Please choose another one.")
                    }
                }
            case let .failure(error): // if the API fails the enum API result will be of the type failure
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                    self.newActivityIndicator.stopAnimating()
                    self.presentNetworkError()
                    print("error case .failure", error)
                }
            }
        }
    }
}
