//
//  QuickPassDetailViewController.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 8/2/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
class QuickPassDetailViewController: UIViewController {
    private let centerTitleStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 30.0)))]))
    private let detailsStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 20.0)))]))
    private let priceStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(Float(getWidthRatio(horizontalConstant: 20)))]))
    private let businessNameStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(Float(getWidthRatio(horizontalConstant: 20)))]))
    private let ETAStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(Float(getWidthRatio(horizontalConstant: 20)))]))

    private let businessNameLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Venue"), .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 22.0)))]))
    private let businessNameValueLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 22.0)))]))

    private let priceLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Price"), .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 22.0)))]))
    private let priceValueLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 22.0)))]))

    private let shouldDisplayExpirationTime: Bool!

    private let ETALabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Expiration time"), .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 22.0)))]))
    private let ETAValueLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 22.0)))]))

    private let descriptionTextView = UITextViewFixed(theme: Theme.UIView(props: []))
    private let imageView = UIImageView(theme: Theme.UIView(props: []))

    private let quickPassLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("QuickPass"), .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 30.0)))]))
    private let sloganTextView = UITextView(theme: Theme.UITextView(props: [.font(UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 26.0))), .text("Less waiting, more doing")]))

    private let addToOrderButton = UIButton(theme: Theme.UIView(props: []))
    @UsesAutoLayout private var newActivityIndicator = UIActivityIndicatorView(style: .large)

    init() {
        shouldDisplayExpirationTime = QuickPassData.shared.shouldDisplayExpirationTime
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
        setNavbar()
    }

    private func setupView() {
        view.backgroundColor = .white
        view.addSubview(centerTitleStackView)
        view.addSubview(detailsStackView)
        view.addSubview(descriptionTextView)
        view.addSubview(addToOrderButton)

        newActivityIndicator.backgroundColor = UIColor(white: 0, alpha: 0.4)

        view.addSubview(newActivityIndicator)
        view.bringSubviewToFront(newActivityIndicator)

        let largeFont = UIFont.systemFont(ofSize: getFontRatio(fontSize: 70))
        let configuration = UIImage.SymbolConfiguration(font: largeFont)
        imageView.image = UIImage(systemName: "ticket", withConfiguration: configuration)
        imageView.tintColor = UIColor.themeColor

        descriptionTextView.backgroundColor = .white

        centerTitleStackView.alignment = .center
        centerTitleStackView.addArrangedSubview(quickPassLabel)
        centerTitleStackView.addArrangedSubview(imageView)
        detailsStackView.addArrangedSubview(sloganTextView)

        sloganTextView.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: getHeightRatio(verticalConstant: 15.0
        ), right: 0)
        sloganTextView.addBottomBorder(with: UIColor.themeColor, andWidth: 3.0)
        sloganTextView.backgroundColor = .white
        sloganTextView.isScrollEnabled = false
        sloganTextView.textAlignment = .center
        sloganTextView.textColor = .themeColor

        quickPassLabel.textColor = .themeColor

        if QuickPassData.shared.soldOut == true {
            let nextHour = Calendar.current.component(.hour, from: Date()) + 1
            let nextHourDateTime = Calendar.current.date(from: DateComponents(year: Calendar.current.component(.year, from: Date()), month: Calendar.current.component(.month, from: Date()), day: Calendar.current.component(.day, from: Date()), hour: nextHour))

            let formatter = DateFormatter()
            formatter.timeZone = Calendar.current.timeZone
            formatter.locale = Calendar.current.locale
            formatter.dateFormat = "h a" // "a" prints "pm" or "am"

            let currentHourString = formatter.string(from: Date()) // "12 AM"
            let nextHourString = formatter.string(from: nextHourDateTime!) // "12 AM"

            let soldOutLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont(name: "GillSans", size: getFontRatio(fontSize: getFontRatio(fontSize: 22.0)))), .text("QuickPasses are sold out for \(currentHourString) - \(nextHourString).")]))
            let soldOutLabel2 = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont(name: "GillSans", size: getFontRatio(fontSize: getFontRatio(fontSize: 22.0)))), .text("Check back after \(nextHourString) when a new batch of QuickPasses become available.")]))

            soldOutLabel.textAlignment = .left
            soldOutLabel.numberOfLines = 0
            soldOutLabel.lineBreakMode = .byWordWrapping

            soldOutLabel2.textAlignment = .left
            soldOutLabel2.numberOfLines = 0
            soldOutLabel2.lineBreakMode = .byWordWrapping

            detailsStackView.addArrangedSubview(soldOutLabel)
            detailsStackView.addArrangedSubview(soldOutLabel2)

            let safeArea = view.safeAreaLayoutGuide
            NSLayoutConstraint.activate([
                detailsStackView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 20.0)),
                detailsStackView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -20.0)),
                detailsStackView.topAnchor.constraint(equalTo: centerTitleStackView.bottomAnchor, constant: getHeightRatio(verticalConstant: 20.0)),

                sloganTextView.leadingAnchor.constraint(equalTo: detailsStackView.leadingAnchor),
                sloganTextView.trailingAnchor.constraint(equalTo: detailsStackView.trailingAnchor),

                centerTitleStackView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
                centerTitleStackView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: getHeightRatio(verticalConstant: 60.0)),

                newActivityIndicator.topAnchor.constraint(equalTo: safeArea.topAnchor),
                newActivityIndicator.bottomAnchor.constraint(equalTo: view.bottomAnchor),
                newActivityIndicator.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
                newActivityIndicator.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
            ])
            return
        }

        detailsStackView.addArrangedSubview(businessNameStackView)
        detailsStackView.addArrangedSubview(priceStackView)
        detailsStackView.alignment = .leading

        businessNameStackView.addArrangedSubview(businessNameLabel)
        businessNameStackView.addArrangedSubview(businessNameValueLabel)

        priceStackView.addArrangedSubview(priceLabel)
        priceStackView.addArrangedSubview(priceValueLabel)

        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency

        // localize to your grouping and decimal separator
        currencyFormatter.locale = Locale.current
        businessNameValueLabel.text = {
            let business = CheckoutCart.shared.activeBusinessArray.first(where: {
                $0.id == QuickPassData.shared.businessId
            })
            return business!.name
        }()
        priceValueLabel.text = currencyFormatter.string(from: NSNumber(value: QuickPassData.shared.price))!
        if shouldDisplayExpirationTime == true {
            detailsStackView.addArrangedSubview(ETAStackView)
            ETAStackView.addArrangedSubview(ETALabel)
            ETAStackView.addArrangedSubview(ETAValueLabel)
            let dateformatter = DateFormatter() // 2-2
            dateformatter.timeStyle = .short // 2-3
            ETAValueLabel.text = dateformatter.string(from: QuickPassData.shared.expirationTime)
        }

        descriptionTextView.backgroundColor = .clear
        descriptionTextView.textColor = .black
        descriptionTextView.isEditable = false

        let descriptionText = "Use this QuickPass to skip the line! Just head to \(CheckoutCart.shared.quickPassBusiness!.name) before the expiration time, find the bouncer, and tell them your name. Cheers!"

        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        let attributes = [NSAttributedString.Key.paragraphStyle: style, NSAttributedString.Key.font: UIFont(name: "GillSans", size: getFontRatio(fontSize: 22.0))!]
        descriptionTextView.attributedText = NSAttributedString(string: descriptionText, attributes: attributes)

        addToOrderButton.setTitle("Review Order", for: .normal)

        addToOrderButton.backgroundColor = UIColor.themeColor
        addToOrderButton.titleLabel?.font = UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 30.0))
        addToOrderButton.contentHorizontalAlignment = .center

        addToOrderButton.addTarget(self, action: #selector(buyButtonPressed), for: .touchUpInside)
    }

    private func setupConstraints() {
        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            addToOrderButton.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
            detailsStackView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 20.0)),
            detailsStackView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -20.0)),
            detailsStackView.topAnchor.constraint(equalTo: centerTitleStackView.bottomAnchor, constant: getHeightRatio(verticalConstant: 20.0)),

            sloganTextView.leadingAnchor.constraint(equalTo: detailsStackView.leadingAnchor),
            sloganTextView.trailingAnchor.constraint(equalTo: detailsStackView.trailingAnchor),

            descriptionTextView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 20.0)),
            descriptionTextView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -10.0)),
            descriptionTextView.topAnchor.constraint(equalTo: detailsStackView.bottomAnchor, constant: getHeightRatio(verticalConstant: 30.0)),
            descriptionTextView.heightAnchor.constraint(equalTo: safeArea.heightAnchor, constant: 0.3),

            centerTitleStackView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            centerTitleStackView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: getHeightRatio(verticalConstant: 60.0)),

            addToOrderButton.topAnchor.constraint(greaterThanOrEqualTo: centerTitleStackView.bottomAnchor, constant: getHeightRatio(verticalConstant: 70.0)),
            addToOrderButton.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            addToOrderButton.widthAnchor.constraint(equalTo: addToOrderButton.superview!.widthAnchor),
            addToOrderButton.heightAnchor.constraint(equalTo: safeArea.heightAnchor, multiplier: 0.143032),
            addToOrderButton.bottomAnchor.constraint(equalTo: addToOrderButton.superview!.bottomAnchor),

            newActivityIndicator.topAnchor.constraint(equalTo: safeArea.topAnchor),
            newActivityIndicator.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            newActivityIndicator.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            newActivityIndicator.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
        ])
    }

    @objc private func buyButtonPressed(_: RoundButton) {
        let delay = 300 // miliseconds
        newActivityIndicator.startAnimating()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(delay)) {
            self.newActivityIndicator.stopAnimating()
            // bifurcate pop to home page here
            self.navigationController!.pushViewController(ReviewQuickPassOrderViewController(), animated: true)
        }
    }

    override func viewWillDisappear(_: Bool) {
        CheckoutCart.shared.calculateCost()
        newActivityIndicator.stopAnimating()
    }
}
