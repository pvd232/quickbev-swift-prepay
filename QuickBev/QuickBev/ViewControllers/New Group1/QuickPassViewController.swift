//
//  QuickPassTableViewController.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 7/30/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
class QuickPassViewController: UIViewController, UITableViewDelegate {
    private let tableView = UITableView(theme: Theme.UIView(props: []))
    private var quickPasses = [QuickPassItem]()
    private let cellReuseIdentifier = "cell"
    private lazy var dataSource = makeDataSource()
    init() {
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(tableView)
        setupView()
        setupHeaderView(otherView: tableView, header: "QuickPasses")

        setNavbar()
    }

    override func viewWillAppear(_: Bool) {
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
        navigationController?.isHiddenHairline = true

        quickPasses = []
        for quickPass in CheckoutCart.shared.allQuickPasses {
            if let orderBar = CheckoutCart.shared.activeBusinessArray.first(where: {
                $0.id == quickPass.businessId
            }) {
                let largeFont = UIFont.systemFont(ofSize: getFontRatio(fontSize: 28.0))
                let configuration = UIImage.SymbolConfiguration(font: largeFont)
                guard let quickPassImage = UIImage(systemName: "ticket", withConfiguration: configuration) else {
                    return
                }
                let quickPassItem = QuickPassItem(quickPass: quickPass, business: orderBar, image: quickPassImage)

                quickPasses.append(quickPassItem)
            }
        }

        quickPasses.sort {
            $0.expirationTime > $1.expirationTime
        }
        update(with: quickPasses)
    }

    private func update(with newQuickPasses: [QuickPassItem], animate _: Bool = true) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, QuickPassItem>()
        snapshot.appendSections([.main])
        snapshot.appendItems(newQuickPasses, toSection: .main)
        dataSource.apply(snapshot, animatingDifferences: false)
    }

    private func makeDataSource() -> UITableViewDiffableDataSource<Section, QuickPassItem> {
        let reuseIdentifier = cellReuseIdentifier
        return UITableViewDiffableDataSource(
            tableView: tableView,
            cellProvider: { tableView, indexPath, quickPassItem in
                let cell = tableView.dequeueReusableCell(
                    withIdentifier: reuseIdentifier,
                    for: indexPath
                )
                cell.clipsToBounds = true
                cell.contentConfiguration = nil
                cell.backgroundColor = .clear
                var content = QuickPassContentViewConfiguration(quickPassItem: quickPassItem)
                let dateformatter = DateFormatter()
                dateformatter.dateStyle = .medium
                dateformatter.timeStyle = .short
                content.dateTime = dateformatter.string(from: quickPassItem.activationTime)

                cell.contentConfiguration = content
                return cell
            }
        )
    }

    private func setupView() {
        view.backgroundColor = .white
        view.addSubview(tableView)
        tableView.backgroundColor = .white
        tableView.separatorColor = .clear

        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor),
        ])

        tableView.delegate = self
        tableView.dataSource = dataSource
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }

    func tableView(_ tableView: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        return (tableView.frame.height / 4)
    }

    func numberOfSections(in _: UITableView) -> Int {
        return 1
    }

    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return CheckoutCart.shared.allQuickPasses.count
    }

    func tableView(_: UITableView, didSelectRowAt index: IndexPath) {
        let selectedQuickPass = CheckoutCart.shared.allQuickPasses.first(where: {
            $0.id == quickPasses[index.row].quickPassId
        })

        navigationController?.pushViewController(QuickPassReceiptViewController(SelectedQuickPass: selectedQuickPass!, IsModal: false), animated: true)
    }
}
