//
//  ReviewQuickPassOrderViewController.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 8/2/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//
import PassKit
import Stripe
import UIKit
class ReviewQuickPassOrderViewController: UIViewController {
    private let orderSummaryStackView = UIStackView(theme: Theme.UIStackView(props: [.spacing(20.0), .vertical]))

    private let subtotalStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal]))
    private let subtotalHeaderLabel = UILabel(theme: Theme.UILabel(props: [.text("Subtotal"), .font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 22.0)))]))
    private let subtotalLabel = UILabel(theme: Theme.UILabel(props: [.font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 22.0)))]))

    private let salesTaxStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal]))
    private let salesTaxHeaderLabel = UILabel(theme: Theme.UILabel(props: [.text("Sales Tax"), .font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 22.0)))]))
    private let salesTaxLabel = UILabel(theme: Theme.UILabel(props: [.font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 22.0)))]))

    private let totalStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal]))
    private let totalHeaderLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Total"), .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 24.0)))]))
    private let orderTotalLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 24.0)))]))

    private let defaultPaymentMethodView = UIView(theme: Theme.UIView(props: []))
    private let cardLogoImageView = UIImageView(theme: Theme.UIView(props: []))
    private let cardCompanyName = UILabel(theme: Theme.UILabel(props: [.textColor, .font(.themeLabelFont)]))
    private let addPaymentMethodButton = RoundButton(theme: Theme.UIView(props: []))
    private let editPaymentMethodButton = RoundButton(theme: Theme.UIView(props: []))
    private let editQuickPassButton = RoundButton(theme: Theme.UIView(props: []))
    private let applePayButton = PKPaymentButton(paymentButtonType: .plain, paymentButtonStyle: .black)
    @UsesAutoLayout private var newActivityIndicator = UIActivityIndicatorView(style: .large)

    // need to dynamically apply this based on business location the user is at, should probably store a list of states and associated sales taxes in core data when first loading the app
    private lazy var paymentContext: STPPaymentContext = {
        STPPaymentContext(customerContext: CheckoutCart.shared.customerContext)
    }()

    init() {
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    private func setupView() {
        // load subviews
        view.addSubview(orderSummaryStackView)
        view.addSubview(defaultPaymentMethodView)
        view.addSubview(addPaymentMethodButton)
        view.addSubview(editQuickPassButton)

        newActivityIndicator.backgroundColor = UIColor(white: 0, alpha: 0.4)
        view.addSubview(newActivityIndicator)
        setupHeaderView(otherView: orderSummaryStackView, header: "Order summary", distanceFromHeader: getHeightRatio(verticalConstant: -20.0))

        navigationItem.title = "Checkout"

        // setup stripe
        paymentContext.delegate = self
        paymentContext.hostViewController = self
        // this is the amount that is displayed to the user on the screen
        // page content and formatting

        subtotalLabel.textColor = .systemGray.mix(with: .black, amount: 0.3)
        subtotalHeaderLabel.textColor = .systemGray.mix(with: .black, amount: 0.3)
        subtotalLabel.setContentHuggingPriority(UILayoutPriority(500), for: .horizontal)

        salesTaxLabel.textColor = .systemGray.mix(with: .black, amount: 0.3)
        salesTaxHeaderLabel.textColor = .systemGray.mix(with: .black, amount: 0.3)
        salesTaxLabel.setContentHuggingPriority(UILayoutPriority(500), for: .horizontal)

        orderTotalLabel.setContentHuggingPriority(UILayoutPriority(500), for: .horizontal)

        orderSummaryStackView.addArrangedSubview(subtotalStackView)
        orderSummaryStackView.addArrangedSubview(salesTaxStackView)
        orderSummaryStackView.addArrangedSubview(totalStackView)

        subtotalStackView.addArrangedSubview(subtotalHeaderLabel)
        subtotalStackView.addArrangedSubview(subtotalLabel)

        salesTaxStackView.addArrangedSubview(salesTaxHeaderLabel)
        salesTaxStackView.addArrangedSubview(salesTaxLabel)

        totalStackView.addArrangedSubview(totalHeaderLabel)
        totalStackView.addArrangedSubview(orderTotalLabel)

        defaultPaymentMethodView.addSubview(cardLogoImageView)
        defaultPaymentMethodView.addSubview(cardCompanyName)
        defaultPaymentMethodView.addSubview(editPaymentMethodButton)

        editPaymentMethodButton.refreshColor(color: UIColor.themeColor)
        editPaymentMethodButton.refreshTitle(newTitle: "edit")
        editPaymentMethodButton.titleLabel?.font = UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 18.0))

        addPaymentMethodButton.madeStandard()
        addPaymentMethodButton.refreshTitle(newTitle: "Add payment method")

        applePayButton.titleLabel?.font = UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 20.0))

        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            orderSummaryStackView.heightAnchor.constraint(equalTo: safeArea.heightAnchor, multiplier: 0.17),
            orderSummaryStackView.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.90),
            orderSummaryStackView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),

            defaultPaymentMethodView.heightAnchor.constraint(equalTo: safeArea.heightAnchor, multiplier: 0.0841584),
            defaultPaymentMethodView.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.9),
            defaultPaymentMethodView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            defaultPaymentMethodView.topAnchor.constraint(equalTo: orderSummaryStackView.bottomAnchor, constant: getHeightRatio(verticalConstant: 35.0)),

            cardLogoImageView.centerYAnchor.constraint(equalTo: cardLogoImageView.superview!.centerYAnchor),
            cardLogoImageView.widthAnchor.constraint(equalTo: cardLogoImageView.superview!.widthAnchor, multiplier: 0.15),
            cardLogoImageView.heightAnchor.constraint(equalTo: cardLogoImageView.superview!.heightAnchor, multiplier: 0.529412),
            cardLogoImageView.leadingAnchor.constraint(equalTo: cardLogoImageView.superview!.leadingAnchor),

            cardCompanyName.widthAnchor.constraint(equalTo: cardCompanyName.superview!.widthAnchor, multiplier: 0.365517),
            cardCompanyName.leadingAnchor.constraint(equalTo: cardLogoImageView.trailingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            cardCompanyName.centerYAnchor.constraint(equalTo: cardCompanyName.superview!.centerYAnchor),

            editPaymentMethodButton.heightAnchor.constraint(equalTo: editPaymentMethodButton.superview!.heightAnchor, multiplier: 0.497059),
            editPaymentMethodButton.trailingAnchor.constraint(equalTo: editPaymentMethodButton.superview!.trailingAnchor),
            editPaymentMethodButton.widthAnchor.constraint(greaterThanOrEqualTo: editPaymentMethodButton.superview!.widthAnchor, multiplier: 0.25),
            editPaymentMethodButton.centerYAnchor.constraint(equalTo: editPaymentMethodButton.superview!.centerYAnchor),

            newActivityIndicator.topAnchor.constraint(equalTo: safeArea.topAnchor),
            newActivityIndicator.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            newActivityIndicator.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            newActivityIndicator.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),

        ])
        view.bringSubviewToFront(newActivityIndicator)
        conditionallyDisplayPaymentButtons()

        paymentContextDidChange(paymentContext)
        addPaymentMethodButton.addTarget(self, action: #selector(choosePaymentButtonTapped), for: .touchUpInside)
        applePayButton.addTarget(self, action: #selector(handleApplePayButtonTapped), for: .touchUpInside)

        editPaymentMethodButton.addTarget(self, action: #selector(changeDefaultPaymentMethodButtonClicked), for: .touchUpInside)
    }

    override func viewWillAppear(_: Bool) {
        salesTaxLabel.text = CheckoutCart.shared.calculateQuickPassSalesTax().formattedCurrency().formattedCurrencyString()
        subtotalLabel.text = QuickPassData.shared.price.formattedCurrency().formattedCurrencyString()
        orderTotalLabel.text = CheckoutCart.shared.calculateQuickPassTotal().formattedCurrency().formattedCurrencyString()
        paymentContext.paymentAmount = Int((CheckoutCart.shared.total * 100).rounded(digits: 2)) // This is in cents, i.e. $50 USD
        navigationController?.isHiddenHairline = true
    }

    func adaptivePresentationStyle(for _: UIPresentationController, traitCollection _: UITraitCollection) -> UIModalPresentationStyle
    {
        return .none
    }

    @objc private func handleApplePayButtonTapped() {
        CheckoutCart.shared.calculateCost()

        let merchantIdentifier = "merchant.com.theQuickCompany.QuickBev"
        let paymentRequest = StripeAPI.paymentRequest(withMerchantIdentifier: merchantIdentifier, country: "US", currency: "USD")

        // Configure the line items on the payment request
        paymentRequest.paymentSummaryItems = [
            // The final line should represent your company;
            // it'll be prepended with the word "Pay" (i.e. "Pay iHats, Inc $50")
            PKPaymentSummaryItem(label: CheckoutCart.shared.quickPassBusiness!.name, amount: NSDecimalNumber(value: CheckoutCart.shared.calculateQuickPassTotal().rounded(digits: 2))),
        ]
        // Initialize an STPApplePayContext instance
        if let applePayContext = STPApplePayContext(paymentRequest: paymentRequest, delegate: self) {
            // Present Apple Pay payment sheet
            applePayContext.presentApplePay()
        } else {
            // There is a problem with your Apple Pay configuration
            print("error with apple pay configuration")
        }
    }

    private func conditionallyDisplayPaymentButtons() {
        // Only offer Apple Pay if the customer can pay with it
        let safeArea = view.safeAreaLayoutGuide
        if !StripeAPI.deviceSupportsApplePay() {
            view.addSubview(addPaymentMethodButton)
            NSLayoutConstraint.activate([
                addPaymentMethodButton.widthAnchor.constraint(equalTo: addPaymentMethodButton.heightAnchor, multiplier: 373 / 60),
                addPaymentMethodButton.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
                addPaymentMethodButton.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -10.0)),
                addPaymentMethodButton.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: getHeightRatio(verticalConstant: -20.0)),
            ])
        } else {
            let bottomButtonStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical]))
            view.addSubview(bottomButtonStackView)

            bottomButtonStackView.addArrangedSubview(applePayButton)
            bottomButtonStackView.addArrangedSubview(addPaymentMethodButton)
            bottomButtonStackView.distribution = .equalSpacing
            bottomButtonStackView.spacing = getHeightRatio(verticalConstant: 10.0)
            NSLayoutConstraint.activate([
                addPaymentMethodButton.widthAnchor.constraint(equalTo: addPaymentMethodButton.heightAnchor, multiplier: 373 / 60),
                applePayButton.widthAnchor.constraint(equalTo: applePayButton.heightAnchor, multiplier: 373 / 60),
                bottomButtonStackView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: getHeightRatio(verticalConstant: -20.0)),
                bottomButtonStackView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
                bottomButtonStackView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -10.0)),
                bottomButtonStackView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            ])
        }
    }

    @objc private func choosePaymentButtonTapped(_: RoundButton) {
        if paymentContext.selectedPaymentOption != nil {
            newActivityIndicator.startAnimating()
            paymentContext.requestPayment()
        } else {
            paymentContext.presentPaymentOptionsViewController()
        }
    }

    @objc private func changeDefaultPaymentMethodButtonClicked(_: RoundButton) {
        paymentContext.presentPaymentOptionsViewController()
    }
}

extension ReviewQuickPassOrderViewController: STPPaymentContextDelegate {
    func paymentContext(_: STPPaymentContext, didFailToLoadWithError error: Error) {
        print("error loading payment context \(error)")
    }

    func paymentContextDidChange(_: STPPaymentContext) {
        if paymentContext.selectedPaymentOption != nil {
            defaultPaymentMethodView.isHidden = false
            cardCompanyName.text = paymentContext.selectedPaymentOption?.label
            cardLogoImageView.image = paymentContext.selectedPaymentOption!.templateImage
            addPaymentMethodButton.refreshTitle(newTitle: "Pay with card")
        } else {
            defaultPaymentMethodView.isHidden = true
        }
    }

    func paymentContext(_ paymentContext: STPPaymentContext, didCreatePaymentResult paymentResult: STPPaymentResult, completion: @escaping STPPaymentStatusBlock) {
        QuickPassData.shared.cardInformation = cardCompanyName.text!
        CoreDataManager.sharedManager.saveContext()
        StripeAPIClient.shared.createPaymentIntent(order: nil, quickPassData: QuickPassData.shared) { result in
            switch result {
            case let .success(clientSecret):
                // Assemble the PaymentIntent parameters
                let paymentIntentParams = STPPaymentIntentParams(clientSecret: clientSecret)
                paymentIntentParams.paymentMethodId = paymentResult.paymentMethod?.stripeId

                // Attach payment method to customer and charge for tips
                // Confirm the PaymentIntent
                STPPaymentHandler.shared().confirmPayment(paymentIntentParams, with: paymentContext) { status, _, error in
                    switch status {
                    case .succeeded:
                        // Your backend asynchronously fulfills the customer's order, e.g. via webhook

                        let request = try! APIRequest(method: .post, path: "/quick_pass/" + CheckoutCart.shared.sessionToken, body: QuickPassData.shared)
                        APIClient().perform(request) { result in
                            switch result {
                            case let .success(response):
                                if response.statusCode == 500 {
                                    completion(.error, nil)
                                } else {
                                    if response.statusCode == 200, let response = try? response.decode(to: [String: QuickPass].self), let _ = response.body["quick_pass_order"]
                                    {
                                        // need to save that isDummyPass = false before deleting checkout card quickpass
                                        CoreDataManager.sharedManager.saveContext()
                                        DispatchQueue.main.async {
                                            SceneDelegate.shared.rootViewController.homeViewController?.setConditionalUIProperties()
                                        }
                                        completion(.success, nil)
                                    }
                                }
                            case let .failure(error):
                                completion(.error, nil)
                                print("error", error)
                            }
                        }

                    case .failed:
                        completion(.error, error) // Report error
                    case .canceled:
                        completion(.userCancellation, nil) // Customer cancelled
                    @unknown default:
                        completion(.error, nil)
                    }
                }

            case let .failure(error):
                print("error in attaching payment method id \(error.localizedDescription)")
                completion(.error, error) // Report error
            }
        }
    }

    func paymentContext(_: STPPaymentContext, didFinishWith status: STPPaymentStatus, error: Error?) {
        let title: String
        newActivityIndicator.stopAnimating()
        switch status {
        case .error:
            title = "Error"
            print("error with order payment", error?.localizedDescription ?? "")
        case .success:
            title = "Success"
        case .userCancellation:
            return ()
        @unknown default:
            return ()
        }
        newActivityIndicator.stopAnimating()
        if title == "Error" {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(2500)) {
                self.presentNetworkError()
            }
        } else {
            dismiss(animated: true) {
                SceneDelegate.shared.rootViewController.checkForPresentedViewControllers { _ in
                    SceneDelegate.shared.rootViewController.homeViewController?.presentActiveQuickPassViewController()
                }
            }
        }
    }
}

extension ReviewQuickPassOrderViewController: STPApplePayContextDelegate {
    func applePayContext(_: STPApplePayContext, didCreatePaymentMethod _: STPPaymentMethod, paymentInformation _: PKPayment, completion: @escaping STPIntentClientSecretCompletionBlock) {
        QuickPassData.shared.cardInformation = "Apple Pay"
        CoreDataManager.sharedManager.saveContext()
        StripeAPIClient.shared.createPaymentIntent(order: nil, quickPassData: QuickPassData.shared) { result in
            switch result {
            case let .success(clientSecret):
                completion(clientSecret, nil)
            case let .failure(error):
                print("error in attaching payment method id \(error.localizedDescription)")
                completion(nil, error)
            }
        }
    }

    func applePayContext(_: STPApplePayContext, didCompleteWith status: STPPaymentStatus, error _: Error?) {
        var title = ""
        let group = DispatchGroup()
        group.enter()
        switch status {
        case .success:
            // Your backend asynchronously fulfills the customer's order, e.g. via webhook
            let request = try! APIRequest(method: .post, path: "/quick_pass/" + CheckoutCart.shared.sessionToken, body: QuickPassData.shared)
            APIClient().perform(request) { result in
                switch result {
                case let .success(response):
                    if response.statusCode == 500 {
                        title = "Error"
                        group.leave()
                    } else {
                        if response.statusCode == 200, let response = try? response.decode(to: [String: QuickPass].self), let _ = response.body["quick_pass_order"]
                        {
                            title = "Success"
                            CoreDataManager.sharedManager.saveContext()
                            DispatchQueue.main.async {
                                SceneDelegate.shared.rootViewController.homeViewController?.setConditionalUIProperties()
                            }
                        }
                        group.leave()
                    }
                case let .failure(error):
                    print("error", error)
                    group.leave()
                }
            }

        case .error:
            // Payment failed, show the error
            group.leave()
            title = "Error"

        case .userCancellation:
            group.leave()
            // return so the alert controller is not presented
            return ()
        @unknown default:
            group.leave()
            title = "Error"
        }
        newActivityIndicator.stopAnimating()
        if title == "Error" {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(2500)) {
                self.presentNetworkError()
            }
        } else {
            dismiss(animated: true) {
                SceneDelegate.shared.rootViewController.checkForPresentedViewControllers { _ in
                    SceneDelegate.shared.rootViewController.homeViewController?.presentActiveQuickPassViewController()
                }
            }
        }
    }
}
