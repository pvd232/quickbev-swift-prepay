//
//  QuickPassContentView.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 7/30/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
class QuickPassContentView: UIView, UIContentView {
    var configuration: UIContentConfiguration {
        didSet {
            configure(configuration: configuration as! QuickPassContentViewConfiguration)
        }
    }

    private let barName = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.primaryTableCellContentViewFont)]))
    private let barAddress = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.secondaryTableCellContentViewFont)]))
    private let expirationTime = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.secondaryTableCellContentViewFont)]))
    private let quickPassStatus = RoundedImageView(theme: Theme.UIView(props: []))
    private let quickPassStatusLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.secondaryTableCellContentViewFont)]))
    private let quickPassIsActive: Bool
    init(configuration: QuickPassContentViewConfiguration) {
        self.configuration = configuration
        quickPassIsActive = configuration.quickPassIsActive
        super.init(frame: .zero)
        configure(configuration: configuration)
    }

    private func setupView() {
        addSubview(barName)
        addSubview(quickPassStatus)
        addSubview(barAddress)
        addSubview(expirationTime)

        quickPassStatus.addSubview(quickPassStatusLabel)
        backgroundColor = .white

        let safeArea = safeAreaLayoutGuide
        barAddress.textColor = .systemGray
        expirationTime.textColor = .systemGray

        if quickPassIsActive == true {
            quickPassStatus.contentMode = .left
            quickPassStatus.backgroundColor = UIColor.themeColor.mix(with: UIColor.white, amount: 0.78)
            quickPassStatus.tintColor = .purple
            quickPassStatus.refreshBorderColor(color: .clear)
            quickPassStatus.refreshCorners(value: 18.0)
            quickPassStatusLabel.textColor = UIColor.purple
            quickPassStatusLabel.text = "Active"
            NSLayoutConstraint.activate([
                quickPassStatusLabel.leadingAnchor.constraint(equalTo: quickPassStatus.leadingAnchor, constant: getWidthRatio(horizontalConstant: 60.0)),
            ])
        } else {
            quickPassStatusLabel.text = "Inactive"
            quickPassStatus.backgroundColor = UIColor.red.mix(with: UIColor.white, amount: 0.78)
            quickPassStatus.refreshBorderColor(color: .clear)
            quickPassStatus.refreshCorners(value: 18.0)
            quickPassStatusLabel.textColor = UIColor.red
            NSLayoutConstraint.activate([
                quickPassStatusLabel.centerXAnchor.constraint(equalTo: quickPassStatus.centerXAnchor),
            ])
        }

        NSLayoutConstraint.activate([
            barName.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            quickPassStatusLabel.centerYAnchor.constraint(equalTo: quickPassStatus.centerYAnchor),

            quickPassStatus.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: getHeightRatio(verticalConstant: 20.0)),
            quickPassStatus.leadingAnchor.constraint(equalTo: barName.leadingAnchor),
            quickPassStatus.bottomAnchor.constraint(equalTo: barName.topAnchor, constant: getHeightRatio(verticalConstant: -10.0)),
            quickPassStatus.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.0425),
            quickPassStatus.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.35),

            barAddress.topAnchor.constraint(equalTo: barName.bottomAnchor, constant: getHeightRatio(verticalConstant: 10.0)),
            barAddress.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),

            expirationTime.topAnchor.constraint(equalTo: barAddress.bottomAnchor, constant: getHeightRatio(verticalConstant: 10.0)),
            expirationTime.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
        ])
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configure(configuration: QuickPassContentViewConfiguration) {
        barName.text = configuration.businessName
        barAddress.text = configuration.businessAddress
        expirationTime.text = "Expires on " + configuration.expirationTime
        if quickPassIsActive == true {
            quickPassStatus.image = configuration.quickPassImage.imageWithInsets(insets: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))!.withRenderingMode(.alwaysTemplate)
        }
        setupView()
    }
}
