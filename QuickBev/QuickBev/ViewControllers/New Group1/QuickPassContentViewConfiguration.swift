//
//  QuickPassContentViewConfiguration.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 7/30/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit

struct QuickPassContentViewConfiguration: UIContentConfiguration {
    func updated(for _: UIConfigurationState) -> QuickPassContentViewConfiguration {
        return self
    }

    let quickPassImage: UIImage!
    let price: Double
    var dateTime: String!
    var expirationTime: String!
    let businessId: UUID
    let quickPassIsActive: Bool
    let businessName: String
    let businessAddress: String

    init(quickPassItem: QuickPassItem) {
        quickPassIsActive = quickPassItem.active
        price = quickPassItem.price
        let dateformatter = DateFormatter() // 2-2
        dateformatter.dateStyle = .medium
        dateformatter.timeStyle = .short // 2-3
        dateTime = dateformatter.string(from: quickPassItem.activationTime)
        expirationTime = dateformatter.string(from: quickPassItem.expirationTime)
        businessId = quickPassItem.businessId
        businessAddress = quickPassItem.businessAddress
        businessName = quickPassItem.businessName
        quickPassImage = quickPassItem.image
    }

    func makeContentView() -> UIView & UIContentView {
        return QuickPassContentView(configuration: self)
    }
}
