//
//  BusinessArrayViewController.swift
//  Businessz
//
//  Created by Peter Vail Driscoll II on 12/17/20.
//  Copyright © 2020 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
protocol BusinessPickerProtocol {
    func selectedBusinessHandler(_ selectedBusiness: Business)
}

class BusinessSearchViewController: UIViewController, UIGestureRecognizerDelegate {
    private let tableView = UITableView(theme: Theme.UIView(props: []))
    private var filteredBusinessItems = [BusinessItem]()
    private var businessItems = [BusinessItem]()
    private lazy var dataSource = makeDataSource()
    private var isPresenting: Bool = false
    @UsesAutoLayout var newActivityIndicator = UIActivityIndicatorView(style: .large)

    private let searchController = UISearchController(searchResultsController: nil)

    var businessPickerDelegate: NewBusinessPickedProtocol!
    var eventBusinessPickerDelegate: BusinessPickerProtocol!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(newActivityIndicator)

        navigationController?.configureNavigationBar(largeTitleColor: UIColor.white, backgroundColor: UIColor.white, tintColor: UIColor.white, preferredLargeTitle: false, titleTextAttributes: nil)
        let searchField: UITextField? = searchController.searchBar.value(forKey: "searchField") as? UITextField
        searchField?.backgroundColor = UIColor.white
        navigationController?.view.layoutIfNeeded()
        navigationController?.view.setNeedsLayout()

        // 1
        searchController.searchResultsUpdater = self
        // 2
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        // 3
        searchController.searchBar.placeholder = "Search our partner venues"

        // 4
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        // 5
        view.addSubview(tableView)

        newActivityIndicator.backgroundColor = UIColor(white: 0, alpha: 0.4)
        newActivityIndicator.frame = view.bounds
        setupView()
        setNavbar()
    }

    private func update(with newBusinessItems: [BusinessItem], animate _: Bool = true) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, BusinessItem>()
        snapshot.appendSections([.main])
        snapshot.appendItems(newBusinessItems, toSection: .main)
        dataSource.apply(snapshot, animatingDifferences: false)
    }

    private func makeDataSource() -> UITableViewDiffableDataSource<Section, BusinessItem> {
        return UITableViewDiffableDataSource(
            tableView: tableView,
            cellProvider: { tableView, indexPath, _ in
                let cell = tableView.dequeueReusableCell(
                    withIdentifier: "cell",
                    for: indexPath
                )
                cell.contentConfiguration = nil
                let businessItem: BusinessItem = {
                    if self.isFiltering {
                        return self.filteredBusinessItems[indexPath.row]
                    } else {
                        return self.businessItems[indexPath.row]
                    }

                }()
                cell.backgroundColor = .clear
                let contentConfiguration = BusinessSearchContentViewConfiguration(businessItem: businessItem)
                cell.contentConfiguration = contentConfiguration

                return cell
            }
        )
    }

    private func setupView() {
        tableView.delegate = self
        tableView.dataSource = dataSource
        tableView.backgroundColor = UIColor.white

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.separatorColor = .clear
        var newBusinessItems = [BusinessItem]()
        var openBusinessItems = [BusinessItem]()
        var closedBusinessItems = [BusinessItem]()

        for business in CheckoutCart.shared.activeBusinessArray {
            let newBusinessItem = BusinessItem(business: business)
            if business.isOpen == true {
                openBusinessItems.append(newBusinessItem)
            } else {
                closedBusinessItems.append(newBusinessItem)
            }
        }
        openBusinessItems.sort {
            $0.distance < $1.distance
        }
        for item in openBusinessItems {
            newBusinessItems.append(item)
        }
        for item in closedBusinessItems {
            newBusinessItems.append(item)
        }
        // populate the business items list and update the datasource to match
        businessItems = newBusinessItems
        update(with: newBusinessItems)

        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            tableView.heightAnchor.constraint(equalTo: tableView.superview!.heightAnchor),
            tableView.widthAnchor.constraint(equalTo: safeArea.widthAnchor),
            newActivityIndicator.topAnchor.constraint(equalTo: safeArea.topAnchor),
            newActivityIndicator.bottomAnchor.constraint(equalTo: tableView.bottomAnchor),
            newActivityIndicator.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            newActivityIndicator.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
        ])
        view.bringSubviewToFront(newActivityIndicator)
    }

    @objc private func launchBottomSheetViewController(selectedBusiness: Business) {
        let popoverContent = BottomSheetViewController(business: selectedBusiness)
        popoverContent.buttonPressedDelegate = self
        popoverContent.modalPresentationStyle = .custom
        popoverContent.transitioningDelegate = self
        isPresenting = true
        present(popoverContent, animated: true, completion: nil)
    }

    func filterContentForSearchText(_ searchText: String) {
        // update the value of the filtered business item list
        filteredBusinessItems = businessItems.filter { (businessItem: BusinessItem) -> Bool in
            for (letter1, letter2) in zip(searchText.lowercased(), businessItem.name.lowercased()) {
                if letter1 != letter2 {
                    return false
                }
            }
            return true
        }
        // update the datasource business item list so it is in sync with filtered business items
        update(with: filteredBusinessItems)
    }

    var isSearchBarEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }

    var isFiltering: Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!isSearchBarEmpty || searchBarScopeIsFiltering)
    }
}

extension BusinessSearchViewController: UITableViewDelegate {
    func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        return (tableView.frame.height / 4.25)
    }

    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        if isFiltering {
            return businessItems.count
        }
        return CheckoutCart.shared.activeBusinessArray.count
    }

    func numberOfSections(in _: UITableView) -> Int {
        return 1
    }

    func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedBusinessItem: BusinessItem = {
            if isFiltering {
                return filteredBusinessItems[indexPath.row]
            } else {
                return businessItems[indexPath.row]
            }

        }()
        if let selectedBusiness = CheckoutCart.shared.activeBusinessArray.first(where: {
            $0.id == selectedBusinessItem.businessId
        }) {
            if let bottomSheetVC = presentedViewController as? BottomSheetViewController {
                newActivityIndicator.stopAnimating()
                bottomSheetVC.businessDetails = selectedBusiness
                bottomSheetVC.setupView()
            } else {
                launchBottomSheetViewController(selectedBusiness: selectedBusiness)
            }
        }
    }
}

extension BusinessSearchViewController: ButtonPressedProtocol {
    func getQuickPassButtonPressed(businessId: UUID) {
        newActivityIndicator.startAnimating()
        updateQuickPass(businessId: businessId) {
            result in switch result {
            case true:
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                    self.newActivityIndicator.stopAnimating()
                    self.dismiss(animated: true) {
                        let popoverContent = QuickPassDetailViewController()
                        popoverContent.modalPresentationStyle = .popover
                        let popover = popoverContent.popoverPresentationController
                        popover!.sourceView = self.view

                        // the position of the popover where it's showed
                        popover!.sourceRect = self.view!.bounds

                        // the size you want to display
                        popoverContent.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
                        popover!.delegate = self
                        self.present(popoverContent, animated: true, completion: nil)
                    }
                }
            case false:
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                    self.newActivityIndicator.stopAnimating()
                    self.dismiss(animated: true) {
                        self.presentNetworkError()
                    }
                }
            }
        }
    }

    func checkInButtonPressed() {
        newActivityIndicator.startAnimating()

        businessPickerDelegate.businessPicked()

        let delay = 500 // miliseconds
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(delay)) {
            self.newActivityIndicator.stopAnimating()
            self.dismiss(animated: true) {
                self.dismiss(animated: true) {
                    SceneDelegate.shared.rootViewController.homeViewController?.launchMenuViewController()
                }
            }
        }
    }
}

extension BusinessSearchViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
}

extension BusinessSearchViewController: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting _: UIViewController?, source _: UIViewController) -> UIPresentationController? {
        return CustomPresentationController(presentedViewController: presented, presenting: presentingViewController)
    }
}

extension BusinessSearchViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyleForPresentationController(controller _: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.fullScreen
    }

    func presentationController(_ controller: UIPresentationController, viewControllerForAdaptivePresentationStyle _: UIModalPresentationStyle) -> UIViewController? {
        return UINavigationController(rootViewController: controller.presentedViewController)
    }
}
