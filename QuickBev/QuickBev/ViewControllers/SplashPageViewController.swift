//
//  SplashPageViewController.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 6/3/20.
//  Copyright © 2020 Peter Vail Driscoll II. All rights reserved.
//

import CoreLocation
import Stripe
import UIKit

class SplashPageViewController: UIViewController {
    private var splashView = UIView(theme: Theme.UIView(props: []))
    private let locationManager = CLLocationManager()

    init() {
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    private func configureLocationServices() {
        switch locationManager.authorizationStatus {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            return
        default:
            return
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        splashView = SplashView(frame: view.frame)
        splashView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(splashView)
        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([splashView.topAnchor.constraint(equalTo: safeArea.topAnchor),
                                     splashView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor),
                                     splashView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
                                     splashView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor)])
    }

    override func viewDidAppear(_: Bool) {
        SceneDelegate.shared.rootViewController.activityIndicator.stopAnimating()
        configureLocationServices()
    }
}
