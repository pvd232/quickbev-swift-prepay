//
//  CustomPresentationController.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 9/19/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit

class CustomPresentationController: UIPresentationController {
    override var frameOfPresentedViewInContainerView: CGRect {
        // no fucking idea how this works
        return CGRect(x: 0, y: -(UIViewController.screenSize.height * 0.75), width: UIViewController.screenSize.width, height: UIViewController.screenSize.height / 3.5)
    }

    override func presentationTransitionWillBegin() {
        // no fucking idea how this works
        containerView?.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height / 3.5).isActive = true
        containerView?.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width).isActive = true
        containerView?.frame.origin = CGPoint(x: 0.0, y: UIViewController.screenSize.height * 0.4)
        containerView?.bounds = frameOfPresentedViewInContainerView
    }
}
