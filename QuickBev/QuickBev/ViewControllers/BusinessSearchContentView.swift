//
//  BusinessSearchContentView.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 9/20/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
class BusinessSearchContentView: UIView, UIContentView {
    var configuration: UIContentConfiguration {
        didSet {
            configure(configuration: configuration as! BusinessSearchContentViewConfiguration)
        }
    }

    private let name = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont.primaryTableCellContentViewFont)]))
    private let distance = UILabel(theme: Theme.UILabel(props: [.font(UIFont.secondaryTableCellContentViewFont)]))
    private let address = UILabel(theme: Theme.UILabel(props: [.font(UIFont.secondaryTableCellContentViewFont)]))
    private let textStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 15)))]))
    private let labelsStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal]))
    private var isOpen: Bool!

    init(configuration: BusinessSearchContentViewConfiguration) {
        self.configuration = configuration
        super.init(frame: .zero)
        // ... configure the subviews ...
        configure(configuration: configuration)
    }

    private func setupView() {
        addSubview(labelsStackView)

        backgroundColor = .white
        distance.textColor = UIColor.systemGray
        address.textColor = .black
        if name.text!.count >= 20 {
            name.font = UIFont.secondaryTableCellContentViewFont
        } else {
            name.font = UIFont.primaryTableCellContentViewFont
        }

        textStackView.distribution = .fill
        textStackView.alignment = .fill
        textStackView.addArrangedSubview(name)
        textStackView.addArrangedSubview(address)
        textStackView.addArrangedSubview(distance)

        labelsStackView.distribution = .fill
        labelsStackView.alignment = .fill

        labelsStackView.addArrangedSubview(textStackView)

        let safeArea = safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            labelsStackView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 20.0)),
        ])
        let statusLabel = RoundButton(theme: Theme.RoundButton(props: [.text("Closed"), .titleLabelFont(UIFont.secondaryTableCellContentViewFont)]))
        addSubview(statusLabel)

        statusLabel.refreshBorderWidth(value: 0.0)
        statusLabel.refreshCorners(value: 18.0)
        NSLayoutConstraint.activate([
            statusLabel.widthAnchor.constraint(equalToConstant: getWidthRatio(horizontalConstant: 115)),
            statusLabel.heightAnchor.constraint(equalToConstant: getHeightRatio(verticalConstant: 30.0)),
            statusLabel.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            statusLabel.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: getHeightRatio(verticalConstant: 30.0)),
            labelsStackView.topAnchor.constraint(equalTo: statusLabel.bottomAnchor, constant: getHeightRatio(verticalConstant: 15.0)),
        ])

        if !isOpen {
            statusLabel.refreshColor(color: UIColor.red.mix(with: UIColor.white, amount: 0.78))
            statusLabel.setTitleColor(UIColor.red, for: .normal)
        } else {
            statusLabel.refreshTitle(newTitle: "Open")
            statusLabel.refreshColor(color: UIColor.systemGreen.mix(with: UIColor.white, amount: 0.3))
            statusLabel.setTitleColor(.black, for: .normal)
        }
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configure(configuration: BusinessSearchContentViewConfiguration) {
        name.text = configuration.name
        address.text = configuration.address
        if configuration.distancePrecise <= 1.0 {
            let distanceFeet = Int(5280 * configuration.distancePrecise)
            distance.text = distanceFeet.description + " feet away"
        } else {
            distance.text = configuration.distance + " miles away"
        }
        isOpen = configuration.isOpen
        setupView()
    }
}
