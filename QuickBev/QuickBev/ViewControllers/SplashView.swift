//
//  SignInOrSignUpView.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 2/3/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
class SplashView: UIView {
    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private lazy var navController: UINavigationController = {
        SceneDelegate.shared.rootViewController.current
    }()

    private var shouldSetupConstraints = true
    private let logoImage = UIImage(named: "charterRomanPurpleLogo-30")

    private let logoImageView = UIImageView(theme: Theme.UIView(props: []))
    private let centerLabel = UILabel(theme: Theme.UILabel(props: [.font(UIFont.largeThemeLabelFont), .text("Let's get started"), .textColor]))
    private let optionsStackView = UIStackView(theme: Theme.UIView(props: []))
    private let firstButton = RoundButton(theme: Theme.RoundButton(props: [.color, .text("Sign in"), .titleLabelFont(UIFont.themeButtonFont)]))
    private let secondButton = RoundButton(theme: Theme.RoundButton(props: [.color, .text("Sign up"), .titleLabelFont(UIFont.themeButtonFont)]))
    private let orLabel = UILabel(theme: Theme.UILabel(props: [.textColor]))

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(logoImageView)
        addSubview(centerLabel)
        addSubview(optionsStackView)

        logoImageView.image = logoImage

        optionsStackView.axis = .vertical
        optionsStackView.spacing = CGFloat(getHeightRatio(verticalConstant: 14.0))

        orLabel.text = "OR"
        orLabel.font = UIFont.secondaryTableCellContentViewFont
        orLabel.textAlignment = .center

        centerLabel.textAlignment = .center
        centerLabel.textColor = UIColor.black

        optionsStackView.addArrangedSubview(firstButton)
        optionsStackView.addArrangedSubview(orLabel)
        optionsStackView.addArrangedSubview(secondButton)

        firstButton.madeStandard()
        secondButton.madeStandard()

        let safeArea = safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            logoImageView.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: CGFloat(Double.logoSizeMultiplier)),
            logoImageView.heightAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: CGFloat(Double.logoSizeMultiplier)),
            logoImageView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 0.02 * UIViewController.screenSize.height),
            logoImageView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),

            firstButton.widthAnchor.constraint(equalTo: firstButton.heightAnchor, multiplier: 197 / 25),
            secondButton.widthAnchor.constraint(equalTo: secondButton.heightAnchor, multiplier: 197 / 25),

            optionsStackView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -10.0)),
            optionsStackView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: getHeightRatio(verticalConstant: -10.0)),
            optionsStackView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),

            centerLabel.centerYAnchor.constraint(equalTo: safeArea.centerYAnchor),
            centerLabel.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            centerLabel.heightAnchor.constraint(equalTo: safeArea.heightAnchor, multiplier: 0.07),
        ])
        firstButton.addTarget(self, action: #selector(firstButtonTouchUp), for: .touchUpInside)
        secondButton.addTarget(self, action: #selector(secondButtonTouchUp), for: .touchUpInside)
    }

    override func updateConstraints() {
        // only want to update the contraints once
        if shouldSetupConstraints {
            // AutoLayout constraints
            shouldSetupConstraints = false
        }
        super.updateConstraints()
    }

    @objc private func firstButtonTouchUp() {
        navController.pushViewController(LoginViewController(), animated: true)
    }

    @objc private func secondButtonTouchUp() {
        navController.pushViewController(RegistrationViewController(), animated: true)
    }
}
