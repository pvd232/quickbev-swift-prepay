//
//  BusinessMapViewCotroller.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 1/29/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import CoreLocation
import MapKit
import UIKit

protocol NewBusinessPickedProtocol {
    func businessPicked()
}

class BusinessMapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate, UIGestureRecognizerDelegate {
    private let mapView = MKMapView(theme: Theme.UIView(props: []))
    @UsesAutoLayout private var newActivityIndicator = UIActivityIndicatorView(style: .large)

    private let locationManager = CLLocationManager()
    private var customAnnotations = [CustomAnnotation]()
    private var closeCustomAnnotations = [CustomAnnotation]()
    var businessPickerDelegate: NewBusinessPickedProtocol?
    private let regionRadius: Double = 5000
    private var isPresenting = false

    init() {
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(mapView)
        view.addSubview(newActivityIndicator)
        view.bringSubviewToFront(newActivityIndicator)
        mapView.delegate = self
        mapView.showsUserLocation = true
        locationManager.delegate = self

        newActivityIndicator.backgroundColor = UIColor(white: 0, alpha: 0.4)

        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            mapView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            mapView.heightAnchor.constraint(equalTo: mapView.superview!.heightAnchor),
            mapView.widthAnchor.constraint(equalTo: safeArea.widthAnchor),
            newActivityIndicator.topAnchor.constraint(equalTo: safeArea.topAnchor),
            newActivityIndicator.bottomAnchor.constraint(equalTo: mapView.bottomAnchor),
            newActivityIndicator.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            newActivityIndicator.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
        ])
        view.bringSubviewToFront(newActivityIndicator)

        navigationController?.interactivePopGestureRecognizer?.delegate = self
        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            locationManager.allowsBackgroundLocationUpdates = true
            locationManager.requestLocation()
            locationManager.startUpdatingLocation()
            makeBusinessServiceCall()
        }
    }

    private func makeBusinessServiceCall() {
        newActivityIndicator.startAnimating()
        for business in CheckoutCart.shared.activeBusinessArray {
            let customCoordinate = business.coordinate
            let customAnnotation = CustomAnnotation(coordinate: customCoordinate, businessName: business.formattedName, businessId: business.id, radius: regionRadius)
            if customAnnotation.isClose(from: locationManager.location ?? CheckoutCart.shared.laCavernaLocation) {
                closeCustomAnnotations.append(customAnnotation)
            } else {
                customAnnotations.append(customAnnotation)
            }
            mapView.addAnnotation(customAnnotation)
        }
        newActivityIndicator.stopAnimating()
        setNavbar()
    }

    @objc private func launchBottomSheetViewController(selectedBusiness: Business) {
        let popoverContent = BottomSheetViewController(business: selectedBusiness)
        popoverContent.buttonPressedDelegate = self
        popoverContent.modalPresentationStyle = .custom
        popoverContent.transitioningDelegate = self
        isPresenting = true
        present(popoverContent, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        // Dispose of any resources that can be recreated.
        super.didReceiveMemoryWarning()
        mapView.removeAnnotations(mapView.annotations)
        mapView.delegate = nil
        mapView.removeFromSuperview()
    }

    // MARK: CLLocationManagerDelegate

    func locationManager(_: CLLocationManager, didUpdateLocations _: [CLLocation]) {}

    func locationManager(_: CLLocationManager, didChangeAuthorization _: CLAuthorizationStatus) {
        mapView.showAnnotations(closeCustomAnnotations, animated: false)
    }

    func locationManager(_: CLLocationManager, didFailWithError error: Error) {
        print("location manager error", error)
    }

    func mapView(_: MKMapView, didSelect view: MKAnnotationView) {
        if let annotation = view.annotation as? CustomAnnotation {
            if let selectedBusiness = CheckoutCart.shared.activeBusinessArray.first(where: {
                $0.id == annotation.businessId
            }) {
                if let bottomSheetVC = presentedViewController as? BottomSheetViewController {
                    bottomSheetVC.businessDetails = selectedBusiness
                    bottomSheetVC.setupView()
                } else {
                    launchBottomSheetViewController(selectedBusiness: selectedBusiness)
                }
            }
        }
    }

    private func setButtonContraints(button: RoundButton, detailView: UIView, isOpen: Bool, theBusiness: Business) {
        detailView.addSubview(button)
        let buttonWidth: NSLayoutConstraint = {
            if isOpen == true {
                return NSLayoutConstraint(item: button, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: UIViewController.screenSize.width * 0.2)
            } else {
                return NSLayoutConstraint(item: button, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: UIViewController.screenSize.width * 0.6)
            }
        }()

        let buttonHeight = NSLayoutConstraint(item: button, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: UIViewController.screenSize.width * 0.085)
        button.addConstraint(buttonWidth)
        button.addConstraint(buttonHeight)
        button.centerXAnchor.constraint(equalTo: detailView.centerXAnchor).isActive = true
        button.bottomAnchor.constraint(equalTo: detailView.bottomAnchor, constant: getHeightRatio(verticalConstant: 5.0)).isActive = true

        if isOpen == false {
            button.refreshColor(color: UIColor.white)
            button.refreshBorderColor(color: UIColor.white)
            button.setTitleColor(UIColor.black, for: .normal)
            button.refreshTitle(newTitle: "\(theBusiness.name.capitalized) is closed")
            button.isUserInteractionEnabled = false
            button.titleLabel?.font = UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 15.0))
        }
    }
}

extension BusinessMapViewController: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting _: UIViewController?, source _: UIViewController) -> UIPresentationController? {
        return CustomPresentationController(presentedViewController: presented, presenting: presentingViewController)
    }
}

extension BusinessMapViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyleForPresentationController(controller _: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.fullScreen
    }

    func presentationController(_ controller: UIPresentationController, viewControllerForAdaptivePresentationStyle _: UIModalPresentationStyle) -> UIViewController? {
        return UINavigationController(rootViewController: controller.presentedViewController)
    }
}

extension BusinessMapViewController: ButtonPressedProtocol {
    func getQuickPassButtonPressed(businessId: UUID) {
        newActivityIndicator.startAnimating()
        updateQuickPass(businessId: businessId) {
            result in switch result {
            case true:
                DispatchQueue.main.async {
                    self.dismiss(animated: true) {
                        let popoverContent = QuickPassDetailViewController()
                        popoverContent.modalPresentationStyle = .popover
                        let popover = popoverContent.popoverPresentationController
                        popover!.sourceView = self.mapView

                        // the position of the popover where it's showed
                        popover!.sourceRect = self.view!.bounds

                        // the size you want to display
                        popoverContent.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
                        popover!.delegate = self
                        self.present(popoverContent, animated: true) {
                            self.newActivityIndicator.stopAnimating()
                        }
                    }
                }
            case false:
                DispatchQueue.main.async {
                    self.dismiss(animated: true) {
                        self.newActivityIndicator.stopAnimating()
                        self.presentNetworkError()
                    }
                }
            }
        }
    }

    func checkInButtonPressed() {
        newActivityIndicator.startAnimating()
        businessPickerDelegate?.businessPicked()
        let delay = 500 // miliseconds
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(delay)) {
            self.newActivityIndicator.stopAnimating()
            self.dismiss(animated: true) {
                self.dismiss(animated: true) {
                    SceneDelegate.shared.rootViewController.homeViewController?.launchMenuViewController()
                }
            }
        }
    }
}
