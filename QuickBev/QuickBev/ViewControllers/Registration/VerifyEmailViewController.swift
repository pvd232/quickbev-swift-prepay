//
//  VerifyEmailViewController.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 2/13/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit

class VerifyEmailViewController: UIViewController {
    private let verifyEmailStackView = UIStackView(theme: Theme.UIView(props: []))
    private let confirmationEmailLabel = UILabel(theme: Theme.UILabel(props: [.font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 19.458))), .textColor]))
    private let verifyEmailLabel = UILabel(theme: Theme.UILabel(props: [.textColor]))
    private let resendConfirmationLabel = UILabel(theme: Theme.UILabel(props: [.font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 19.458))), .textColor]))
    private let resendButton = RoundButton(theme: Theme.RoundButton(props: [.color, .text("Resend email"), .titleLabelFont(nil)]))
    @UsesAutoLayout private var newActivityIndicator = UIActivityIndicatorView(style: .large)

    init() {
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewWillDisappear(_: Bool) {
        newActivityIndicator.stopAnimating()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(newActivityIndicator)
        view.addSubview(verifyEmailStackView)
        view.addSubview(resendButton)

        verifyEmailStackView.addArrangedSubview(verifyEmailLabel)
        verifyEmailStackView.addArrangedSubview(confirmationEmailLabel)
        verifyEmailStackView.addArrangedSubview(resendConfirmationLabel)
        verifyEmailStackView.axis = .vertical
        verifyEmailStackView.spacing = CGFloat(getHeightRatio(verticalConstant: 80.0))

        verifyEmailLabel.text = "Verify your email"
        verifyEmailLabel.font = UIFont(name: "GillSans", size: getFontRatio(fontSize: 40.0))

        confirmationEmailLabel.text = "Check your email for the confirmation we sent you. Click the verify button and you'll be good to go!"
        confirmationEmailLabel.numberOfLines = 0
        confirmationEmailLabel.lineBreakMode = .byWordWrapping

        resendConfirmationLabel.text = "Didn't receive an email? Check your spam folder! Otherwise, click the button below to send another one"
        resendConfirmationLabel.numberOfLines = 0
        resendConfirmationLabel.lineBreakMode = .byWordWrapping

        newActivityIndicator.backgroundColor = UIColor(white: 0, alpha: 0.4)
        newActivityIndicator.frame = view.bounds

        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            verifyEmailStackView.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.9),
            verifyEmailStackView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: getHeightRatio(verticalConstant: 20.0)),
            verifyEmailStackView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            resendButton.topAnchor.constraint(equalTo: verifyEmailStackView.bottomAnchor, constant: UIViewController.screenSize.height * 0.05),
            resendButton.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            resendButton.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.43),
            resendButton.heightAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.1),
        ])
        resendButton.addTarget(self, action: #selector(firstButtonTouchUp), for: .touchUpInside)
    }

    @objc func firstButtonTouchUp() {
        resendButton.setTitle("", for: .normal)
        newActivityIndicator.startAnimating()

        let request = try! APIRequest(method: .put, path: "/customer/\(CheckoutCart.shared.sessionToken)", body: CheckoutCart.shared.user)
        APIClient().perform(request) { result in
            DispatchQueue.main.async {
                self.resendButton.setTitle("Resend", for: .normal)
                SceneDelegate.shared.rootViewController.activityIndicator.stopAnimating()
            }
            switch result {
            case .success:
                ()
            case let .failure(error):
                print("error \(error)")
            }
        }
    }
}
