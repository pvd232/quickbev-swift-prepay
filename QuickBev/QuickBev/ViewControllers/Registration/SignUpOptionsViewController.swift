//
//  SignUpOptionsViewController.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 11/10/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import AuthenticationServices
import CoreLocation
import FacebookLogin
import Stripe
import UIKit

class SignUpOptionsViewController: UIViewController {
    private let logoImage = UIImage(named: "charterRomanPurpleLogo-30")
    private let logoImageView = UIImageView(theme: Theme.UIView(props: []))
    private let centerLabel = UILabel(theme: Theme.UILabel(props: [.font(UIFont.largeThemeLabelFont), .text("Let's get started"), .textColor]))
    private let optionsStackView = UIStackView(theme: Theme.UIView(props: []))
    private let signUpButton = RoundButton(theme: Theme.RoundButton(props: [.color, .text("Continue with email"), .titleLabelFont(UIFont.themeButtonFont)]))
    private let appleButton = RoundButton(theme: Theme.RoundButton(props: [.color, .text("Continue with Apple"), .titleLabelFont(UIFont.themeButtonFont)]))
    private let appleLogo = UIImage(systemName: "applelogo")
    private let facebookButton = RoundButton(theme: Theme.RoundButton(props: [.color, .text("Continue with Facebook"), .titleLabelFont(UIFont.themeButtonFont)]))
    private let orLabel = UILabel(theme: Theme.UILabel(props: [.textColor]))
    private let locationManager = CLLocationManager()

    init() {
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white
        view.addSubview(logoImageView)
        view.addSubview(centerLabel)
        view.addSubview(optionsStackView)
        setupView()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewDidAppear(_: Bool) {
        SceneDelegate.shared.rootViewController.activityIndicator.stopAnimating()
        configureLocationServices()
    }

    func setupView() {
        logoImageView.image = logoImage
        optionsStackView.axis = .vertical
        optionsStackView.spacing = CGFloat(getHeightRatio(verticalConstant: 14.0))

        orLabel.text = "OR"
        orLabel.font = UIFont.secondaryTableCellContentViewFont
        orLabel.textAlignment = .center

        centerLabel.textAlignment = .center
        centerLabel.textColor = UIColor.black

        optionsStackView.addArrangedSubview(facebookButton)
        optionsStackView.addArrangedSubview(appleButton)
        optionsStackView.addArrangedSubview(orLabel)
        optionsStackView.addArrangedSubview(signUpButton)

        signUpButton.madeStandard()
        appleButton.madeStandard()
        facebookButton.madeStandard()

        facebookButton.refreshColor(color: UIColor(red: 66 / 255, green: 103 / 255, blue: 178 / 255, alpha: 1.0))
        facebookButton.refreshBorderColor(color: .white)
        facebookButton.setTitleColor(.white, for: .normal)

        appleButton.refreshColor(color: .black)
        appleButton.refreshBorderColor(color: .white)
        appleButton.setTitleColor(.white, for: .normal)
        appleButton.setImage(appleLogo, for: .normal)
        appleButton.tintColor = .white
        appleButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 12)

        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            logoImageView.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: CGFloat(Double.logoSizeMultiplier)),
            logoImageView.heightAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: CGFloat(Double.logoSizeMultiplier)),
            logoImageView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 0.02 * UIViewController.screenSize.height),
            logoImageView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),

            signUpButton.widthAnchor.constraint(equalTo: signUpButton.heightAnchor, multiplier: 197 / 25),
            appleButton.widthAnchor.constraint(equalTo: appleButton.heightAnchor, multiplier: 197 / 25),
            facebookButton.widthAnchor.constraint(equalTo: facebookButton.heightAnchor, multiplier: 197 / 25),

            optionsStackView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -10.0)),
            optionsStackView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: getHeightRatio(verticalConstant: -10.0)),
            optionsStackView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),

            centerLabel.centerYAnchor.constraint(equalTo: safeArea.centerYAnchor),
            centerLabel.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            centerLabel.heightAnchor.constraint(equalTo: safeArea.heightAnchor, multiplier: 0.07),
        ])

        signUpButton.addTarget(self, action: #selector(signUpButtonTouchUp), for: .touchUpInside)
        facebookButton.addTarget(self, action: #selector(facebookButtonTouchUp), for: .touchUpInside)
        appleButton.addTarget(self, action: #selector(handleAuthorizationAppleIDButtonPress), for: .touchUpInside)
    }

    func configureLocationServices() {
        switch locationManager.authorizationStatus {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            return
        default:
            return
        }
    }

    @objc func signUpButtonTouchUp() {
        SceneDelegate.shared.rootViewController.current.pushViewController(SplashPageViewController(), animated: true)
    }

    @objc func facebookButtonTouchUp() {
        LoginManager().logIn(permissions: [Permission.email, Permission.publicProfile], viewController: self) { loginResult in
            switch loginResult {
            case .success:
                let requestedFields = "email, first_name, last_name"
                GraphRequest(graphPath: "me", parameters: ["fields": requestedFields]).start { _, result, _ -> Void in
                    if let resultDictionary = result as? NSDictionary {
                        if let email = resultDictionary["email"] as? String, let firstName = resultDictionary["first_name"] as? String, let lastName = resultDictionary["last_name"] as? String {
                            let newUserData = UserData(FirstName: firstName, LastName: lastName, Email: email)
                            let userCredentials: [HTTPHeader] = [HTTPHeader(field: "user-id", value: email)]
                            let validateRequest = try! APIRequest(method: .get, path: "/customer/validate", headers: userCredentials)

                            SceneDelegate.shared.rootViewController.animateActivityIndicator()

                            self.validateUser(request: validateRequest, requestedNewUser: newUserData) {
                                result in
                                switch result {
                                case true:
                                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                                        SceneDelegate.shared.rootViewController.activityIndicator.stopAnimating()
                                        SceneDelegate.shared.rootViewController.switchToHomePageViewController()
                                    }
                                case false:
                                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                                        SceneDelegate.shared.rootViewController.stopAnimatingActivityIndicator()
                                        let newUser = User(userData: newUserData)
                                        CheckoutCart.shared.user = newUser
                                        newUser.userToCheckoutCart = CheckoutCart.shared
                                        CoreDataManager.sharedManager.saveContext()
                                        SceneDelegate.shared.rootViewController.current.pushViewController(PasswordRegistrationViewController(), animated: true)
                                    }
                                case nil:
                                    DispatchQueue.main.async {
                                        SceneDelegate.shared.rootViewController.activityIndicator.stopAnimating()
                                        self.presentServerError()
                                    }
                                default:
                                    ()
                                }
                            }
                        }
                    }
                }
            case .cancelled:
                print("Login: cancelled.")
            case let .failed(error):
                print("Login with error: \(error.localizedDescription)")
            }
        }
    }

    /// - Tag: perform_appleid_request
    @objc func handleAuthorizationAppleIDButtonPress() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]

        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }

    private func validateUser(request: APIRequest, requestedNewUser _: UserData, completion: @escaping (Bool?) -> Void) {
        APIClient().perform(request) { result in
            switch result {
            case let .success(response):
                let sessionToken = response.headers["jwt-token"] as! String
                // set the token in keychain and use it whenever calling the backend
                try! SecureStore(secureStoreQueryable: GenericPasswordQueryable()).setValue(sessionToken, for: "sessionToken")
                if response.statusCode == 201, let response = try? response.decode(to: [String: UserData].self) {
                    if let userData = response.body["customer"] {
                        if let users = CoreDataManager.sharedManager.fetchEntities(entityName: "User") as? [User], users.count >= 1 {
                            // there will always be a matching user here unless the user deleted the app and redownloaded it
                            if let matchingUser = users.first(where: {
                                $0.email == userData.email
                            }) {
                                matchingUser.update(userData: userData)
                                CheckoutCart.shared.user = matchingUser
                                matchingUser.userToCheckoutCart = CheckoutCart.shared
                                CoreDataManager.sharedManager.saveContext()
                            } else {
                                let localCopyOfExistingUser = User(userData: userData)
                                CheckoutCart.shared.user = localCopyOfExistingUser
                                localCopyOfExistingUser.userToCheckoutCart = CheckoutCart.shared
                                CoreDataManager.sharedManager.saveContext()
                            }
                            completion(true)
                            return
                        } else {
                            let localCopyOfExistingUser = User(userData: userData)
                            CheckoutCart.shared.user = localCopyOfExistingUser
                            localCopyOfExistingUser.userToCheckoutCart = CheckoutCart.shared
                            CoreDataManager.sharedManager.saveContext()
                            completion(true)
                            return
                        }
                    }
                }

                // user doesnt exist
                else if response.statusCode == 200 {
                    completion(false)
                    return
                }
                // server error
                else {
                    completion(nil)
                    return
                }
            // this will never get called if FB or Apple login is being used because they will throw a network error first
            case let .failure(error):
                print("error case .failure", error)
                completion(nil)
            }
        }
    }
}

extension SignUpOptionsViewController: ASAuthorizationControllerDelegate {
    /// - Tag: did_complete_authorization
    func authorizationController(controller _: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        SceneDelegate.shared.rootViewController.animateActivityIndicator()
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            // Create an account in your system.
            let userIdentifier = appleIDCredential.user

            // the user is signing up
            if let fullName = appleIDCredential.fullName, let firstName = fullName.givenName, let lastName = fullName.familyName, let email = appleIDCredential.email
            {
                let newUserData = UserData(FirstName: firstName, LastName: lastName, Email: email, AppleId: userIdentifier)
                let userCredentials: [HTTPHeader] = [HTTPHeader(field: "user-id", value: email)]
                let validateRequest = try! APIRequest(method: .get, path: "/customer/validate", headers: userCredentials)
                validateUser(request: validateRequest, requestedNewUser: newUserData) {
                    result in
                    switch result {
                    case true:
                        APIClient().setAppleId(userId: email, appleId: userIdentifier) { result in
                            switch result {
                            case true:
                                // if the username already exists then the user already has an account, but their apple id is not linked to their account
                                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                                    self.presentAlertController(title: "Username exists", message: "Your apple id is already associated with an account. We have updated your account accordingly.") {
                                        SceneDelegate.shared.rootViewController.switchToHomePageViewController()
                                    }
                                }

                            case false:
                                self.presentNetworkError()
                            }
                        }
                    case false:
                        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                            let newUser = User(userData: newUserData)
                            CheckoutCart.shared.user = newUser
                            newUser.userToCheckoutCart = CheckoutCart.shared
                            CoreDataManager.sharedManager.saveContext()
                            SceneDelegate.shared.rootViewController.stopAnimatingActivityIndicator()
                            SceneDelegate.shared.rootViewController.current.pushViewController(PasswordRegistrationViewController(), animated: true)
                        }
                    case nil:
                        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                            self.presentServerError()
                        }
                    default:
                        ()
                    }
                }
            }
            // the user is signing in
            else {
                // query the user in the backend using their unique apple id
                APIClient().checkUserAppleId(appleId: userIdentifier) { result in
                    switch result {
                    case true:
                        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                            SceneDelegate.shared.rootViewController.activityIndicator.stopAnimating()
                            SceneDelegate.shared.rootViewController.switchToHomePageViewController()
                        }
                    case false:
                        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                            self.presentAlertController(title: "Previous Registration", message: "Whoops, you registered your apple id with an alpha version of the app. Please go to your system preferences, select your apple id, then select password & security settings, then select \"apps using apple id\" and unlink the QuickBev app before registering with your apple id!")
                        }
                    case nil:
                        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                            self.presentNetworkError()
                        }
                    default:
                        ()
                    }
                }
            }
        default:
            break
        }
    }

    func authorizationController(controller _: ASAuthorizationController, didCompleteWithError _: Error) {
        print("error")
        // Handle error.
    }
}

extension SignUpOptionsViewController: ASAuthorizationControllerPresentationContextProviding {
    /// - Tag: provide_presentation_anchor
    func presentationAnchor(for _: ASAuthorizationController) -> ASPresentationAnchor {
        return view.window!
    }
}
