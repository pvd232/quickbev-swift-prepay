//
//  PasswordRegistrationViewController.swift
//  QuickBev
//
//  Created by Peter Vail Driscoll II on 11/10/21.
//  Copyright © 2021 Peter Vail Driscoll II. All rights reserved.
//

import UIKit
class PasswordRegistrationViewController: UIViewController, UITextFieldDelegate {
    private let passwordTextField = RoundedUITextField(theme: Theme.UITextField(props: [.font(nil), .placeHolderText("Your password"), .autocapitalizationType(autocapitalizationType: .none), .borderStyle(borderStyle: .roundedRect), .backgroundColor(UIColor.clear)]))
    private let confirmPasswordTextField = RoundedUITextField(theme: Theme.UITextField(props: [.font(nil), .placeHolderText("Confirm your password"), .autocapitalizationType(autocapitalizationType: .none), .borderStyle(borderStyle: .roundedRect), .backgroundColor(UIColor.clear)]))
    private let passwordLabel = UILabel(theme: Theme.UILabel(props: [.text("Password"), .font(nil), .textColor]))
    private let confirmPasswordLabel = UILabel(theme: Theme.UILabel(props: [.text("Confirm Password"), .font(nil), .textColor]))
    private let registrationStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 10.0)))]))
    private let submitButton = RoundButton(theme: Theme.RoundButton(props: [.color, .text("Submit"), .titleLabelFont(nil)]))
    private let logoImageView = UIImageView(theme: Theme.UIView(props: []))
    private let logoImage = UIImage(named: "charterRomanPurpleLogo-30")
    private let centerStackView = UIStackView(theme: Theme.UIStackView(props: [.vertical, .spacing(Float(getHeightRatio(verticalConstant: 10.0)))]))
    private let centerLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .text("Choose a password"), .center, .font(UIFont.largeThemeLabelFont)]))

    @UsesAutoLayout private var newActivityIndicator = UIActivityIndicatorView(style: .large)

    private var formValues: [String: String] = [:]

    init() {
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(registrationStackView)
        view.addSubview(submitButton)
        view.addSubview(newActivityIndicator)
        view.addSubview(centerStackView)
        view.addSubview(logoImageView)

        registrationStackView.addArrangedSubview(passwordLabel)
        registrationStackView.addArrangedSubview(passwordTextField)
        registrationStackView.addArrangedSubview(confirmPasswordLabel)
        registrationStackView.addArrangedSubview(confirmPasswordTextField)

        centerStackView.addArrangedSubview(centerLabel)

        passwordTextField.delegate = self
        confirmPasswordTextField.delegate = self
        passwordTextField.isSecureTextEntry = true
        confirmPasswordTextField.isSecureTextEntry = true

        newActivityIndicator.backgroundColor = UIColor(white: 0, alpha: 0.4)
        newActivityIndicator.frame = view.bounds

        logoImageView.image = logoImage

        let safeArea = view.safeAreaLayoutGuide

        NSLayoutConstraint.activate([
            logoImageView.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: CGFloat(0.5)),
            logoImageView.heightAnchor.constraint(equalTo: logoImageView.widthAnchor),
            logoImageView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            logoImageView.bottomAnchor.constraint(lessThanOrEqualTo: centerStackView.topAnchor, constant: getHeightRatio(verticalConstant: -10.0)),
            logoImageView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: getHeightRatio(verticalConstant: 40.0)),

            submitButton.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: getHeightRatio(verticalConstant: -10.0)),
            submitButton.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            submitButton.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -10.0)),
            submitButton.widthAnchor.constraint(equalTo: submitButton.heightAnchor, multiplier: 197 / 25),
            submitButton.topAnchor.constraint(lessThanOrEqualTo: registrationStackView.bottomAnchor, constant: UIViewController.screenSize.height * 0.31),

            passwordTextField.widthAnchor.constraint(equalTo: passwordTextField.heightAnchor, multiplier: 197 / 25),
            confirmPasswordTextField.widthAnchor.constraint(equalTo: confirmPasswordTextField.heightAnchor, multiplier: 197 / 25),

            newActivityIndicator.topAnchor.constraint(equalTo: safeArea.topAnchor),
            newActivityIndicator.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            newActivityIndicator.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            newActivityIndicator.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),

            registrationStackView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: getWidthRatio(horizontalConstant: -10.0)),
            registrationStackView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 10.0)),
            registrationStackView.bottomAnchor.constraint(equalTo: submitButton.topAnchor, constant: getHeightRatio(verticalConstant: -20.0)),

            centerStackView.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            centerStackView.bottomAnchor.constraint(lessThanOrEqualTo: registrationStackView.topAnchor, constant: -(0.095 * UIViewController.screenSize.height)),
            centerStackView.heightAnchor.constraint(greaterThanOrEqualTo: safeArea.heightAnchor, multiplier: 0.12),
        ]
        )
        submitButton.addTarget(self, action: #selector(submitRegistration), for: .touchUpInside)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.placeholder == "Your password" {
            formValues["password"] = textField.text
        }
        if textField.placeholder == "Confirm your password" {
            formValues["confirmPassword"] = textField.text
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.placeholder == "Your password" {
            formValues["password"] = textField.text
        }
        if textField.placeholder == "Confirm your password" {
            formValues["confirmPassword"] = textField.text
        }
        return true
    }

    @objc private func submitRegistration(_: RoundButton) {
        DispatchQueue.main.async {
            self.view.bringSubviewToFront(self.newActivityIndicator)
            self.newActivityIndicator.startAnimating()
        }

        if formValues["email"] != formValues["confirmEmail"] || formValues["password"] != formValues["confirmPassword"] {
            var title = ""
            var message = ""

            if formValues["email"] != formValues["confirmEmail"], formValues["password"] == formValues["confirmPassword"] {
                title = "Email mismatch"
                message = "Email and confirmation email are different. Please correct them and resubmit"
            } else if formValues["password"] != formValues["confirmPassword"], formValues["email"] == formValues["confirmEmail"] {
                title = "Password mismatch"
                message = "Password and confirmation password are different. Please correct them and resubmit"
            } else if formValues["email"] != formValues["confirmEmail"], formValues["password"] != formValues["confirmPassword"] {
                title = "Email and password mismatch"
                message = "Email, confirmation email, password and confirmation password are different. Please correct them and resubmit"
            }

            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Try again", style: .cancel) { _ in
                self.dismiss(animated: true)
            })
            DispatchQueue.main.async {
                SceneDelegate.shared.rootViewController.activityIndicator.stopAnimating()
                self.present(alertController, animated: true, completion: nil)
            }
        } else {
            CheckoutCart.shared.user?.password = passwordTextField.text!
            CoreDataManager.sharedManager.saveContext()

            // send the device token with the user when they are created to associate the user with the device token
            let userCredentials: [HTTPHeader] = [HTTPHeader(field: "device-token", value: CheckoutCart.shared.deviceToken)]
            let request = try! APIRequest(method: .post, path: "/customer/\(CheckoutCart.shared.sessionToken)", body: CheckoutCart.shared.user!, headers: userCredentials)
            APIClient().perform(request) { result in
                switch result {
                case let .success(response):
                    if response.statusCode == 200, let response = try? response.decode(to: UserData.self) {
                        CheckoutCart.shared.user?.update(userData: response.body)
                        DispatchQueue.main.async {
                            SceneDelegate.shared.rootViewController.activityIndicator.stopAnimating()
                            SceneDelegate.shared.rootViewController.switchToHomePageViewController()
                        }
                    } else {
                        DispatchQueue.main.async {
                            SceneDelegate.shared.rootViewController.activityIndicator.stopAnimating()
                            self.alertError(message: "An error occured. Please try signing up again", title: "Server error")
                        }
                    }
                case let .failure(error): // if the API fails the enum API result will be of the type failure
                    DispatchQueue.main.async {
                        SceneDelegate.shared.rootViewController.activityIndicator.stopAnimating()
                        self.alertError(message: "A network error has occured. Check your internet connection and if necessary, restart the app.", title: "Network Error")
                        print("error case .failure", error)
                    }
                }
            }
        }
    }

    private func alertError(message: String, title: String) {
        return alert(
            title: title,
            message: message,
            alertType: "error"
        )
    }

    private func alert(title: String, message: String, alertType: String) {
        let alertCtrl = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if alertType == "accountCreation" {
            alertCtrl.addAction(UIAlertAction(title: "Okay", style: .cancel) { _ in
                SceneDelegate.shared.rootViewController.switchToHomePageViewController()
            })
        } else if alertType == "error" {
            alertCtrl.addAction(UIAlertAction(title: "Okay", style: .cancel) { _ in
                self.dismiss(animated: true) {
                    DispatchQueue.main.async {
                        self.newActivityIndicator.stopAnimating()
                    }
                }
            })
        }
        present(alertCtrl, animated: true, completion: nil)
    }
}
