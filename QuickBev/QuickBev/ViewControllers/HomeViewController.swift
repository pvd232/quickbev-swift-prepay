//
//  HomeViewController.swift
//  Drinkz
//
//  Created by Peter Vail Driscoll II on 6/3/20.
//  Copyright © 2020 Peter Vail Driscoll II. All rights reserved.
//

import CoreLocation
import MapKit
import Stripe
import UIKit

class HomeViewController: UIViewController {
    let bottomButtonsViewContainer = ToolbarView(theme: Theme.UIView(props: []))
    private let newView = UIView(theme: Theme.UIView(props: []))

    private let accountLabelContainerView = UIStackView(theme: Theme.UIStackView(props: [.vertical]))
    private let accountTextView = UITextView(theme: Theme.UITextView(props: [.textColor, .font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 30.0)))]))
    private let theNightIsYoungTextView = UITextView(theme: Theme.UITextView(props: [.textColor, .font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 30.0)))]))
    private let accountImageView = AlignmentImageView(theme: Theme.UIView(props: []))
    private let businessTableView = UITableView(theme: Theme.UIView(props: []))
    private let checkoutCartButton = RoundButton(theme: Theme.UIView(props: []))

    private var greeting = ""

    private var isPresenting = false

    private var businesses = [Business]()
    private var drinks = [Drink]()

    private var dataSource: UITableViewDiffableDataSource<Section, BusinessItem>!

    private var visitedBusinessItems = [BusinessItem]()
    private var nearbyBusinessItems = [BusinessItem]()
    private var moreBusinessItems = [BusinessItem]()

    private var businessItemDictArray = [[Section: [BusinessItem]]]()

    init() {
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white

        // set delegate to be the HomeViewController because it is assuming functionality of the ToolBarView by receiving the touch from the user
        bottomButtonsViewContainer.businessSearchControllerDelegate = self
        bottomButtonsViewContainer.businessMapViewControllerDelegate = self
        bottomButtonsViewContainer.activeQuickPassViewControllerDelegate = self
        bottomButtonsViewContainer.menuViewControllerDelegate = self
        bottomButtonsViewContainer.ordersViewControllerDelegate = self

        // this triggers the StripeAPI to call the create customer key function, sending the existing stripe id of the user returned from logging in as a parameter
        _ = CheckoutCart.shared.customerContext
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("coder not set up")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if CheckoutCart.shared.businessList.isEmpty {
            SceneDelegate.shared.rootViewController.getDrinksAndBusinesses { _ in
                self.initiateViewSetup()
            }
        } else {
            initiateViewSetup()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setConditionalUIProperties()
        SceneDelegate.shared.rootViewController.stopAnimatingActivityIndicator()
        navigationController?.isHiddenHairline = true
    }

    private func initiateViewSetup() {
        businessTableView.isHidden = true
        setupView()
        setupTableView()
        // if a new business is added then the location must be set
        if SceneDelegate.shared.rootViewController.didUpdateBusinesses == true {
            setBusinessLocations { _ in
                self.loadTable()
            }
        } else {
            loadTable()
        }
        addCheckoutCartButton()
        updateNavigationBarView()
    }

    private func getLocalBusinesses() -> [Business]? {
        var localBusinesses = [Business]()
        for business in CheckoutCart.shared.activeBusinessArray {
            // this should never be true
            if business.coordinateString == "" {
                business.getLocation(from: business.address) { _ in
                    if business.isLocal(from: SceneDelegate.shared.rootViewController.currentLocation) {
                        localBusinesses.append(business)
                    }
                }
            } else {
                if business.isLocal(from: SceneDelegate.shared.rootViewController.currentLocation) {
                    localBusinesses.append(business)
                }
            }
        }
        if !localBusinesses.isEmpty {
            return localBusinesses
        } else {
            return nil
        }
    }

    private func setBusinessLocations(completion: @escaping (_ status: Bool?) -> Void) {
        // get business coordinates
        let newGroup = DispatchGroup()
        for business in CheckoutCart.shared.activeBusinessArray {
            if business.coordinateString == "" {
                newGroup.enter()
                business.getLocation(from: business.address) { _ in
                    newGroup.leave()
                }
            }
        }
        newGroup.notify(queue: .main, execute: {
            completion(true)
        })
    }

    private func setupView() {
        let safeArea = view.safeAreaLayoutGuide
        view.backgroundColor = .white
        view.addSubview(bottomButtonsViewContainer)
        view.addSubview(accountLabelContainerView)

        accountTextView.textColor = .black
        accountLabelContainerView.addArrangedSubview(accountTextView)
        accountLabelContainerView.alignment = .leading
        accountLabelContainerView.spacing = UIStackView.spacingUseSystem
        accountLabelContainerView.isLayoutMarginsRelativeArrangement = true

        // this has to equal the bottom anchor constant on the accountImageView
        accountLabelContainerView.directionalLayoutMargins = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: getHeightRatio(verticalConstant: 20), trailing: 0)

        let nightIsYoungStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(0.0)]))
        nightIsYoungStackView.addArrangedSubview(theNightIsYoungTextView)
        nightIsYoungStackView.addArrangedSubview(accountImageView)

        nightIsYoungStackView.spacing = UIStackView.spacingUseSystem
        nightIsYoungStackView.isLayoutMarginsRelativeArrangement = true

        // this has to equal the left inset of the accountTextView
        nightIsYoungStackView.directionalLayoutMargins = NSDirectionalEdgeInsets(top: 0, leading: getWidthRatio(horizontalConstant: 20.0), bottom: 0, trailing: 0)

        accountLabelContainerView.addArrangedSubview(nightIsYoungStackView)
        accountLabelContainerView.addBottomBorder(with: UIColor.lightGray.withAlphaComponent(0.5), andWidth: 1.0)

        accountTextView.textContainerInset = UIEdgeInsets(top: getHeightRatio(verticalConstant: 20.0), left: getWidthRatio(horizontalConstant: 20.0), bottom: getHeightRatio(verticalConstant: 10.0
        ), right: 0)
        theNightIsYoungTextView.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0.0,
                                                                  right: 0)

        // this gives the textView an inherent width based on the number of characters of text and font size
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 0
        let attributes = [NSAttributedString.Key.paragraphStyle: style, NSAttributedString.Key.font: UIFont(name: "GillSans", size: getFontRatio(fontSize: 30.0))]
        accountTextView.attributedText = NSAttributedString(string: greeting, attributes: attributes as [NSAttributedString.Key: Any])
        accountTextView.textAlignment = .left
        accountTextView.backgroundColor = .white

        accountTextView.isScrollEnabled = false

        accountImageView.verticalAlignment = .top
        accountImageView.tintColor = UIColor.systemYellow

        theNightIsYoungTextView.isScrollEnabled = false
        theNightIsYoungTextView.backgroundColor = .clear
        theNightIsYoungTextView.isUserInteractionEnabled = false
        accountTextView.isUserInteractionEnabled = false

        NSLayoutConstraint.activate([
            bottomButtonsViewContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            bottomButtonsViewContainer.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
            bottomButtonsViewContainer.widthAnchor.constraint(equalTo: safeArea.widthAnchor),
            bottomButtonsViewContainer.heightAnchor.constraint(equalTo: safeArea.heightAnchor, multiplier: 0.12),

            accountLabelContainerView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: getHeightRatio(verticalConstant: 20.0)),
            accountLabelContainerView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: getWidthRatio(horizontalConstant: 0.0)),
            accountLabelContainerView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),

            accountTextView.trailingAnchor.constraint(equalTo: accountLabelContainerView.trailingAnchor),

            accountImageView.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.09),
            accountImageView.bottomAnchor.constraint(equalTo: accountLabelContainerView.bottomAnchor, constant: getHeightRatio(verticalConstant: -20.0)),
            accountImageView.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.045),
        ])
    }

    class MyDataSource: UITableViewDiffableDataSource<Section, BusinessItem> {
        var mySections: [Section] = []
    }

    private func loadTable() {
        dataSource = MyDataSource(tableView: businessTableView) {
            (tableView: UITableView, indexPath: IndexPath, businessItem: BusinessItem) -> UITableViewCell? in
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
                let content = HomeTableViewContentConfiguration(businessItem: businessItem)

                // this fixed an issue with cell seperator color reappearing
                cell.clipsToBounds = true
                if let url = businessItem.url as NSURL? {
                    ImageCache.publicCache.load(url: url as NSURL, item: businessItem) { fetchedItem, image in
                        if let img = image, fetchedItem.image != img {
                            var updatedSnapshot = self.dataSource.snapshot()
                            fetchedItem.image = img
                            updatedSnapshot.reloadItems([fetchedItem as! BusinessItem])
                            self.dataSource.apply(updatedSnapshot, animatingDifferences: false)
                        }
                    }
                }
                cell.contentConfiguration = content
                cell.selectionStyle = .none
                return cell
        }
        dataSource.defaultRowAnimation = .fade

        // Get our image URLs for processing.
        if moreBusinessItems.isEmpty {
            for business in CheckoutCart.shared.activeBusinessArray {
                let businessItem = BusinessItem(business: business)
                moreBusinessItems.append(businessItem)
            }
            var initialSnapshot = NSDiffableDataSourceSnapshot<Section, BusinessItem>()
            if let dataSource = dataSource as? MyDataSource {
                if let userVisitedBusinesses = CheckoutCart.shared.user?.getVisitedBusinesses(), !userVisitedBusinesses.isEmpty {
                    dataSource.mySections.append(.recentlyVisited)

                    // append the sections to the dataSource for display in the tableView
                    initialSnapshot.appendSections([.recentlyVisited])

                    for business in userVisitedBusinesses {
                        visitedBusinessItems.append(BusinessItem(business: business))
                    }

                    initialSnapshot.appendItems(visitedBusinessItems, toSection: .recentlyVisited)
                    businessItemDictArray.append([.recentlyVisited: visitedBusinessItems])
                    // remove recently visited business from the allOtherBusiness
                    let filteredRemainingObjects = moreBusinessItems.filter { businessImageObject in
                        if visitedBusinessItems.first(where: {
                            $0.businessId == businessImageObject.businessId
                        }) != nil {
                            return false
                        } else {
                            return true
                        }
                    }

                    moreBusinessItems = filteredRemainingObjects
                }

                if let nearbyBusinesses = getLocalBusinesses(), !nearbyBusinesses.isEmpty {
                    for business in nearbyBusinesses {
                        nearbyBusinessItems.append(BusinessItem(business: business))
                    }

                    // if the bar is not present in moreBarsImageObjects then it was removed because it was present as a visited business, thus filter out bars that are not present in moreBarImageObjects
                    let filteredNearbyBusinessImageObjects = nearbyBusinessItems.filter { businessItem -> Bool in
                        if moreBusinessItems.first(where: {
                            $0.businessId == businessItem.businessId
                        }) != nil {
                            return true
                        } else {
                            return false
                        }
                    }
                    nearbyBusinessItems = filteredNearbyBusinessImageObjects
                    if !nearbyBusinessItems.isEmpty {
                        let sortedNearbyBusinessImageObjects = filteredNearbyBusinessImageObjects.sorted {
                            $0.distance < $1.distance
                        }
                        dataSource.mySections.append(.nearbyBars)

                        // append the sections to the dataSource for display in the tableView
                        initialSnapshot.appendSections([.nearbyBars])
                        initialSnapshot.appendItems(sortedNearbyBusinessImageObjects, toSection: .nearbyBars)

                        businessItemDictArray.append([.nearbyBars: nearbyBusinessItems])
                    }
                }
                // remove recently visited business from the the "More businesses" category
                let filteredRemainingObjects = moreBusinessItems.filter { businessImageObject -> Bool in
                    if nearbyBusinessItems.first(where: {
                        $0.businessId == businessImageObject.businessId
                    }) != nil {
                        return false
                    } else {
                        return true
                    }
                }
                moreBusinessItems = filteredRemainingObjects
                // if all of the businesses are nearby then dont show the more buisnesses category

                if !moreBusinessItems.isEmpty {
                    dataSource.mySections.append(.moreBars)
                    initialSnapshot.appendSections([.moreBars])
                    initialSnapshot.appendItems(moreBusinessItems, toSection: .moreBars)
                    businessItemDictArray.append([.moreBars: moreBusinessItems])
                }

                dataSource.apply(initialSnapshot, animatingDifferences: false)
                businessTableView.isHidden = false
            }
        }
    }

    func setConditionalUIProperties() {
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        if hour < 12 && hour > 4 {
            greeting = "Good morning,  "
        } else if hour < 17 && hour >= 12 {
            greeting = "Good afternoon,  "
        } else if hour >= 17 || hour <= 4 {
            greeting = "Good evening,  "
        }
        if CheckoutCart.shared.user != nil {
            greeting += "\(CheckoutCart.shared.user!.firstName.capitalizingFirstLetter())"
        }

        let sunImage = UIImage(systemName: "sun.max")
        let moonImage = UIImage(systemName: "moon")

        if hour <= 4 || hour >= 17 {
            theNightIsYoungTextView.text = "The night is young"
            accountImageView.image = moonImage
            theNightIsYoungTextView.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.62).isActive = true
        } else {
            theNightIsYoungTextView.text = "The world is your oyster"
            accountImageView.image = sunImage
            theNightIsYoungTextView.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.81).isActive = true
        }
        accountTextView.text = greeting

        checkoutCartButton.refreshTitle(newTitle: "View cart (\(CheckoutCart.shared.orderDrinkCount))")
        checkoutCartButton.isHidden = !CheckoutCart.shared.canPay

        bottomButtonsViewContainer.updateOrderStatus()
        bottomButtonsViewContainer.updateQuickPassStatus()
    }

    private func updateNavigationBarView() {
        // instantiate UI elements for nav bar
        let hamburgerButton = UIButton(theme: Theme.UIView(props: []))
        let chevronArrowImageView = UIImageView(theme: Theme.UIView(props: []))
        let navigationBarStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal]))
        let barLabelAndChevronArrowStackView = UIStackView(theme: Theme.UIStackView(props: [.horizontal, .spacing(Float(getWidthRatio(horizontalConstant: 10.0)))]))
        let navigationBarLabel = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont(name: "GillSans", size: getFontRatio(fontSize: 18.0)))]))

        // setup the hamburger button
        let hamburgerImage = UIImage(named: "hamburger")
        hamburgerButton.setImage(hamburgerImage, for: .normal)
        hamburgerButton.contentVerticalAlignment = .fill
        hamburgerButton.contentHorizontalAlignment = .fill
        let clockImage = UIImage(systemName: "clock.arrow.circlepath")
        let clockImageView = UIImageView(theme: Theme.UIView(props: []))
        clockImageView.image = clockImage!
        clockImageView.tintColor = UIColor.white
        clockImageView.isUserInteractionEnabled = true

        let chevronArrowImage = UIImage(systemName: "location.fill")
        chevronArrowImageView.image = chevronArrowImage!
        chevronArrowImageView.contentMode = .scaleAspectFit
        chevronArrowImageView.tintColor = UIColor.systemBlue
        chevronArrowImageView.isUserInteractionEnabled = true

        // add gesture recognizers + targets
        hamburgerButton.addTarget(self, action: #selector(launchAccountViewController), for: .touchUpInside)
        barLabelAndChevronArrowStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(launchBusinessViewController)))

        // if business exists put business label in the nav bar
        if let business = CheckoutCart.shared.userBusiness, business.isOpen {
            // setup the navigation bar
            barLabelAndChevronArrowStackView.addArrangedSubview(navigationBarLabel)
            barLabelAndChevronArrowStackView.addArrangedSubview(chevronArrowImageView)

            navigationBarStackView.distribution = .equalSpacing
            navigationBarStackView.addArrangedSubview(hamburgerButton)
            navigationBarStackView.addArrangedSubview(barLabelAndChevronArrowStackView)
            navigationBarStackView.addArrangedSubview(clockImageView)
            navigationItem.titleView = navigationBarStackView

            navigationBarLabel.textAlignment = .center
            navigationBarLabel.text = business.formattedName

            NSLayoutConstraint.activate([
                hamburgerButton.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.085),
                hamburgerButton.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.085),
                hamburgerButton.leadingAnchor.constraint(equalTo: navigationBarStackView.leadingAnchor),

                chevronArrowImageView.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.07),
                clockImageView.trailingAnchor.constraint(equalTo: navigationBarStackView.trailingAnchor),
                clockImageView.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.085),
                navigationBarStackView.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.9),
            ])

            if business.name.count >= 20 {
                navigationBarLabel.font = UIFont(name: "GillSans", size: getFontRatio(fontSize: 18.0))
            } else {
                navigationBarLabel.font = UIFont(name: "GillSans", size: getFontRatio(fontSize: 22.0))
            }
        }

        // if the business doesn't exist put a location arrow in the nav bar
        else {
            // if the business is closed we want to reset the user's selected business from their last use of the application
            CheckoutCart.shared.user?.userToBusiness = nil
            CheckoutCart.shared.emptyCart()
            CoreDataManager.sharedManager.saveContext()

            barLabelAndChevronArrowStackView.addArrangedSubview(chevronArrowImageView)

            navigationBarStackView.distribution = .equalSpacing
            navigationBarStackView.addArrangedSubview(hamburgerButton)
            navigationBarStackView.addArrangedSubview(barLabelAndChevronArrowStackView)
            navigationBarStackView.addArrangedSubview(clockImageView)
            navigationItem.titleView = navigationBarStackView

            navigationBarLabel.textAlignment = .center

            NSLayoutConstraint.activate([
                hamburgerButton.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.085),
                hamburgerButton.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.085),
                hamburgerButton.leadingAnchor.constraint(equalTo: navigationBarStackView.leadingAnchor),

                chevronArrowImageView.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.07),
                clockImageView.trailingAnchor.constraint(equalTo: navigationBarStackView.trailingAnchor),
                clockImageView.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.085),

                navigationBarStackView.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.9),
            ]
            )
        }
    }

    @objc private func launchBottomSheetViewController(selectedBusiness: Business) {
        let popoverContent = BottomSheetViewController(business: selectedBusiness)
        popoverContent.buttonPressedDelegate = self
        popoverContent.modalPresentationStyle = .custom
        popoverContent.transitioningDelegate = self
        isPresenting = true
        present(popoverContent, animated: true, completion: nil)
    }

    @objc private func launchAccountViewController() {
        let popoverContent = AccountViewController()
        popoverContent.modalPresentationStyle = .popover
        let popover = popoverContent.popoverPresentationController
        popover!.sourceView = bottomButtonsViewContainer

        // the position of the popover where it's showed
        popover!.sourceRect = view!.bounds

        // the size you want to display
        popoverContent.preferredContentSize = CGSize(width: view.frame.width, height: view.frame.height)
        popover!.delegate = self
        if isPresenting == true {
            dismiss(animated: true) {
                self.present(popoverContent, animated: true, completion: nil)
            }
        } else {
            present(popoverContent, animated: true, completion: nil)
        }
        isPresenting = false
    }

    @objc private func launchCheckoutCartViewController() {
        let popoverContent = CheckoutCartViewController()
        popoverContent.modalPresentationStyle = .popover
        let popover = popoverContent.popoverPresentationController
        popover!.sourceView = bottomButtonsViewContainer

        // the position of the popover where it's showed
        popover!.sourceRect = view!.bounds

        // the size you want to display
        popoverContent.preferredContentSize = CGSize(width: view.frame.width, height: view.frame.height)
        popover!.delegate = self
        if isPresenting == true {
            dismiss(animated: true) {
                self.present(popoverContent, animated: true, completion: nil)
            }
        } else {
            present(popoverContent, animated: true, completion: nil)
        }
        isPresenting = false
    }

    @objc private func launchBusinessViewController() {
        let popoverContent = BusinessMapViewController()
        popoverContent.modalPresentationStyle = .popover
        let popover = popoverContent.popoverPresentationController
        popover!.sourceView = navigationController?.navigationBar

        // the position of the popover where it's showed
        popover!.sourceRect = view!.bounds

        // the size you want to display
        popoverContent.preferredContentSize = CGSize(width: view.frame.width, height: view.frame.height)
        popoverContent.businessPickerDelegate = self
        popover!.delegate = self
        if isPresenting == true {
            dismiss(animated: true) {
                self.present(popoverContent, animated: true, completion: nil)
            }
        } else {
            present(popoverContent, animated: true, completion: nil)
        }
        isPresenting = false
    }

    @objc private func launchBusinessSearchViewController() {
        let popoverContent = BusinessSearchViewController()
        popoverContent.modalPresentationStyle = .popover
        let popover = popoverContent.popoverPresentationController
        popover!.sourceView = navigationController?.navigationBar

        // the position of the popover where it's showed
        popover!.sourceRect = view!.bounds

        // the size you want to display
        popoverContent.preferredContentSize = CGSize(width: view.frame.width, height: view.frame.height)
        popoverContent.businessPickerDelegate = self
        popover!.delegate = self
        if isPresenting == true {
            dismiss(animated: true) {
                self.present(popoverContent, animated: true, completion: nil)
            }
        } else {
            present(popoverContent, animated: true, completion: nil)
        }
        isPresenting = false
    }

    @objc private func launchActiveQuickPassViewController() {
        let popoverContent = QuickPassViewController()
        popoverContent.modalPresentationStyle = .popover
        let popover = popoverContent.popoverPresentationController
        popover!.sourceView = bottomButtonsViewContainer

        // the position of the popover where it's showed
        popover!.sourceRect = view!.bounds

        // the size you want to display
        popoverContent.preferredContentSize = CGSize(width: view.frame.width, height: view.frame.height)
        popover!.delegate = self

        if isPresenting == true {
            dismiss(animated: true) {
                self.present(popoverContent, animated: true, completion: nil)
            }
        } else {
            present(popoverContent, animated: true, completion: nil)
        }
        isPresenting = false
    }

    @objc private func launchOrdersViewController() {
        let popoverContent = PreviousOrdersViewController()
        popoverContent.modalPresentationStyle = .popover
        let popover = popoverContent.popoverPresentationController
        popover!.sourceView = bottomButtonsViewContainer

        // the position of the popover where it's showed
        popover!.sourceRect = view!.bounds

        // the size you want to display
        popoverContent.preferredContentSize = CGSize(width: view.frame.width, height: view.frame.height)
        popover!.delegate = self

        if isPresenting == true {
            dismiss(animated: true) {
                self.present(popoverContent, animated: true, completion: nil)
            }
        } else {
            present(popoverContent, animated: true, completion: nil)
        }
        isPresenting = false
    }

    @objc func launchMenuViewController() {
        let popoverContent = MenuViewController()
        popoverContent.modalPresentationStyle = .popover
        let popover = popoverContent.popoverPresentationController
        popover!.sourceView = bottomButtonsViewContainer

        // the position of the popover where it's showed
        popover!.sourceRect = view!.bounds

        // the size you want to display
        popoverContent.preferredContentSize = CGSize(width: view.frame.width, height: view.frame.height)
        popover!.delegate = self

        if isPresenting == true {
            dismiss(animated: true) {
                self.present(popoverContent, animated: true, completion: nil)
            }
        } else {
            present(popoverContent, animated: true, completion: nil)
        }
        isPresenting = false
    }

    private func addCheckoutCartButton() {
        // add button to view
        view.addSubview(checkoutCartButton)
        // refresh checkout cart button quantity description

        // set button properties
        checkoutCartButton.refreshColor(color: UIColor.themeColor.mix(with: UIColor.white, amount: 0.75))
        checkoutCartButton.titleLabel?.font = UIFont(name: "GillSans-Bold", size: getFontRatio(fontSize: 20.0))
        checkoutCartButton.setTitleColor(UIColor.black, for: .normal)
        checkoutCartButton.refreshTitle(newTitle: "View cart (\(CheckoutCart.shared.cart.count))")
        checkoutCartButton.refreshCorners(value: 8)
        checkoutCartButton.refreshBorderWidth(value: 2.0)
        checkoutCartButton.refreshBorderColor(color: UIColor.themeColor)

        // constrain the button
        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            checkoutCartButton.bottomAnchor.constraint(equalTo: bottomButtonsViewContainer.topAnchor, constant: getHeightRatio(verticalConstant: -20.0)),
            checkoutCartButton.widthAnchor.constraint(equalToConstant: UIViewController.screenSize.width * 0.9),
            checkoutCartButton.heightAnchor.constraint(equalToConstant: UIViewController.screenSize.height * 0.048),
            checkoutCartButton.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
        ])

        checkoutCartButton.addTarget(self, action: #selector(launchCheckoutCartViewController), for: .touchUpInside)
    }

    private func setupTableView() {
        view.addSubview(businessTableView)

        businessTableView.backgroundColor = .white
        businessTableView.separatorColor = .clear

        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            businessTableView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            businessTableView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
            businessTableView.bottomAnchor.constraint(equalTo: bottomButtonsViewContainer.topAnchor),
            businessTableView.topAnchor.constraint(equalTo: accountLabelContainerView.bottomAnchor, constant: getHeightRatio(verticalConstant: 5.0)),
        ])
        businessTableView.delegate = self
        businessTableView.dataSource = dataSource
        businessTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
}

extension HomeViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyleForPresentationController(controller _: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.fullScreen
    }

    func presentationController(_ controller: UIPresentationController, viewControllerForAdaptivePresentationStyle _: UIModalPresentationStyle) -> UIViewController? {
        return UINavigationController(rootViewController: controller.presentedViewController)
    }
}

extension HomeViewController: LaunchMenuViewControllerProtocol {
    func presentMenuViewController() {
        launchMenuViewController()
    }
}

extension HomeViewController: LaunchActiveQuickPassViewControllerProtocol {
    func presentActiveQuickPassViewController() {
        launchActiveQuickPassViewController()
    }
}

extension HomeViewController: LaunchBusinessSearchControllerProtocol {
    func presentSearchViewController() {
        launchBusinessSearchViewController()
    }
}

extension HomeViewController: LaunchBusinessMapViewControllerProtocol {
    func presentBusinessMapViewController() {
        launchBusinessViewController()
    }
}

extension HomeViewController: LaunchOrdersControllerProtocol {
    func presentOrdersViewController() {
        launchOrdersViewController()
    }
}

extension HomeViewController: NewBusinessPickedProtocol {
    func businessPicked() {
        updateNavigationBarView()
        setConditionalUIProperties()
    }
}

extension HomeViewController: ButtonPressedProtocol {
    func getQuickPassButtonPressed(businessId: UUID) {
        SceneDelegate.shared.rootViewController.animateActivityIndicator()
        updateQuickPass(businessId: businessId) {
            result in switch result {
            case true:
                DispatchQueue.main.async {
                    self.dismiss(animated: true) {
                        SceneDelegate.shared.rootViewController.stopAnimatingActivityIndicator()
                        let popoverContent = QuickPassDetailViewController()
                        popoverContent.modalPresentationStyle = .popover
                        let popover = popoverContent.popoverPresentationController
                        popover!.sourceView = self.bottomButtonsViewContainer

                        // the position of the popover where it's showed
                        popover!.sourceRect = self.view!.bounds

                        // the size you want to display
                        popoverContent.preferredContentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
                        popover!.delegate = self
                        self.present(popoverContent, animated: true, completion: nil)
                    }
                }
            case false:
                DispatchQueue.main.async {
                    SceneDelegate.shared.rootViewController.stopAnimatingActivityIndicator()
                    self.dismiss(animated: true) {
                        self.presentNetworkError()
                    }
                }
            }
        }
    }

    func checkInButtonPressed() {
        SceneDelegate.shared.rootViewController.animateActivityIndicator()
        let delay = 500 // miliseconds
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(delay)) {
            self.updateNavigationBarView()
            self.setConditionalUIProperties()
            SceneDelegate.shared.rootViewController.stopAnimatingActivityIndicator()
            self.dismiss(animated: true) {
                self.bottomButtonsViewContainer.launchMenuViewController()
            }
        }
    }
}

extension HomeViewController: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting _: UIViewController?, source _: UIViewController) -> UIPresentationController? {
        return CustomPresentationController(presentedViewController: presented, presenting: presentingViewController)
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        return (tableView.frame.height / 5.7)
    }

    func numberOfSections(in _: UITableView) -> Int {
        if let typedDataSource = dataSource as? MyDataSource {
            return typedDataSource.mySections.count
        } else {
            return 1
        }
    }

    func tableView(_: UITableView, numberOfRowsInSection index: Int) -> Int {
        // return the number of sections corresponding to the number of items in that section

        let typedDataSource = dataSource as! MyDataSource
        let theSection = typedDataSource.mySections[index]

        return businessItemDictArray.first(where: { $0.index(forKey: theSection) != nil
        })!.count
    }

    func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        let businessItem = dataSource.itemIdentifier(for: indexPath)
        guard let business = CheckoutCart.shared.activeBusinessArray.first(where: { $0.id == businessItem?.businessId }) else {
            return
        }

        if let bottomSheetVC = presentedViewController as? BottomSheetViewController {
            bottomSheetVC.businessDetails = business
            bottomSheetVC.setupView()
        } else {
            launchBottomSheetViewController(selectedBusiness: business)
        }
    }

    func tableView(_: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let typedDataSource = dataSource as? MyDataSource {
            let section = typedDataSource.mySections[section]

            let view = UIView(frame: CGRect(x: 0, y: 0, width: businessTableView.frame.size.width, height: 0))
            view.backgroundColor = .white

            let label = UILabel(theme: Theme.UILabel(props: [.textColor, .font(UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 28.0)))]))

            label.textColor = .black
            label.font = UIFont(name: "GillSans-SemiBold", size: getFontRatio(fontSize: 28.0))
            label.textAlignment = .center
            label.text = section.rawValue

            view.addSubview(label)

            NSLayoutConstraint.activate([
                label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: getWidthRatio(horizontalConstant: 20.0)),
                label.topAnchor.constraint(equalTo: view.topAnchor, constant: getHeightRatio(verticalConstant: 2.0)),
                label.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            ])
            return view
        } else {
            return nil
        }
    }
}
